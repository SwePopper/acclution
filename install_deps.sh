#!/bin/sh

# This script installs some system dependencies that might be needed in a new developement environment.
# It then later updates the repository submodules via --init --recursive
# You usually only need to run this once
#
# Current dependencies:
#	Cmake: Used for generating the project files
#	GLM: Used for general math/vec/matrix stuff
#	ASIO: Not used yet but intended as the network api
#	SDL2: Used for getting the window up and running cross-platform
#	Expat: Used for reading xml files
#	mesa and glew: Used to get OpenGL up and running
#	Vulkan: Trying out vulkan

apt_get_packages=(cmake libglm-dev libasio-dev libsdl2-dev libexpat1-dev libglew-dev libgl1-mesa-dev libvulkan-dev)
pacman_packages=(cmake glm asio sdl2 expat glew mesa vulkan-devel)

echo >&2 "Installing dependencies.."
if hash apt-get 2>/dev/null; then
	# Found Debian/Ubuntu package manager
	sudo apt-get update
	sudo apt-get install ${apt_get_packages[@]}
elif hash pacman 2>/dev/null; then
	# Found Arch Linux package manager
	sudo pacman -Sy
	sudo pacman --needed -S ${pacman_packages[@]}
else
	echo >&2 "Cant find package manager"
	exit 1
fi

# Update the submodules
echo >&2 "Updating git submodules.."
git submodule update --init --recursive

exit 0;
