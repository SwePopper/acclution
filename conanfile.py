from conans import ConanFile, CMake

class AcclutionConan(ConanFile):
	settings = "os", "compiler", "build_type", "arch"
	generators = "cmake"

	requires = ("Asio/1.10.6@fmorgner/stable",
				"assimp/3.3.1@lasote/vcpkg",
				"expat/2.1.1@lasote/vcpkg",
				"freeimage/3.17.0@hilborn/stable",
				"freetype/2.6.3@lasote/stable",
				"glew/2.0.0@coding3d/stable",
				"glm/0.9.7.6@dlarudgus20/stable",
				"SDL2/2.0.4@lasote/stable",
				"zlib/1.2.8@lasote/stable")

	default_options =  ("freeimage:shared=True",
						"glew:shared=True",
						"SDL2:shared=True",
						"zlib:shared=True")

	def imports(self):
		self.copy("*.dll", dst="bin", src="bin") # From bin to bin
		self.copy("*.dylib*", dst="bin", src="lib") # From lib to bin