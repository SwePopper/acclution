## acclution

This is my game engine hobby project. I am trying to make this engine multithreaded with both an opengl and vulkan renderer. The Ultimate goal is to have my
own Unity like game engine =)

## Why make my own engine

This is a place for me to test out new ideas. My main job is writing C# games in Unity but I like C++ more so I need a place to vent my C++ ideas and to try out
new C++ stuff and to keep intouch with modern C++11/14/17.

## Installation

Run git clone in the directory where you want the project to be located. Then run the one time install_deps script. It will try to install the main system dependencies
that is not linked via submodule. It might not be up to date (because I might not remember to update it when I add new dependencies). So keep a lookout when running
cmake. If it complanes then you might need to install dependencies yourself.

	git clone https://gitlab.com/SwePopper/acclution.git
	cd acclution
	./install_deps.sh
	mkdir build
	cd build
	cmake ..
	cmake --build .

## TODO

* Create a functioning rendering api

## License

Acclution
Copyright (c) 2016 Christoffer Lindqvist

This software is provided 'as-is', without any express or implied
warranty. In no event will the authors be held liable for any
damages arising from the use of this software.

Permission is granted to anyone to use this software for any
purpose, including commercial applications, and to alter it and
redistribute it freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented; you
  must not claim that you wrote the original software. If you use
  this software in a product, an acknowledgment in the product
  documentation would be appreciated but is not required.

2. Altered source versions must be plainly marked as such, and
  must not be misrepresented as being the original software.

3. This notice may not be removed or altered from any source
  distribution.

Christoffer Lindqvist
swepopper@gmail.com
