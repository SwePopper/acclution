#version 330 core
in vec2 fragmentUv;
in vec4 fragmentColor;

uniform sampler2D atlasOne;
uniform sampler2D atlasTwo;
uniform sampler2D atlasThree;
uniform sampler2D atlasFour;

out vec4 color;

void main() {
	vec4 finalColor = fragmentColor;
	if(fragmentUv.x < 1.0) finalColor = finalColor * texture(atlasOne, fragmentUv);
	else if(fragmentUv.x < 2.0) finalColor = finalColor * texture(atlasTwo, fragmentUv);
	else if(fragmentUv.x < 3.0) finalColor = finalColor * texture(atlasThree, fragmentUv);
	else finalColor = finalColor * texture(atlasFour, fragmentUv);
	color = finalColor;
}
