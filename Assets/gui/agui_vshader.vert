#version 330 core
layout(location = 0) in vec3 position;
layout(location = 1) in vec2 uv;
layout(location = 2) in vec4 color;

layout (std140) uniform Matrices
{
	mat4 orthoProjection;
};

out vec2 fragmentUv;
out vec4 fragmentColor;

void main() {
	gl_Position = orthoProjection * vec4(position, 1.0);

	fragmentUv = uv;
	fragmentColor = color;
}
