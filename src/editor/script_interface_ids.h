#ifndef SCRIPT_INTERFACE_IDS_H
#define SCRIPT_INTERFACE_IDS_H
#pragma once

#include <RuntimeObjectSystem/IObject.h>

enum class ScriptInterfaceID : uint32_t
{
	kQuitButton = IID_ENDInterfaceID
};

#endif // SCRIPT_INTERFACE_IDS_H
