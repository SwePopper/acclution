#include "engine/platform.h"
#include "engine/engine.h"
#include "engine/task/taskscheduler.h"

#include <RuntimeObjectSystem/ObjectInterfacePerModule.h>
#include "engine/gui/action.h"

#include "editor/script_interface_ids.h"

class QuitButton : public TInterface<(uint32_t)ScriptInterfaceID::kQuitButton, Acclution::Action>
{
public:
	virtual ~QuitButton() {}

	virtual void OnAccept()
	{
		Acclution::Engine::GetInstance()->GetTaskScheduler()->Stop();
	}
};

REGISTERCLASS(QuitButton)
