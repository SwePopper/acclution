/*
   Acclution
   Copyright (c) 2016 Christoffer Lindqvist

   This software is provided 'as-is', without any express or implied
   warranty. In no event will the authors be held liable for any
   damages arising from the use of this software.

   Permission is granted to anyone to use this software for any
   purpose, including commercial applications, and to alter it and
   redistribute it freely, subject to the following restrictions:

   1. The origin of this software must not be misrepresented; you
      must not claim that you wrote the original software. If you use
      this software in a product, an acknowledgment in the product
      documentation would be appreciated but is not required.

   2. Altered source versions must be plainly marked as such, and
      must not be misrepresented as being the original software.

   3. This notice may not be removed or altered from any source
      distribution.

   Christoffer Lindqvist
   swepopper@gmail.com
*/

#include "engine/platform.h"
#include "engine/engine.h"

#include "applicationeventrunner.h"
#include "engine/asset/contentmanager.h"
#include "engine/components/component.h"
#include "engine/components/componentmanager.h"
#include "engine/config/config.h"
#include "engine/graphics/commandbuffer.h"
#include "engine/graphics/opengl/video_opengl.h"
#include "engine/graphics/video.h"
#include "engine/graphics/vulkan/video_vulkan.h"
#include "engine/gui/gui.h"
#include "engine/input/input.h"
#include "engine/physics/physics.h"
#include "engine/platform_updater/platform_updater.h"
#include "engine/plugin/pluginsystem.h"
#include "engine/scene/scene.h"
#include "engine/scene/scenerenderer.h"
#include "engine/script/scriptmanager.h"
#include "engine/task/task.h"
#include "engine/task/taskscheduler.h"
#include "engine/utilities.h"
#include "engine/memory/buddyallocator.h"
#include "engine/memory/frameallocator.h"

namespace Acclution {

Engine* Engine::s_instance = nullptr;

Allocator* CreateDefaultAllocator()
{
	static BuddyAllocator default_allocator(4, 22);
	Allocator::SetDefaultAllocator(&default_allocator);
	return &default_allocator;
}

Engine::Engine()
	: m_update_type(UpdateType::kFrameBased),
	  m_frame_count(0),
	  m_next_game_play_frame(-1),
	  m_next_command_buffer_frame(-1),
	  m_next_frame_render_frame(-1),
	  m_last_time(),
	  m_last_smoothed_delta_time_nano(0),
	  m_default_allocator(CreateDefaultAllocator()),
	  m_component_manager(new ComponentManager),
	  m_config(new Config),
	  m_content_manager(new ContentManager),
	  m_gui(new GUI),
	  m_input(new Input),
	  m_log(new Log),
	  m_physics(new Physics),
	  m_platform_updater(nullptr),
	  m_plugin_system(new PluginSystem),
	  m_scene(new Scene),
	  m_scene_renderer(new SceneRenderer),
	  m_script_manager(new ScriptManager),
	  m_task_scheduler(new TaskScheduler),
	  m_video(nullptr)
{
	memset(m_frames, 0, sizeof(m_frames));
}

Engine::~Engine()
{
	Release();
}

bool Engine::Initialize(const UpdateType& update_type)
{
	if(s_instance != nullptr)
		return false;
	s_instance = this;

	m_platform_updater = PlatformUpdater::Create();
	if(m_platform_updater == nullptr)
		return false;

	m_update_type = update_type;

	TaskScheduler::SetCoreAffinity(0);

	// the first frame will be 0 anyway so if any initializers want to use the frame allocator.
	m_next_game_play_frame = 0;

	ALogInfo("Started");

	if(!m_script_manager->Initialize())
		return false;

	if(!m_config->Initialize())
		return false;

#if defined(USE_VULKAN)
	//m_video.reset(new VideoVulkan);
	//if(!m_video->Initialize())
#endif
	{
		m_video.reset(new VideoOpenGL);
		if(!m_video->Initialize())
		{
			return false;
		}
	}

	const astd::string& com_buffer_size_string = m_config->GetValue(Utilities::Hash32("FrameConfig"), Utilities::Hash32("frame_command_buffer_size"));
	const astd::string& frame_alloc_size_string = m_config->GetValue(Utilities::Hash32("FrameConfig"), Utilities::Hash32("frame_allocator_size"));

	uint32_t com_buffer_size = static_cast<uint32_t>(strtoul(com_buffer_size_string.c_str(), nullptr, 10));
	if(com_buffer_size == 0)
		com_buffer_size = 256*1024;

	uint32_t frame_alloc_size = static_cast<uint32_t>(strtoul(frame_alloc_size_string.c_str(), nullptr, 10));
	if(frame_alloc_size == 0)
		frame_alloc_size = 1024*1024;

	for(uint32_t i = 0; i < 3; ++i)
	{
		m_frames[i].m_command_buffer = m_video->CreateCommandBuffer(com_buffer_size);
		m_frames[i].m_frame_allocator = new FrameAllocator(frame_alloc_size);
	}

	if(!m_content_manager->Initialize(m_config->GetAssetPath().c_str()))
		return false;

	if(!m_input->Initialize())
		return false;

	if(!m_physics->Initialize())
		return false;

	if(!m_task_scheduler->Initialize(m_update_type == UpdateType::kEventBased))
		return false;

	if(!m_scene_renderer->Initialize())
		return false;

	if(!m_gui->Initialize())
		return false;

	m_last_smoothed_delta_time_nano = 0;
	m_last_time = std::chrono::high_resolution_clock::now();

	m_scene->LoadScene(m_config->GetStartupScene().c_str());

	m_frame_count = std::numeric_limits<unsigned long long>::max();

	ALogInfo("Engine initialized");
	return true;
}

void Engine::Release()
{
	m_task_scheduler->Release();
	m_gui->Release();
	m_scene_renderer->Release();
	m_scene->ClearScene();
	m_physics->Release();
	m_input->Release();
	m_content_manager->Release();

	for(uint32_t i = 0; i < 3; ++i)
	{
		delete m_frames[i].m_command_buffer;
		delete m_frames[i].m_frame_allocator;
	}
	memset(m_frames, 0, sizeof(m_frames));

	if(m_video != nullptr)
		m_video->Release();

	m_plugin_system->Release();
	m_script_manager->Release();

	SDL_Quit();

	delete m_platform_updater;
	m_platform_updater = nullptr;

	s_instance = nullptr;
}

void Engine::Run()
{
	// Initialize the update promise and future
	std::promise<int> update_promise;
	update_promise.set_value(-1);
	std::future<int> update_future = update_promise.get_future();

	// Initialize the draw promise and future
	std::promise<int> draw_promise;
	draw_promise.set_value(-1);
	std::future<int> draw_future = draw_promise.get_future();

	while(m_platform_updater->Update()) {

		if(m_update_type == UpdateType::kEventBased)
		{
			// Update and get the future directly
			update_promise = std::promise<int>();
			int frame = Update(&update_promise).get();

			// Draw and get the future directly
			draw_promise = std::promise<int>();
			m_next_frame_render_frame = Draw(&draw_promise, frame).get();
		}
		else
		{
			// Get last update frame and begin the new update frame
			int draw_frame = update_future.get();
			update_promise = std::promise<int>();
			update_future = Update(&update_promise);

			if(UNLIKELY(draw_frame < 0))
				continue;

			// Get last draw frame and begin the new draw frame
			m_next_frame_render_frame = draw_future.get();
			draw_promise = std::promise<int>();
			draw_future = Draw(&draw_promise, draw_frame);

			if(UNLIKELY(m_next_frame_render_frame < 0))
				continue;
		}

		// Draw the commands to the window. This needs to be on the main thread (for now)
		m_video->PlaybackRenderCommandBuffers(m_frames[m_next_frame_render_frame].m_command_buffer, 1);

		// Reset current command and frame allocator
		m_frames[m_next_frame_render_frame].m_frame_allocator->Reset();
	}

	m_task_scheduler->Stop();
}

std::future<int> Engine::Update(std::promise<int>* promise)
{
	++m_frame_count;
	int next_frame = (m_frame_count % 3);

	m_task_scheduler->AddTask(Task::make_taskmf(&Engine::GamePlayStage, this, promise, next_frame));
	return promise->get_future();
}

std::future<int> Engine::Draw(std::promise<int>* promise, int frame_index)
{
	m_task_scheduler->AddTask(Task::make_taskmf(&Engine::RenderCommandBufferStageStart, this, promise, frame_index));
	return promise->get_future();
}

void Engine::GamePlayStage(std::promise<int>* promise, int frame)
{
	m_next_game_play_frame = frame;

	m_frames[m_next_game_play_frame].m_count = m_frame_count.load();
	m_frames[m_next_game_play_frame].m_frame_time.Update(m_last_time, m_last_smoothed_delta_time_nano);
	m_last_smoothed_delta_time_nano = m_frames[m_next_game_play_frame].m_frame_time.GetSmoothedDeltaTimeNano();

	m_physics->WaitAndSync();
	m_physics->Step();

	m_script_manager->Update();
	m_gui->Update();

	promise->set_value(m_next_game_play_frame);
}

void Engine::RenderCommandBufferStageStart(std::promise<int>* promise, int frame_index)
{
	m_next_command_buffer_frame = frame_index;
	FrameData* frame = &m_frames[m_next_command_buffer_frame];

	frame->m_command_buffer->Reset();
	frame->m_command_buffer->Clear(0.0f, 0.0f, 0.0f, 0.0f);

	m_scene_renderer->BeginScene(frame->m_command_buffer);

	m_task_scheduler->AddTask(Task::make_taskmf(&Engine::RenderCommandBufferStageEnd, this, promise));
}

void Engine::RenderCommandBufferStageEnd(std::promise<int>* promise)
{
	FrameData* frame = &m_frames[m_next_command_buffer_frame];
	m_scene_renderer->EndScene(frame->m_command_buffer);
	m_gui->Render(frame->m_command_buffer);

	promise->set_value(m_next_command_buffer_frame);
}

}
