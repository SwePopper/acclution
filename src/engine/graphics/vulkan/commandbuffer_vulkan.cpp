#include "engine/platform.h"
#include "commandbuffer_vulkan.h"

#if defined(USE_VULKAN)

#include "engine/engine.h"
#include "engine/graphics/vulkan/video_vulkan.h"

namespace Acclution
{

CommandBufferVulkan::CommandBufferVulkan()
	: m_command_buffer_data(new CommandBufferData)
{
}

CommandBufferVulkan::~CommandBufferVulkan()
{
	delete m_command_buffer_data;
	m_command_buffer_data = nullptr;
}

void CommandBufferVulkan::Reset()
{
	VideoVulkan* video = (VideoVulkan*)Engine::GetInstance()->GetVideo().get();

	video->ReturnCommandBuffer(*m_command_buffer_data);
	*m_command_buffer_data = video->GetCommandBuffer();
}

///////////////////////////////////////////////////////////
// Shader Commands
void CommandBufferVulkan::UseShader(uint32_t index)
{
	ASTD_UNUSED(index);
}
///////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////
// Vertex Buffer
void CommandBufferVulkan::UpdateVertexBuffer(uint32_t index, const void* data, uint32_t data_size, BufferType buffer_type, uint32_t offset)
{
	ASTD_UNUSED(index);
	ASTD_UNUSED(data);
	ASTD_UNUSED(data_size);
	ASTD_UNUSED(buffer_type);
	ASTD_UNUSED(offset);
}
///////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////
// Texture
void CommandBufferVulkan::BindTexture(uint32_t index, uint8_t slot, uint32_t uniform_name_hash)
{
	ASTD_UNUSED(index);
	ASTD_UNUSED(slot);
	ASTD_UNUSED(uniform_name_hash);
}

void CommandBufferVulkan::UnBindTexture(uint8_t slot)
{
	ASTD_UNUSED(slot);
}
///////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////
// Uniforms
void CommandBufferVulkan::Uniform(uint32_t uniform_name_hash, Types type, uint32_t component_count, uint32_t data_count, const void* data)
{
	ASTD_UNUSED(uniform_name_hash);
	ASTD_UNUSED(type);
	ASTD_UNUSED(component_count);
	ASTD_UNUSED(data_count);
	ASTD_UNUSED(data);
}

void CommandBufferVulkan::UniformBuffer(uint32_t uniform_buffer, uint32_t binding, bool range, uint32_t start, uint32_t end)
{
	ASTD_UNUSED(uniform_buffer);
	ASTD_UNUSED(binding);
	ASTD_UNUSED(range);
	ASTD_UNUSED(start);
	ASTD_UNUSED(end);
}
///////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////
// Drawing
void CommandBufferVulkan::Clear(float r, float g, float b, float a)
{
	ASTD_UNUSED(r);
	ASTD_UNUSED(g);
	ASTD_UNUSED(b);
	ASTD_UNUSED(a);
}

void CommandBufferVulkan::ClearDepth()
{
}

void CommandBufferVulkan::EnableVertexAttrib(uint32_t buffer, uint32_t index, int32_t component_count, Types type, bool normalized, int32_t stride, const void* pointer)
{
	ASTD_UNUSED(buffer);
	ASTD_UNUSED(index);
	ASTD_UNUSED(component_count);
	ASTD_UNUSED(type);
	ASTD_UNUSED(normalized);
	ASTD_UNUSED(stride);
	ASTD_UNUSED(pointer);
}

void CommandBufferVulkan::DisableVertexAttrib(uint32_t attribute_index)
{
	ASTD_UNUSED(attribute_index);
}

void CommandBufferVulkan::DrawArrays(Modes mode, int first, int size)
{
	ASTD_UNUSED(mode);
	ASTD_UNUSED(first);
	ASTD_UNUSED(size);
}

void CommandBufferVulkan::DrawElements(Modes mode, int index_count, Types type, uint32_t buffer)
{
	ASTD_UNUSED(mode);
	ASTD_UNUSED(index_count);
	ASTD_UNUSED(type);
	ASTD_UNUSED(buffer);
}
///////////////////////////////////////////////////////////

}

#endif
