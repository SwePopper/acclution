#include "engine/platform.h"
#include "video_vulkan.h"

#if defined(USE_VULKAN)

#include "vulkanswapchain.h"
#include "engine/engine.h"
#include "engine/config/config.h"
#include "engine/utilities.h"
#include "shader_vulkan.h"
#include "commandbuffer_vulkan.h"

#include <vulkan/vulkan.h>

#ifndef VK_API_VERSION
//VK_MAKE_VERSION(1, 0, 0)
#define VK_API_VERSION VK_API_VERSION_1_0
#endif

namespace Acclution
{

VKAPI_ATTR VkBool32 VKAPI_CALL VulkanReportCallback(VkDebugReportFlagsEXT flags, VkDebugReportObjectTypeEXT /*object_type*/, uint64_t /*object*/,
													 size_t /*location*/, int32_t message_code, const char* layer_prefix, const char* message,
													 void* /*user_data*/)
{
	if(flags & VK_DEBUG_REPORT_INFORMATION_BIT_EXT ||
	   flags & VK_DEBUG_REPORT_DEBUG_BIT_EXT)
	{
		//ALogInfo("VK INFO: %s\nLayer: %s\nCode:%i", message, layer_prefix, message_code);
	}
	else if(flags & VK_DEBUG_REPORT_WARNING_BIT_EXT ||
			flags & VK_DEBUG_REPORT_PERFORMANCE_WARNING_BIT_EXT)
	{
		ALogWarning("VK WARNING: %s\nLayer: %s\nCode:%i", message, layer_prefix, message_code);
	}
	else if(flags & VK_DEBUG_REPORT_ERROR_BIT_EXT)
	{
		ALogError("VK ERROR: %s\nLayer: %s\nCode:%i", message, layer_prefix, message_code);
	}
	return VK_FALSE;
}

const char* VideoVulkan::GetResultString(const int32_t& result)
{
	VkResult res = (VkResult)result;

#define RESULT_CASE(result) case VkResult::result: return #result
	switch(res)
	{
	RESULT_CASE(VK_SUCCESS);
	RESULT_CASE(VK_NOT_READY);
	RESULT_CASE(VK_TIMEOUT);
	RESULT_CASE(VK_EVENT_SET);
	RESULT_CASE(VK_EVENT_RESET);
	RESULT_CASE(VK_INCOMPLETE);
	RESULT_CASE(VK_ERROR_OUT_OF_HOST_MEMORY);
	RESULT_CASE(VK_ERROR_OUT_OF_DEVICE_MEMORY);
	RESULT_CASE(VK_ERROR_INITIALIZATION_FAILED);
	RESULT_CASE(VK_ERROR_DEVICE_LOST);
	RESULT_CASE(VK_ERROR_MEMORY_MAP_FAILED);
	RESULT_CASE(VK_ERROR_LAYER_NOT_PRESENT);
	RESULT_CASE(VK_ERROR_EXTENSION_NOT_PRESENT);
	RESULT_CASE(VK_ERROR_FEATURE_NOT_PRESENT);
	RESULT_CASE(VK_ERROR_INCOMPATIBLE_DRIVER);
	RESULT_CASE(VK_ERROR_TOO_MANY_OBJECTS);
	RESULT_CASE(VK_ERROR_FORMAT_NOT_SUPPORTED);
	RESULT_CASE(VK_ERROR_SURFACE_LOST_KHR);
	RESULT_CASE(VK_ERROR_NATIVE_WINDOW_IN_USE_KHR);
	RESULT_CASE(VK_SUBOPTIMAL_KHR);
	RESULT_CASE(VK_ERROR_OUT_OF_DATE_KHR);
	RESULT_CASE(VK_ERROR_INCOMPATIBLE_DISPLAY_KHR);
	RESULT_CASE(VK_ERROR_VALIDATION_FAILED_EXT);
	RESULT_CASE(VK_ERROR_INVALID_SHADER_NV);
	RESULT_CASE(VK_ERROR_FRAGMENTED_POOL);
	RESULT_CASE(VK_RESULT_RANGE_SIZE);
	RESULT_CASE(VK_RESULT_MAX_ENUM);
	default: break;
	}
#undef RESULT_CASE

	return "Unknown Error";
}

VideoVulkan::VideoVulkan()
	: m_instance(VK_NULL_HANDLE),
	  m_debug_report(VK_NULL_HANDLE),
	  m_physical_device(VK_NULL_HANDLE),
	  m_physical_device_memory_props(VK_NULL_HANDLE),
	  m_device(VK_NULL_HANDLE),
	  m_queue(VK_NULL_HANDLE),
	  m_swapchain(nullptr),
	  m_command_pools_lock(false),
	  m_image_available_semaphore(VK_NULL_HANDLE),
	  m_render_finished_semaphore(VK_NULL_HANDLE),
	  m_render_pass(VK_NULL_HANDLE),
	  m_pipeline_cache(VK_NULL_HANDLE)
{
	m_depth_stencil.m_image = VK_NULL_HANDLE;
	m_depth_stencil.m_memory = VK_NULL_HANDLE;
	m_depth_stencil.m_view = VK_NULL_HANDLE;
}

bool VideoVulkan::Initialize()
{
	if(!CreateSDLWindow())
	{
		ALogFatal("Failed to create window");
		return false;
	}

	if(!CreateInstance())
	{
		ALogFatal("Failed to create vulkan instance");
		return false;
	}

#if defined(BUILD_TYPE_DEBUG)
	if (!CreateDebugReporter())
	{
		ALogFatal("Failed to create vulkan debug reporter");
		return false;
	}
#endif

	if (!CreatePhysicalDevice())
	{
		ALogFatal("Failed to create vulkan physical device");
		return false;
	}

	if (!CreateDevice())
	{
		ALogFatal("Failed to create vulkan device");
		return false;
	}
	
	// Get the supported depth format
	VkFormat depth_formats[]{
		VK_FORMAT_D32_SFLOAT_S8_UINT,
		VK_FORMAT_D32_SFLOAT,
		VK_FORMAT_D24_UNORM_S8_UINT,
		VK_FORMAT_D16_UNORM_S8_UINT,
		VK_FORMAT_D16_UNORM
	};

	m_depth_format = VK_FORMAT_UNDEFINED;
	for(uint32_t i = 0; i < ASTD_ARRAY_COUNT(depth_formats); ++i)
	{
		VkFormatProperties format_props;
		vkGetPhysicalDeviceFormatProperties(m_physical_device, depth_formats[i], &format_props);

		if(format_props.optimalTilingFeatures & VK_FORMAT_FEATURE_DEPTH_STENCIL_ATTACHMENT_BIT)
		{
			m_depth_format = depth_formats[i];
			break;
		}
	}

	if(VK_FORMAT_UNDEFINED == m_depth_format)
	{
		ALogError("Could not find a depth format!");
		return false;
	}

	m_swapchain = std::make_shared<VulkanSwapChain>();
	if(!m_swapchain->Initialize(m_instance, m_physical_device, m_device, m_window))
	{
		ALogError("Failed to create swapchain!");
		return false;
	}

	VkCommandPool_T* command_pool = GetCommandPool();
	if(command_pool == nullptr)
	{
		ALogError("Failed to get command pool!");
		return false;
	}

	VkCommandBuffer_T* setup_command_buffer = SetupCommandBuffers();
	if(setup_command_buffer == nullptr)
	{
		ALogError("Failed to Command Buffers!");
		return false;
	}

	if(!SetupDepthStencil(setup_command_buffer))
	{
		ALogError("Failed to setup depth stencil!");
		return false;
	}

	if(!SetupRenderPass())
	{
		ALogError("Failed to setup render pass!");
		return false;
	}

	// Create pipeline cache
	VkPipelineCacheCreateInfo pipeline_cache_create_info = {
		VK_STRUCTURE_TYPE_PIPELINE_CACHE_CREATE_INFO,
		nullptr,
		0,
		0,
		nullptr
	};

	VkResult res = vkCreatePipelineCache(m_device, &pipeline_cache_create_info, nullptr, &m_pipeline_cache);
	if(VK_SUCCESS !=  res)
	{
		ALogError("Failed to create pipeline cache! %s", GetResultString(res));
		return false;
	}

	if(!SetupFrameBuffer())
	{
		ALogError("Failed to setup frame buffer!");
		return false;
	}

	res = vkEndCommandBuffer(setup_command_buffer);
	if(VK_SUCCESS !=  res)
	{
		ALogError("Failed to end setup command buffer! %s", GetResultString(res));
		return false;
	}

	VkSubmitInfo submit_info = {
		VK_STRUCTURE_TYPE_SUBMIT_INFO,
		nullptr,
		0,
		nullptr,
		nullptr,
		1,
		&setup_command_buffer,
		0,
		nullptr
	};

	res = vkQueueSubmit(m_queue, 1, &submit_info, nullptr);
	if(VK_SUCCESS !=  res)
	{
		ALogError("Failed to submit queue! %s", GetResultString(res));
		return false;
	}

	res = vkQueueWaitIdle(m_queue);
	if(VK_SUCCESS !=  res)
	{
		ALogError("Failed to wait on queue! %s", GetResultString(res));
		return false;
	}

	vkFreeCommandBuffers(m_device, command_pool, 1, &setup_command_buffer);
	setup_command_buffer = nullptr;

	VkSemaphoreCreateInfo semaphore_info = {};
	semaphore_info.sType = VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO;

	res = vkCreateSemaphore(m_device, &semaphore_info, nullptr, &m_image_available_semaphore);
	if(VK_SUCCESS != res)
	{
		ALogError("vkCreateSemaphore failed: %s", GetResultString(res));
		return false;
	}

	res = vkCreateSemaphore(m_device, &semaphore_info, nullptr, &m_render_finished_semaphore);
	if(VK_SUCCESS != res)
	{
		ALogError("vkCreateSemaphore failed: %s", GetResultString(res));
		return false;
	}

	return true;
}

void VideoVulkan::Release()
{
	if(m_device != VK_NULL_HANDLE)
	{
		vkDeviceWaitIdle(m_device);
	}

	for(ShaderContainer::iterator it = m_shaders.begin(); it != m_shaders.end(); ++it)
	{
		if((*it).second != nullptr)
		{
			ShaderVulkan* shader = (ShaderVulkan*)(*it).second;
			shader->Release();
			delete shader;
		}
	}
	m_shaders.clear();

	if(m_render_finished_semaphore != VK_NULL_HANDLE)
	{
		vkDestroySemaphore(m_device, m_render_finished_semaphore, nullptr);
		m_render_finished_semaphore = VK_NULL_HANDLE;
	}

	if(m_image_available_semaphore != VK_NULL_HANDLE)
	{
		vkDestroySemaphore(m_device, m_image_available_semaphore, nullptr);
		m_image_available_semaphore = VK_NULL_HANDLE;
	}

	for(FrameBufferContainer::iterator it = m_frame_buffers.begin(); it != m_frame_buffers.end(); ++it)
	{
		vkDestroyFramebuffer(m_device, *it, nullptr);
	}
	m_frame_buffers.clear();

	if(m_pipeline_cache != VK_NULL_HANDLE)
	{
		vkDestroyPipelineCache(m_device, m_pipeline_cache, nullptr);
		m_pipeline_cache = VK_NULL_HANDLE;
	}

	if(m_render_pass != VK_NULL_HANDLE)
	{
		vkDestroyRenderPass(m_device, m_render_pass, nullptr);
		m_render_pass = VK_NULL_HANDLE;
	}

	if(m_depth_stencil.m_view != VK_NULL_HANDLE)
	{
		vkDestroyImageView(m_device, m_depth_stencil.m_view, nullptr);
		m_depth_stencil.m_view = VK_NULL_HANDLE;
	}

	if(m_depth_stencil.m_image != VK_NULL_HANDLE)
	{
		vkDestroyImage(m_device, m_depth_stencil.m_image, nullptr);
		m_depth_stencil.m_image = VK_NULL_HANDLE;
	}

	if(m_depth_stencil.m_memory != VK_NULL_HANDLE)
	{
		vkFreeMemory(m_device, m_depth_stencil.m_memory, nullptr);
		m_depth_stencil.m_memory = VK_NULL_HANDLE;
	}

	if(m_draw_buffers.size() > 0)
	{
		for(uint32_t i = 0; i < m_draw_buffers.size(); ++i)
		{
			vkDestroyFence(m_device, m_draw_buffers[i].m_fence, nullptr);
			vkFreeCommandBuffers(m_device, m_draw_buffers[i].m_command_pool, 1, &m_draw_buffers[i].m_command_buffer);
		}

		m_draw_buffers.clear();
	}

	for(CommandPoolContainer::iterator it = m_command_pools.begin(); it != m_command_pools.end(); ++it)
	{
		vkDestroyCommandPool(m_device, it->second, nullptr);
	}
	m_command_pools.clear();

	if(m_swapchain != nullptr)
	{
		m_swapchain->Release();
		m_swapchain.reset();
	}

	m_physical_device_properties.reset();
	m_physical_device_features.reset();
	m_physical_device_memory_props.reset();

	if(m_device != VK_NULL_HANDLE)
	{
		vkDestroyDevice(m_device, nullptr);
		m_device = VK_NULL_HANDLE;
	}

	m_physical_device = nullptr;

	if(m_debug_report != VK_NULL_HANDLE)
	{
		PFN_vkDestroyDebugReportCallbackEXT vkDestroyDebugReportCallbackEXT = reinterpret_cast<PFN_vkDestroyDebugReportCallbackEXT>(vkGetInstanceProcAddr(m_instance, "vkDestroyDebugReportCallbackEXT"));
		vkDestroyDebugReportCallbackEXT(m_instance, m_debug_report, nullptr);
		m_debug_report = VK_NULL_HANDLE;
	}

	if(m_instance != VK_NULL_HANDLE)
	{
		vkDestroyInstance(m_instance, nullptr);
		m_instance = VK_NULL_HANDLE;
	}

	DestroySDLWindow();
}

void VideoVulkan::PlaybackRenderCommandBuffers(CommandBuffer* /*command_buffers*/, uint32_t /*count*/)
{
	uint32_t image_index = 0;
	VkResult res = vkAcquireNextImageKHR(m_device, m_swapchain->GetSwapChain(), std::numeric_limits<uint64_t>::max(), m_image_available_semaphore, VK_NULL_HANDLE,
										 &image_index);
	if(VK_SUCCESS != res)
	{
		ALogError("vkAcquireNextImageKHR failed: %s", GetResultString(res));
		return;
	}

	do {
		res = vkGetFenceStatus(m_device, m_draw_buffers[image_index].m_fence);
	} while(VK_NOT_READY == res);

	// Seems like my amd driver did not install correctly.. so fencing with vkGetFenceStatus...
	// TODO: test this next driver update
	/*do {
		res = vkWaitForFences(m_device, 1, &m_draw_buffer_fences[image_index].m_fence, VK_TRUE, 1000);
	} while(VK_TIMEOUT == res);*/

	if(VK_SUCCESS != res)
	{
		ALogError("vkWaitForFences failed: %s", GetResultString(res));
	}

	res = vkResetFences(m_device, 1, &m_draw_buffers[image_index].m_fence);
	if(VK_SUCCESS != res)
	{
		ALogError("vkResetFences failed: %s", GetResultString(res));
		return;
	}

	{
		VkCommandBufferBeginInfo begin_info = {};
		begin_info.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
		begin_info.flags = VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT | VK_COMMAND_BUFFER_USAGE_SIMULTANEOUS_USE_BIT;
		begin_info.pInheritanceInfo = nullptr; // Optional

		res = vkBeginCommandBuffer(m_draw_buffers[image_index].m_command_buffer, &begin_info);
		if(VK_SUCCESS != res)
		{
			ALogError("vkBeginCommandBuffer failed: %s", GetResultString(res));
			return;
		}

		VkRenderPassBeginInfo render_begin_info = {};
		render_begin_info.sType = VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO;
		render_begin_info.pNext = nullptr;
		render_begin_info.renderPass = m_render_pass;
		render_begin_info.framebuffer = m_frame_buffers[image_index];

		render_begin_info.renderArea.offset = {0, 0};
		render_begin_info.renderArea.extent = { m_swapchain->GetWidth(), m_swapchain->GetHeight() };

		VkClearValue clear_color;
		clear_color.color.float32[0] = clear_color.color.float32[1] = clear_color.color.float32[2] = 0.0f;
		clear_color.color.float32[3] = 1.0f;

		VkClearValue clear_colors[2] = { clear_color, clear_color };

		render_begin_info.clearValueCount = 2;
		render_begin_info.pClearValues = clear_colors;

		vkCmdBeginRenderPass(m_draw_buffers[image_index].m_command_buffer, &render_begin_info, VK_SUBPASS_CONTENTS_SECONDARY_COMMAND_BUFFERS);

		// TODO: run secondary commands here
		//vkCmdExecuteCommands();

		vkCmdEndRenderPass(m_draw_buffers[image_index].m_command_buffer);

		res = vkEndCommandBuffer(m_draw_buffers[image_index].m_command_buffer);
		if(VK_SUCCESS != res)
		{
			ALogError("vkEndCommandBuffer failed: %s", GetResultString(res));
			return;
		}
	}

	VkSubmitInfo submit_info = {};
	submit_info.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
	submit_info.pNext = nullptr;

	VkSemaphore wait_semaphores[] = { m_image_available_semaphore };
	VkPipelineStageFlags wait_stages[] = { VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT };
	submit_info.waitSemaphoreCount = 1;
	submit_info.pWaitSemaphores = wait_semaphores;
	submit_info.pWaitDstStageMask = wait_stages;

	submit_info.commandBufferCount = 1;
	submit_info.pCommandBuffers = &m_draw_buffers[image_index].m_command_buffer;

	VkSemaphore signal_semaphores[] = { m_render_finished_semaphore };
	submit_info.signalSemaphoreCount = 1;
	submit_info.pSignalSemaphores = signal_semaphores;

	res = vkQueueSubmit(m_queue, 1, &submit_info, m_draw_buffers[image_index].m_fence);
	if(VK_SUCCESS != res)
	{
		ALogError("vkQueueSubmit failed: %s", GetResultString(res));
		return;
	}

	VkPresentInfoKHR present_info = {};
	present_info.sType = VK_STRUCTURE_TYPE_PRESENT_INFO_KHR;
	present_info.pNext = nullptr;
	present_info.waitSemaphoreCount = 1;
	present_info.pWaitSemaphores = signal_semaphores;

	VkSwapchainKHR swapchains[] = { m_swapchain->GetSwapChain() };
	present_info.swapchainCount = 1;
	present_info.pSwapchains = swapchains;
	present_info.pImageIndices = &image_index;
	present_info.pResults = nullptr;

	res = vkQueuePresentKHR(m_queue, &present_info);
	if(VK_SUCCESS != res)
	{
		ALogError("vkQueuePresentKHR failed: %s", GetResultString(res));
		return;
	}
}

uint32_t VideoVulkan::CreateShader(ShaderData** shader_datas, uint32_t count)
{
	ASTD_UNUSED(shader_datas);
	ASTD_UNUSED(count);
	return UINT32_MAX;
}

void VideoVulkan::DestroyShader(uint32_t shader_index)
{
	ASTD_UNUSED(shader_index);
}

uint32_t VideoVulkan::CreateTexture(uint32_t width, uint32_t height, bool generate_mipmaps, uint32_t format, uint32_t internal_format, uint32_t type, const void* data)
{
	ASTD_UNUSED(width);
	ASTD_UNUSED(height);
	ASTD_UNUSED(generate_mipmaps);
	ASTD_UNUSED(format);
	ASTD_UNUSED(internal_format);
	ASTD_UNUSED(type);
	ASTD_UNUSED(data);
	return UINT32_MAX;
}

void VideoVulkan::DestroyTexture(uint32_t index)
{
	ASTD_UNUSED(index);
}

uint32_t VideoVulkan::CreateVertexBuffer(const void* data, uint32_t data_size, BufferType buffer_type, BufferUsage usage)
{
	ASTD_UNUSED(data);
	ASTD_UNUSED(data_size);
	ASTD_UNUSED(buffer_type);
	ASTD_UNUSED(usage);
	return UINT32_MAX;
}

void VideoVulkan::DestroyVertexBuffer(uint32_t index)
{
	ASTD_UNUSED(index);
}

void VideoVulkan::UpdateVertexBuffer(uint32_t index, const void* data, uint32_t data_size, BufferType buffer_type, uint32_t offset)
{
	ASTD_UNUSED(index);
	ASTD_UNUSED(data);
	ASTD_UNUSED(data_size);
	ASTD_UNUSED(buffer_type);
	ASTD_UNUSED(offset);
}

CommandBuffer* VideoVulkan::CreateCommandBuffer(const uint32_t&)
{
	return new CommandBufferVulkan;
}

CommandBufferData VideoVulkan::GetCommandBuffer()
{
	// TODO: see if caching command buffers is better
	CommandBufferData command_buffer;

	command_buffer.m_command_pool = GetCommandPool();
	if(command_buffer.m_command_pool == nullptr)
	{
		ALogError("Failed to get command pool!");
		return CommandBufferData();
	}

	VkCommandBufferAllocateInfo command_buffer_allocate_info = {};
	command_buffer_allocate_info.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
	command_buffer_allocate_info.pNext = nullptr;
	command_buffer_allocate_info.commandPool = command_buffer.m_command_pool;
	command_buffer_allocate_info.level = VK_COMMAND_BUFFER_LEVEL_SECONDARY;
	command_buffer_allocate_info.commandBufferCount = 1;

	VkResult res = vkAllocateCommandBuffers(m_device, &command_buffer_allocate_info, &command_buffer.m_command_buffer);
	if(VK_SUCCESS !=  res)
	{
		ALogError("Failed to allocate command buffer! %s", GetResultString(res));
		return CommandBufferData();
	}
	return command_buffer;
}

void VideoVulkan::ReturnCommandBuffer(const CommandBufferData& command_buffer)
{
	if(command_buffer.m_command_buffer != nullptr)
	{
		vkFreeCommandBuffers(m_device, command_buffer.m_command_pool, 1, &command_buffer.m_command_buffer);
	}
}

void VideoVulkan::OnWindowResized()
{
	vkDeviceWaitIdle(m_device);

	if(m_draw_buffers.size() > 0)
	{
		for(uint32_t i = 0; i < m_draw_buffers.size(); ++i)
		{
			vkDestroyFence(m_device, m_draw_buffers[i].m_fence, nullptr);
			vkFreeCommandBuffers(m_device, m_draw_buffers[i].m_command_pool, 1, &m_draw_buffers[i].m_command_buffer);
		}

		m_draw_buffers.clear();
	}

	VkCommandBuffer_T* setup_command_buffer = SetupCommandBuffers();
	if(setup_command_buffer == nullptr)
	{
		ALogError("Failed to SetupCommandBuffers!");
		return;
	}

	// Recreate the frame buffers
	vkDestroyImageView(m_device, m_depth_stencil.m_view, nullptr);
	vkDestroyImage(m_device, m_depth_stencil.m_image, nullptr);
	vkFreeMemory(m_device, m_depth_stencil.m_memory, nullptr);
	m_depth_stencil.m_view = VK_NULL_HANDLE;
	m_depth_stencil.m_image = VK_NULL_HANDLE;
	m_depth_stencil.m_memory = VK_NULL_HANDLE;

	SetupDepthStencil(setup_command_buffer);

	for (uint32_t i = 0; i < m_frame_buffers.size(); i++)
	{
		vkDestroyFramebuffer(m_device, m_frame_buffers[i], nullptr);
	}
	m_frame_buffers.clear();

	SetupFrameBuffer();

	VkResult res = vkEndCommandBuffer(setup_command_buffer);
	if(VK_SUCCESS !=  res)
	{
		ALogError("Failed to end setup command buffer! %s", GetResultString(res));
		return;
	}

	VkSubmitInfo submit_info = {
		VK_STRUCTURE_TYPE_SUBMIT_INFO,
		nullptr,
		0,
		nullptr,
		nullptr,
		1,
		&setup_command_buffer,
		0,
		nullptr
	};

	res = vkQueueSubmit(m_queue, 1, &submit_info, nullptr);
	if(VK_SUCCESS !=  res)
	{
		ALogError("Failed to submit queue! %s", GetResultString(res));
		return;
	}

	res = vkQueueWaitIdle(m_queue);
	if(VK_SUCCESS !=  res)
	{
		ALogError("Failed to wait on queue! %s", GetResultString(res));
		return;
	}

	vkFreeCommandBuffers(m_device, GetCommandPool(), 1, &setup_command_buffer);
	setup_command_buffer = VK_NULL_HANDLE;

	vkQueueWaitIdle(m_queue);
	vkDeviceWaitIdle(m_device);
}

bool VideoVulkan::CreateInstance()
{
	const astd::string& major_version_string = Engine::GetInstance()->GetConfig()->GetValue(Utilities::Hash32("PublishingInfo"), Utilities::Hash32("major_version"));
	const astd::string& minor_version_string = Engine::GetInstance()->GetConfig()->GetValue(Utilities::Hash32("PublishingInfo"), Utilities::Hash32("minor_version"));
	const astd::string& patch_version_string = Engine::GetInstance()->GetConfig()->GetValue(Utilities::Hash32("PublishingInfo"), Utilities::Hash32("patch_version"));

	uint32_t major_version = 0, minor_version = 0, patch_version = 1;
	if (!major_version_string.empty()) major_version = static_cast<uint32_t>(strtoul(major_version_string.c_str(), nullptr, 10));
	if (!minor_version_string.empty()) minor_version = static_cast<uint32_t>(strtoul(minor_version_string.c_str(), nullptr, 10));
	if (!patch_version_string.empty()) patch_version = static_cast<uint32_t>(strtoul(patch_version_string.c_str(), nullptr, 10));

	// TODO popper: should I randomize names =)
	VkApplicationInfo app_info{
		VK_STRUCTURE_TYPE_APPLICATION_INFO,
		nullptr,
		Engine::GetInstance()->GetConfig()->GetGameName().c_str(),
		VK_MAKE_VERSION(major_version,minor_version,patch_version),
		PROJECT_NAME,
		VK_MAKE_VERSION(ACCLUTION_VERSION_MAJOR,ACCLUTION_VERSION_MINOR,ACCLUTION_VERSION_PATCH),
		VK_API_VERSION
	};

	astd::vector<const char*> enabled_instance_layers;
#if defined(BUILD_TYPE_DEBUG)
	enabled_instance_layers.push_back("VK_LAYER_LUNARG_standard_validation");
	enabled_instance_layers.push_back("VK_LAYER_LUNARG_core_validation");
	enabled_instance_layers.push_back("VK_LAYER_LUNARG_image");
	enabled_instance_layers.push_back("VK_LAYER_LUNARG_parameter_validation");
	//enabled_instance_layers.push_back("VK_LAYER_LUNARG_object_tracker");
#endif

	/*
	VK_LAYER_LUNARG_core_validation
	VK_LAYER_LUNARG_image
	VK_LAYER_LUNARG_parameter_validation
	VK_LAYER_LUNARG_swapchain
	VK_LAYER_LUNARG_object_tracker
	VK_LAYER_LUNARG_core_validation
	VK_LAYER_LUNARG_image
	VK_LAYER_LUNARG_parameter_validation
	VK_LAYER_LUNARG_swapchain
	VK_LAYER_LUNARG_object_tracker
	VK_LAYER_VALVE_steam_overlay_32
	VK_LAYER_VALVE_steam_overlay_64
	VK_LAYER_LUNARG_standard_validation
	*/

	astd::vector<const char*> enabled_instance_extensions;
	enabled_instance_extensions.push_back(VK_KHR_SURFACE_EXTENSION_NAME);

#if VK_KHR_win32_surface
	enabled_instance_extensions.push_back(VK_KHR_WIN32_SURFACE_EXTENSION_NAME);
#endif

#if VK_KHR_xlib_surface
	enabled_instance_extensions.push_back(VK_KHR_XLIB_SURFACE_EXTENSION_NAME);
#endif

#if VK_KHR_xcb_surface
	enabled_instance_extensions.push_back(VK_KHR_XCB_SURFACE_EXTENSION_NAME);
#endif

#if VK_KHR_wayland_surface
	enabled_instance_extensions.push_back(VK_KHR_WAYLAND_SURFACE_EXTENSION_NAME);
#endif

#if VK_KHR_mir_surface
	enabled_instance_extensions.push_back(VK_KHR_MIR_SURFACE_EXTENSION_NAME);
#endif

#if VK_KHR_android_surface
	enabled_instance_extensions.push_back(VK_KHR_ANDROID_SURFACE_EXTENSION_NAME);
#endif

#if defined(BUILD_TYPE_DEBUG)
	enabled_instance_extensions.push_back(VK_EXT_DEBUG_REPORT_EXTENSION_NAME);
#endif

	ValidateLayers(true, false, enabled_instance_layers);
	ValidateLayers(true, true, enabled_instance_extensions);

	//////////////////////////////////////////////////////////////////////////////////////////
	// Create Instance stuff
	VkInstanceCreateInfo instance_create_info{
		VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO,
		nullptr,
		0,
		&app_info,
		(uint32_t)enabled_instance_layers.size(),
		enabled_instance_layers.data(),
		(uint32_t)enabled_instance_extensions.size(),
		enabled_instance_extensions.data()
	};

	m_instance = nullptr;
	VkResult res = vkCreateInstance(&instance_create_info, nullptr, &m_instance);
	if(VK_SUCCESS != res)
	{
		ALogError("Failed to create vulkan instance! %s", GetResultString(res));
		return false;
	}
	return true;
}

bool VideoVulkan::CreateDebugReporter()
{
#if VK_EXT_debug_report
	// Setup callback creation information
	VkDebugReportCallbackCreateInfoEXT debug_report_callback_create_info {
		VK_STRUCTURE_TYPE_DEBUG_REPORT_CREATE_INFO_EXT,
		nullptr,
		VK_DEBUG_REPORT_INFORMATION_BIT_EXT | VK_DEBUG_REPORT_WARNING_BIT_EXT | VK_DEBUG_REPORT_PERFORMANCE_WARNING_BIT_EXT | VK_DEBUG_REPORT_ERROR_BIT_EXT | VK_DEBUG_REPORT_DEBUG_BIT_EXT,
		&VulkanReportCallback,
		nullptr
	};

	// Register the callback
	PFN_vkCreateDebugReportCallbackEXT vkCreateDebugReportCallbackEXT = reinterpret_cast<PFN_vkCreateDebugReportCallbackEXT>(vkGetInstanceProcAddr(m_instance, "vkCreateDebugReportCallbackEXT"));
	VkResult res = vkCreateDebugReportCallbackEXT(m_instance, &debug_report_callback_create_info, nullptr, &m_debug_report);
	if (VK_SUCCESS != res)
	{
		ALogError("Failed to create debug report! %s", GetResultString(res));
		return false;
	}
#endif
	return true;
}

bool VideoVulkan::CreatePhysicalDevice()
{
	uint32_t gpu_count = 0;
	VkResult res = vkEnumeratePhysicalDevices(m_instance, &gpu_count, nullptr);
	if (VK_SUCCESS != res || gpu_count == 0)
	{
		ALogError("Failed to enumerate vulkan devices! %s", GetResultString(res));
		return false;
	}

	astd::vector<VkPhysicalDevice> physical_devices(gpu_count);
	res = vkEnumeratePhysicalDevices(m_instance, &gpu_count, physical_devices.data());
	if (VK_SUCCESS != res || gpu_count == 0)
	{
		ALogError("Failed to enumerate vulkan devices! %s", GetResultString(res));
		return false;
	}

	// TODO popper: Should the user be able to decide what device to use?
	m_physical_device = physical_devices[0];
	return true;
}

bool VideoVulkan::CreateDevice()
{
	// Get Physical Device Queue
	uint32_t queue_count = 0;
	vkGetPhysicalDeviceQueueFamilyProperties(m_physical_device, &queue_count, nullptr);
	if (queue_count == 0)
	{
		ALogError("Failed to get device queue properties!");
		return false;
	}

	astd::vector<VkQueueFamilyProperties> queue_props(queue_count);
	vkGetPhysicalDeviceQueueFamilyProperties(m_physical_device, &queue_count, queue_props.data());

	uint32_t graphics_queue_index = 0;
	for (graphics_queue_index = 0; graphics_queue_index < queue_count; ++graphics_queue_index)
	{
		if (queue_props[graphics_queue_index].queueFlags & VK_QUEUE_GRAPHICS_BIT)
			break;
	}

	if (graphics_queue_index >= queue_count)
	{
		ALogError("Failed to get the correct device queue!");
		return false;
	}

	float queue_priorities[] = { 0.0f };
	VkDeviceQueueCreateInfo queue_create_info {
		VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO,
		nullptr,
		0,
		graphics_queue_index,
		1,
		queue_priorities
	};

	////////////////////////////////////////////////////////////////////
	// Create Device

	astd::vector<const char*> enabled_device_layers;
#if defined(BUILD_TYPE_DEBUG)
	enabled_device_layers.push_back("VK_LAYER_LUNARG_standard_validation");
	enabled_device_layers.push_back("VK_LAYER_LUNARG_core_validation");
	enabled_device_layers.push_back("VK_LAYER_LUNARG_image");
	enabled_device_layers.push_back("VK_LAYER_LUNARG_parameter_validation");
#endif

	astd::vector<const char*> enabled_device_extensions;
	enabled_device_extensions.push_back(VK_KHR_SWAPCHAIN_EXTENSION_NAME);

	ValidateLayers(false, false, enabled_device_layers);
	ValidateLayers(false, true, enabled_device_extensions);

	VkDeviceCreateInfo device_create_info {
		VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO,
		nullptr,
		0,
		1, &queue_create_info, // queue infos
		(uint32_t)enabled_device_layers.size(), enabled_device_layers.data(), // layers
		(uint32_t)enabled_device_extensions.size(), enabled_device_extensions.data(), // extensions
		nullptr
	};

	VkResult res = vkCreateDevice(m_physical_device, &device_create_info, nullptr, &m_device);
	if (VK_SUCCESS != res)
	{
		ALogError("Failed to create vulkan device! %s", GetResultString(res));
		return false;
	}

	// Store properties (including limits) and features of the phyiscal device
	m_physical_device_properties = std::make_shared<VkPhysicalDeviceProperties>();
	vkGetPhysicalDeviceProperties(m_physical_device, m_physical_device_properties.get());
	m_physical_device_features = std::make_shared<VkPhysicalDeviceFeatures>();
	vkGetPhysicalDeviceFeatures(m_physical_device, m_physical_device_features.get());

	// Get memory properties
	m_physical_device_memory_props = std::make_shared<VkPhysicalDeviceMemoryProperties>();
	vkGetPhysicalDeviceMemoryProperties(m_physical_device, m_physical_device_memory_props.get());

	// Get the device queue
	vkGetDeviceQueue(m_device, graphics_queue_index, 0, &m_queue);
	return true;
}

void VideoVulkan::ValidateLayers(bool instance, bool extention, astd::vector<const char*>& layers)
{
	if(layers.empty())
		return;

	VkResult res = VK_SUCCESS;
	uint32_t count = 0;

	if(extention)
	{
		VkExtensionProperties* extension_properties = nullptr;
		if(instance)
		{
			res = vkEnumerateInstanceExtensionProperties(nullptr, &count, extension_properties);
			if(VK_SUCCESS !=  res)
			{
				ALogError("Vulkan vkEnumerateInstanceExtensionProperties error! %s", GetResultString(res));
				return;
			}

			extension_properties = (VkExtensionProperties*)alloca(count * sizeof(VkExtensionProperties));
			res = vkEnumerateInstanceExtensionProperties(nullptr, &count, extension_properties);
			if(VK_SUCCESS !=  res)
			{
				ALogError("Vulkan vkEnumerateInstanceExtensionProperties error! %s", GetResultString(res));
				return;
			}
		}
		else
		{
			res = vkEnumerateDeviceExtensionProperties(m_physical_device, nullptr, &count, extension_properties);
			if(VK_SUCCESS !=  res)
			{
				ALogError("Vulkan vkEnumerateDeviceExtensionProperties error! %s", GetResultString(res));
				return;
			}

			extension_properties = (VkExtensionProperties*)alloca(count * sizeof(VkExtensionProperties));
			res = vkEnumerateDeviceExtensionProperties(m_physical_device, nullptr, &count, extension_properties);
			if(VK_SUCCESS !=  res)
			{
				ALogError("Vulkan vkEnumerateDeviceExtensionProperties error! %s", GetResultString(res));
				return;
			}
		}

		for(astd::vector<const char*>::iterator it = layers.begin(); it != layers.end(); ++it)
		{
			auto layer_pred = [it](VkExtensionProperties& layer){ return strncmp(layer.extensionName, *it, VK_MAX_EXTENSION_NAME_SIZE) == 0 && layer.specVersion <= VK_API_VERSION; };
			if(std::find_if(extension_properties, &extension_properties[count], layer_pred) == &extension_properties[count])
			{
				it = layers.erase(it);
				if(layers.empty())
					break;
			}
		}
	}
	else
	{
		VkLayerProperties* instance_properties = nullptr;
		if(instance)
		{
			res = vkEnumerateInstanceLayerProperties(&count, instance_properties);
			if(VK_SUCCESS !=  res)
			{
				ALogError("Vulkan vkEnumerateInstanceLayerProperties error! %s", GetResultString(res));
				return;
			}

			instance_properties = (VkLayerProperties*)alloca(count * sizeof(VkLayerProperties));
			res = vkEnumerateInstanceLayerProperties(&count, instance_properties);
			if(VK_SUCCESS !=  res)
			{
				ALogError("Vulkan vkEnumerateInstanceLayerProperties error! %s", GetResultString(res));
				return;
			}
		}
		else
		{
			res = vkEnumerateDeviceLayerProperties(m_physical_device, &count, instance_properties);
			if(VK_SUCCESS !=  res)
			{
				ALogError("Vulkan vkEnumerateDeviceLayerProperties error! %s", GetResultString(res));
				return;
			}

			instance_properties = (VkLayerProperties*)alloca(count * sizeof(VkLayerProperties));
			res = vkEnumerateDeviceLayerProperties(m_physical_device, &count, instance_properties);
			if(VK_SUCCESS !=  res)
			{
				ALogError("Vulkan vkEnumerateDeviceLayerProperties error! %s", GetResultString(res));
				return;
			}
		}

		for(astd::vector<const char*>::iterator it = layers.begin(); it != layers.end(); ++it)
		{
			auto layer_pred = [it](VkLayerProperties& layer){ return strncmp(layer.layerName, *it, VK_MAX_EXTENSION_NAME_SIZE) == 0 && layer.specVersion <= VK_API_VERSION; };
			if(std::find_if(instance_properties, &instance_properties[count], layer_pred) == &instance_properties[count])
			{
				it = layers.erase(it);
				if(layers.empty())
					break;
			}
		}
	}
}

VkCommandBuffer_T* VideoVulkan::SetupCommandBuffers()
{
	// Create the command buffer for setup
	VkCommandBufferAllocateInfo command_buffer_allocate_info = {};
	command_buffer_allocate_info.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
	command_buffer_allocate_info.pNext = nullptr;
	command_buffer_allocate_info.commandPool = GetCommandPool();
	command_buffer_allocate_info.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
	command_buffer_allocate_info.commandBufferCount = 1;

	VkCommandBuffer_T* setup_command_buffer = nullptr;
	VkResult res = vkAllocateCommandBuffers(m_device, &command_buffer_allocate_info, &setup_command_buffer);
	if(VK_SUCCESS !=  res)
	{
		ALogError("Failed to allocate setup command buffer! %s", GetResultString(res));
		return nullptr;
	}

	VkCommandBufferBeginInfo command_buffer_begin_info = {};
	command_buffer_begin_info.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
	command_buffer_begin_info.pNext = nullptr;
	command_buffer_begin_info.flags = 0;
	command_buffer_begin_info.pInheritanceInfo = nullptr;

	res = vkBeginCommandBuffer(setup_command_buffer, &command_buffer_begin_info);
	if(VK_SUCCESS !=  res)
	{
		ALogError("Failed to begin setup command buffer! %s", GetResultString(res));
		return nullptr;
	}

	if(!m_swapchain->Setup(m_physical_device, setup_command_buffer))
	{
		ALogError("Failed to begin setup swapchain!");
		return nullptr;
	}

	return setup_command_buffer;
}

bool VideoVulkan::SetupDepthStencil(VkCommandBuffer_T* setup_command_buffer)
{
	// Create depth image
	VkImageCreateInfo image = {
		VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO,
		nullptr,
		0,
		VK_IMAGE_TYPE_2D,
		(VkFormat)m_depth_format,
		{ m_swapchain->GetWidth(), m_swapchain->GetHeight(), 1 },
		1,
		1,
		VK_SAMPLE_COUNT_1_BIT,
		VK_IMAGE_TILING_OPTIMAL,
		VK_IMAGE_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT | VK_IMAGE_USAGE_TRANSFER_SRC_BIT,
		VK_SHARING_MODE_EXCLUSIVE,
		0,
		nullptr,
		VK_IMAGE_LAYOUT_UNDEFINED
	};

	VkResult res = vkCreateImage(m_device, &image, nullptr, &m_depth_stencil.m_image);
	if(VK_SUCCESS !=  res)
	{
		ALogError("Failed to create depth image! %s", GetResultString(res));
		return false;
	}

	// Create depth image memory
	VkMemoryRequirements memory_requirements;
	vkGetImageMemoryRequirements(m_device, m_depth_stencil.m_image, &memory_requirements);

	VkMemoryAllocateInfo memory_alloc = {
		VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO,
		nullptr,
		memory_requirements.size,
		0
	};

	GetMemoryType(memory_requirements.memoryTypeBits, VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT, memory_alloc.memoryTypeIndex);

	res = vkAllocateMemory(m_device, &memory_alloc, nullptr, &m_depth_stencil.m_memory);
	if(VK_SUCCESS !=  res)
	{
		ALogError("Failed to allocate depth image memory! %s", GetResultString(res));
		return false;
	}

	// Bind the image to the memory
	res = vkBindImageMemory(m_device, m_depth_stencil.m_image, m_depth_stencil.m_memory, 0);
	if(VK_SUCCESS !=  res)
	{
		ALogError("Failed to bind depth image to depth memory! %s", GetResultString(res));
		return false;
	}

	SetImageLayout(setup_command_buffer, m_depth_stencil.m_image, VK_IMAGE_ASPECT_DEPTH_BIT, VK_IMAGE_LAYOUT_UNDEFINED, VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL);

	// Create the depth image view
	VkImageViewCreateInfo depth_stencil_view_info;
	depth_stencil_view_info.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
	depth_stencil_view_info.pNext = nullptr;
	depth_stencil_view_info.flags = 0;
	depth_stencil_view_info.image = m_depth_stencil.m_image;
	depth_stencil_view_info.viewType = VK_IMAGE_VIEW_TYPE_2D;
	depth_stencil_view_info.format = (VkFormat)m_depth_format;

	depth_stencil_view_info.components.r =
			depth_stencil_view_info.components.g =
			depth_stencil_view_info.components.b =
			depth_stencil_view_info.components.a = VK_COMPONENT_SWIZZLE_IDENTITY;

	depth_stencil_view_info.subresourceRange.aspectMask = VK_IMAGE_ASPECT_DEPTH_BIT | VK_IMAGE_ASPECT_STENCIL_BIT;
	depth_stencil_view_info.subresourceRange.baseMipLevel = 0;
	depth_stencil_view_info.subresourceRange.levelCount = 1;
	depth_stencil_view_info.subresourceRange.baseArrayLayer = 0;
	depth_stencil_view_info.subresourceRange.layerCount = 1;

	res = vkCreateImageView(m_device, &depth_stencil_view_info, nullptr, &m_depth_stencil.m_view);
	if(VK_SUCCESS !=  res)
	{
		ALogError("Failed to create depth stencil image view! %s", GetResultString(res));
		return false;
	}

	return true;
}

bool VideoVulkan::SetupRenderPass()
{
	VkAttachmentDescription attachments[2];
	attachments[0].flags = 0;
	attachments[0].format = (VkFormat)m_swapchain->GetColorFormat();
	attachments[0].samples = VK_SAMPLE_COUNT_1_BIT;
	attachments[0].loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR;
	attachments[0].storeOp = VK_ATTACHMENT_STORE_OP_STORE;
	attachments[0].stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
	attachments[0].stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
	attachments[0].initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
	attachments[0].finalLayout = VK_IMAGE_LAYOUT_PRESENT_SRC_KHR;

	attachments[1] = attachments[0];
	attachments[1].format = (VkFormat)m_depth_format;
	attachments[1].initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
	attachments[1].finalLayout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL;

	VkAttachmentReference color_reference = {
		0,
		VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL
	};

	VkAttachmentReference depth_reference = {
		1,
		VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL
	};

	VkSubpassDescription subpass = {
		0,
		VK_PIPELINE_BIND_POINT_GRAPHICS,
		0,
		nullptr,
		1,
		&color_reference,
		nullptr,
		&depth_reference,
		0,
		nullptr
	};

	VkSubpassDependency dependency = {};
	dependency.srcSubpass = VK_SUBPASS_EXTERNAL;
	dependency.dstSubpass = 0;
	dependency.srcStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
	dependency.srcAccessMask = 0;
	dependency.dstStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
	dependency.dstAccessMask = VK_ACCESS_COLOR_ATTACHMENT_READ_BIT | VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT;

	VkRenderPassCreateInfo render_pass_info = {};
	render_pass_info.sType = VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO;
	render_pass_info.pNext = nullptr;
	render_pass_info.flags = 0;
	render_pass_info.attachmentCount = 2;
	render_pass_info.pAttachments = attachments;
	render_pass_info.subpassCount = 1;
	render_pass_info.pSubpasses = &subpass;
	render_pass_info.dependencyCount = 1;
	render_pass_info.pDependencies = &dependency;

	VkResult res = vkCreateRenderPass(m_device, &render_pass_info, nullptr, &m_render_pass);
	if(VK_SUCCESS !=  res)
	{
		ALogError("Failed to create renderpass! %s", GetResultString(res));
		return false;
	}

	return true;
}

bool VideoVulkan::SetupFrameBuffer()
{
	VkImageView attachments[2];

	// Depth/Stencil attachment is the same for all frame buffers
	attachments[1] = m_depth_stencil.m_view;

	VkFramebufferCreateInfo frame_buffer_create_info = {
		VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO,
		nullptr,
		0,
		m_render_pass,
		2,
		attachments,
		m_swapchain->GetWidth(),
		m_swapchain->GetHeight(),
		1
	};

	// Create frame buffers for every swap chain image
	for(uint32_t i = 0; i < m_swapchain->GetImageCount(); ++i)
	{
		attachments[0] = m_swapchain->GetSwapChainBuffer(i).m_view;
		VkFramebuffer buffer = nullptr;
		VkResult res = vkCreateFramebuffer(m_device, &frame_buffer_create_info, nullptr, &buffer);
		if(VK_SUCCESS !=  res)
		{
			ALogError("Failed to create framebuffer! %s", GetResultString(res));
			return false;
		}
		m_frame_buffers.push_back(buffer);
	}

	VkCommandPool command_pool = GetCommandPool();

	m_draw_buffers.resize(m_frame_buffers.size());
	for(uint32_t i = 0; i < m_draw_buffers.size(); ++i)
	{
		m_draw_buffers[i].m_command_pool = command_pool;

		VkCommandBufferAllocateInfo command_buffer_allocate_info = {};
		command_buffer_allocate_info.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
		command_buffer_allocate_info.pNext = nullptr;
		command_buffer_allocate_info.commandPool = command_pool;
		command_buffer_allocate_info.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
		command_buffer_allocate_info.commandBufferCount = 1;

		VkResult res = vkAllocateCommandBuffers(m_device, &command_buffer_allocate_info, &m_draw_buffers[i].m_command_buffer);
		if(VK_SUCCESS !=  res)
		{
			ALogError("Failed to allocate draw command buffer! %s", GetResultString(res));
			return false;
		}

		VkFenceCreateInfo fence_info = {};
		fence_info.sType = VK_STRUCTURE_TYPE_FENCE_CREATE_INFO;
		fence_info.flags = VK_FENCE_CREATE_SIGNALED_BIT;

		res = vkCreateFence(m_device, &fence_info, nullptr, &m_draw_buffers[i].m_fence);
		if(VK_SUCCESS != res)
		{
			ALogError("Failed to create draw command buffer fences: %s", GetResultString(res));
			return false;
		}
	}

	return true;
}

VkCommandPool_T* VideoVulkan::GetCommandPool()
{
	// Do as mush work as possible before locking
	VkCommandPool_T* command_pool = VK_NULL_HANDLE;

	VkCommandPoolCreateInfo cmd_pool_info = {};
	cmd_pool_info.sType = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO;
	cmd_pool_info.pNext = nullptr;
	cmd_pool_info.flags = VK_COMMAND_POOL_CREATE_RESET_COMMAND_BUFFER_BIT;
	cmd_pool_info.queueFamilyIndex = m_swapchain->GetQueueNodeIndex();

	size_t thread_id = std::hash<std::thread::id>()(std::this_thread::get_id());

	// Lock the command pools container (spinlock)
	while(m_command_pools_lock.exchange(true)) {}

	CommandPoolContainer::iterator it = m_command_pools.find(thread_id);
	if(it != m_command_pools.end())
	{
		command_pool = it->second;
	}
	else
	{
		VkResult res = vkCreateCommandPool(m_device, &cmd_pool_info, nullptr, &command_pool);
		if(VK_SUCCESS !=  res)
		{
			ALogError("Failed to create command pool! %s for thread: %u", GetResultString(res), thread_id);
		}
		else
		{
			m_command_pools[thread_id] = command_pool;
		}
	}

	m_command_pools_lock.store(false);
	return command_pool;
}

uint32_t VideoVulkan::GetMemoryType(uint32_t type_bits, uint32_t properties, uint32_t& type_index)
{
	for(uint32_t i = 0; i < 32; ++i)
	{
		if((type_bits & 1) == 1)
		{
			if((m_physical_device_memory_props->memoryTypes[i].propertyFlags & properties) == properties)
			{
				type_index = i;
				return true;
			}
		}
		type_bits >>= 1;
	}
	return false;
}

void VideoVulkan::SetImageLayout(VkCommandBuffer_T* command_buffer, VkImage_T* image, uint32_t aspect_mask, uint32_t old_image_layout, uint32_t new_image_layout)
{
	// Create an image barrier object
	VkImageMemoryBarrier image_memory_barrier;
	image_memory_barrier.sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER;
	image_memory_barrier.pNext = nullptr;
	image_memory_barrier.srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
	image_memory_barrier.dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;

	image_memory_barrier.oldLayout = (VkImageLayout)old_image_layout;
	image_memory_barrier.newLayout = (VkImageLayout)new_image_layout;
	image_memory_barrier.image = image;

	image_memory_barrier.subresourceRange.aspectMask = aspect_mask;
	image_memory_barrier.subresourceRange.baseMipLevel = 0;
	image_memory_barrier.subresourceRange.levelCount = 1;
	image_memory_barrier.subresourceRange.baseArrayLayer = 0;
	image_memory_barrier.subresourceRange.layerCount = 1;

	image_memory_barrier.dstAccessMask = 0;
	image_memory_barrier.srcAccessMask = 0;

	// Source layouts (old)
	switch(old_image_layout)
	{
	// Image layout is undefined (or does not matter). Only valid as initial layout. No flags required, listed only for completeness
	case VK_IMAGE_LAYOUT_UNDEFINED: image_memory_barrier.srcAccessMask = 0; break;

	// Image is preinitialized. Only valid as initial layout for linear images, preserves memory contents. Make sure host writes have been finished
	case VK_IMAGE_LAYOUT_PREINITIALIZED: image_memory_barrier.srcAccessMask = VK_ACCESS_HOST_WRITE_BIT; break;

	// Image is a color attachment. Make sure any writes to the color buffer have been finished
	case VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL: image_memory_barrier.srcAccessMask = VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT; break;

	// Image is a depth/stencil attachment. Make sure any writes to the depth/stencil buffer have been finished
	case VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL: image_memory_barrier.srcAccessMask = VK_ACCESS_DEPTH_STENCIL_ATTACHMENT_WRITE_BIT; break;

	// Image is a transfer source. Make sure any reads from the image have been finished
	case VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL: image_memory_barrier.srcAccessMask = VK_ACCESS_TRANSFER_READ_BIT; break;

	// Image is a transfer destination. Make sure any writes to the image have been finished
	case VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL: image_memory_barrier.srcAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT; break;

	// Image is read by a shader. Make sure any shader reads from the image have been finished
	case VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL: image_memory_barrier.srcAccessMask = VK_ACCESS_SHADER_READ_BIT; break;
	}

	// Target layouts (new)
	switch(new_image_layout)
	{
	// Image will be used as a transfer destination. Make sure any writes to the image have been finished
	case VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL: image_memory_barrier.dstAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT; break;

	// Image will be used as a transfer source. Make sure any reads from and writes to the image have been finished
	case VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL: {
			image_memory_barrier.srcAccessMask = image_memory_barrier.srcAccessMask | VK_ACCESS_TRANSFER_READ_BIT;
			image_memory_barrier.dstAccessMask = VK_ACCESS_TRANSFER_READ_BIT;
		} break;

	// Image will be used as a color attachment. Make sure any writes to the color buffer have been finished
	case VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL: {
			image_memory_barrier.srcAccessMask = VK_ACCESS_TRANSFER_READ_BIT;
			image_memory_barrier.dstAccessMask = VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT;
		} break;

	// Image layout will be used as a depth/stencil attachment. Make sure any writes to depth/stencil buffer have been finished
	case VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL: {
			image_memory_barrier.dstAccessMask = image_memory_barrier.dstAccessMask | VK_ACCESS_DEPTH_STENCIL_ATTACHMENT_WRITE_BIT;
		} break;

	// Image will be read in a shader (sampler, input attachment). Make sure any writes to the image have been finished
	case VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL: {
			if(image_memory_barrier.srcAccessMask == 0)
			{
				image_memory_barrier.srcAccessMask = VK_ACCESS_HOST_WRITE_BIT | VK_ACCESS_TRANSFER_WRITE_BIT;
			}
			image_memory_barrier.dstAccessMask = VK_ACCESS_SHADER_READ_BIT;
		} break;

	// Dest AccessMask 0 [None] must have required access bit 32768 [VK_ACCESS_MEMORY_READ_BIT]  when layout is VK_IMAGE_LAYOUT_PRESENT_SRC_KHR
	case VK_IMAGE_LAYOUT_PRESENT_SRC_KHR: if(image_memory_barrier.dstAccessMask == 0) image_memory_barrier.dstAccessMask = VK_ACCESS_MEMORY_READ_BIT; break;
	}

	// Put barrier on top
	VkPipelineStageFlagBits src_stage = VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT;
	VkPipelineStageFlagBits dest_stage = VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT;

	// Put barrier inside setup command buffer
	vkCmdPipelineBarrier(command_buffer, src_stage, dest_stage, 0, 0, nullptr, 0, nullptr, 1, &image_memory_barrier);
}

}

#endif
