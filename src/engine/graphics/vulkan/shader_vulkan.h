/*
   Acclution
   Copyright (c) 2016 Christoffer Lindqvist

   This software is provided 'as-is', without any express or implied
   warranty. In no event will the authors be held liable for any
   damages arising from the use of this software.

   Permission is granted to anyone to use this software for any
   purpose, including commercial applications, and to alter it and
   redistribute it freely, subject to the following restrictions:

   1. The origin of this software must not be misrepresented; you
      must not claim that you wrote the original software. If you use
      this software in a product, an acknowledgment in the product
      documentation would be appreciated but is not required.

   2. Altered source versions must be plainly marked as such, and
      must not be misrepresented as being the original software.

   3. This notice may not be removed or altered from any source
      distribution.

   Christoffer Lindqvist
   swepopper@gmail.com
*/

#ifndef SHADER_VULKAN_H
#define SHADER_VULKAN_H
#pragma once

#include "engine/graphics/shader.h"

struct VkShaderModule_T;
struct VkDevice_T;

namespace Acclution {

class ShaderData;

class ShaderVulkan : public Shader
{
public:
	// https://github.com/KhronosGroup/SPIRV-Cross
	ShaderVulkan();
	virtual ~ShaderVulkan() {}

	bool Initialize(VkDevice_T* device, ShaderData** shader_datas, uint32_t count);
	void Release();

	virtual uintptr_t GetNativeHandle() const { return (uintptr_t)m_shader_module; }

private:
	VkDevice_T*				m_device;
	uint32_t				m_count;
	VkShaderModule_T**		m_shader_module;
};

}

#endif // SHADER_H
