#ifndef VULKANSWAPCHAIN_H
#define VULKANSWAPCHAIN_H
#pragma once

#if defined(USE_VULKAN)

struct VkInstance_T;
struct VkPhysicalDevice_T;
struct VkDevice_T;
struct VkQueue_T;
struct VkSurfaceKHR_T;
struct VkSwapchainKHR_T;
struct VkImage_T;
struct VkImageView_T;
struct VkCommandBuffer_T;
struct VkSurfaceCapabilitiesKHR;
struct SDL_Window;

namespace vk
{
class SurfaceFormatKHR;
}

namespace Acclution
{

class VulkanSwapChain
{
public:
	struct SwapChainBuffers
	{
		VkImage_T*		m_image;
		VkImageView_T*	m_view;
	};

	VulkanSwapChain();
	~VulkanSwapChain();

	bool Initialize(VkInstance_T* instance, VkPhysicalDevice_T* physical_device, VkDevice_T* device, SDL_Window* window);
	void Release();

	bool Setup(VkPhysicalDevice_T* physical_device, VkCommandBuffer_T* command_buffer);

	VkSwapchainKHR_T*& GetSwapChain() { return m_swapchain; }
	const uint32_t& GetQueueNodeIndex() const { return m_queue_node_index; }
	const uint32_t& GetImageCount() const { return m_image_count; }
	const uint32_t& GetWidth() const { return m_width; }
	const uint32_t& GetHeight() const { return m_height; }

	const uint32_t& GetColorFormat() const { return m_color_format; }

	SwapChainBuffers& GetSwapChainBuffer(uint32_t index) { return m_buffers[index]; }

private:
	VkInstance_T*				m_instance;
	VkDevice_T*					m_device;
	VkSurfaceKHR_T*				m_surface;
	uint32_t					m_queue_node_index;

	VkSwapchainKHR_T*			m_swapchain;

	uint32_t					m_image_count;
	SwapChainBuffers*			m_buffers;

	uint32_t					m_color_format;
	uint32_t					m_color_space;

	uint32_t					m_width;
	uint32_t					m_height;
};

}

#endif

#endif // VULKANSWAPCHAIN_H
