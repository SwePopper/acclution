#ifndef COMMANDBUFFERVULKAN_H
#define COMMANDBUFFERVULKAN_H

#include "engine/graphics/commandbuffer.h"

namespace Acclution
{

class CommandBufferVulkan : public CommandBuffer
{
public:
	CommandBufferVulkan();
	virtual ~CommandBufferVulkan();

	///////////////////////////////////////////////////////////
	// Shader Commands
	virtual void UseShader(uint32_t index);
	///////////////////////////////////////////////////////////

	///////////////////////////////////////////////////////////
	// Vertex Buffer
	virtual void UpdateVertexBuffer(uint32_t index, const void* data, uint32_t data_size, BufferType buffer_type, uint32_t offset = 0);
	///////////////////////////////////////////////////////////

	///////////////////////////////////////////////////////////
	// Texture
	virtual void BindTexture(uint32_t index, uint8_t slot, uint32_t uniform_name_hash);
	virtual void UnBindTexture(uint8_t slot);
	///////////////////////////////////////////////////////////

	///////////////////////////////////////////////////////////
	// Uniforms
	virtual void Uniform(uint32_t uniform_name_hash, Types type, uint32_t component_count, uint32_t data_count, const void* data);
	virtual void UniformBuffer(uint32_t uniform_buffer, uint32_t binding, bool range = false, uint32_t start = 0, uint32_t end = 0);
	///////////////////////////////////////////////////////////

	///////////////////////////////////////////////////////////
	// Drawing
	virtual void Clear(float r, float g, float b, float a);
	virtual void ClearDepth();
	virtual void EnableVertexAttrib(uint32_t buffer, uint32_t index, int32_t component_count, Types type, bool normalized, int32_t stride, const void* pointer);
	virtual void DisableVertexAttrib(uint32_t attribute_index);
	virtual void DrawArrays(Modes mode, int first, int size);
	virtual void DrawElements(Modes mode, int index_count, Types type, uint32_t buffer);
	///////////////////////////////////////////////////////////

	virtual void Reset();

private:
	struct CommandBufferData*	m_command_buffer_data;
};

}

#endif // COMMANDBUFFEROPENGL_H
