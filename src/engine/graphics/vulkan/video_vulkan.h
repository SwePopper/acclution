#ifndef VIDEOVULKAN_H
#define VIDEOVULKAN_H
#pragma once

#include "engine/graphics/video.h"

#if defined(USE_VULKAN)

struct VkInstance_T;
struct VkDebugReportCallbackEXT_T;
struct VkPhysicalDevice_T;
struct VkDevice_T;
struct VkQueue_T;
struct VkCommandPool_T;
struct VkCommandBuffer_T;
struct VkImage_T;
struct VkDeviceMemory_T;
struct VkImageView_T;
struct VkRenderPass_T;
struct VkPipelineCache_T;
struct VkFramebuffer_T;
struct VkPhysicalDeviceMemoryProperties;
struct VkPhysicalDeviceProperties;
struct VkPhysicalDeviceFeatures;
struct VkShaderModule_T;
struct VkSemaphore_T;
struct VkFence_T;

namespace Acclution
{

class VulkanSwapChain;

struct CommandBufferData
{
	CommandBufferData() : m_command_buffer(nullptr), m_command_pool(nullptr) {}

	VkCommandBuffer_T*		m_command_buffer;
	VkCommandPool_T*		m_command_pool;
};

class VideoVulkan : public Video
{
public:
	VideoVulkan();
	virtual ~VideoVulkan() { Release(); }

	virtual bool Initialize();
	virtual void Release();

	virtual VideoType GetVideoType() const { return VideoType::kVulkan; }

	virtual void PlaybackRenderCommandBuffers(CommandBuffer* command_buffers, uint32_t count);

	virtual uint32_t CreateShader(ShaderData** shader_datas, uint32_t count);
	virtual void DestroyShader(uint32_t shader_index);

	virtual uint32_t CreateTexture(uint32_t width, uint32_t height, bool generate_mipmaps, uint32_t format, uint32_t internal_format, uint32_t type, const void* data);
	virtual void DestroyTexture(uint32_t index);

	virtual uint32_t CreateVertexBuffer(const void* data, uint32_t data_size, BufferType buffer_type, BufferUsage usage);
	virtual void DestroyVertexBuffer(uint32_t index);
	virtual void UpdateVertexBuffer(uint32_t index, const void* data, uint32_t data_size, BufferType buffer_type, uint32_t offset = 0);

	virtual CommandBuffer* CreateCommandBuffer(const uint32_t& size);

	CommandBufferData GetCommandBuffer();
	void ReturnCommandBuffer(const CommandBufferData& command_buffer);

	static const char* GetResultString(const int32_t& result);
	static void SetImageLayout(VkCommandBuffer_T* command_buffer, VkImage_T* image, uint32_t aspect_mask, uint32_t old_image_layout, uint32_t new_image_layout);

private:
	struct CommandBufferFencePair
	{
		CommandBufferFencePair() : m_command_buffer(nullptr), m_fence(nullptr), m_command_pool(nullptr) {}

		VkCommandBuffer_T*		m_command_buffer;
		VkFence_T*				m_fence;
		VkCommandPool_T*		m_command_pool;
	};

	typedef astd::vector<CommandBufferFencePair>		CommandBufferContainer;
	typedef astd::vector<VkFramebuffer_T*>				FrameBufferContainer;
	typedef astd::map<size_t, VkCommandPool_T*>			CommandPoolContainer;

	virtual void OnWindowResized();

	bool CreateInstance();
	bool CreateDebugReporter();
	bool CreatePhysicalDevice();
	bool CreateDevice();

	void ValidateLayers(bool instance, bool extention, astd::vector<const char*>& layers);
	VkCommandBuffer_T* SetupCommandBuffers();
	bool SetupDepthStencil(VkCommandBuffer_T* setup_command_buffer);
	bool SetupRenderPass();
	bool SetupFrameBuffer();

	VkCommandPool_T* GetCommandPool();

	VkShaderModule_T* CreateShaderModule(const char* file_path);

	uint32_t GetMemoryType(uint32_t typeBits, uint32_t properties, uint32_t& typeIndex);

	VkInstance_T*						m_instance;
	VkDebugReportCallbackEXT_T*			m_debug_report;

	VkPhysicalDevice_T*									m_physical_device;
	std::shared_ptr<VkPhysicalDeviceProperties>			m_physical_device_properties;
	std::shared_ptr<VkPhysicalDeviceFeatures>			m_physical_device_features;
	std::shared_ptr<VkPhysicalDeviceMemoryProperties>	m_physical_device_memory_props;

	VkDevice_T*							m_device;
	VkQueue_T*							m_queue;

	std::shared_ptr<VulkanSwapChain>	m_swapchain;

	CommandPoolContainer				m_command_pools;
	std::atomic_bool					m_command_pools_lock;

	CommandBufferContainer				m_draw_buffers;

	VkSemaphore_T*						m_image_available_semaphore;
	VkSemaphore_T*						m_render_finished_semaphore;

	struct
	{
		VkImage_T*			m_image;
		VkDeviceMemory_T*	m_memory;
		VkImageView_T*		m_view;
	} m_depth_stencil;

	VkRenderPass_T*			m_render_pass;
	VkPipelineCache_T*		m_pipeline_cache;

	FrameBufferContainer	m_frame_buffers;

	uint32_t				m_depth_format;
};

}

#else

namespace Acclution
{
class VideoVulkan : public Video
{
public:
	bool Initialize() { return false; }
	void Release() {}

	virtual void PlaybackRenderCommandBuffers(CommandBuffer*, uint32_t) {}

	virtual int32_t GetWindowWidth() const { return 0; }
	virtual int32_t GetWindowHeight() const { return 0; }
};
}

#endif

#endif // VIDEOVULKAN_H
