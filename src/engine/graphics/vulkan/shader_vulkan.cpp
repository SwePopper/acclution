/*
   Acclution
   Copyright (c) 2016 Christoffer Lindqvist

   This software is provided 'as-is', without any express or implied
   warranty. In no event will the authors be held liable for any
   damages arising from the use of this software.

   Permission is granted to anyone to use this software for any
   purpose, including commercial applications, and to alter it and
   redistribute it freely, subject to the following restrictions:

   1. The origin of this software must not be misrepresented; you
      must not claim that you wrote the original software. If you use
      this software in a product, an acknowledgment in the product
      documentation would be appreciated but is not required.

   2. Altered source versions must be plainly marked as such, and
      must not be misrepresented as being the original software.

   3. This notice may not be removed or altered from any source
      distribution.

   Christoffer Lindqvist
   swepopper@gmail.com
*/

#include "engine/platform.h"
#include "shader_vulkan.h"
#include "engine/utilities.h"

#include "engine/asset/shaderdata.h"

#if defined(USE_VULKAN)

#include <vulkan/vulkan.h>

namespace Acclution {

ShaderVulkan::ShaderVulkan()
	: m_device(VK_NULL_HANDLE),
	  m_count(0),
	  m_shader_module(nullptr)
{
}

bool ShaderVulkan::Initialize(VkDevice_T* device, ShaderData** shader_datas, uint32_t count)
{
	m_device = device;
	m_count = count;
	m_shader_module = new VkShaderModule_T*[m_count];
	for(uint32_t i = 0; i < m_count; ++i)
	{
		VkShaderModuleCreateInfo info = {};
		info.sType = VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO;
		info.pNext = VK_NULL_HANDLE;
		info.flags = 0;
		info.codeSize = shader_datas[i]->m_spirv_vulkan_size;
		info.pCode = (uint32_t*)shader_datas[i]->m_spirv_vulkan;

		VkResult res = vkCreateShaderModule(device, &info, nullptr, &m_shader_module[i]);
		if(VK_SUCCESS != res)
		{
			ALogError("Failed to create shader module: %i", res);
			return false;
		}
	}

	return true;
}

void ShaderVulkan::Release()
{
	for(uint32_t i = 0; i < m_count; ++i)
	{
		vkDestroyShaderModule(m_device, m_shader_module[i], nullptr);
	}

	m_device = VK_NULL_HANDLE;
	m_count = 0;
	delete[] m_shader_module;
	m_shader_module = nullptr;
}

}

#endif
