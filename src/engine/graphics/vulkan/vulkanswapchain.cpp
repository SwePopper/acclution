#include "engine/platform.h"
#include "vulkanswapchain.h"

#if defined(USE_VULKAN)

#include "video_vulkan.h"
#include "engine/engine.h"
#include "engine/config/config.h"
#include "engine/utilities.h"

#include <SDL2/SDL.h>
#include <SDL2/SDL_syswm.h>

#if SDL_VIDEO_DRIVER_WINDOWS

#if !defined(GWL_HINSTANCE)
#define GWL_HINSTANCE			(-6)
#endif

#endif

#include <vulkan/vulkan.h>

namespace Acclution
{

VulkanSwapChain::VulkanSwapChain()
	: m_instance(VK_NULL_HANDLE),
	  m_device(VK_NULL_HANDLE),
	  m_surface(VK_NULL_HANDLE),
	  m_queue_node_index(0),
	  m_swapchain(VK_NULL_HANDLE),
	  m_image_count(0),
	  m_buffers(VK_NULL_HANDLE),
	  m_color_format(0),
	  m_color_space(0),
	  m_width(1024),
	  m_height(768)
{
}

VulkanSwapChain::~VulkanSwapChain()
{
	Release();
}

bool VulkanSwapChain::Initialize(VkInstance_T* instance, VkPhysicalDevice_T* physical_device, VkDevice_T* device, SDL_Window* window)
{
	m_instance = instance;
	m_device = device;

	// Will hold our Window information
	SDL_SysWMinfo info;
	SDL_VERSION(&info.version); //Set SDL version

	if(SDL_GetWindowWMInfo(window, &info) == SDL_FALSE)
	{
		ALogError("Could not get window info! %s", SDL_GetError());
		return false;
	}

	// Create window surface based on window manager
	VkResult res = VK_ERROR_FEATURE_NOT_PRESENT;
	switch(info.subsystem)
	{
	case SDL_SYSWM_WINDOWS:
#if VK_KHR_win32_surface
		{
			intptr_t hinstance = (intptr_t)GetWindowLong(info.info.win.window, GWL_HINSTANCE);
			VkWin32SurfaceCreateInfoKHR surface_create_info = {
				VK_STRUCTURE_TYPE_WIN32_SURFACE_CREATE_INFO_KHR,
				nullptr,
				0,
				(HINSTANCE)hinstance,
				info.info.win.window
			};

			res = vkCreateWin32SurfaceKHR(m_instance, &surface_create_info, nullptr, &m_surface);
		} break;
#else
		ALogError("Could not get window handle!");
		return false;
#endif
	case SDL_SYSWM_X11:
#if VK_KHR_xlib_surface
		{
			PFN_vkCreateXlibSurfaceKHR create = (PFN_vkCreateXlibSurfaceKHR)vkGetInstanceProcAddr(m_instance, "vkCreateXlibSurfaceKHR");
			if(create == nullptr)
			{
				ALogError("vkGetInstanceProcAddr vkCreateXlibSurfaceKHR failed!");
				return false;
			}

			VkXlibSurfaceCreateInfoKHR surface_create_info = {
				VK_STRUCTURE_TYPE_XLIB_SURFACE_CREATE_INFO_KHR,
				nullptr,
				0,
				info.info.x11.display,
				info.info.x11.window
			};
			res = create(m_instance, &surface_create_info, nullptr, &m_surface);
		} break;
#else
		ALogError("Could not get window handle!");
		return false;
#endif
	case SDL_SYSWM_WAYLAND:
	case SDL_SYSWM_MIR:
	case SDL_SYSWM_ANDROID:
	case SDL_SYSWM_DIRECTFB:
	case SDL_SYSWM_COCOA:
	case SDL_SYSWM_UIKIT:
	case SDL_SYSWM_WINRT:
	case SDL_SYSWM_VIVANTE:
	case SDL_SYSWM_UNKNOWN:
	default:
		ALogError("Could not get window handle!");
		return false;
	}

	if(VK_SUCCESS != res)
	{
		ALogError("Could not create window surface! %s", VideoVulkan::GetResultString(res));
		return false;
	}

	// Get queue properies
	uint32_t queue_count = 0;
	vkGetPhysicalDeviceQueueFamilyProperties(physical_device, &queue_count, nullptr);

	astd::vector<VkQueueFamilyProperties> queue_props(queue_count);
	vkGetPhysicalDeviceQueueFamilyProperties(physical_device, &queue_count, queue_props.data());

	// Search for a graphics and a present queue in the array of queue
	// families, try to find one that supports both
	m_queue_node_index = UINT32_MAX;
	for(uint32_t i = 0; i < queue_count; ++i)
	{
		VkBool32 supports_present = false;
		res = vkGetPhysicalDeviceSurfaceSupportKHR(physical_device, i, m_surface, &supports_present);
		if(VK_SUCCESS !=  res)
		{
			ALogError("Failed to get supportinfo! %s", VideoVulkan::GetResultString(res));
			return false;
		}

		if(queue_props[i].queueFlags & VK_QUEUE_GRAPHICS_BIT && supports_present)
		{
			m_queue_node_index = i;
			break;
		}
	}

	if(m_queue_node_index == UINT32_MAX)
	{
		ALogError("Failed to find a queue that supports graphics and presenting");
		return false;
	}

	// Get supported formats
	uint32_t format_count = 0;
	res = vkGetPhysicalDeviceSurfaceFormatsKHR(physical_device, m_surface, &format_count, nullptr);
	if(VK_SUCCESS !=  res || format_count == 0)
	{
		ALogError("Failed to get supported surface formats! %s", VideoVulkan::GetResultString(res));
		return false;
	}

	astd::vector<VkSurfaceFormatKHR> surface_formats(format_count);
	res = vkGetPhysicalDeviceSurfaceFormatsKHR(physical_device, m_surface, &format_count, surface_formats.data());
	if(VK_SUCCESS !=  res || format_count == 0)
	{
		ALogError("Failed to get supported surface formats! %s", VideoVulkan::GetResultString(res));
		return false;
	}

	// If the format list includes just one entry of VK_FORMAT_UNDEFINED,
	// the surface has no preferred format.  Otherwise, at least one
	// supported format will be returned.
	m_color_format = surface_formats[0].format == VK_FORMAT_UNDEFINED ? VK_FORMAT_R8G8B8A8_UNORM : surface_formats[0].format;
	m_color_space = surface_formats[0].colorSpace;
	return true;
}

void VulkanSwapChain::Release()
{
	for(uint32_t i = 0; i < m_image_count; ++i)
	{
		// We should not destroy the VkImage only the VkImageView. The implementation destroys the VkImage.
		vkDestroyImageView(m_device, m_buffers[i].m_view, VK_NULL_HANDLE);
	}
	m_image_count = 0;

	delete[] m_buffers;
	m_buffers = VK_NULL_HANDLE;

	if(m_swapchain != VK_NULL_HANDLE)
	{
		vkDestroySwapchainKHR(m_device, m_swapchain, nullptr);
		m_swapchain = VK_NULL_HANDLE;
	}

	if(m_surface != VK_NULL_HANDLE)
	{
		vkDestroySurfaceKHR(m_instance, m_surface, nullptr);
		m_surface = VK_NULL_HANDLE;
	}

	m_device = VK_NULL_HANDLE;
	m_instance = VK_NULL_HANDLE;
}

bool VulkanSwapChain::Setup(VkPhysicalDevice_T* physical_device, VkCommandBuffer_T* command_buffer)
{
	VkSwapchainKHR old_swapchain = m_swapchain;

	VkSurfaceCapabilitiesKHR surface_caps;
	VkResult res = vkGetPhysicalDeviceSurfaceCapabilitiesKHR(physical_device, m_surface, &surface_caps);
	if(VK_SUCCESS !=  res)
	{
		ALogError("Failed to get surface capabilities! %s", VideoVulkan::GetResultString(res));
		return false;
	}

	uint32_t present_mode_count = 0;
	res = vkGetPhysicalDeviceSurfacePresentModesKHR(physical_device, m_surface, &present_mode_count, nullptr);
	if(VK_SUCCESS !=  res)
	{
		ALogError("Failed to get present modes! %s", VideoVulkan::GetResultString(res));
		return false;
	}

	VkPresentModeKHR* present_modes = (VkPresentModeKHR*)alloca(present_mode_count * sizeof(VkPresentModeKHR));
	res = vkGetPhysicalDeviceSurfacePresentModesKHR(physical_device, m_surface, &present_mode_count, present_modes);
	if(VK_SUCCESS !=  res)
	{
		ALogError("Failed to get present modes! %s", VideoVulkan::GetResultString(res));
		return false;
	}

	// Try to use mailbox mode
	// Low latency and non-tearing
	VkPresentModeKHR swapchain_present_mode = VK_PRESENT_MODE_FIFO_KHR;
	for(uint32_t i = 0; i < present_mode_count; ++i)
	{
		if(present_modes[i] == VK_PRESENT_MODE_MAILBOX_KHR)
		{
			swapchain_present_mode = VK_PRESENT_MODE_MAILBOX_KHR;
			break;
		}
		else if(present_modes[i] == VK_PRESENT_MODE_IMMEDIATE_KHR)
		{
			swapchain_present_mode = VK_PRESENT_MODE_IMMEDIATE_KHR;
		}
	}

	// Set the width and height if it is -1
	if(surface_caps.currentExtent.width == UINT32_MAX)
	{
		surface_caps.currentExtent.width = m_width;
		surface_caps.currentExtent.height = m_height;
	}

	surface_caps.currentExtent.width = astd::clamp(surface_caps.currentExtent.width, surface_caps.minImageExtent.width, surface_caps.maxImageExtent.width);
	surface_caps.currentExtent.height = astd::clamp(surface_caps.currentExtent.height, surface_caps.minImageExtent.height, surface_caps.maxImageExtent.height);

	m_width = surface_caps.currentExtent.width;
	m_height = surface_caps.currentExtent.height;

	uint32_t wanted_swapchain_image_count = surface_caps.minImageCount + 2;
	if(surface_caps.maxImageCount > 0 && wanted_swapchain_image_count > surface_caps.maxImageCount)
		wanted_swapchain_image_count = surface_caps.maxImageCount;

	VkSurfaceTransformFlagBitsKHR pre_transform = surface_caps.currentTransform;
	if(surface_caps.supportedTransforms & VK_SURFACE_TRANSFORM_IDENTITY_BIT_KHR)
		pre_transform = VK_SURFACE_TRANSFORM_IDENTITY_BIT_KHR;

	// Should not fail here... I think
	if(!(surface_caps.supportedUsageFlags & VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT))
	{
		ALogError("Unsupported image usage!");
		return false;
	}

	VkCompositeAlphaFlagBitsKHR composite_alpha =
			(surface_caps.supportedCompositeAlpha & VK_COMPOSITE_ALPHA_INHERIT_BIT_KHR) ? VK_COMPOSITE_ALPHA_INHERIT_BIT_KHR :
																						  VK_COMPOSITE_ALPHA_OPAQUE_BIT_KHR;

	if(!(surface_caps.supportedCompositeAlpha & composite_alpha))
	{
		ALogError("Unsupported composite alpha!");
		return false;
	}

	// Create the swapchain
	VkSwapchainCreateInfoKHR swapchain_create_info = {
		VK_STRUCTURE_TYPE_SWAPCHAIN_CREATE_INFO_KHR,
		nullptr,
		0,
		m_surface,
		wanted_swapchain_image_count,
		(VkFormat)m_color_format,
		(VkColorSpaceKHR)m_color_space,
		surface_caps.currentExtent,
		1,
		VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT,
		VK_SHARING_MODE_EXCLUSIVE,
		0,
		nullptr,
		pre_transform,
		composite_alpha,
		swapchain_present_mode,
		true,
		old_swapchain
	};

	res = vkCreateSwapchainKHR(m_device, &swapchain_create_info, nullptr, &m_swapchain);
	if(VK_SUCCESS !=  res)
	{
		ALogError("Failed to create swapchain! %s", VideoVulkan::GetResultString(res));
		return false;
	}

	if(old_swapchain != nullptr)
	{
		vkDestroySwapchainKHR(m_device, old_swapchain, nullptr);
	}

	m_image_count = 0;
	res = vkGetSwapchainImagesKHR(m_device, m_swapchain, &m_image_count, nullptr);
	if(VK_SUCCESS !=  res)
	{
		ALogError("Failed to get swapchain images! %s", VideoVulkan::GetResultString(res));
		return false;
	}

	VkImage* images = (VkImage*)alloca(m_image_count * sizeof(VkImage));
	res = vkGetSwapchainImagesKHR(m_device, m_swapchain, &m_image_count, images);
	if(VK_SUCCESS !=  res)
	{
		ALogError("Failed to get swapchain images! %s", VideoVulkan::GetResultString(res));
		return false;
	}

	VkImageViewCreateInfo color_attachment_view;
	color_attachment_view.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
	color_attachment_view.pNext = nullptr;
	color_attachment_view.flags = 0;
	color_attachment_view.viewType = VK_IMAGE_VIEW_TYPE_2D;
	color_attachment_view.format = (VkFormat)m_color_format;

	color_attachment_view.components.r = VK_COMPONENT_SWIZZLE_R;
	color_attachment_view.components.g = VK_COMPONENT_SWIZZLE_G;
	color_attachment_view.components.b = VK_COMPONENT_SWIZZLE_B;
	color_attachment_view.components.a = VK_COMPONENT_SWIZZLE_A;

	color_attachment_view.subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
	color_attachment_view.subresourceRange.baseMipLevel = 0;
	color_attachment_view.subresourceRange.levelCount = 1;
	color_attachment_view.subresourceRange.baseArrayLayer = 0;
	color_attachment_view.subresourceRange.layerCount = 1;

	m_buffers = new SwapChainBuffers[m_image_count];
	for(uint32_t i = 0; i < m_image_count; ++i)
	{
		color_attachment_view.image =
				m_buffers[i].m_image = images[i];

		VideoVulkan::SetImageLayout(command_buffer, m_buffers[i].m_image, VK_IMAGE_ASPECT_COLOR_BIT, VK_IMAGE_LAYOUT_UNDEFINED, VK_IMAGE_LAYOUT_PRESENT_SRC_KHR);

		res = vkCreateImageView(m_device, &color_attachment_view, nullptr, &m_buffers[i].m_view);
		if(VK_SUCCESS !=  res)
		{
			ALogError("Failed to create image view! %s", VideoVulkan::GetResultString(res));
			return false;
		}
	}

	return true;
}

}

#endif
