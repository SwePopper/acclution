/*
   Acclution
   Copyright (c) 2016 Christoffer Lindqvist

   This software is provided 'as-is', without any express or implied
   warranty. In no event will the authors be held liable for any
   damages arising from the use of this software.

   Permission is granted to anyone to use this software for any
   purpose, including commercial applications, and to alter it and
   redistribute it freely, subject to the following restrictions:

   1. The origin of this software must not be misrepresented; you
      must not claim that you wrote the original software. If you use
      this software in a product, an acknowledgment in the product
      documentation would be appreciated but is not required.

   2. Altered source versions must be plainly marked as such, and
      must not be misrepresented as being the original software.

   3. This notice may not be removed or altered from any source
      distribution.

   Christoffer Lindqvist
   swepopper@gmail.com
*/

#ifndef COMMANDBUFFER_H
#define COMMANDBUFFER_H
#pragma once

namespace Acclution
{

class ShaderData;

class CommandBuffer
{
public:
	virtual ~CommandBuffer() {}

	///////////////////////////////////////////////////////////
	// Shader Commands
	virtual void UseShader(uint32_t index)=0;
	///////////////////////////////////////////////////////////

	///////////////////////////////////////////////////////////
	// Vertex Buffer
	virtual void UpdateVertexBuffer(uint32_t index, const void* data, uint32_t data_size, BufferType buffer_type, uint32_t offset = 0)=0;
	///////////////////////////////////////////////////////////

	///////////////////////////////////////////////////////////
	// Texture
	virtual void BindTexture(uint32_t index, uint8_t slot, uint32_t uniform_name_hash)=0;
	virtual void UnBindTexture(uint8_t slot)=0;
	///////////////////////////////////////////////////////////

	///////////////////////////////////////////////////////////
	// Uniforms
	virtual void Uniform(uint32_t uniform_name_hash, Types type, uint32_t component_count, uint32_t data_count, const void* data)=0;
	virtual void UniformBuffer(uint32_t uniform_buffer, uint32_t binding, bool range = false, uint32_t start = 0, uint32_t end = 0)=0;
	///////////////////////////////////////////////////////////

	///////////////////////////////////////////////////////////
	// Drawing
	virtual void Clear(float r, float g, float b, float a)=0;
	virtual void ClearDepth()=0;
	virtual void EnableVertexAttrib(uint32_t buffer, uint32_t index, int32_t component_count, Types type, bool normalized, int32_t stride, const void* pointer)=0;
	virtual void DisableVertexAttrib(uint32_t attribute_index)=0;
	virtual void DrawArrays(Modes mode, int first, int size)=0;
	virtual void DrawElements(Modes mode, int index_count, Types type, uint32_t buffer)=0;
	///////////////////////////////////////////////////////////

	virtual void Reset() {}
};

}

#endif // COMMANDLINE_H
