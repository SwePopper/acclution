#include "engine/platform.h"
#include "video.h"

#include "engine/engine.h"
#include "engine/platform_updater/platform_updater.h"
#include "engine/platform_updater/platform_events.h"
#include "engine/config/config.h"
#include "engine/utilities.h"

#include <SDL.h>

namespace Acclution
{

std::atomic_uint Video::s_unique_id(0);

bool Video::CreateSDLWindow()
{
	const astd::string& window_width_string = Engine::GetInstance()->GetConfig()->GetValue(Utilities::Hash32("Graphics"), Utilities::Hash32("window_width"));
	const astd::string& window_height_string = Engine::GetInstance()->GetConfig()->GetValue(Utilities::Hash32("Graphics"), Utilities::Hash32("window_height"));
	const astd::string& window_fullscreen_string = Engine::GetInstance()->GetConfig()->GetValue(Utilities::Hash32("Graphics"), Utilities::Hash32("window_fullscreen"));
	const astd::string& window_screen_string = Engine::GetInstance()->GetConfig()->GetValue(Utilities::Hash32("Graphics"), Utilities::Hash32("window_screen"));
	astd::string window_title_string = Engine::GetInstance()->GetConfig()->GetValue(Utilities::Hash32("PublishingInfo"), Utilities::Hash32("game_name"));

	uint32_t width = static_cast<uint32_t>(strtoul(window_width_string.c_str(), nullptr, 10));
	if (width != 0)
		m_width = width;

	uint32_t height = static_cast<uint32_t>(strtoul(window_height_string.c_str(), nullptr, 10));
	if (height != 0)
		m_height = height;

	bool fullscreen = strtol(window_fullscreen_string.c_str(), nullptr, 10) != 0;
	int window_screen = static_cast<int>(strtol(window_screen_string.c_str(), nullptr, 10));

	if(fullscreen)
	{
		SDL_Rect screen_rect;
		if(SDL_GetDisplayBounds(window_screen, &screen_rect) == 0)
		{
			m_width = static_cast<uint32_t>(screen_rect.w);
			m_height = static_cast<uint32_t>(screen_rect.h);
		}
	}

	if (window_title_string.empty())
		window_title_string = "Acclution";

	uint32_t window_flags = SDL_WINDOW_OPENGL | SDL_WINDOW_SHOWN | SDL_WINDOW_ALLOW_HIGHDPI;
	if (fullscreen)
		window_flags |= SDL_WINDOW_FULLSCREEN;
	else
		window_flags |= SDL_WINDOW_RESIZABLE;

	m_window = SDL_CreateWindow(window_title_string.c_str(),
		static_cast<int>(SDL_WINDOWPOS_CENTERED_DISPLAY(window_screen)), static_cast<int>(SDL_WINDOWPOS_CENTERED_DISPLAY(window_screen)),
		static_cast<int>(m_width), static_cast<int>(m_height), window_flags);

	Engine::GetInstance()->GetPlatformUpdater()->AddEventCallback(&Video::OnSystemEvent, this);

	return m_window != NULL;
}

void Video::DestroySDLWindow()
{
	if(m_window != nullptr)
	{
		SDL_DestroyWindow(m_window);
		m_window = nullptr;
	}
	SDL_QuitSubSystem(SDL_INIT_VIDEO);
}

void Video::OnSystemEvent(const PlatformEvent* event, void* user_param)
{
	if(user_param != nullptr)
	{
		Video* video = reinterpret_cast<Video*>(user_param);
		if(event->GetEventType() == PlatformEventType::kWindowResize)
		{
			const PlatformEvents::WindowResize* resize = dynamic_cast<const PlatformEvents::WindowResize*>(event);

			if(video->m_width != resize->GetWidth() || video->m_height != resize->GetHeight())
			{
				video->m_width = resize->GetWidth();
				video->m_height = resize->GetHeight();

				video->OnWindowResized();
			}
		}
	}
}

}
