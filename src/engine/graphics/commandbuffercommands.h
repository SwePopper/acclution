/*
   Acclution
   Copyright (c) 2016 Christoffer Lindqvist

   This software is provided 'as-is', without any express or implied
   warranty. In no event will the authors be held liable for any
   damages arising from the use of this software.

   Permission is granted to anyone to use this software for any
   purpose, including commercial applications, and to alter it and
   redistribute it freely, subject to the following restrictions:

   1. The origin of this software must not be misrepresented; you
      must not claim that you wrote the original software. If you use
      this software in a product, an acknowledgment in the product
      documentation would be appreciated but is not required.

   2. Altered source versions must be plainly marked as such, and
      must not be misrepresented as being the original software.

   3. This notice may not be removed or altered from any source
      distribution.

   Christoffer Lindqvist
   swepopper@gmail.com
*/

#ifndef COMMANDBUFFERCOMMANDS_H
#define COMMANDBUFFERCOMMANDS_H

#include "engine/defines.h"

namespace Acclution
{
namespace CommandBufferCommands
{

enum class Command : uint8_t
{
	// Shader Commands
	kCreateShader,
	kDestroyShader,
	kUseShader,

	// Vertex Buffer Commands
	kCreateVertexBuffer,
	kDestroyVertexBuffer,
	kUpdateVertexBuffer,

	// Texture Commands
	kCreateTexture,
	kDestroyTexture,
	kBindTexture,
	kUnBindTexture,

	// Uniforms
	kUniform,
	kUniformBuffer,

	// Drawing
	kClear,
	kClearDepth,
	kEnableVertexAttrib,
	kDisableVertexAttrib,
	kDrawArrays,
	kDrawElements,

	kCount
};

// TODO: Align and pad the structs if needed.

// Used for commands that only needs to send an index like UseShader.
struct GenericIndexCommand { Command command; uint32_t unique_id; };

struct CreateShaderCommand { Command command; uint32_t unique_id; const char* vertex_shader_code; const char* fragment_shader_code; }; // TODO: add geom, tesselation and compute?
struct CreateVertexBufferCommand { Command command; uint32_t unique_id; const void* data; uint32_t data_size; uint8_t buffer_type; uint8_t usage; };
struct CreateTextureCommand { Command command; uint32_t unique_id; uint32_t width, height; bool generate_mipmaps; uint32_t format; uint32_t internal_format; uint32_t type; const void* data; };

struct UpdateVertexBufferCommand { Command command; uint32_t buffer; const void* data; uint32_t data_size; uint8_t buffer_type; uint32_t offset; };

struct ClearCommand { Command command; float r,g,b,a; };
struct EnableVertexAttribCommand { Command command; uint32_t buffer; uint32_t index; int32_t size; Types type; bool normalized; int32_t stride; const void* pointer; };
struct DrawArraysCommand { Command command; Modes mode; int first; int size; };
struct DrawElementsCommand { Command command; Modes mode; int count; Types type; uint32_t buffer; };
struct UniformCommand { Command command; uint32_t uniform_name_hash; Types type; uint32_t component_count; uint32_t data_count; };
struct UniformBufferCommand { Command command; uint32_t uniform_buffer; uint32_t binding; bool range; uint32_t start; uint32_t end; };
struct BindTextureCommand { Command command; uint32_t unique_id; uint8_t slot; uint32_t uniform_name_hash; };

}
}

#endif // COMMANDLINECOMMANDS_H
