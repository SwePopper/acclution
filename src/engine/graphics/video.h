#ifndef VIDEO_H
#define VIDEO_H
#pragma once

struct SDL_Window;

namespace Acclution
{

class CommandBuffer;
class PlatformEvent;
class ShaderData;
class Shader;

class Video
{
public:
	enum class VideoType : uint8_t
	{
		kOpenGL,
		kVulkan
	};

	Video() : m_window(nullptr), m_width(1024), m_height(768) {}
	virtual ~Video() {}

	virtual bool Initialize()=0;
	virtual void Release()=0;
	virtual VideoType GetVideoType()const=0;

	virtual void PlaybackRenderCommandBuffers(CommandBuffer* command_buffers, uint32_t count)=0;

	SDL_Window* GetWindow() const { return m_window; }
	const uint32_t& GetWindowWidth() const { return m_width; }
	const uint32_t& GetWindowHeight() const { return m_height; }

	virtual uint32_t CreateShader(ShaderData** shader_datas, uint32_t count)=0;
	virtual void DestroyShader(uint32_t shader_index)=0;

	virtual uint32_t CreateTexture(uint32_t width, uint32_t height, bool generate_mipmaps, uint32_t format, uint32_t internal_format, uint32_t type, const void* data)=0;
	virtual void DestroyTexture(uint32_t index)=0;

	virtual uint32_t CreateVertexBuffer(const void* data, uint32_t data_size, BufferType buffer_type, BufferUsage usage)=0;
	virtual void DestroyVertexBuffer(uint32_t index)=0;
	virtual void UpdateVertexBuffer(uint32_t index, const void* data, uint32_t data_size, BufferType buffer_type, uint32_t offset = 0)=0;

	virtual CommandBuffer* CreateCommandBuffer(const uint32_t& size)=0;

protected:
	typedef astd::map<uint32_t, Shader*> ShaderContainer;

	bool CreateSDLWindow();
	void DestroySDLWindow();

	static void OnSystemEvent(const PlatformEvent* event, void* user_param);

	virtual void OnWindowResized() {}

	// UniqueID for the containers
	static std::atomic_uint	s_unique_id;
	static uint32_t GetNextUniqueID() { return s_unique_id.fetch_add(1); }

	SDL_Window*			m_window;
	uint32_t			m_width;
	uint32_t			m_height;

	ShaderContainer		m_shaders;

	friend class CommandBufferOpenGL;
};

}

#endif
