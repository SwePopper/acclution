/*
   Acclution
   Copyright (c) 2016 Christoffer Lindqvist

   This software is provided 'as-is', without any express or implied
   warranty. In no event will the authors be held liable for any
   damages arising from the use of this software.

   Permission is granted to anyone to use this software for any
   purpose, including commercial applications, and to alter it and
   redistribute it freely, subject to the following restrictions:

   1. The origin of this software must not be misrepresented; you
      must not claim that you wrote the original software. If you use
      this software in a product, an acknowledgment in the product
      documentation would be appreciated but is not required.

   2. Altered source versions must be plainly marked as such, and
      must not be misrepresented as being the original software.

   3. This notice may not be removed or altered from any source
      distribution.

   Christoffer Lindqvist
   swepopper@gmail.com
*/

#include "engine/platform.h"
#include "shader_opengl.h"
#include "engine/utilities.h"

#define GL3_PROTOTYPES 1
#define GL_GLEXT_PROTOTYPES
#include <GL/glew.h>
#include <SDL_opengl.h>

namespace Acclution {

ShaderOpenGL::ShaderOpenGL()
	: m_program(0),
	  m_attributes(nullptr),
	  m_attribute_count(0),
	  m_uniforms(nullptr),
	  m_uniform_count(0),
	  m_uniform_blocks(nullptr),
	  m_uniform_block_count(0)
{
}

bool ShaderOpenGL::Initialize(const char* vertex_shader_code, const char* fragment_shader_code)
{
	static GLenum shader_type[] = { GL_VERTEX_SHADER, GL_FRAGMENT_SHADER };
	const char* sources[] = { vertex_shader_code, fragment_shader_code };
	GLuint shaders[] = {0,0};
	
	bool failed = false;
	for(uint32_t i = 0; i < sizeof(shader_type) / sizeof(shader_type[0]); ++i)
	{
		if(sources[i] != nullptr)
		{
			shaders[i] = glCreateShader(shader_type[i]);
			CheckGLError();

			if(shaders[i] == 0)
			{
				failed = true;
				break;
			}

			glShaderSource(shaders[i], 1, &sources[i], nullptr);
			CheckGLError();

			glCompileShader(shaders[i]);
			CheckGLError();

			GLint res = GL_FALSE;
			glGetShaderiv(shaders[i], GL_COMPILE_STATUS, &res);
			CheckGLError();
			if(res == GL_FALSE)
			{
				int log_length = 0;
				glGetShaderiv(shaders[i], GL_INFO_LOG_LENGTH, &log_length);
				char* log = (char*)alloca(log_length);
				glGetShaderInfoLog(shaders[i], log_length, nullptr, log);
				ALogError("OpenGL Error(%s)    line: %i\n", log, __LINE__);
				failed = true;
				break;
			}
		}
		else
		{
			shaders[i] = 0;
		}
	}

	if(!failed)
	{
		GLuint program = glCreateProgram();
		CheckGLError();
		if(program != 0)
		{
			for(uint32_t i = 0; i < sizeof(shader_type) / sizeof(shader_type[0]); ++i)
			{
				if(shaders[i] != 0)
				{
					glAttachShader(program, shaders[i]);
					CheckGLError();
				}
			}

			glLinkProgram(program);
			CheckGLError();

			int res = GL_FALSE;
			glGetProgramiv(program, GL_LINK_STATUS, &res);
			CheckGLError();
			if(res == GL_FALSE)
			{
				int log_length = 0;
				glGetProgramiv(program, GL_INFO_LOG_LENGTH, &log_length);
				char* log = (char*)alloca(log_length);
				glGetProgramInfoLog(program, log_length, nullptr, log);
				ALogError("OpenGL Error(%s)    line: %i\n", log, __LINE__);
				glDeleteProgram(program);
			}
			else
			{
				m_program = program;
			}
		}
	}

	for(uint32_t i = 0; i < sizeof(shader_type) / sizeof(shader_type[0]); ++i)
	{
		if(shaders[i] != 0)
		{
			glDetachShader(m_program, shaders[i]);
			CheckGLError();
			glDeleteShader(shaders[i]);
			CheckGLError();
		}
	}

	int max_size = 0;
	glGetProgramiv(m_program, GL_ACTIVE_ATTRIBUTE_MAX_LENGTH, &max_size);
	CheckGLError();

	m_attribute_count = 0;
	glGetProgramiv(m_program, GL_ACTIVE_ATTRIBUTES, &m_attribute_count);
	CheckGLError();

	m_attributes = new VariableInfo[m_attribute_count];
	for(int i = 0; i < m_attribute_count; ++i)
	{
		int size = max_size+1;
		char* name = (char*)alloca(size);

		int attrib_size = 0;
		GLenum attrib_type;
		glGetActiveAttrib(m_program, i, size, &size, &attrib_size, &attrib_type, name);
		CheckGLError();
		if(size <= 0)
			continue;

		m_attributes[i].name = new char[size+1];
		strncpy(m_attributes[i].name, name, static_cast<size_t>(size));
		m_attributes[i].name[size] = 0;
		m_attributes[i].name_hash = Utilities::Hash32(m_attributes[i].name);
		m_attributes[i].data_size = attrib_size;
		m_attributes[i].type = attrib_type;

		m_attributes[i].location = glGetAttribLocation(m_program, name);
		CheckGLError();
		if(m_attributes[i].location < 0)
		{
			ALogWarning("Shader attribute not used: %s\n", name);
		}
	}

	max_size = 0;
	glGetProgramiv(m_program, GL_ACTIVE_UNIFORM_MAX_LENGTH, &max_size);
	CheckGLError();

	m_uniform_count = 0;
	glGetProgramiv(m_program, GL_ACTIVE_UNIFORMS, &m_uniform_count);
	CheckGLError();

	m_uniforms = new VariableInfo[m_uniform_count];
	for(int i = 0; i < m_uniform_count; ++i)
	{
		int size = max_size+1;
		char* name = (char*)alloca(size);
		int attrib_size = 0;
		GLenum attrib_type;
		glGetActiveUniform(m_program, i, size, &size, &attrib_size, &attrib_type, name);
		CheckGLError();
		if(size <= 0)
			break;

		m_uniforms[i].name = new char[size+1];
		strncpy(m_uniforms[i].name, name, static_cast<size_t>(size));
		m_uniforms[i].name[size] = 0;
		m_uniforms[i].data_size = attrib_size;
		m_uniforms[i].type = attrib_type;
		m_uniforms[i].name_hash = Utilities::Hash32(m_uniforms[i].name);
		m_uniforms[i].location = glGetUniformLocation(m_program, m_uniforms[i].name);
		CheckGLError();
		if(m_uniforms[i].location < 0)
		{
			m_uniforms[i].location = static_cast<int>(__glewGetUniformBlockIndex(m_program, m_uniforms[i].name));

			ALogWarning("Shader uniform not used: %s", name);
		}
	}

	m_uniform_block_count = 0;
	glGetProgramiv(m_program, GL_ACTIVE_UNIFORM_BLOCKS, &m_uniform_block_count);
	CheckGLError();

	m_uniform_blocks = new UniformBlock[m_uniform_block_count];
	for(int i = 0; i < m_uniform_block_count; ++i)
	{
		int size = max_size+1;
		char* name = (char*)alloca(size);
		glGetActiveUniformBlockName(m_program, i, size, &size, name);
		CheckGLError();
		if(size <= 0)
			break;

		m_uniform_blocks[i].name = new char[size+1];
		strncpy(m_uniform_blocks[i].name, name, static_cast<size_t>(size));
		m_uniform_blocks[i].name[size] = 0;
		m_uniform_blocks[i].name_hash = Utilities::Hash32(m_uniform_blocks[i].name);

		m_uniform_blocks[i].index = glGetUniformBlockIndex(m_program, name);

		glGetActiveUniformBlockiv(m_program, m_uniform_blocks[i].index, GL_UNIFORM_BLOCK_BINDING, &m_uniform_blocks[i].binding);
		CheckGLError();
		if(m_uniform_blocks[i].binding == 0)
		{
			m_uniform_blocks[i].binding = i;
			glUniformBlockBinding(m_program, m_uniform_blocks[i].index, m_uniform_blocks[i].binding);
			CheckGLError();
		}

		glGetActiveUniformBlockiv(m_program, m_uniform_blocks[i].index, GL_UNIFORM_BLOCK_DATA_SIZE, &m_uniform_blocks[i].data_size);
		CheckGLError();

		GLint ivar = 0;
		glGetActiveUniformBlockiv(m_program, m_uniform_blocks[i].index, GL_UNIFORM_BLOCK_ACTIVE_UNIFORMS, &ivar);
		CheckGLError();

		m_uniform_blocks[i].vars_count = ivar < 0 ? 0 : static_cast<uint32_t>(ivar);

		m_uniform_blocks[i].vars = nullptr;
		if(m_uniform_blocks[i].vars_count > 0)
		{
			m_uniform_blocks[i].vars = new UniformBlockVar[m_uniform_blocks[i].vars_count];
			GLint* indices = (GLint*)alloca(sizeof(GLint) * m_uniform_blocks[i].vars_count);

			glGetActiveUniformBlockiv(m_program, m_uniform_blocks[i].index, GL_UNIFORM_BLOCK_ACTIVE_UNIFORM_INDICES, indices);
			CheckGLError();

			GLint* types = (GLint*)alloca(sizeof(GLint) * m_uniform_blocks[i].vars_count);
			glGetActiveUniformsiv(m_program, m_uniform_blocks[i].vars_count, (GLuint*)indices, GL_UNIFORM_TYPE, types);
			CheckGLError();

			GLint* offsets = (GLint*)alloca(sizeof(GLint) * m_uniform_blocks[i].vars_count);
			glGetActiveUniformsiv(m_program, m_uniform_blocks[i].vars_count, (GLuint*)indices, GL_UNIFORM_OFFSET, offsets);
			CheckGLError();

			GLint* sizes = (GLint*)alloca(sizeof(GLint) * m_uniform_blocks[i].vars_count);
			glGetActiveUniformsiv(m_program, m_uniform_blocks[i].vars_count, (GLuint*)indices, GL_UNIFORM_SIZE, sizes);
			CheckGLError();

			for(uint32_t j = 0; j < m_uniform_blocks[i].vars_count; ++j)
			{
				m_uniform_blocks[i].vars[j].index = indices[j];
				m_uniform_blocks[i].vars[j].type = types[j];
				m_uniform_blocks[i].vars[j].offset = offsets[j];
				m_uniform_blocks[i].vars[j].count = sizes[j];
			}
		}
	}

	return true;
}

void ShaderOpenGL::Release()
{
	glDeleteProgram(m_program);
	m_program = 0;

	for(int i = 0; i < m_attribute_count; ++i)
	{
		delete[] m_attributes[i].name;
	}
	delete[] m_attributes;
	m_attributes = nullptr;
	m_attribute_count = 0;

	for(int i = 0; i < m_uniform_count; ++i)
	{
		delete[] m_uniforms[i].name;
	}
	delete[] m_uniforms;
	m_uniforms = nullptr;
	m_uniform_count = 0;

	for(int i = 0; i < m_uniform_block_count; ++i)
	{
		delete[] m_uniform_blocks[i].name;
		delete[] m_uniform_blocks[i].vars;
	}
	delete[] m_uniform_blocks;
	m_uniform_blocks = nullptr;
	m_uniform_block_count = 0;
}

}
