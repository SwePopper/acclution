#ifndef COMMANDBUFFEROPENGL_H
#define COMMANDBUFFEROPENGL_H

#include "engine/graphics/commandbuffer.h"

namespace Acclution
{

class CommandBufferOpenGL : public CommandBuffer
{
public:
	CommandBufferOpenGL(const uint32_t& size);
	virtual ~CommandBufferOpenGL();

	///////////////////////////////////////////////////////////
	// Shader Commands
	virtual void UseShader(uint32_t index);
	///////////////////////////////////////////////////////////

	///////////////////////////////////////////////////////////
	// Vertex Buffer
	virtual void UpdateVertexBuffer(uint32_t index, const void* data, uint32_t data_size, BufferType buffer_type, uint32_t offset = 0);
	///////////////////////////////////////////////////////////

	///////////////////////////////////////////////////////////
	// Texture
	virtual void BindTexture(uint32_t index, uint8_t slot, uint32_t uniform_name_hash);
	virtual void UnBindTexture(uint8_t slot);
	///////////////////////////////////////////////////////////

	///////////////////////////////////////////////////////////
	// Uniforms
	virtual void Uniform(uint32_t uniform_name_hash, Types type, uint32_t component_count, uint32_t data_count, const void* data);
	virtual void UniformBuffer(uint32_t uniform_buffer, uint32_t binding, bool range = false, uint32_t start = 0, uint32_t end = 0);
	///////////////////////////////////////////////////////////

	///////////////////////////////////////////////////////////
	// Drawing
	virtual void Clear(float r, float g, float b, float a);
	virtual void ClearDepth();
	virtual void EnableVertexAttrib(uint32_t buffer, uint32_t index, int32_t component_count, Types type, bool normalized, int32_t stride, const void* pointer);
	virtual void DisableVertexAttrib(uint32_t attribute_index);
	virtual void DrawArrays(Modes mode, int first, int size);
	virtual void DrawElements(Modes mode, int index_count, Types type, uint32_t buffer);
	///////////////////////////////////////////////////////////

	///////////////////////////////////////////////////////////
	// Misc
	void AddCommandLine(const CommandBufferOpenGL& src);
	///////////////////////////////////////////////////////////

	const uint8_t* GetData() const { return m_command_line_start; }
	uint32_t GetDataSize() const { return (uint32_t)(m_command_line.load() - m_command_line_start); }

	virtual void Reset() { m_command_line = m_command_line_start; }

private:
	template<typename T>
	void GenericCommand(T* obj)
	{
		ASSERT_ON_NON_TRIVIAL(T);
		uint8_t* copy_to = m_command_line.fetch_add(sizeof(T));
		memcpy(copy_to, obj, sizeof(T));
	}

	template<typename T>
	void GenericCommand(T* obj, const void* extra_data, uint32_t extra_data_size)
	{
		ASSERT_ON_NON_TRIVIAL(T);
		uint8_t* copy_to = m_command_line.fetch_add(sizeof(T)+extra_data_size);
		memcpy(copy_to, obj, sizeof(T));
		copy_to += sizeof(T);
		memcpy(copy_to, extra_data, extra_data_size);
	}

	uint8_t* m_command_line_start;
	uint32_t m_command_line_size;
	std::atomic<uint8_t*>	m_command_line;

	friend class VideoOpenGL;
};

}

#endif // COMMANDBUFFEROPENGL_H
