﻿/*
   Acclution
   Copyright (c) 2016 Christoffer Lindqvist

   This software is provided 'as-is', without any express or implied
   warranty. In no event will the authors be held liable for any
   damages arising from the use of this software.

   Permission is granted to anyone to use this software for any
   purpose, including commercial applications, and to alter it and
   redistribute it freely, subject to the following restrictions:

   1. The origin of this software must not be misrepresented; you
      must not claim that you wrote the original software. If you use
      this software in a product, an acknowledgment in the product
      documentation would be appreciated but is not required.

   2. Altered source versions must be plainly marked as such, and
      must not be misrepresented as being the original software.

   3. This notice may not be removed or altered from any source
      distribution.

   Christoffer Lindqvist
   swepopper@gmail.com
*/

#include "engine/platform.h"
#include "video_opengl.h"
#include "engine/graphics/commandbuffer.h"
#include "engine/graphics/commandbuffercommands.h"
#include "engine/engine.h"
#include "engine/config/config.h"
#include "engine/utilities.h"

#define GL3_PROTOTYPES 1
#define GL_GLEXT_PROTOTYPES
#include <GL/glew.h>
#include <SDL_opengl.h>

#include "engine/asset/shaderdata.h"
#include "shader_opengl.h"
#include "commandbuffer_opengl.h"

#define SPINLOCK_LOCK(lock) do {} while(m_object_creator_cmd_lock.exchange(true))
#define SPINLOCK_UNLOCK(lock) m_object_creator_cmd_lock.store(false)

namespace Acclution {

using namespace CommandBufferCommands;

#define GLFunction(exp)			\
	exp;						\
	CheckGLError()

VideoOpenGL::VideoOpenGL()
	: m_context(nullptr),
	  m_command_functions(nullptr),
	  m_object_creator_cmd(nullptr),
	  m_object_creator_cmd_lock(false),
	  m_vertex_array_object(0),
	  m_max_texture_units(80), // atleast 80 acording to opengl
	  m_bound_textures(nullptr),
	  m_current_shader(nullptr)
{
}

bool VideoOpenGL::Initialize()
{
	if(!CreateSDLWindow())
	{
		ALogFatal("Failed to create window");
		return false;
	}

	SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);
	SDL_GL_SetAttribute(SDL_GL_DEPTH_SIZE, 24);
	SDL_GL_SetAttribute(SDL_GL_STENCIL_SIZE, 8);
	SDL_GL_SetAttribute(SDL_GL_RED_SIZE, 5);
	SDL_GL_SetAttribute(SDL_GL_GREEN_SIZE, 5);
	SDL_GL_SetAttribute(SDL_GL_BLUE_SIZE, 5);
	SDL_GL_SetAttribute(SDL_GL_DEPTH_SIZE, 16);
	SDL_GL_SetAttribute(SDL_GL_MULTISAMPLEBUFFERS, 1);
	SDL_GL_SetAttribute(SDL_GL_MULTISAMPLESAMPLES, 4);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);

	// OpenGL versions to test
	int major_core_versions[] = { 4, 4, 4, 4, 4, 4, 3, 3 };
	int minor_core_versions[] = { 5, 4, 3, 2, 1, 0, 3, 0 };
	for(uint32_t i = 0; i < ASTD_ARRAY_COUNT(major_core_versions); ++i)
	{
		SDL_GetError();
		SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, major_core_versions[i]);
		SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, minor_core_versions[i]);

		m_context = SDL_GL_CreateContext(m_window);
		if(m_context != NULL)
			break;
	}

	if(m_context == NULL)
	{
		SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_ES);
		int major_es_versions[] = { 3, 2 };
		int minor_es_versions[] = { 0, 0 };

		for (uint32_t i = 0; i < ASTD_ARRAY_COUNT(major_es_versions); ++i)
		{
			SDL_GetError();
			SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, major_es_versions[i]);
			SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, minor_es_versions[i]);

			m_context = SDL_GL_CreateContext(m_window);
			if (m_context != NULL)
				break;
		}

		if (m_context == NULL)
		{
			ALogError("Failed to create sdl context: %s", SDL_GetError());
			return false;
		}
	}

	ALogInfo("Created OpenGL %s - %s - %s", glGetString(GL_VERSION), glGetString(GL_VENDOR), glGetString(GL_RENDERER));

	if(SDL_GL_MakeCurrent(m_window, m_context) != 0)
	{
		ALogError("Make Current Error: %s", SDL_GetError());
		return false;
	}

	glewExperimental = GL_TRUE;
	if(GLEW_OK != glewInit())
	{
		ALogError("Failed to initialize GLEW");
		return false;
	}

	glGetError();
	CheckGLError();

	GLFunction(glGenVertexArrays(1, &m_vertex_array_object));
	GLFunction(glBindVertexArray(m_vertex_array_object));

	// try late-swap-tearing first. If not supported, try normal vsync.
	if(SDL_GL_SetSwapInterval(-1) == -1)
	{
		CheckGLError();
		if(SDL_GL_SetSwapInterval(1) == -1)
		{
			CheckGLError();
			SDL_GL_SetSwapInterval(0);
		}
	}

	CheckGLError();

	GLFunction(glEnable(GL_DEPTH_TEST));
	GLFunction(glDepthFunc(GL_LESS));
	//GLFunction(glDisable(GL_CULL_FACE));
	GLFunction(glFrontFace(GL_CW));

	GLFunction(glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA));
	GLFunction(glEnable(GL_BLEND));

	GLFunction(glClearColor(0.0f, 0.0f, 0.0f, 0.0f));

	GLFunction(glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT));
	GLFunction(SDL_GL_SwapWindow(m_window));

	GLFunction(glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT));
	GLFunction(SDL_GL_SwapWindow(m_window));

	GLint max_texture_units = 0;
	GLFunction(glGetIntegerv(GL_MAX_TEXTURE_IMAGE_UNITS, &max_texture_units));
	if(max_texture_units > 0)
		m_max_texture_units = (uint16_t)max_texture_units;

	m_bound_textures = new uint32_t[m_max_texture_units];
	for(uint16_t i = 0; i < m_max_texture_units; ++i)
	{
		m_next_free_texture_unit.push(i);
		m_bound_textures[i] = UINT32_MAX;
	}

	m_command_functions = new CommandFunction[(uint8_t)Command::kCount];
	memset(m_command_functions, 0, sizeof(CommandFunction) * (uint8_t)Command::kCount);

	// Shader Commands
	m_command_functions[(uint8_t)Command::kCreateShader] = &VideoOpenGL::CreateShaderProgram;
	m_command_functions[(uint8_t)Command::kDestroyShader] = &VideoOpenGL::DestroyShaderProgram;
	m_command_functions[(uint8_t)Command::kUseShader] = &VideoOpenGL::UseShaderProgram;

	// Vertex Buffer Commands
	m_command_functions[(uint8_t)Command::kCreateVertexBuffer] = &VideoOpenGL::CreateVertexBuffer;
	m_command_functions[(uint8_t)Command::kDestroyVertexBuffer] = &VideoOpenGL::DestroyVertexBuffer;
	m_command_functions[(uint8_t)Command::kUpdateVertexBuffer] = &VideoOpenGL::UpdateVertexBuffer;

	// Texture Commands
	m_command_functions[(uint8_t)Command::kCreateTexture] = &VideoOpenGL::CreateTexture;
	m_command_functions[(uint8_t)Command::kDestroyTexture] = &VideoOpenGL::DestroyTexture;
	m_command_functions[(uint8_t)Command::kBindTexture] = &VideoOpenGL::BindTexture;
	m_command_functions[(uint8_t)Command::kUnBindTexture] = &VideoOpenGL::UnBindTexture;

	// Uniforms
	m_command_functions[(uint8_t)Command::kUniform] = &VideoOpenGL::Uniform;
	m_command_functions[(uint8_t)Command::kUniformBuffer] = &VideoOpenGL::UniformBuffer;

	// Drawing
	m_command_functions[(uint8_t)Command::kClear] = &VideoOpenGL::Clear;
	m_command_functions[(uint8_t)Command::kClearDepth] = &VideoOpenGL::ClearDepth;
	m_command_functions[(uint8_t)Command::kEnableVertexAttrib] = &VideoOpenGL::EnableVertexAttrib;
	m_command_functions[(uint8_t)Command::kDisableVertexAttrib] = &VideoOpenGL::DisableVertexAttrib;
	m_command_functions[(uint8_t)Command::kDrawArrays] = &VideoOpenGL::DrawArrays;
	m_command_functions[(uint8_t)Command::kDrawElements] = &VideoOpenGL::DrawElements;

	m_object_creator_cmd = new CommandBufferOpenGL(128 * 1024);
	m_object_creator_cmd_lock = false;

	ALogInfo("VideoOpenGL initialized");
	return true;
}

void VideoOpenGL::Release()
{
	for(ShaderContainer::iterator it = m_shaders.begin(); it != m_shaders.end(); ++it)
	{
		if((*it).second != nullptr)
		{
			ShaderOpenGL* shader = (ShaderOpenGL*)(*it).second;
			shader->Release();
			delete shader;
		}
	}
	m_shaders.clear();

	for(ObjectContainer::iterator it = m_vertex_buffers.begin(); it != m_vertex_buffers.end(); ++it)
	{
		glDeleteBuffers(1, &it->second);
	}
	m_vertex_buffers.clear();

	for(ObjectContainer::iterator it = m_textures.begin(); it != m_textures.end(); ++it)
	{
		glDeleteTextures(1, &(*it).second);
	}
	m_textures.clear();

	while(!m_next_free_texture_unit.empty())
		m_next_free_texture_unit.pop();

	delete[] m_bound_textures;
	m_bound_textures = nullptr;

	if(m_vertex_array_object != 0)
		glDeleteVertexArrays(1, &m_vertex_array_object);

	delete[] m_command_functions;
	m_command_functions = nullptr;

	if(m_context != nullptr)
	{
		SDL_GL_DeleteContext(m_context);
		m_context = nullptr;
	}

	DestroySDLWindow();

	s_unique_id.store(0);
}

void VideoOpenGL::RunCommands(const uint8_t* start, const uint8_t* end)
{
	while(start < end)
	{
		start = (this->*m_command_functions[*start])(start);
	}
}

void VideoOpenGL::PlaybackRenderCommandBuffers(CommandBuffer* command_buffers, uint32_t count)
{
	SPINLOCK_LOCK(m_object_creator_cmd_lock);
	{
		const uint8_t* start = m_object_creator_cmd->GetData();
		RunCommands(start, start + m_object_creator_cmd->GetDataSize());
		m_object_creator_cmd->Reset();
	}
	SPINLOCK_UNLOCK(m_object_creator_cmd_lock);

	for(uint32_t i = 0; i < count; ++i)
	{
		CommandBufferOpenGL* commands = dynamic_cast<CommandBufferOpenGL*>(&command_buffers[i]);
		if(commands != nullptr)
		{
			const uint8_t* start = commands->GetData();
			RunCommands(start, start + commands->GetDataSize());
		}
	}

	Flush();
	Swap();
}

uint32_t VideoOpenGL::CreateShader(ShaderData** shader_datas, uint32_t count)
{
	CreateShaderCommand com;
	com.command = Command::kCreateShader;
	com.unique_id = GetNextUniqueID();
	com.vertex_shader_code = com.fragment_shader_code = nullptr;

	for(uint32_t i = 0; i < count; ++i)
	{
		switch(shader_datas[i]->m_type)
		{
		case ShaderData::ShaderType::kVertex: {
				if(com.vertex_shader_code != nullptr)
				{
					ALogError("Multiple vertex shaders specified!");
					return UINT32_MAX;
				}
				com.vertex_shader_code = shader_datas[i]->m_glsl_shader.c_str();
			} break;

		case ShaderData::ShaderType::kFragment: {
				if(com.fragment_shader_code != nullptr)
				{
					ALogError("Multiple fragment shaders specified!");
					return UINT32_MAX;
				}
				com.fragment_shader_code = shader_datas[i]->m_glsl_shader.c_str();
			} break;

		case ShaderData::ShaderType::kGeometry:
		case ShaderData::ShaderType::kTesselationEvaluation:
		case ShaderData::ShaderType::kTessellationControl:
		case ShaderData::ShaderType::kCompute:
		case ShaderData::ShaderType::kUnknown:
		default: ALogError("Unsupported shader type!");
		}
	}

	SPINLOCK_LOCK(m_object_creator_cmd_lock);
		m_object_creator_cmd->GenericCommand<CreateShaderCommand>(&com);
	SPINLOCK_UNLOCK(m_object_creator_cmd_lock);

	return com.unique_id;
}

void VideoOpenGL::DestroyShader(uint32_t shader_index)
{
	GenericIndexCommand destroy { Command::kDestroyShader, shader_index};
	SPINLOCK_LOCK(m_object_creator_cmd_lock);
		m_object_creator_cmd->GenericCommand<GenericIndexCommand>(&destroy);
	SPINLOCK_UNLOCK(m_object_creator_cmd_lock);
}

uint32_t VideoOpenGL::CreateTexture(uint32_t width, uint32_t height, bool generate_mipmaps, uint32_t format, uint32_t internal_format, uint32_t type, const void* data)
{
	CreateTextureCommand com { Command::kCreateTexture, GetNextUniqueID(), width, height, generate_mipmaps, format, internal_format, type, data };
	SPINLOCK_LOCK(m_object_creator_cmd_lock);
		m_object_creator_cmd->GenericCommand<CreateTextureCommand>(&com);
	SPINLOCK_UNLOCK(m_object_creator_cmd_lock);
	return com.unique_id;
}

void VideoOpenGL::DestroyTexture(uint32_t index)
{
	GenericIndexCommand destroy { Command::kDestroyTexture, index};
	SPINLOCK_LOCK(m_object_creator_cmd_lock);
		m_object_creator_cmd->GenericCommand<GenericIndexCommand>(&destroy);
	SPINLOCK_UNLOCK(m_object_creator_cmd_lock);
}

uint32_t VideoOpenGL::CreateVertexBuffer(const void* data, uint32_t data_size, BufferType buffer_type, BufferUsage usage)
{
	CreateVertexBufferCommand com { Command::kCreateVertexBuffer, Video::GetNextUniqueID(), data, data_size, (uint8_t)buffer_type, (uint8_t)usage };
	SPINLOCK_LOCK(m_object_creator_cmd_lock);
		m_object_creator_cmd->GenericCommand<CreateVertexBufferCommand>(&com);
	SPINLOCK_UNLOCK(m_object_creator_cmd_lock);
	return com.unique_id;
}

void VideoOpenGL::DestroyVertexBuffer(uint32_t index)
{
	GenericIndexCommand destroy { Command::kDestroyVertexBuffer, index};
	SPINLOCK_LOCK(m_object_creator_cmd_lock);
		m_object_creator_cmd->GenericCommand<GenericIndexCommand>(&destroy);
	SPINLOCK_UNLOCK(m_object_creator_cmd_lock);
}

void VideoOpenGL::UpdateVertexBuffer(uint32_t index, const void* data, uint32_t data_size, BufferType buffer_type, uint32_t offset)
{
	SPINLOCK_LOCK(m_object_creator_cmd_lock);
		m_object_creator_cmd->UpdateVertexBuffer(index, data, data_size, buffer_type, offset);
	SPINLOCK_UNLOCK(m_object_creator_cmd_lock);
}

void VideoOpenGL::OnWindowResized()
{
	glViewport(0, 0, static_cast<GLsizei>(m_width), static_cast<GLsizei>(m_height));
}

CommandBuffer* VideoOpenGL::CreateCommandBuffer(const uint32_t& size)
{
	return new CommandBufferOpenGL(size);
}

///////////////////////////////////////////////////////////
// Shader Commands
const uint8_t* VideoOpenGL::CreateShaderProgram(const uint8_t* command)
{
	const CreateShaderCommand* create = reinterpret_cast<const CreateShaderCommand*>(command);
	command += sizeof(CreateShaderCommand);

	if(m_shaders.find(create->unique_id) != m_shaders.end())
		return command;

	ShaderOpenGL* shader = new ShaderOpenGL;
	if(!shader->Initialize(create->vertex_shader_code, create->fragment_shader_code))
	{
		delete shader;
	}
	else
	{
		m_shaders[create->unique_id] = shader;
	}
	CheckGLError();
	return command;
}

const uint8_t* VideoOpenGL::DestroyShaderProgram(const uint8_t* command)
{
	const GenericIndexCommand* destroy = reinterpret_cast<const GenericIndexCommand*>(command);
	command += sizeof(GenericIndexCommand);

	ShaderContainer::iterator it = m_shaders.find(destroy->unique_id);
	if(it == m_shaders.end())
		return command;

	if(it->second != NULL)
	{
		ShaderOpenGL* shader = (ShaderOpenGL*)it->second;
		shader->Release();
		delete shader;
	}
	m_shaders.erase(it);
	return command;
}

const uint8_t* VideoOpenGL::UseShaderProgram(const uint8_t* command)
{
	const GenericIndexCommand* use = reinterpret_cast<const GenericIndexCommand*>(command);
	command += sizeof(GenericIndexCommand);

	ShaderContainer::iterator it = m_shaders.find(use->unique_id);
	if(it == m_shaders.end())
		return command; // should I reset the shader?

	m_current_shader = (ShaderOpenGL*)it->second;
	GLFunction(glUseProgram((GLuint)m_current_shader->GetNativeHandle()));
	return command;
}
///////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////
// Vertex Buffer Commands
const uint8_t* VideoOpenGL::CreateVertexBuffer(const uint8_t* command)
{
	const CreateVertexBufferCommand* create = reinterpret_cast<const CreateVertexBufferCommand*>(command);
	command += sizeof(CreateVertexBufferCommand);

	if(m_vertex_buffers.find(create->unique_id) != m_vertex_buffers.end())
		return command;

	static const GLenum type[] = { GL_ARRAY_BUFFER, GL_ELEMENT_ARRAY_BUFFER, GL_UNIFORM_BUFFER };
	static const GLenum usages[] = { GL_STATIC_DRAW, GL_DYNAMIC_DRAW };

	GLuint vertex_buffer = 0;
	GLFunction(glGenBuffers(1, &vertex_buffer));
	GLFunction(glBindBuffer(type[create->buffer_type], vertex_buffer));
	GLFunction(glBufferData(type[create->buffer_type], create->data_size, create->data, usages[create->usage]));
	GLFunction(glBindBuffer(type[create->buffer_type], 0));

	m_vertex_buffers[create->unique_id] = vertex_buffer;
	return command;
}

const uint8_t* VideoOpenGL::DestroyVertexBuffer(const uint8_t* command)
{
	const GenericIndexCommand* destroy = reinterpret_cast<const GenericIndexCommand*>(command);
	command += sizeof(GenericIndexCommand);

	ObjectContainer::iterator it = m_vertex_buffers.find(destroy->unique_id);
	if(it == m_vertex_buffers.end())
		return command;

	GLFunction(glDeleteBuffers(1, &it->second));
	m_vertex_buffers.erase(it);
	return command;
}

const uint8_t* VideoOpenGL::UpdateVertexBuffer(const uint8_t* command)
{
	const UpdateVertexBufferCommand* update = reinterpret_cast<const UpdateVertexBufferCommand*>(command);
	command += sizeof(UpdateVertexBufferCommand);

	ObjectContainer::iterator it = m_vertex_buffers.find(update->buffer);
	if(it == m_vertex_buffers.end())
		return command;

	static const GLenum type[] = { GL_ARRAY_BUFFER, GL_ELEMENT_ARRAY_BUFFER, GL_UNIFORM_BUFFER };

	GLFunction(glBindBuffer(type[update->buffer_type], it->second));
#if 1
	GLFunction(glBufferSubData(type[update->buffer_type], update->offset, update->data_size, update->data));
#else
	char* mem = (char*)glMapBuffer(type[update->buffer_type], GL_WRITE_ONLY);
	if(mem != NULL)
	{
		memcpy(&mem[update->offset], update->data, update->data_size);
		GLFunction(glUnmapBuffer(type[update->buffer_type]));
	}
#endif

	return command;
}
///////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////
// Texture Commands
const uint8_t* VideoOpenGL::CreateTexture(const uint8_t* command)
{
	const CreateTextureCommand* create = reinterpret_cast<const CreateTextureCommand*>(command);
	command += sizeof(CreateTextureCommand);

	if(m_textures.find(create->unique_id) != m_textures.end())
		return command;

	uint32_t texture = 0;
	GLFunction(glGenTextures(1, &texture));
	GLFunction(glBindTexture(GL_TEXTURE_2D, texture));

	GLFunction(glTexImage2D(GL_TEXTURE_2D, 0, static_cast<GLint>(create->internal_format),
							static_cast<GLsizei>(create->width), static_cast<GLsizei>(create->height), 0, create->format, create->type, create->data));

	if(create->generate_mipmaps)
	{
		GLFunction(glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR));
		GLFunction(glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR));
		GLFunction(glGenerateMipmap(GL_TEXTURE_2D));
	}
	else
	{
		GLFunction(glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST));
		GLFunction(glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST));
	}

	m_textures[create->unique_id] = texture;
	return command;
}

const uint8_t* VideoOpenGL::DestroyTexture(const uint8_t *command)
{
	const GenericIndexCommand* destroy = reinterpret_cast<const GenericIndexCommand*>(command);
	command += sizeof(GenericIndexCommand);

	ObjectContainer::iterator it = m_textures.find(destroy->unique_id);
	if(it == m_textures.end())
		return command;

	GLFunction(glDeleteTextures(1, &it->second));
	m_textures.erase(it);
	return command;
}

const uint8_t* VideoOpenGL::BindTexture(const uint8_t *command)
{
	const BindTextureCommand* bind = reinterpret_cast<const BindTextureCommand*>(command);
	command += sizeof(BindTextureCommand);

	ObjectContainer::iterator it = m_textures.find(bind->unique_id);
	if(it == m_textures.end() || m_current_shader == nullptr)
		return command;

	const ShaderOpenGL::VariableInfo* info = m_current_shader->FundUniform(bind->uniform_name_hash);
	if(info == nullptr || info->location < 0)
		return command;

	// search from most recent texture to least recent to se if we are bound someware.
	uint16_t slot = astd::min((uint16_t)(m_next_free_texture_unit.front()-1), (uint16_t)(m_max_texture_units-1));
	do {
		if(m_bound_textures[slot] == it->second)
		{
			// found it
			GLFunction(glActiveTexture(GL_TEXTURE0 + slot));
			GLFunction(glBindTexture(GL_TEXTURE_2D, it->second));
			break;
		}
		else if(slot == m_next_free_texture_unit.front())
		{
			// We did not find it. take the next free and put ut in the back of the queue.
			slot = m_next_free_texture_unit.front();
			m_next_free_texture_unit.pop();
			m_next_free_texture_unit.push(slot);

			// bind the texture to the slot
			GLFunction(glActiveTexture(GL_TEXTURE0 + slot));
			GLFunction(glBindTexture(GL_TEXTURE_2D, it->second));
			m_bound_textures[slot] = it->second;
			break;
		}
		slot = astd::min((uint16_t)(slot - 1), (uint16_t)(m_max_texture_units-1));
	} while(true);

	GLFunction(glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT));
	GLFunction(glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT));

	GLFunction(glUniform1i(info->location, slot));

	return command;
}

const uint8_t* VideoOpenGL::UnBindTexture(const uint8_t *command)
{
	const GenericIndexCommand* bind = reinterpret_cast<const GenericIndexCommand*>(command);
	command += sizeof(GenericIndexCommand);

	GLFunction(glActiveTexture(GL_TEXTURE0 + bind->unique_id));
	GLFunction(glBindTexture(GL_TEXTURE_2D, 0));
	return command;
}
///////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////
// Uniforms
const uint8_t* VideoOpenGL::Uniform(const uint8_t* command)
{
	const UniformCommand* uniform = reinterpret_cast<const UniformCommand*>(command);
	command += sizeof(UniformCommand);

	uint32_t data_size = uniform->type == Types::kMatrix ? sizeof(float) * uniform->component_count * uniform->component_count :
												  sizeof(float) * uniform->component_count;
	data_size *= uniform->data_count;

	const void* data = command;
	command += data_size;

	if(m_current_shader == nullptr)
		return command;

	const ShaderOpenGL::VariableInfo* info = m_current_shader->FundUniform(uniform->uniform_name_hash);
	if(info == nullptr || info->location < 0)
		return command;

	GLint location = info->location;

	switch(uniform->type)
	{
	case Types::kMatrix:
		{
			typedef void (GLAPIENTRY *MatrixUniformFunc)(GLint location, GLsizei count, GLboolean transpose, const GLfloat *value);
			static MatrixUniformFunc mat_funcs[] = { NULL, glUniformMatrix2fv, glUniformMatrix3fv, glUniformMatrix4fv };

			GLFunction(mat_funcs[uniform->component_count-1](location, static_cast<GLsizei>(uniform->data_count), false, (const float*)data));
		} break;

	case Types::kFloat:
		{
			typedef void (GLAPIENTRY *FloatVecFuncs)(GLint location, GLsizei count, const GLfloat *value);
			static FloatVecFuncs float_funcs[] = { glUniform1fv, glUniform2fv, glUniform3fv, glUniform4fv };

			GLFunction(float_funcs[uniform->component_count-1](location, static_cast<GLsizei>(uniform->data_count), (const float*)data));
		} break;

	case Types::kInt:
		{
			typedef void (GLAPIENTRY *IntVecFuncs)(GLint location, GLsizei count, const GLint *value);
			static IntVecFuncs int_funcs[] = { glUniform1iv, glUniform2iv, glUniform3iv, glUniform4iv };

			GLFunction(int_funcs[uniform->component_count-1](location, static_cast<GLsizei>(uniform->data_count), (const int*)data));
		} break;

	case Types::kByte:
	case Types::kUByte:
	case Types::kShort:
	case Types::kUShort:
	case Types::kUInt:
	case Types::kDouble:
	default:
		ALogError("Invalid uniform type");
	}
	return command;
}

const uint8_t* VideoOpenGL::UniformBuffer(const uint8_t* command)
{
	const UniformBufferCommand* uniform = reinterpret_cast<const UniformBufferCommand*>(command);
	command += sizeof(UniformBufferCommand);

	ObjectContainer::iterator it = m_vertex_buffers.find(uniform->uniform_buffer);
	if(it == m_vertex_buffers.end())
		return command;

	if(uniform->range)
	{
		GLFunction(glBindBufferRange(GL_UNIFORM_BUFFER, uniform->binding, it->second, uniform->start, uniform->end));
	}
	else
	{
		GLFunction(glBindBufferBase(GL_UNIFORM_BUFFER, uniform->binding, it->second));
	}

	return command;
}
///////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////
// Uniforms & Samplers

const uint8_t* VideoOpenGL::Clear(const uint8_t* command)
{
	const ClearCommand* clear = reinterpret_cast<const ClearCommand*>(command);
	command += sizeof(ClearCommand);

	GLFunction(glClearColor(clear->r, clear->g, clear->b, clear->a));
	GLFunction(glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT));
	return command;
}

const uint8_t* VideoOpenGL::ClearDepth(const uint8_t* command)
{
	//const Command* clear = reinterpret_cast<const Command*>(command);
	command += sizeof(Command);

	GLFunction(glClear(GL_DEPTH_BUFFER_BIT));
	return command;
}

const uint8_t* VideoOpenGL::EnableVertexAttrib(const uint8_t* command)
{
	const EnableVertexAttribCommand* enable = reinterpret_cast<const EnableVertexAttribCommand*>(command);
	command += sizeof(EnableVertexAttribCommand);

	ObjectContainer::iterator it = m_vertex_buffers.find(enable->buffer);
	if(it == m_vertex_buffers.end())
		return command;

	static GLenum gl_types[] = { GL_BYTE, GL_UNSIGNED_BYTE, GL_SHORT, GL_UNSIGNED_SHORT, GL_INT, GL_UNSIGNED_INT, GL_FLOAT, GL_DOUBLE, GL_INVALID_VALUE };

	GLFunction(glEnableVertexAttribArray(enable->index));
	GLFunction(glBindBuffer(GL_ARRAY_BUFFER, it->second));
	GLFunction(glVertexAttribPointer(enable->index, enable->size, gl_types[(uint32_t)enable->type],
			(GLboolean)enable->normalized, enable->stride, enable->pointer));
	return command;
}

const uint8_t* VideoOpenGL::DisableVertexAttrib(const uint8_t* command)
{
	const GenericIndexCommand* disable = reinterpret_cast<const GenericIndexCommand*>(command);
	command += sizeof(GenericIndexCommand);

	GLFunction(glDisableVertexAttribArray(disable->unique_id));
	return command;
}

const uint8_t* VideoOpenGL::DrawArrays(const uint8_t* command)
{
	const DrawArraysCommand* draw = reinterpret_cast<const DrawArraysCommand*>(command);
	command += sizeof(DrawArraysCommand);

	static GLenum modes[] = { GL_POINTS, GL_LINES, GL_LINE_LOOP, GL_LINE_STRIP, GL_TRIANGLES, GL_TRIANGLE_STRIP, GL_TRIANGLE_FAN, GL_QUADS, GL_QUAD_STRIP, GL_POLYGON };

	GLFunction(glDrawArrays(modes[(uint32_t)draw->mode], draw->first, draw->size));
	return command;
}

const uint8_t* VideoOpenGL::DrawElements(const uint8_t* command)
{
	const DrawElementsCommand* draw = reinterpret_cast<const DrawElementsCommand*>(command);
	command += sizeof(DrawElementsCommand);

	ObjectContainer::iterator it = m_vertex_buffers.find(draw->buffer);
	if(it == m_vertex_buffers.end())
		return command;

	static GLenum modes[] = { GL_POINTS, GL_LINES, GL_LINE_LOOP, GL_LINE_STRIP, GL_TRIANGLES, GL_TRIANGLE_STRIP, GL_TRIANGLE_FAN, GL_QUADS, GL_QUAD_STRIP, GL_POLYGON };
	static GLenum types[] = { GL_BYTE, GL_UNSIGNED_BYTE, GL_SHORT, GL_UNSIGNED_SHORT, GL_INT, GL_UNSIGNED_INT, GL_FLOAT, GL_DOUBLE, GL_INVALID_VALUE };

	GLFunction(glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, it->second));
	GLFunction(glDrawElements(modes[(uint32_t)draw->mode], draw->count, types[(uint32_t)draw->type], NULL));
	return command;
}

void VideoOpenGL::Flush()
{
	GLFunction(glFlush());
}

void VideoOpenGL::Swap()
{
	GLFunction(glFinish());
	GLFunction(SDL_GL_SwapWindow(m_window));
}
///////////////////////////////////////////////////////////

}
