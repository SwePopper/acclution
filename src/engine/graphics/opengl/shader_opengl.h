/*
   Acclution
   Copyright (c) 2016 Christoffer Lindqvist

   This software is provided 'as-is', without any express or implied
   warranty. In no event will the authors be held liable for any
   damages arising from the use of this software.

   Permission is granted to anyone to use this software for any
   purpose, including commercial applications, and to alter it and
   redistribute it freely, subject to the following restrictions:

   1. The origin of this software must not be misrepresented; you
      must not claim that you wrote the original software. If you use
      this software in a product, an acknowledgment in the product
      documentation would be appreciated but is not required.

   2. Altered source versions must be plainly marked as such, and
      must not be misrepresented as being the original software.

   3. This notice may not be removed or altered from any source
      distribution.

   Christoffer Lindqvist
   swepopper@gmail.com
*/

#ifndef SHADER_OPENGL_H
#define SHADER_OPENGL_H
#pragma once

#include "engine/graphics/shader.h"

namespace Acclution {

class ShaderOpenGL : public Shader
{
public:
	struct VariableInfo
	{
		char* name;
		uint32_t name_hash;
		int location;
		int data_size;
		uint32_t type;
	};

	struct UniformBlockVar
	{
		int index;
		int type;
		int offset;
		int count;
	};

	struct UniformBlock
	{
		char* name;
		uint32_t name_hash;
		uint32_t index;
		int binding;
		int data_size;
		UniformBlockVar* vars;
		uint32_t vars_count;
	};

	ShaderOpenGL();
	virtual ~ShaderOpenGL() { Release(); }

	bool Initialize(const char* vertex_shader_code, const char* fragment_shader_code);
	void Release();

	virtual uintptr_t GetNativeHandle() const { return m_program; }

	const int& GetAttributeCount() const { return m_attribute_count; }
	const int& GetUniformCount() const { return m_uniform_count; }

	const VariableInfo& GetAttribute(int index) const { return m_attributes[index]; }
	const VariableInfo& GetUniform(int index) const { return m_uniforms[index]; }

	const VariableInfo* FundUniform(uint32_t name_hash)
	{
		for(int i = 0; i < m_uniform_count; ++i)
			if(m_uniforms[i].name_hash == name_hash)
				return &m_uniforms[i];
		return nullptr;
	}

private:
	uint32_t		m_program;
	VariableInfo*	m_attributes;
	int				m_attribute_count;
	VariableInfo*	m_uniforms;
	int				m_uniform_count;
	UniformBlock*	m_uniform_blocks;
	int				m_uniform_block_count;
};

}

#endif // SHADER_H
