/*
   Acclution
   Copyright (c) 2016 Christoffer Lindqvist

   This software is provided 'as-is', without any express or implied
   warranty. In no event will the authors be held liable for any
   damages arising from the use of this software.

   Permission is granted to anyone to use this software for any
   purpose, including commercial applications, and to alter it and
   redistribute it freely, subject to the following restrictions:

   1. The origin of this software must not be misrepresented; you
      must not claim that you wrote the original software. If you use
      this software in a product, an acknowledgment in the product
      documentation would be appreciated but is not required.

   2. Altered source versions must be plainly marked as such, and
      must not be misrepresented as being the original software.

   3. This notice may not be removed or altered from any source
      distribution.

   Christoffer Lindqvist
   swepopper@gmail.com
*/

#ifndef VIDEO_OPENGL_H
#define VIDEO_OPENGL_H
#pragma once

#include <SDL2/SDL.h>

#include "engine/graphics/video.h"

namespace Acclution {

class Shader;
class CommandBuffer;

class VideoOpenGL : public Video
{
public:
	VideoOpenGL();
	virtual ~VideoOpenGL() { Release(); }

	virtual bool Initialize();
	virtual void Release();

	virtual VideoType GetVideoType() const { return VideoType::kOpenGL; }

	virtual void PlaybackRenderCommandBuffers(CommandBuffer* command_buffers, uint32_t count);

	Shader* GetShader(uint32_t index)
	{
		ShaderContainer::iterator it = m_shaders.find(index);
		if(it == m_shaders.end())
			return nullptr;
		return it->second;
	}

	virtual uint32_t CreateShader(ShaderData** shader_datas, uint32_t count);
	virtual void DestroyShader(uint32_t shader_index);

	virtual uint32_t CreateTexture(uint32_t width, uint32_t height, bool generate_mipmaps, uint32_t format, uint32_t internal_format, uint32_t type, const void* data);
	virtual void DestroyTexture(uint32_t index);

	virtual uint32_t CreateVertexBuffer(const void* data, uint32_t data_size, BufferType buffer_type, BufferUsage usage);
	virtual void DestroyVertexBuffer(uint32_t index);
	virtual void UpdateVertexBuffer(uint32_t index, const void* data, uint32_t data_size, BufferType buffer_type, uint32_t offset = 0);

	virtual CommandBuffer* CreateCommandBuffer(const uint32_t& size);

private:
	virtual void OnWindowResized();
	void RunCommands(const uint8_t* start, const uint8_t* end);

	// How a command function should look like.
	typedef const uint8_t* (VideoOpenGL::*CommandFunction)(const uint8_t* command);

	// Shader Commands
	const uint8_t* CreateShaderProgram(const uint8_t* command);
	const uint8_t* DestroyShaderProgram(const uint8_t* command);
	const uint8_t* UseShaderProgram(const uint8_t* command);

	// Vertex Buffer Commands
	const uint8_t* CreateVertexBuffer(const uint8_t* command);
	const uint8_t* CreateDynamicVertexBuffer(const uint8_t* command);
	const uint8_t* DestroyVertexBuffer(const uint8_t* command);
	const uint8_t* UpdateVertexBuffer(const uint8_t* command);

	// Texture Commands
	const uint8_t* CreateTexture(const uint8_t* command);
	const uint8_t* DestroyTexture(const uint8_t* command);
	const uint8_t* BindTexture(const uint8_t *command);
	const uint8_t* UnBindTexture(const uint8_t *command);

	// Uniforms
	const uint8_t* Uniform(const uint8_t* command);
	const uint8_t* UniformBuffer(const uint8_t* command);

	// Drawing
	const uint8_t* Clear(const uint8_t* command);
	const uint8_t* ClearDepth(const uint8_t* command);
	const uint8_t* EnableVertexAttrib(const uint8_t* command);
	const uint8_t* DisableVertexAttrib(const uint8_t* command);
	const uint8_t* DrawArrays(const uint8_t* command);
	const uint8_t* DrawElements(const uint8_t* command);

	void Flush();
	void Swap();

	SDL_GLContext			m_context;
	CommandFunction*		m_command_functions;

	class CommandBufferOpenGL*	m_object_creator_cmd;
	std::atomic_bool		m_object_creator_cmd_lock;

	typedef astd::map<uint32_t, uint32_t> ObjectContainer;
	ObjectContainer			m_vertex_buffers;
	ObjectContainer			m_textures;

	uint32_t				m_vertex_array_object;

	uint16_t				m_max_texture_units;
	astd::queue<uint16_t>	m_next_free_texture_unit;
	uint32_t*				m_bound_textures;

	class ShaderOpenGL*		m_current_shader;
};

}

#endif // VIDEO_H
