#include "engine/platform.h"
#include "commandbuffer_opengl.h"
#include "engine/graphics/commandbuffercommands.h"
#include "engine/graphics/video.h"
#include "engine/asset/shaderdata.h"

namespace Acclution
{

using namespace CommandBufferCommands;

CommandBufferOpenGL::CommandBufferOpenGL(const uint32_t& size)
	: m_command_line_size(size)
{
	// TODO: ALIGN
	m_command_line = m_command_line_start = new uint8_t[m_command_line_size];
}

CommandBufferOpenGL::~CommandBufferOpenGL()
{
	m_command_line_size = 0;
	m_command_line = nullptr;
	delete[] m_command_line_start;
	m_command_line_start = nullptr;
}

///////////////////////////////////////////////////////////
// Shader Commands
void CommandBufferOpenGL::UseShader(uint32_t index)
{
	GenericIndexCommand use_shader { Command::kUseShader, index };
	GenericCommand<GenericIndexCommand>(&use_shader);
}
///////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////
// Vertex Buffer
void CommandBufferOpenGL::UpdateVertexBuffer(uint32_t index, const void* data, uint32_t data_size, BufferType buffer_type, uint32_t offset)
{
	UpdateVertexBufferCommand update { Command::kUpdateVertexBuffer, index, data, data_size, (uint8_t)buffer_type, offset};
	GenericCommand<UpdateVertexBufferCommand>(&update);
}
///////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////
// Texture
void CommandBufferOpenGL::BindTexture(uint32_t index, uint8_t slot, uint32_t uniform_name_hash)
{
	BindTextureCommand bind { Command::kBindTexture, index, slot, uniform_name_hash};
	GenericCommand<BindTextureCommand>(&bind);
}

void CommandBufferOpenGL::UnBindTexture(uint8_t slot)
{
	GenericIndexCommand unbind { Command::kUnBindTexture, slot};
	GenericCommand<GenericIndexCommand>(&unbind);
}
///////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////
// Uniforms
void CommandBufferOpenGL::Uniform(uint32_t uniform_name_hash, Types type, uint32_t component_count, uint32_t data_count, const void* data)
{
	UniformCommand uniform { Command::kUniform,uniform_name_hash,type,component_count, data_count };
	uint32_t data_size = type == Types::kMatrix ? sizeof(float) * component_count * component_count :
												  sizeof(float) * component_count;
	data_size *= data_count;
	GenericCommand<UniformCommand>(&uniform, data, data_size);
}

void CommandBufferOpenGL::UniformBuffer(uint32_t uniform_buffer, uint32_t binding, bool range, uint32_t start, uint32_t end)
{
	UniformBufferCommand uniform { Command::kUniformBuffer, uniform_buffer, binding, range, start, end };
	GenericCommand<UniformBufferCommand>(&uniform);
}
///////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////
// Drawing
void CommandBufferOpenGL::Clear(float r, float g, float b, float a)
{
	ClearCommand clear { Command::kClear, r, g, b, a };
	GenericCommand<ClearCommand>(&clear);
}

void CommandBufferOpenGL::ClearDepth()
{
	Command command = Command::kClearDepth;
	GenericCommand<Command>(&command);
}

void CommandBufferOpenGL::EnableVertexAttrib(uint32_t buffer, uint32_t index, int32_t component_count, Types type, bool normalized, int32_t stride, const void* pointer)
{
	EnableVertexAttribCommand enable { Command::kEnableVertexAttrib, buffer, index, component_count, type, normalized, stride, pointer };
	GenericCommand<EnableVertexAttribCommand>(&enable);
}

void CommandBufferOpenGL::DisableVertexAttrib(uint32_t attribute_index)
{
	GenericIndexCommand disable { Command::kDisableVertexAttrib, attribute_index };
	GenericCommand<GenericIndexCommand>(&disable);
}

void CommandBufferOpenGL::DrawArrays(Modes mode, int first, int size)
{
	DrawArraysCommand draw { Command::kDrawArrays, mode, first, size };
	GenericCommand<DrawArraysCommand>(&draw);
}

void CommandBufferOpenGL::DrawElements(Modes mode, int index_count, Types type, uint32_t buffer)
{
	DrawElementsCommand draw { Command::kDrawElements, mode, index_count, type, buffer };
	GenericCommand<DrawElementsCommand>(&draw);
}

///////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////
// Misc
void CommandBufferOpenGL::AddCommandLine(const CommandBufferOpenGL& src)
{
	uint32_t size = (uint32_t)(src.m_command_line.load() - src.m_command_line_start);
	if(src.m_command_line_start == nullptr || size > src.m_command_line_size)
		return;

	uint8_t* copy_to = m_command_line.fetch_add(size);
	std::copy(src.m_command_line_start, src.m_command_line_start + size, copy_to);
}
///////////////////////////////////////////////////////////

}
