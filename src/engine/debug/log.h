#ifndef LOG_H
#define LOG_H
#pragma once

namespace Acclution
{

#if !defined(NO_LOGGING)

class Log
{
public:
	enum class LogSeverity : uint8_t
	{
		kInfo,
		kDebug,
		kWarning,
		kError,
		kFatal,
		kCount
	};

	Log();
	~Log();

	static void LogMessage(const char* file, int line, LogSeverity severity, const char* format, ...);

private:
	void Message(const char* file, int line, LogSeverity severity, const char* message);

	std::mutex	m_log_lock;
	FILE*		m_log_file;
};

#define ALogInfo(...) Acclution::Log::LogMessage(__FILE__, __LINE__, Acclution::Log::LogSeverity::kInfo, __VA_ARGS__)
#define ALogDebug(...) Acclution::Log::LogMessage(__FILE__, __LINE__, Acclution::Log::LogSeverity::kDebug, __VA_ARGS__)
#define ALogWarning(...) Acclution::Log::LogMessage(__FILE__, __LINE__, Acclution::Log::LogSeverity::kWarning, __VA_ARGS__)
#define ALogError(...) Acclution::Log::LogMessage(__FILE__, __LINE__, Acclution::Log::LogSeverity::kError, __VA_ARGS__)
#define ALogFatal(...) Acclution::Log::LogMessage(__FILE__, __LINE__, Acclution::Log::LogSeverity::kFatal, __VA_ARGS__)

#else

#define ALogInfo(...)		(void)0
#define ALogDebug(...)		(void)0
#define ALogWarning(...)	(void)0
#define ALogError(...)		(void)0
#define ALogFatal(...)		(void)0

class Log {};

#endif

}

#endif // LOG_H
