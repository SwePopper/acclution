#include "engine/platform.h"
#include "log.h"
#include "engine/engine.h"
#include "engine/utilities.h"

#include <cstdio>

namespace Acclution
{

#if !defined(NO_LOGGING)

Log::Log()
{
	if(Utilities::FileExist("output.log"))
	{
		if(Utilities::FileExist("old_output.log"))
		{
			// remove oldest log
			std::remove("old_output.log");
		}
		// Move old log to "old_output.log"
		std::rename("output.log", "old_output.log");
	}

	char log_file_path[1024] = { 0 };
	snprintf(log_file_path, 1024, "output.log");

	m_log_file = fopen(log_file_path, "w");
	if(m_log_file == NULL)
	{
		m_log_file = NULL;
		fprintf(stderr, "Failed to open logfile: %s\n", log_file_path);
		return;
	}

	time_t t = time(0);
	struct tm* now = localtime(&t);

	char* time_string = asctime(now);
	time_string[strlen(time_string)-1] = '\0';

	fwrite("Started logging: ", 1, strlen("Started logging: "), m_log_file);
	fwrite(time_string, 1, strlen(time_string), m_log_file);
	fwrite("\n\n", 1, 2, m_log_file);
}

Log::~Log()
{
	if(m_log_file != NULL)
	{
		fclose(m_log_file);
		m_log_file = NULL;
	}
}

void Log::LogMessage(const char* file, int line, LogSeverity severity, const char* format, ...)
{
	char buff[BUF_SIZE];
	va_list args;
	va_start(args, format);

#ifdef PLATFORM_LINUX
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wformat-nonliteral"
#endif

	vsnprintf(buff, BUF_SIZE, format, args);

#ifdef PLATFORM_LINUX
#pragma GCC diagnostic pop
#endif

	va_end(args);

	static FILE* severity_logs[] = { stdout, stdout, stdout, stderr, stderr };
	fprintf(severity_logs[(int)severity], "%s::%i - %s\n", file, line, buff);

#if defined(PLATFORM_WINDOWS)
	{
		char winmess[BUF_SIZE];
		snprintf(winmess, BUF_SIZE, "%s::%i - %s\n", file, line, buff);
		OutputDebugString(winmess);
	}
#endif

	std::unique_ptr<Log>& instance = Engine::GetInstance()->GetLog();
	if(instance)
	{
		instance->Message(file, line, severity, buff);
	}
}

static const char* serverLogMessageStart =
		"message: %s\n"
		"time: %s\n"
		"file: %s\n"
		"line: %i\n"
		"severity: %s\n\n";

static const char* severityStrings[] = { "Info", "Debug", "Warning", "Error", "Fatal" };

void Log::Message(const char* file, int line, LogSeverity severity, const char* message)
{
	if(m_log_file == nullptr)
		return;

	std::lock_guard<std::mutex> lock(m_log_lock);

	time_t t = time(0);
	struct tm* now = localtime(&t);

	char* time_string = asctime(now);
	time_string[strlen(time_string)-1] = '\0';

	char buffer[BUF_SIZE] = { 0 };

#ifdef PLATFORM_LINUX
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wformat-nonliteral"
#endif

	snprintf(buffer, BUF_SIZE, serverLogMessageStart, message, time_string, file,
			 line, severityStrings[(int)severity]);

#ifdef PLATFORM_LINUX
#pragma GCC diagnostic pop
#endif

	fwrite(buffer, 1, strlen(buffer), m_log_file);
}

#endif

}
