#ifndef ALLOCATOR_H
#define ALLOCATOR_H
#pragma once

namespace Acclution
{

class Allocator
{
public:
	Allocator() {}
	virtual ~Allocator() {}

	virtual void* Allocate(size_t size, size_t alignment = 4, const char* file = nullptr, int line = 0)=0;
	virtual void Free(void* memory)=0;

	template<typename T, typename... Args>
	T* New(Args... args)
	{
		T* memory = (T*)Allocate(sizeof(T), alignof(T));
		// Only need to call constructor on non pod types
		if(!std::is_pod<T>::value)
			new (memory) T(std::forward<Args>(args)...);
		return memory;
	}

	template<typename T>
	void Delete(T* obj)
	{
		// Only need to call destructor on non pod types
		if(!std::is_pod<T>::value)
			obj->~T();
		Free(obj);
	}

	template<typename T, typename... Args>
	T* NewArray(size_t count, Args... args)
	{
		// Allocate the memory
		uintptr_t memory = (uintptr_t)Allocate(sizeof(T)*count + sizeof(size_t), alignof(T));

		// Store array count
		*((size_t*)memory) = count;
		memory += sizeof(size_t);

		// call the constructor in the elements on non POD types
		if(!std::is_pod<T>::value)
		{
			for(T* cur = (T*)memory, *end = ((T*)memory)+count; cur != end; ++cur)
			{
				new (cur) T(std::forward<Args>(args)...);
			}
		}

		return (T*)memory;
	}

	template<typename T>
	void DeleteArray(T* array)
	{
		// Get count and memory location
		uintptr_t memory = (uintptr_t)array;
		memory -= sizeof(size_t);
		size_t count = *((size_t*)memory);

		// Call the destuctor on all elements on non POD types
		if(!std::is_pod<T>::value)
		{
			for(T* end = array + count - 1; array <= end; --end)
			{
				end->~T();
			}
		}

		Free((void*)memory);
	}

	static Allocator* GetCurrent();
	static void SetCurrent(Allocator* allocator);

	static Allocator* GetDefaultAllocator() { return sDefaultAllocator; }
	static void SetDefaultAllocator(Allocator* allocator) { sDefaultAllocator = allocator; }

private:
	thread_local static Allocator*	sCurrentAllocator;
	static Allocator*				sDefaultAllocator;
};

class AllocatorScope
{
public:
	AllocatorScope(Allocator* allocator) : m_old(Allocator::GetCurrent()) { Allocator::SetCurrent(allocator); }
	~AllocatorScope() { Allocator::SetCurrent(m_old); }

private:
	Allocator* m_old;
};

}

#endif // ALLOCATOR_H
