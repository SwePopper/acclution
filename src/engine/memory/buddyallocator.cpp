#include "engine/platform.h"
#include "buddyallocator.h"

namespace Acclution
{

BuddyAllocator::BuddyAllocator(size_t leaf_size, uint32_t level_count)
	: m_memory(nullptr),
	  m_memory_size(0),
	  m_level_count(0),
	  m_leaf_size(0)
{
	m_leaf_size = astd::NextPowerTwo(static_cast<uint32_t>(leaf_size + sizeof(Header)));

	m_level_count = astd::min((uint32_t)Settings::sMaxLevels, level_count);

	m_memory_size = (1 << (m_level_count-1)) * m_leaf_size;

	m_memory = malloc(m_memory_size);
	memset(m_memory, 0, m_memory_size);
	m_free_lists[0].push_back(m_memory);

	Header* header = (Header*)m_memory;
	astd::set_bit(header->m_is_free, 0); // Is free
	astd::clear_bit(header->m_is_split, 0); // Is not split
}

BuddyAllocator::~BuddyAllocator()
{
	free(m_memory);
}

void* BuddyAllocator::Allocate(size_t size, size_t alignment, const char* /*file*/, int /*line*/)
{
	uint32_t full_size = astd::max(astd::NextPowerTwo(static_cast<uint32_t>(size + alignment + sizeof(Header))), m_leaf_size);
	uint32_t level = astd::LogBase2(static_cast<uint32_t>(m_memory_size / full_size));

	void* data = nullptr;
	{
		std::lock_guard<std::mutex> guard(m_lock);
		if(m_free_lists[level].empty())
		{
			SplitFree(level-1);
			if(m_free_lists[level].empty())
			{
				ALogFatal("Could not allocate %u", full_size);
				return nullptr;
			}
		}

		data = m_free_lists[level].back();
		m_free_lists[level].pop_back();

		Header* header = (Header*)data;
		astd::clear_bit(header->m_is_free, level);

		data = ((char*)data) + sizeof(Header);
	}
	return data;
}

void BuddyAllocator::Free(void* memory)
{
	memory = ((char*)memory) - sizeof(Header);
	Header* header = (Header*)memory;

	std::lock_guard<std::mutex> guard(m_lock);
	uint32_t level = FindLevel(memory);

	astd::set_bit(header->m_is_free, level);

	m_free_lists[level].push_back(memory);
	TryMerge(memory, level);
}

void BuddyAllocator::SplitFree(uint32_t level)
{
	if(level > 0 && m_free_lists[level].empty())
		SplitFree(level - 1);

	void* a = m_free_lists[level].back();
	m_free_lists[level].pop_back();

	uint32_t level_size = GetLevelSize(level);
	void* b = (((char*)a) + (level_size / 2));

	m_free_lists[level+1].push_back(a);
	m_free_lists[level+1].push_back(b);

	// Set Header
	Header* header = (Header*)a;
	astd::clear_bit(header->m_is_free, level); // parent
	astd::set_bit(header->m_is_split, level); // parent

	astd::set_bit(header->m_is_free, level + 1); // a
	astd::clear_bit(header->m_is_split, level + 1); // a

	header = (Header*)b;
	astd::set_bit(header->m_is_free, level + 1); // b
	astd::clear_bit(header->m_is_split, level + 1); // b
}

void BuddyAllocator::TryMerge(void* memory, uint32_t level)
{
	if(level == 0)
		return;

	uint32_t level_size = GetLevelSize(level);
	uint32_t level_index = static_cast<uint32_t>((uintptr_t)memory - (uintptr_t)m_memory) / level_size;

	void* buddy = nullptr;
	void* first = nullptr;
	if(level_index % 2)
	{
		first = buddy = ((char*)memory) - level_size;
	}
	else
	{
		buddy = ((char*)memory) + level_size;
		first = memory;
	}
	
	if(astd::check_bit(*(uint32_t*)memory, level) && astd::check_bit(*(uint32_t*)buddy, level))
	{
		// Merge
		astd::EraseRemove(m_free_lists[level], memory);
		astd::EraseRemove(m_free_lists[level], buddy);

		// Set Header
		Header* header = (Header*)memory;
		astd::clear_bit(header->m_is_free, level);
		header = (Header*)buddy;
		astd::clear_bit(header->m_is_free, level);
		header = (Header*)first;
		astd::set_bit(header->m_is_free, level - 1);
		astd::clear_bit(header->m_is_split, level - 1);

		m_free_lists[level-1].push_back(first);
		TryMerge(first, level-1);
	}
}

uint32_t BuddyAllocator::FindLevel(void* memory) const
{
	Header* header = (Header*)memory;
	for(uint32_t i = m_level_count - 1; i > 0; --i)
	{
		header = (Header*)GetParent(header, i);
		if(astd::check_bit(header->m_is_split, i-1))
			return i;
	}
	return 0;
}

void* BuddyAllocator::GetParent(void* memory, uint32_t level) const
{
	uint32_t level_size = GetLevelSize(level);
	uint32_t level_index = static_cast<uint32_t>((uintptr_t)memory - (uintptr_t)m_memory) / level_size;

	if (level_index % 2)
		return ((char*)memory) - level_size;
	return memory;
}

}
