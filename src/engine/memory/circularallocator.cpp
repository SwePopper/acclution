#include "engine/platform.h"
#include "circularallocator.h"

namespace Acclution
{

CircularAllocator::CircularAllocator(size_t allocator_size)
	: Allocator(),
	  m_memory(0),
	  m_memory_size(0),
	  m_current_position(0),
	  m_lock(false)
{
	m_current_position = m_memory = reinterpret_cast<uintptr_t>(malloc(allocator_size));
	m_memory_size = allocator_size;
}

CircularAllocator::~CircularAllocator()
{
	free(reinterpret_cast<void*>(m_memory));
	m_current_position = m_memory = 0;
	m_memory_size = 0;
}

void* CircularAllocator::Allocate(size_t size, size_t alignment, const char* /*file*/, int /*line*/)
{
	size_t full_size = size + alignment;
	void* mem = nullptr;

	{
		// TODO: find a lockless solution
		while(m_lock.exchange(true))
			;

		if(m_current_position + full_size > m_memory + m_memory_size)
			m_current_position = m_memory;

		mem = (void*)m_current_position;
		m_current_position += full_size;

		m_lock.store(false);
	}

	astd::align(alignment, size, mem, full_size);
	return mem;
}

void CircularAllocator::Free(void* /*memory*/)
{
}

}
