#ifndef OBJECTALLOCATOR_H
#define OBJECTALLOCATOR_H
#pragma once

namespace Acclution
{

template<typename T>
class ObjectAllocator
{
public:
	ObjectAllocator(size_t count)
		: m_memory(nullptr),
		  m_free(count)
	{
		// Get memory
		size_t full_size = count * sObjectSize + sObjectAlignment;
		m_memory = malloc(full_size);

		void* aligned_memory = m_memory;
		astd::align(sObjectAlignment, sObjectSize, aligned_memory, full_size);

		// Get Start and end
		char* start = (char*)aligned_memory;
		char* end = (char*)start + count * sObjectSize;

		// Fill free list
		while(start < end)
		{
			m_free.enqueue(start);
			start += sObjectSize;
		}
	}

	virtual ~ObjectAllocator()
	{
		free(m_memory);
	}

	template<typename... Args>
	T* New(Args... args)
	{
		void* memory = nullptr;
		if(m_free.try_dequeue(memory) && !std::is_pod<T>::value)
		{
			// Only need to call constructor on non pod types
			new (memory) T(std::forward<Args>(args)...);
		}
		return (T*)memory;
	}

	void Delete(T* obj)
	{
		// Only need to call destructor on non pod types
		if(!std::is_pod<T>::value)
			obj->~T();
		m_free.enqueue(obj);
	}

private:
	static constexpr size_t					sObjectSize = sizeof(T);
	static constexpr size_t					sObjectAlignment = alignof(T);

	void*									m_memory;
	moodycamel::ConcurrentQueue<void*>		m_free;
};

}

#endif // OBJECTALLOCATOR_H
