#include "engine/platform.h"
#include "frameallocator.h"

namespace Acclution
{

void FrameAllocator::Reset()
{
	void* memory = nullptr;
	while(m_attached_memory.try_dequeue(memory))
		free(memory);
	LinearAllocator::Reset();
}

void* FrameAllocator::AllocateBig(uint64_t size)
{
	void* memory = malloc(size);
	if(memory != nullptr)
		m_attached_memory.enqueue(memory);
	return memory;
}

}
