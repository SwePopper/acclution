#ifndef LINEARALLOCATOR_H
#define LINEARALLOCATOR_H
#pragma once

#include "allocator.h"

namespace Acclution
{

class LinearAllocator : public Allocator
{
public:
	LinearAllocator(size_t allocator_size);
	virtual ~LinearAllocator();

	virtual void* Allocate(size_t size, size_t alignment = 4, const char* file = nullptr, int line = 0);
	virtual void Free(void* memory);

	virtual void Reset();

private:
	uintptr_t				m_memory;
	size_t					m_memory_size;

	std::atomic<uintptr_t>	m_current_position;
};

}

#endif // LINEARALLOCATOR_H
