#ifndef FRAMEALLOCATOR_H
#define FRAMEALLOCATOR_H
#pragma once

#include "linearallocator.h"

namespace Acclution
{

class FrameAllocator : public LinearAllocator
{
public:
	FrameAllocator(uint32_t allocator_size) : LinearAllocator(allocator_size) {}
	virtual ~FrameAllocator() { Reset(); }

	virtual void Reset();
	void* AllocateBig(uint64_t size);

private:
	moodycamel::ConcurrentQueue<void*>	m_attached_memory;
};

}

#endif // FRAMEALLOCATOR_H
