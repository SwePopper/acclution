#ifndef BUDDYALLOCATOR_H
#define BUDDYALLOCATOR_H
#pragma once

#include "allocator.h"

namespace Acclution
{

class BuddyAllocator : public Allocator
{
public:
	BuddyAllocator(size_t leaf_size, uint32_t level_count);
	virtual ~BuddyAllocator();

	virtual void* Allocate(size_t size, size_t alignment = 4, const char* file = nullptr, int line = 0);
	virtual void Free(void* memory);

private:
	void SplitFree(uint32_t level);
	void TryMerge(void* memory, uint32_t level);
	uint32_t FindLevel(void* memory) const;
	void* GetParent(void* memory, uint32_t level) const;
	inline uint32_t GetLevelSize(const uint32_t& level) const { return static_cast<uint32_t>(m_memory_size / (1ull << level)); }

	enum class Settings : uint32_t
	{
		sMaxLevels = 32,
		sMaxSplits = 1u << (sMaxLevels - 1u)
	};

	using FreeList = astd::vector<void*, AstdAllocator<void*> >;

	struct Header
	{
		uint32_t	m_is_free;
		uint32_t	m_is_split;
	};

	std::mutex						m_lock;
	FreeList						m_free_lists[(uint32_t)Settings::sMaxLevels];

	void*							m_memory;
	size_t							m_memory_size;

	uint32_t						m_level_count;
	uint32_t						m_leaf_size;
};

}

#endif // BUDDYALLOCATOR_H
