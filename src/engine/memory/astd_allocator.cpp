#include "engine/platform.h"
#include "astd_allocator.h"

#if defined(EASTL_USER_DEFINED_ALLOCATOR) && defined(USE_EASTL)

namespace eastl
{
	/// gDefaultAllocator
	/// Default global allocator instance. 
	EASTL_API allocator   gDefaultAllocator;
	EASTL_API allocator* gpDefaultAllocator = &gDefaultAllocator;

	EASTL_API allocator* GetDefaultAllocator()
	{
		return gpDefaultAllocator;
	}

	EASTL_API allocator* SetDefaultAllocator(allocator* pAllocator)
	{
		allocator* const pPrevAllocator = gpDefaultAllocator;
		gpDefaultAllocator = pAllocator;
		return pPrevAllocator;
	}
} // namespace eastl


#endif