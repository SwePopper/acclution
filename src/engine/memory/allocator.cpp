#include "engine/platform.h"
#include "allocator.h"

namespace Acclution
{

thread_local Allocator* Allocator::sCurrentAllocator = nullptr;
Allocator* Allocator::sDefaultAllocator = nullptr;

Allocator* Allocator::GetCurrent()
{
	if(sCurrentAllocator == nullptr)
	{
		sCurrentAllocator = sDefaultAllocator;
	}
	return sCurrentAllocator;
}

void Allocator::SetCurrent(Allocator* allocator)
{
	if(allocator == nullptr)
	{
		allocator = sDefaultAllocator;
	}
	sCurrentAllocator = allocator;
}

}
