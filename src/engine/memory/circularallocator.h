#ifndef CIRCULARALLOCATOR_H
#define CIRCULARALLOCATOR_H
#pragma once

#include "allocator.h"

namespace Acclution
{

class CircularAllocator : public Allocator
{
public:
	CircularAllocator(size_t allocator_size);
	virtual ~CircularAllocator();

	virtual void* Allocate(size_t size, size_t alignment = 4, const char* file = nullptr, int line = 0);
	virtual void Free(void* memory);

private:
	uintptr_t			m_memory;
	size_t				m_memory_size;

	uintptr_t			m_current_position;
	std::atomic_bool	m_lock;
};

}

#endif // CIRCULARALLOCATOR_H
