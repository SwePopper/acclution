#include "engine/platform.h"
#include "linearallocator.h"

namespace Acclution
{

LinearAllocator::LinearAllocator(size_t allocator_size)
	: Allocator(),
	  m_memory(0),
	  m_memory_size(0),
	  m_current_position(0)
{
	m_current_position = m_memory = reinterpret_cast<uintptr_t>(malloc(allocator_size));
	m_memory_size = allocator_size;
}

LinearAllocator::~LinearAllocator()
{
	free(reinterpret_cast<void*>(m_memory));
	m_current_position = m_memory = 0;
	m_memory_size = 0;
}

void* LinearAllocator::Allocate(size_t size, size_t alignment, const char* /*file*/, int /*line*/)
{
	size_t full_size = size + alignment;
	void* mem = reinterpret_cast<void*>(std::atomic_fetch_add(&m_current_position, full_size));
	astd::align(alignment, size, mem, full_size);

	// Bounds check
	if(((uintptr_t)mem) + size >= m_memory + m_memory_size)
	{
		ALogError("Allocator our of memory!");
		return nullptr;
	}

	return mem;
}

void LinearAllocator::Free(void* /*memory*/)
{
}

void LinearAllocator::Reset()
{
	m_current_position.store(m_memory);
}

}
