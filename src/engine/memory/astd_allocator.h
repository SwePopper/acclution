#ifndef ASTD_ALLOCATOR_H
#define ASTD_ALLOCATOR_H
#pragma once

#include "allocator.h"

#if defined(EASTL_USER_DEFINED_ALLOCATOR) && defined(USE_EASTL)

namespace Acclution
{
	class AstdAllocatorSystem
	{
	public:
		EASTL_ALLOCATOR_EXPLICIT AstdAllocatorSystem(const char* name = EASTL_NAME_VAL(EASTL_ALLOCATOR_DEFAULT_NAME))
		{
			set_name(name ? name : EASTL_ALLOCATOR_DEFAULT_NAME);
		}

		AstdAllocatorSystem(const AstdAllocatorSystem& EASTL_NAME(x))
		{
#if EASTL_NAME_ENABLED
			m_name = x.m_name;
#endif
		}

		AstdAllocatorSystem(const AstdAllocatorSystem&, const char* name)
		{
			set_name(name ? name : EASTL_ALLOCATOR_DEFAULT_NAME);
		}

		AstdAllocatorSystem& operator=(const AstdAllocatorSystem& x)
		{
#if EASTL_NAME_ENABLED
			m_name = x.m_name;
#endif
			return *this;
		}

		void* allocate(size_t n, int flags = 0)
		{
			ASTD_UNUSED(flags);
			return malloc(n);
		}

		void* allocate(size_t n, size_t alignment, size_t offset, int flags = 0)
		{
			ASTD_UNUSED(alignment);
			ASTD_UNUSED(offset);
			ASTD_UNUSED(flags);
			return malloc(n);
		}

		void  deallocate(void* p, size_t /*n*/)
		{
			free(p);
		}

#if EASTL_NAME_ENABLED
		const char* get_name() const { return m_name; }
		void        set_name(const char* pName) { m_name = pName; }
#else
		const char* get_name() const { return nullptr; }
		void        set_name(const char* pName) {}
#endif

	protected:
#if EASTL_NAME_ENABLED
		const char* m_name; // Debug name, used to track memory.
#endif
	};

	class AstdAllocatorBase
	{
	public:
		EASTL_ALLOCATOR_EXPLICIT AstdAllocatorBase(const char* name = EASTL_NAME_VAL(EASTL_ALLOCATOR_DEFAULT_NAME))
		{
			set_name(name ? name : EASTL_ALLOCATOR_DEFAULT_NAME);
			m_allocator = Acclution::Allocator::GetDefaultAllocator();
		}

		AstdAllocatorBase(const AstdAllocatorBase& x)
		{
#if EASTL_NAME_ENABLED
			m_name = x.m_name;
#endif
			m_allocator = x.m_allocator;
		}

		AstdAllocatorBase(const AstdAllocatorBase& x, const char* name)
		{
			set_name(name ? name : EASTL_ALLOCATOR_DEFAULT_NAME);
			m_allocator = x.m_allocator;
		}

		AstdAllocatorBase& operator=(const AstdAllocatorBase& x)
		{
#if EASTL_NAME_ENABLED
			m_name = x.m_name;
#endif
			m_allocator = x.m_allocator;
			return *this;
		}

		void* allocate(size_t n, int flags = 0)
		{
			ASTD_UNUSED(flags);

#if EASTL_NAME_ENABLED
#define pName m_name
#else
#define pName EASTL_ALLOCATOR_DEFAULT_NAME " : " __FILE__
#endif
			if(m_allocator == nullptr)
				m_allocator = Acclution::Allocator::GetDefaultAllocator();
			return m_allocator->Allocate(n, 4, pName, __LINE__);
		}

		void* allocate(size_t n, size_t alignment, size_t offset, int flags = 0)
		{
			ASTD_UNUSED(offset);
			ASTD_UNUSED(flags);
			if (m_allocator == nullptr)
				m_allocator = Acclution::Allocator::GetDefaultAllocator();
			return m_allocator->Allocate(n, alignment, pName, __LINE__);
#undef pName  // See above for the definition of this.
		}

		void  deallocate(void* p, size_t /*n*/)
		{
			m_allocator->Free(p);
		}

#if EASTL_NAME_ENABLED
		const char* get_name() const { return m_name; }
		void        set_name(const char* pName) { m_name = pName; }
#else
		const char* get_name() const { return nullptr; }
		void        set_name(const char* pName) {}
#endif

		Allocator* get_allocator() const { return m_allocator; }

	protected:
#if EASTL_NAME_ENABLED
		const char* m_name; // Debug name, used to track memory.
#endif
		Allocator* m_allocator;
	};

	inline bool operator==(const AstdAllocatorBase& a, const AstdAllocatorBase& b)
	{
		return a.get_allocator() == b.get_allocator();
	}

	inline bool operator!=(const AstdAllocatorBase& a, const AstdAllocatorBase& b)
	{
		return a.get_allocator() != b.get_allocator();
	}

	template<typename T>
	using AstdAllocator = AstdAllocatorSystem;
}

namespace eastl
{
	inline allocator::allocator(const char* EASTL_NAME(pName))
	{
#if EASTL_NAME_ENABLED
		mpName = pName ? pName : EASTL_ALLOCATOR_DEFAULT_NAME;
#endif
	}

	inline allocator::allocator(const allocator& EASTL_NAME(alloc))
	{
#if EASTL_NAME_ENABLED
		mpName = alloc.mpName;
#endif
	}

	inline allocator::allocator(const allocator&, const char* EASTL_NAME(pName))
	{
#if EASTL_NAME_ENABLED
		mpName = pName ? pName : EASTL_ALLOCATOR_DEFAULT_NAME;
#endif
	}

	inline allocator& allocator::operator=(const allocator& EASTL_NAME(alloc))
	{
#if EASTL_NAME_ENABLED
		mpName = alloc.mpName;
#endif
		return *this;
	}

	inline const char* allocator::get_name() const
	{
#if EASTL_NAME_ENABLED
		return mpName;
#else
		return EASTL_ALLOCATOR_DEFAULT_NAME;
#endif
	}

	inline void allocator::set_name(const char* EASTL_NAME(pName))
	{
#if EASTL_NAME_ENABLED
		mpName = pName;
#endif
	}

	inline void* allocator::allocate(size_t n, int /*flags*/)
	{
#if EASTL_NAME_ENABLED
#define pName mpName
#else
#define pName EASTL_ALLOCATOR_DEFAULT_NAME " : " __FILE__
#endif

		Acclution::Allocator* default_allocator = Acclution::Allocator::GetDefaultAllocator();
		return default_allocator->Allocate(n, 4, pName, __LINE__);
	}

	inline void* allocator::allocate(size_t n, size_t alignment, size_t /*offset*/, int /*flags*/)
	{
		Acclution::Allocator* default_allocator = Acclution::Allocator::GetDefaultAllocator();
		return default_allocator->Allocate(n, alignment, pName, __LINE__);

#undef pName  // See above for the definition of this.
	}

	inline void allocator::deallocate(void* p, size_t)
	{
		Acclution::Allocator* default_allocator = Acclution::Allocator::GetDefaultAllocator();
		default_allocator->Free(p);
	}

	inline bool operator==(const allocator&, const allocator&)
	{
		return true; // All allocators are considered equal, as they merely use global new/delete.
	}

	inline bool operator!=(const allocator&, const allocator&)
	{
		return false; // All allocators are considered equal, as they merely use global new/delete.
	}
} // namespace eastl

#else

namespace Acclution
{
	template<class _Ty>
	using AstdAllocator = std::allocator<_Ty>;
}

#endif

#endif // ALLOCATOR_H
