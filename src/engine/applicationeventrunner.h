/*
   Acclution
   Copyright (c) 2016 Christoffer Lindqvist

   This software is provided 'as-is', without any express or implied
   warranty. In no event will the authors be held liable for any
   damages arising from the use of this software.

   Permission is granted to anyone to use this software for any
   purpose, including commercial applications, and to alter it and
   redistribute it freely, subject to the following restrictions:

   1. The origin of this software must not be misrepresented; you
      must not claim that you wrote the original software. If you use
      this software in a product, an acknowledgment in the product
      documentation would be appreciated but is not required.

   2. Altered source versions must be plainly marked as such, and
      must not be misrepresented as being the original software.

   3. This notice may not be removed or altered from any source
      distribution.

   Christoffer Lindqvist
   swepopper@gmail.com
*/

#ifndef APPLICATIONEVENTRUNNER_H
#define APPLICATIONEVENTRUNNER_H
#pragma once

union SDL_Event;

namespace Acclution
{

class ApplicationEventRunner
{
public:
	ApplicationEventRunner();

	bool Initialize(const UpdateType& update_type);

	void Update();

	typedef void (*EventCallback)(SDL_Event* event, void* user_param);

	void AddEventCallback(EventCallback callback, void* user_param = nullptr);

	void ClearAll();

private:
	void UpdateEventBased();
	void UpdateFrameBased();
	inline void RunEvent(SDL_Event* event);

	typedef std::pair<EventCallback, void*> EventCallbackPair;
	typedef astd::vector<EventCallbackPair> EventCallbacks;

	using UpdateFunction = void (ApplicationEventRunner::*)();

	EventCallbacks		m_event_callbacks;
	uint32_t			m_frame_rate;
	UpdateFunction		m_update_functions[2];
	UpdateType			m_update_type;
	uint32_t			m_last_time;
};

}

#endif // APPLICATIONEVENTRUNNER_H
