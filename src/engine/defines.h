/*
   Acclution
   Copyright (c) 2016 Christoffer Lindqvist

   This software is provided 'as-is', without any express or implied
   warranty. In no event will the authors be held liable for any
   damages arising from the use of this software.

   Permission is granted to anyone to use this software for any
   purpose, including commercial applications, and to alter it and
   redistribute it freely, subject to the following restrictions:

   1. The origin of this software must not be misrepresented; you
      must not claim that you wrote the original software. If you use
      this software in a product, an acknowledgment in the product
      documentation would be appreciated but is not required.

   2. Altered source versions must be plainly marked as such, and
      must not be misrepresented as being the original software.

   3. This notice may not be removed or altered from any source
      distribution.

   Christoffer Lindqvist
   swepopper@gmail.com
*/

#ifndef DEFINES
#define DEFINES

#if defined(WIN32) || defined(_WIN32) || defined(WIN64) || defined(_WIN64)

#define PLATFORM_WINDOWS

#define WIN32_LEAN_AND_MEAN
#define _SCL_SECURE_NO_WARNINGS

#define Main() CALLBACK WinMain(HINSTANCE, HINSTANCE, LPSTR, int)
//#define Main() main()

#define INLINE              __inline
#define FORCE_INLINE		__forceinline

// visual studio compiler dont have a likely
#define LIKELY(x) x
#define UNLIKELY(x) x

#define REGISTER			register

#define BREAKPOINT			__debugbreak()

#define THREADLOCAL			__declspec(thread)

#define EXPORT				__declspec(dllexport)
#define IMPORT				__declspec(dllimport)

#define FUNC_CALL			__cdecl

#define DIRECTORY_SEPARATOR '\\'

#define DEPRECATED __declspec(deprecated)

// Using std::min/max instead
#define NOMINMAX

#elif __linux__

#define PLATFORM_LINUX

#define Main() main(int, char**)

#define INLINE				__inline
#define FORCE_INLINE		__inline

#define LIKELY(x) x
#define UNLIKELY(x) x

#define REGISTER

#define BREAKPOINT

#define THREADLOCAL

#define EXPORT				__attribute__((visibility("default")))
#define IMPORT

#define FUNC_CALL

#define DIRECTORY_SEPARATOR '/'

#define DEPRECATED			__attribute__((deprecated))

#else
#error "Add new platform!"
#endif

#define __STDC_WANT_LIB_EXT1__ 1

#if defined(NDEBUG)
#define BUILD_TYPE_RELEASE
#else
#define BUILD_TYPE_DEBUG
#endif

#define USE_DEBUG_SERVER

#if INTPTR_MAX == INT32_MAX
	#define PLATFORM_32BIT
#elif INTPTR_MAX >= INT64_MAX
	#define PLATFORM_64BIT
#else
	#error "Environment not 32 or 64-bit."
#endif

#define MAKE_VERSION(major, minor, patch) (((major) << 22) | ((minor) << 12) | (patch))
#define VERSION_MAJOR(version) ((uint32_t)(version) >> 22)
#define VERSION_MINOR(version) (((uint32_t)(version) >> 12) & 0x3ff)
#define VERSION_PATCH(version) ((uint32_t)(version) & 0xfff)

#define BUF_SIZE 8192
#define UUID_SIZE 37
#define STRINGIFY(arg) #arg
#define NO_COPY(type)								\
	type(const type&) = delete;						\
	type& operator=(const type&) = delete;			\
	type& operator=(const type&) volatile = delete

#define ASTD_ARRAY_COUNT(arr) (sizeof(arr) / sizeof(arr[0]))
#define ASTD_UNUSED(var) (void)var

// Seems like is_trivially_copyable is not implemented in my compiler..
#define ASSERT_ON_NON_TRIVIAL(T) static_assert(std::is_trivial<T>::value, "Must be a trivial object")

#ifdef _SDL_error_h
#define CheckSDLError()								\
	do {											\
		const char *error = SDL_GetError();			\
		if (*error != '\0') {						\
			LogError("SDL Error: %s", error);		\
			SDL_ClearError();						\
		}											\
	} while(false)
#else
#define CheckSDLError()	(void)0
#endif

#define CheckGLError()													\
	do {																\
		CheckSDLError();												\
		uint32_t gl_error = GL_NO_ERROR;								\
		while((gl_error = glGetError()) != GL_NO_ERROR) {				\
			astd::string gl_error_string = "";							\
			switch(gl_error) {											\
			case GL_INVALID_ENUM: gl_error_string = "GL_INVALID_ENUM"; break;			\
			case GL_INVALID_VALUE: gl_error_string = "GL_INVALID_VALUE"; break;			\
			case GL_INVALID_OPERATION: gl_error_string = "GL_INVALID_OPERATION"; break;	\
			case GL_STACK_OVERFLOW: gl_error_string = "GL_STACK_OVERFLOW"; break;		\
			case GL_STACK_UNDERFLOW: gl_error_string = "GL_STACK_UNDERFLOW"; break;		\
			case GL_OUT_OF_MEMORY: gl_error_string = "GL_OUT_OF_MEMORY"; break;			\
			default:  gl_error_string = "UNKNOWN";										\
			}																			\
			ALogError("OpenGL Error: %i: %s\n", gl_error, gl_error_string.c_str());		\
		}																				\
	} while(false)

enum class Types
{
	kByte,
	kUByte,
	kShort,
	kUShort,
	kInt,
	kUInt,
	kFloat,
	kDouble,
	kMatrix
};

enum class Modes
{
	kPoints,
	kLines,
	kLineLoop,
	kLineStrip,
	kTriangles,
	kTriangleStrip,
	kTriangleFan,
	kQuads,
	kQuadStrip,
	kPolygon
};

enum class UpdateType
{
	kEventBased = 0,
	kFrameBased
};

#endif // DEFINES

