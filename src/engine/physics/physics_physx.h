/*
   Acclution
   Copyright (c) 2016 Christoffer Lindqvist

   This software is provided 'as-is', without any express or implied
   warranty. In no event will the authors be held liable for any
   damages arising from the use of this software.

   Permission is granted to anyone to use this software for any
   purpose, including commercial applications, and to alter it and
   redistribute it freely, subject to the following restrictions:

   1. The origin of this software must not be misrepresented; you
      must not claim that you wrote the original software. If you use
      this software in a product, an acknowledgment in the product
      documentation would be appreciated but is not required.

   2. Altered source versions must be plainly marked as such, and
      must not be misrepresented as being the original software.

   3. This notice may not be removed or altered from any source
      distribution.

   Christoffer Lindqvist
   swepopper@gmail.com
*/

#ifndef PHYSICSPHYSX_H
#define PHYSICSPHYSX_H
#pragma once

#if defined(USE_PHYSX)

namespace physx
{
class PxPhysics;
class PxDefaultAllocator;
class PxErrorCallback;
class PxFoundation;
class PxDefaultCpuDispatcher;
class PxScene;
namespace debugger { namespace comm { class PvdConnection; } }
}

namespace Acclution { namespace detail {

class PhysicsPhysX
{
public:
	PhysicsPhysX();

	bool Initialize();
	void Release();

	void Step();
	void WaitAndSync();

private:
	physx::PxDefaultAllocator*				m_allocator;
	physx::PxPhysics*						m_physics;
	physx::PxFoundation*					m_foundation;
	physx::PxDefaultCpuDispatcher*			m_dispatcher;
	physx::PxScene*							m_scene;

	physx::PxErrorCallback*					m_error_callback;
	physx::debugger::comm::PvdConnection*	m_connection;
};

}}

#endif

#endif // PHYSICSPHYSX_H
