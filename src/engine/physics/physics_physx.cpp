/*
   Acclution
   Copyright (c) 2016 Christoffer Lindqvist

   This software is provided 'as-is', without any express or implied
   warranty. In no event will the authors be held liable for any
   damages arising from the use of this software.

   Permission is granted to anyone to use this software for any
   purpose, including commercial applications, and to alter it and
   redistribute it freely, subject to the following restrictions:

   1. The origin of this software must not be misrepresented; you
      must not claim that you wrote the original software. If you use
      this software in a product, an acknowledgment in the product
      documentation would be appreciated but is not required.

   2. Altered source versions must be plainly marked as such, and
      must not be misrepresented as being the original software.

   3. This notice may not be removed or altered from any source
      distribution.

   Christoffer Lindqvist
   swepopper@gmail.com
*/

#if defined(USE_PHYSX)

#include "engine/platform.h"
#include "physics_physx.h"

#include <PxPhysicsAPI.h>

namespace Acclution { namespace detail {

class ErrorHandler : public physx::PxErrorCallback
{
public:
#if !defined(NO_LOGGING)
	virtual void reportError(physx::PxErrorCode::Enum code, const char* message, const char* file, int line)
	{
		if(code == physx::PxErrorCode::eNO_ERROR)
			return;

		Log::LogSeverity sev = Log::LogSeverity::kInfo;
		switch(code)
		{
		case physx::PxErrorCode::eDEBUG_WARNING:
		case physx::PxErrorCode::ePERF_WARNING:
			sev = Log::LogSeverity::kWarning; break;

		case physx::PxErrorCode::eINVALID_PARAMETER:
		case physx::PxErrorCode::eINVALID_OPERATION:
		case physx::PxErrorCode::eOUT_OF_MEMORY:
		case physx::PxErrorCode::eINTERNAL_ERROR:
			sev = Log::LogSeverity::kError; break;

		case physx::PxErrorCode::eABORT:
		case physx::PxErrorCode::eMASK_ALL:
			sev = Log::LogSeverity::kFatal; break;

		case physx::PxErrorCode::eDEBUG_INFO:
		default: sev = Log::LogSeverity::kInfo; break;
		}

		Log::LogMessage(file, line, sev, "PhysX: %s", message);
	}
#else
	virtual void reportError(physx::PxErrorCode::Enum, const char*, const char*, int) {}
#endif
};

PhysicsPhysX::PhysicsPhysX()
	: m_allocator(NULL),
	  m_physics(NULL),
	  m_foundation(NULL),
	  m_dispatcher(NULL),
	  m_scene(NULL),
	  m_error_callback(NULL),
	  m_connection(NULL)
{
}

bool PhysicsPhysX::Initialize()
{
	m_allocator = new physx::PxDefaultAllocator;
	m_error_callback = new ErrorHandler;
	if(m_allocator == NULL || m_error_callback == NULL)
	{
		return false;
	}

	m_foundation = PxCreateFoundation(PX_PHYSICS_VERSION, *m_allocator, *m_error_callback);
	if(m_foundation == NULL)
	{
		return false;
	}

	physx::PxProfileZoneManager* profile_zone_manager = &physx::PxProfileZoneManager::createProfileZoneManager(m_foundation);
	if(profile_zone_manager == NULL)
	{
		return false;
	}

	m_physics = PxCreatePhysics(PX_PHYSICS_VERSION, *m_foundation, physx::PxTolerancesScale(), true, profile_zone_manager);
	if(m_physics == NULL)
	{
		profile_zone_manager->release();
		return false;
	}

	if(m_physics->getPvdConnectionManager())
	{
		m_physics->getVisualDebugger()->setVisualizeConstraints(true);
		m_physics->getVisualDebugger()->setVisualDebuggerFlag(physx::PxVisualDebuggerFlag::eTRANSMIT_CONTACTS, true);
		m_physics->getVisualDebugger()->setVisualDebuggerFlag(physx::PxVisualDebuggerFlag::eTRANSMIT_SCENEQUERIES, true);
		m_connection = physx::PxVisualDebuggerExt::createConnection(m_physics->getPvdConnectionManager(), "127.0.0.1", 5425, 10);
	}

	m_dispatcher = physx::PxDefaultCpuDispatcherCreate(2);
	if(m_dispatcher == NULL)
	{
		return false;
	}

	physx::PxSceneDesc scene_desc(m_physics->getTolerancesScale());
	scene_desc.gravity = physx::PxVec3(0.0f, -9.81f, 0.0f);
	scene_desc.cpuDispatcher	= m_dispatcher;
	scene_desc.filterShader		= physx::PxDefaultSimulationFilterShader;
	m_scene = m_physics->createScene(scene_desc);

	return m_scene != NULL;
}

void PhysicsPhysX::Release()
{
	if(m_scene != NULL)
	{
		m_scene->fetchResults(true);
		m_scene->release();
		m_scene = NULL;
	}

	if(m_dispatcher != NULL)
	{
		m_dispatcher->release();
		m_dispatcher = NULL;
	}

	if(m_connection != NULL)
	{
		m_connection->release();
		m_connection = NULL;
	}

	if(m_physics != NULL)
	{
		physx::PxProfileZoneManager* profile_zone_manager = m_physics->getProfileZoneManager();
		m_physics->release();
		profile_zone_manager->release();
		m_physics = NULL;
	}

	if(m_foundation != NULL)
	{
		m_foundation->release();
		m_foundation = NULL;
	}

	delete m_error_callback;
	delete m_allocator;
	m_allocator = NULL;
	m_error_callback = NULL;
}

void PhysicsPhysX::Step()
{
	m_scene->simulate(1.0f/60.0f);
}

void PhysicsPhysX::WaitAndSync()
{
	m_scene->fetchResults(true);
}

}}

#endif
