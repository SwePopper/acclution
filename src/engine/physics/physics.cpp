/*
   Acclution
   Copyright (c) 2016 Christoffer Lindqvist

   This software is provided 'as-is', without any express or implied
   warranty. In no event will the authors be held liable for any
   damages arising from the use of this software.

   Permission is granted to anyone to use this software for any
   purpose, including commercial applications, and to alter it and
   redistribute it freely, subject to the following restrictions:

   1. The origin of this software must not be misrepresented; you
      must not claim that you wrote the original software. If you use
      this software in a product, an acknowledgment in the product
      documentation would be appreciated but is not required.

   2. Altered source versions must be plainly marked as such, and
      must not be misrepresented as being the original software.

   3. This notice may not be removed or altered from any source
      distribution.

   Christoffer Lindqvist
   swepopper@gmail.com
*/

#include "engine/platform.h"
#include "physics.h"

#if defined(USE_PHYSX)
#include "physics_physx.h"
#elif defined(USE_BULLET)
#include "physics_bullet.h"
#else
#include "physics_null.h"
#endif

namespace Acclution {

Physics::Physics()
	: m_impl(nullptr)
{
}

bool Physics::Initialize()
{
	m_impl = new detail::PhysicsImpl;
	return m_impl != nullptr && m_impl->Initialize();
}

void Physics::Release()
{
	if(m_impl != nullptr)
	{
		m_impl->Release();
		delete m_impl;
		m_impl = nullptr;
	}
}

void Physics::Step()
{
	if(m_impl != nullptr)
	{
		m_impl->Step();
	}
}

void Physics::WaitAndSync()
{
	if(m_impl != nullptr)
	{
		m_impl->WaitAndSync();
	}
}

}
