/*
   Acclution
   Copyright (c) 2016 Christoffer Lindqvist

   This software is provided 'as-is', without any express or implied
   warranty. In no event will the authors be held liable for any
   damages arising from the use of this software.

   Permission is granted to anyone to use this software for any
   purpose, including commercial applications, and to alter it and
   redistribute it freely, subject to the following restrictions:

   1. The origin of this software must not be misrepresented; you
      must not claim that you wrote the original software. If you use
      this software in a product, an acknowledgment in the product
      documentation would be appreciated but is not required.

   2. Altered source versions must be plainly marked as such, and
      must not be misrepresented as being the original software.

   3. This notice may not be removed or altered from any source
      distribution.

   Christoffer Lindqvist
   swepopper@gmail.com
*/

#include "engine/platform.h"
#include "frame_time.h"

namespace Acclution
{

FrameTime::FrameTime()
		: m_current_time(std::chrono::high_resolution_clock::now()),
		  m_delta_time(0.0),
		  m_delta_time_nano(0),
		  m_smoothed_delta_time(0.0),
		  m_smoothed_delta_time_nano(0)
{
}

void FrameTime::Update(std::chrono::high_resolution_clock::time_point& last_time, int64_t last_smoothed_delta_time_nano)
{
	m_current_time = std::chrono::high_resolution_clock::now();
	m_delta_time_nano = (m_current_time - last_time).count();
	m_delta_time = (double)m_delta_time_nano / 1000000000.0;
	last_time = m_current_time;

	// create smoothed delta time
	// http://www.camelsoftware.com/firetail/blog/uncategorized/scalar-low-pass-filters-a-comparison-of-algorithms/
	// using Brown’s Simple Exponential filter
	// smoothed = alpha * measurement + (1 - alpha) * smoothed;
	m_smoothed_delta_time_nano = m_delta_time_nano / 2 + last_smoothed_delta_time_nano / 2;
	m_smoothed_delta_time = (double)m_smoothed_delta_time_nano / 1000000000.0;
}

}
