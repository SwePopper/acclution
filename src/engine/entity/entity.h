/*
   Acclution
   Copyright (c) 2016 Christoffer Lindqvist

   This software is provided 'as-is', without any express or implied
   warranty. In no event will the authors be held liable for any
   damages arising from the use of this software.

   Permission is granted to anyone to use this software for any
   purpose, including commercial applications, and to alter it and
   redistribute it freely, subject to the following restrictions:

   1. The origin of this software must not be misrepresented; you
      must not claim that you wrote the original software. If you use
      this software in a product, an acknowledgment in the product
      documentation would be appreciated but is not required.

   2. Altered source versions must be plainly marked as such, and
      must not be misrepresented as being the original software.

   3. This notice may not be removed or altered from any source
      distribution.

   Christoffer Lindqvist
   swepopper@gmail.com
*/

#ifndef ENTITY_H
#define ENTITY_H
#pragma once

namespace Acclution {

class Component;

class Entity
{
public:
	Entity();
	virtual ~Entity();

	Entity* GetParent() const { return m_parent; }
	void SetParent(Entity* parent);

	const astd::string& GetName() const { return m_name; }
	void SetName(const char* name) { m_name = name; }

	bool IsEnabled() const { return m_enabled; }
	void SetEnabled(bool enable) { m_enabled = enable; }

	void AddComponent(Component* component);
	void RemoveComponent(Component* component);

private:
	typedef astd::vector<Component*> ComponentContainer;

	astd::string			m_name;
	bool					m_enabled;
	ComponentContainer		m_components;

	Entity*					m_parent;

	typedef astd::vector<Entity*> EntityContainer;
	EntityContainer			m_children;
};

}

#endif // ENTITY_H
