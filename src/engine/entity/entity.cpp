/*
   Acclution
   Copyright (c) 2016 Christoffer Lindqvist

   This software is provided 'as-is', without any express or implied
   warranty. In no event will the authors be held liable for any
   damages arising from the use of this software.

   Permission is granted to anyone to use this software for any
   purpose, including commercial applications, and to alter it and
   redistribute it freely, subject to the following restrictions:

   1. The origin of this software must not be misrepresented; you
      must not claim that you wrote the original software. If you use
      this software in a product, an acknowledgment in the product
      documentation would be appreciated but is not required.

   2. Altered source versions must be plainly marked as such, and
      must not be misrepresented as being the original software.

   3. This notice may not be removed or altered from any source
      distribution.

   Christoffer Lindqvist
   swepopper@gmail.com
*/

#include "engine/platform.h"
#include "entity.h"
#include "engine/components/component.h"

namespace Acclution {

Entity::Entity()
	: m_enabled(true),
	  m_parent()
{
}

Entity::~Entity()
{
	for(EntityContainer::iterator it = m_children.begin(); it != m_children.end(); ++it)
	{
		delete *it;
	}
	m_children.clear();

	for(ComponentContainer::iterator it = m_components.begin(); it != m_components.end(); ++it)
	{
		delete *it;
	}
	m_components.clear();
}

void Entity::SetParent(Entity* parent)
{
	if(m_parent != nullptr)
	{
		astd::EraseRemove(m_parent->m_children, this);
	}
	m_parent = parent;
	if(m_parent != nullptr)
	{
		m_parent->m_children.push_back(this);
	}
}

void Entity::AddComponent(Component* component)
{
	if(!m_components.empty() && std::find(m_components.begin(), m_components.end(), component) != m_components.end())
		return;
	m_components.push_back(component);
}

void Entity::RemoveComponent(Component* component)
{
	if(m_components.empty())
		return;
	astd::EraseRemove(m_components, component);
}

}
