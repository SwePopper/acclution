/*
   Acclution
   Copyright (c) 2016 Christoffer Lindqvist

   This software is provided 'as-is', without any express or implied
   warranty. In no event will the authors be held liable for any
   damages arising from the use of this software.

   Permission is granted to anyone to use this software for any
   purpose, including commercial applications, and to alter it and
   redistribute it freely, subject to the following restrictions:

   1. The origin of this software must not be misrepresented; you
      must not claim that you wrote the original software. If you use
      this software in a product, an acknowledgment in the product
      documentation would be appreciated but is not required.

   2. Altered source versions must be plainly marked as such, and
      must not be misrepresented as being the original software.

   3. This notice may not be removed or altered from any source
      distribution.

   Christoffer Lindqvist
   swepopper@gmail.com
*/

#ifndef SERIALIZER_H
#define SERIALIZER_H
#pragma once

namespace Acclution {

class Serializer
{
public:
	Serializer() : m_data(nullptr), m_data_size(0), m_write_position(0) {}
	~Serializer() { Clear(); }

	template<typename T>
	bool Serialize(const T& value)
	{
		ASSERT_ON_NON_TRIVIAL(T);

		uint32_t size = sizeof(T);
		uint8_t* ptr = Grow(size);
		if(ptr == nullptr)
			return false;
		Serialize<T>(value, ptr, size);
		return true;
	}

	template<typename T>
	bool Serialize(const T* data, uint32_t count)
	{
		ASSERT_ON_NON_TRIVIAL(T);

		uint32_t type_size = sizeof(T);
		uint32_t size = type_size * count;
		uint8_t* ptr = Grow(size);
		if(ptr == nullptr)
			return false;
		for(uint32_t i = 0; i < count; ++i)
		{
			Serialize<T>(data[i], ptr, type_size);
			ptr += type_size;
		}
		return true;
	}

	void ResetWrite() { m_write_position = 0; }
	void Clear();

	const uint64_t& GetWritePosition() const { return m_write_position; }
	const uint64_t& GetDataSize() const { return m_data_size; }
	const void* GetData() const { return m_data; }

	bool Serialize(const bool& value)
	{
		int8_t real_value = value ? 1 : 0;
		return Serialize(real_value);
	}

private:
	uint8_t* Grow(const uint32_t& size);

	template<typename T>
	void Serialize(const T& from, uint8_t* to, uint32_t size)
	{
		ASSERT_ON_NON_TRIVIAL(T);

		uint8_t* ptr = (uint8_t*)&from;
		for(uint32_t i = 0; i < size; ++i)
		{
			to[i] = ptr[i];
		}
	}

	uint8_t*		m_data;
	uint64_t		m_data_size;

	uint64_t		m_write_position;
};

}

#endif // SERIALIZER_H
