#ifndef READTASK_H
#define READTASK_H
#pragma once

#include "deserializer.h"

namespace Acclution
{
namespace IO
{

namespace detail
{

class ReadDataStream
{
public:
	typedef std::basic_ifstream<char> ReadFileStream;

	ReadDataStream()
		: m_file(),
		  m_file_size(0),
		  m_start_offset(0),
		  m_read_buffer_size(0),
		  m_read_buffer(nullptr),
		  m_read_amount(0),
		  m_read_position(0)
	{}
	~ReadDataStream() { Release(); }

	bool Initialize(astd::string path, bool binary, uint64_t start_offset, uint64_t end_offset, uint64_t stream_size);
	void Release();

	void Reset();
	void Skip(const uint64_t& size);
	uint8_t* Read(const uint64_t& size);
	bool Read(void* to, uint64_t size);

	bool IsEOF() { return m_file.eof() && m_read_position >= m_read_amount; }
	uint8_t* GetData();
	const uint64_t& GetDataSize() const { return m_read_amount; }
	void StreamNext();

private:
	ReadFileStream		m_file;
	uint64_t			m_file_size;
	uint64_t			m_start_offset;

	uint64_t			m_read_buffer_size;
	uint8_t*			m_read_buffer;

	uint64_t			m_read_amount;
	uint64_t			m_read_position;
};

}

using FileDeserializer = Deserializer<detail::ReadDataStream>;

template<typename Function>
static std::future<bool> ReadFile(Function callback, astd::string path, bool binary = true, uint64_t start_offset = 0, uint64_t end_offset = 0, uint64_t stream_size = BUF_SIZE)
{
	return std::async(std::launch::async, [callback, path, binary, start_offset, end_offset, stream_size]{
		detail::ReadDataStream stream;
		if(!stream.Initialize(path, binary, start_offset, end_offset, stream_size))
			return false;

		FileDeserializer deserializer(&stream);
		deserializer.GetData(); // pre get some data
		return callback(&deserializer);
	});
}

template<typename Function, typename Object>
static std::future<bool> ReadFile(Function callback, Object object, astd::string path, bool binary = true, uint64_t start_offset = 0, uint64_t end_offset = 0, uint64_t stream_size = BUF_SIZE)
{
	return ReadFile(std::bind(callback, object, std::placeholders::_1), path, binary, start_offset, end_offset, stream_size);
}

template<typename Function, typename Object, typename UserData>
static std::future<bool> ReadFile(Function callback, Object object, UserData data, astd::string path, bool binary = true, uint64_t start_offset = 0, uint64_t end_offset = 0, uint64_t stream_size = BUF_SIZE)
{
	return ReadFile(std::bind(callback, object, std::placeholders::_1, data), path, binary, start_offset, end_offset, stream_size);
}

using XMLStartElement = void (*)(void* user_data, const char* name, const char** atts);
using XMLEndElement = void (*)(void* user_data, const char* name);
using XMLCharacterData = void (*)(void* user_data, const char* s, int lenght);

std::future<bool> ReadXml(void* user_data, XMLStartElement start_element, XMLEndElement end_element, XMLCharacterData char_data, astd::string path, uint64_t start_offset = 0, uint64_t end_offset = 0, uint64_t stream_size = BUF_SIZE);

}
}

#endif // READTASK_H
