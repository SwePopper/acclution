/*
   Acclution
   Copyright (c) 2016 Christoffer Lindqvist

   This software is provided 'as-is', without any express or implied
   warranty. In no event will the authors be held liable for any
   damages arising from the use of this software.

   Permission is granted to anyone to use this software for any
   purpose, including commercial applications, and to alter it and
   redistribute it freely, subject to the following restrictions:

   1. The origin of this software must not be misrepresented; you
      must not claim that you wrote the original software. If you use
      this software in a product, an acknowledgment in the product
      documentation would be appreciated but is not required.

   2. Altered source versions must be plainly marked as such, and
      must not be misrepresented as being the original software.

   3. This notice may not be removed or altered from any source
      distribution.

   Christoffer Lindqvist
   swepopper@gmail.com
*/

#include "engine/platform.h"
#include "serializer.h"

namespace Acclution {

uint8_t* Serializer::Grow(const uint32_t& size)
{
	if(m_write_position+size > m_data_size)
	{
		// Allocate more memory
		m_data = (uint8_t*)realloc(m_data, m_data_size + 4096);
		if(m_data == nullptr)
		{
			m_data_size = 0;
			return nullptr;
		}

		m_data_size += 4096;
	}

	uint8_t* ptr = m_data + m_write_position;
	m_write_position += size;
	return ptr;
}

void Serializer::Clear()
{
	m_write_position = 0;
	free(m_data);
	m_data = nullptr;
	m_data_size = 0;
}

}
