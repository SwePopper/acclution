#ifndef DESERIALIZER_H
#define DESERIALIZER_H
#pragma once

namespace Acclution
{
namespace IO
{

namespace detail
{

class ReadMemoryStream
{
public:
	ReadMemoryStream(uint8_t* memory, uint64_t size) : m_data(memory), m_data_size(size), m_read_position(0) {}

	uint8_t* Read(uint64_t size)
	{
		if(m_read_position+size > m_data_size)
		{
			return nullptr;
		}

		uint8_t* data = &m_data[m_read_position];
		m_read_position += size;
		return data;
	}

	void Skip(uint64_t size)
	{
		if(m_read_position+size > m_data_size)
		{
			m_read_position = m_data_size;
		}
		else
		{
			m_read_position += size;
		}
	}

	void Reset() { m_read_position = 0; }
	bool IsEOF() const { return m_read_position >= m_data_size; }

	uint8_t* GetData() const { return m_data; }
	const uint64_t& GetDataSize() const { return m_data_size; }
	const uint64_t& GetReadPosition() const { return m_read_position; }

	void StreamNext() {}

private:
	uint8_t*		m_data;
	uint64_t		m_data_size;
	uint64_t		m_read_position;
};

}

template<typename DataStream>
class Deserializer
{
public:
	Deserializer(DataStream* stream) : m_stream(stream) {}
	~Deserializer() { }

	void Reset() { m_stream->Reset(); }

	template<typename T>
	bool Deserialize(T& value)
	{
		ASSERT_ON_NON_TRIVIAL(T);

		uint64_t size = sizeof(T);
		uint8_t* read = m_stream->Read(size);
		if(read == nullptr)
			return false;
		Deserialize<T>(read, value, size);
		return true;
	}

	template<typename T>
	bool Deserialize(T* data, uint64_t count)
	{
		ASSERT_ON_NON_TRIVIAL(T);

		uint64_t type_size = sizeof(T);
		uint64_t size = type_size * count;
		uint8_t* ptr = m_stream->Read(size);
		if(ptr == nullptr)
			return false;
		for(uint64_t i = 0; i < count; ++i)
		{
			Deserialize<T>(ptr, data[i], type_size);
			ptr += type_size;
		}
		return true;
	}

	template<typename T>
	void SkipDeserialize(const uint64_t& count)
	{
		ASSERT_ON_NON_TRIVIAL(T);
		m_stream->Skip(sizeof(T) * count);
	}

	bool Read(void* to, uint64_t size)
	{
		return m_stream->Read(to, size);
	}

	bool IsEOF() { return m_stream->IsEOF(); }
	uint8_t* GetData() { return m_stream->GetData(); }
	const uint64_t& GetDataSize() const { return m_stream->GetDataSize(); }
	const uint64_t& GetReadPosition() const { return m_stream->GetReadPosition(); }
	void StreamNext() { m_stream->StreamNext(); }

	bool Deserialize(glm::mat4& value)
	{
		for (int i = 0; i < 4; ++i)
		{
			for (int j = 0; j < 4; ++j)
			{
				if (!Deserialize(value[i][j]))
					return false;
			}
		}
		return true;
	}

	bool Deserialize(glm::vec3& value)
	{
		if (!Deserialize(value[0]))
			return false;
		if (!Deserialize(value[1]))
			return false;
		if (!Deserialize(value[2]))
			return false;
		return true;
	}

	bool Deserialize(glm::vec4& value)
	{
		if (!Deserialize(value[0]))
			return false;
		if (!Deserialize(value[1]))
			return false;
		if (!Deserialize(value[2]))
			return false;
		if (!Deserialize(value[3]))
			return false;
		return true;
	}

	bool Deserialize(bool& value)
	{
		int8_t temp = 0;
		if (!Deserialize(temp))
			return false;
		value = temp != 0;
		return true;
	}

	bool Deserialize(astd::string& value)
	{
		uint64_t string_size = 0;
		if(!Deserialize(string_size))
			return false;
		value.resize(string_size, ' ');
		if(string_size == 0)
			return true;

		return Deserialize(value.data(), string_size);
	}

	template<typename T>
	bool Deserialize(astd::vector<T>& value)
	{
		uint64_t size = 0;
		if(!Deserialize(size))
			return false;
		value.resize(size);

		for(uint64_t i = 0; i < size; ++i)
		{
			if(!Deserialize(value[i]))
				return false;
		}
		return true;
	}

private:
	template<typename T>
	void Deserialize(const uint8_t* from, T& to, uint64_t size)
	{
		ASSERT_ON_NON_TRIVIAL(T);

		uint8_t* ptr = (uint8_t*)&to;
		for(uint64_t i = 0; i < size; ++i)
		{
			ptr[i] = from[i];
		}
	}

	DataStream*		m_stream;
};

}
}

#endif // DESERIALIZER_H
