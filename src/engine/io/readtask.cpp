#include "engine/platform.h"
#include "readtask.h"

#include <expat.h>

#ifdef XML_LARGE_SIZE
#if defined(XML_USE_MSC_EXTENSIONS) && _MSC_VER < 1400
#define XML_FMT_INT_MOD "I64"
#else
#define XML_FMT_INT_MOD "ll"
#endif
#else
#define XML_FMT_INT_MOD "l"
#endif

namespace Acclution
{
namespace IO
{

namespace detail
{

bool ReadDataStream::Initialize(astd::string path, bool binary, uint64_t start_offset, uint64_t end_offset, uint64_t stream_size)
{
	std::ios_base::openmode mode = std::ios_base::in;
	if(binary)
		mode |= std::ios_base::binary;

	m_file.open(path.c_str(), mode);
	if(!m_file.is_open() || m_file.fail())
	{
		ALogError("Failed to open %s", path.c_str());
		return false;
	}

	if(end_offset == 0)
	{
		m_file.seekg(0, m_file.end);
		m_file_size = static_cast<uint64_t>(m_file.tellg());
		m_start_offset = start_offset;
	}
	else
	{
		m_file_size = end_offset;
		m_start_offset = start_offset;
	}
	m_file.seekg(static_cast<long>(m_start_offset), m_file.beg);

	m_read_buffer_size = stream_size == 0 ? m_file_size : stream_size;
	m_read_buffer = new uint8_t[m_read_buffer_size+1];

	m_read_amount = m_read_position = 0;
	return true;
}

void ReadDataStream::Release()
{
	delete[] m_read_buffer;
	m_read_buffer = nullptr;
	if(m_file.is_open())
		m_file.close();
	m_read_position = m_read_amount = m_start_offset = m_read_buffer_size = m_file_size = 0;
}

void ReadDataStream::Reset()
{
	m_file.seekg(static_cast<long>(m_start_offset), m_file.beg);
	m_read_amount = m_read_position = 0;
}

void ReadDataStream::Skip(const uint64_t& size)
{
	if(m_read_position+size > m_read_amount)
	{
		m_file.seekg(static_cast<long>(size - (m_read_amount - m_read_position)), m_file.cur);
		m_read_amount = m_read_position = 0;
	}
	else
	{
		m_read_position += size;
	}
}

uint8_t* ReadDataStream::Read(const uint64_t& size)
{
	if(m_read_position+size > m_read_amount)
	{
		// We are out of data. Move the remaining data to the begining if the read buffer
		// and move the buffer pointer forward so that when we read from file next time
		// the new data is after the data that is left.

		uint64_t data_left = m_read_amount - m_read_position;

		// copy the remaining data to begining of the buffer
		if(data_left > 0)
			std::copy(&m_read_buffer[m_read_position], &m_read_buffer[m_read_buffer_size], m_read_buffer);

		// Read the data
		m_file.read((char*)&m_read_buffer[data_left], static_cast<long>(m_read_buffer_size - data_left - 1));
		if(!m_file.eof() && m_file.fail())
			return nullptr;

		m_read_amount = static_cast<uint64_t>(m_file.gcount()) + data_left;
		m_read_position = 0;
		m_read_buffer[m_read_amount+1] = '\0';
	}

	uint8_t* data = &m_read_buffer[m_read_position];
	m_read_position += size;
	return data;
}

bool ReadDataStream::Read(void* to, uint64_t size)
{
	if(m_read_position+size > m_read_amount)
	{
		uint64_t data_left = m_read_amount - m_read_position;
		memcpy(to, &m_read_buffer[m_read_position], data_left);
		m_read_position = m_read_amount;

		m_file.read(&((char*)to)[data_left], static_cast<long>(size - data_left));
		if(!m_file.eof() && m_file.fail())
			return false;
	}
	else
	{
		memcpy(to, &m_read_buffer[m_read_position], size);
		m_read_position += size;
	}
	return true;
}

uint8_t* ReadDataStream::GetData()
{
	if(m_read_amount == 0)
	{
		StreamNext();
	}
	return m_read_buffer;
}

void ReadDataStream::StreamNext()
{
	m_file.read((char*)m_read_buffer, static_cast<long>(m_read_buffer_size));
	m_read_amount = static_cast<uint64_t>(m_file.gcount());
	m_read_buffer[m_read_amount] = 0;
}

}

std::future<bool> ReadXml(void* user_data, XMLStartElement start_element, XMLEndElement end_element, XMLCharacterData char_data, astd::string path, uint64_t start_offset, uint64_t end_offset, uint64_t stream_size)
{
	return std::async(std::launch::async, [user_data, start_element, end_element, char_data, path, start_offset, end_offset, stream_size]{
		detail::ReadDataStream stream;
		if(!stream.Initialize(path, false, start_offset, end_offset, stream_size))
			return false;

		FileDeserializer deserializer(&stream);

		XML_Parser parser = XML_ParserCreate(nullptr);
		if(parser == nullptr)
		{
			ALogError("Failed to create xml parser!");
			return false;
		}

		XML_SetElementHandler(parser, start_element, end_element);
		if(char_data != nullptr)
			XML_SetCharacterDataHandler(parser, char_data);
		XML_SetUserData(parser, user_data);

		deserializer.GetData();
		deserializer.SkipDeserialize<uint8_t>(deserializer.GetDataSize());

		int done = 0;
		while(done == 0)
		{
			done = deserializer.IsEOF();
			if(XML_Parse(parser, (const char*)deserializer.GetData(), (int)deserializer.GetDataSize(), done) == XML_STATUS_ERROR)
			{
				ALogError("XML ERROR: %s at line %" XML_FMT_INT_MOD "u",
						 XML_ErrorString(XML_GetErrorCode(parser)),
						 XML_GetCurrentLineNumber(parser));
				XML_ParserFree(parser);
				return false;
			}
		}

		XML_ParserFree(parser);
		return true;
	});
}

}
}
