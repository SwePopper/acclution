/*
   Acclution
   Copyright (c) 2016 Christoffer Lindqvist

   This software is provided 'as-is', without any express or implied
   warranty. In no event will the authors be held liable for any
   damages arising from the use of this software.

   Permission is granted to anyone to use this software for any
   purpose, including commercial applications, and to alter it and
   redistribute it freely, subject to the following restrictions:

   1. The origin of this software must not be misrepresented; you
      must not claim that you wrote the original software. If you use
      this software in a product, an acknowledgment in the product
      documentation would be appreciated but is not required.

   2. Altered source versions must be plainly marked as such, and
      must not be misrepresented as being the original software.

   3. This notice may not be removed or altered from any source
      distribution.

   Christoffer Lindqvist
   swepopper@gmail.com
*/

#ifndef UTILITIES_H
#define UTILITIES_H
#pragma once

#include "defines.h"

extern "C"
{
#if defined(PLATFORM_WINDOWS)
#include <rpc.h>
#elif defined(PLATFORM_LINUX)
#include <uuid/uuid.h>
#endif
}

namespace Acclution {

namespace Utilities
{

bool FileExist(const char* name);
bool DirExist(const char* name);
astd::string GetFileName(const astd::string& file_path);
astd::string GetDirectory(const astd::string& file_path);

void StackTrace(size_t count, char* trace, size_t trace_size);

template<typename T>
T Align(T value, uint32_t align)
{

	return ((value / align) + 1) * align;
}

// http://isthe.com/chongo/tech/comp/fnv/#FNV-param
static constexpr uint32_t kHash32Prime = 16777619u;
static constexpr uint32_t kHash32Offset = 2166136261u;
constexpr uint32_t Hash32(const char* s, uint32_t last_hash = kHash32Offset)
{
	return *s ? Hash32(s+1, static_cast<uint32_t>((static_cast<uint32_t>(*s) ^ last_hash) * static_cast<uint64_t>(kHash32Prime))) : last_hash;
}

#if defined(PLATFORM_WINDOWS)
typedef UUID Uuid;
#elif defined(PLATFORM_LINUX)
typedef uuid_t Uuid;
#endif

void GenerateUUID(Uuid& out);
void UUIDToString(Uuid& uuid, char* out, size_t size);
bool StringToUUID(const char* in, Uuid& uuid);
int CompareUUID(const Uuid& a, const Uuid& b);

}

}

#endif // UTILITIES_H
