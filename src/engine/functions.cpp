#include "engine/platform.h"
#include "functions.h"

#if defined(PLATFORM_WINDOWS)
#include <shellapi.h>
#endif

namespace Acclution
{

namespace astd
{

void GetCommandLine(astd::string* command_line, int& count)
{
#if defined(PLATFORM_WINDOWS)
	count = 0;
	LPWSTR* argvW = CommandLineToArgvW(GetCommandLineW(), &count);
	if(command_line != nullptr)
	{
		for(int i = 0; i < count; ++i)
		{
			size_t len = wcslen(argvW[i]);
			char* argv = new char[len+1];
			wcstombs(argv, argvW[i], len);
			argv[len] = '\0';

			command_line[i] = astd::string(argv);
			delete[] argv;
		}
	}
	LocalFree(argvW);
#else
	char data[BUF_SIZE];
	snprintf(data, BUF_SIZE, "/proc/%i/cmdline", getpid());

	FILE* f = fopen(data, "rb");
	if(f == nullptr)
		return;

	size_t read = fread(data, 1, BUF_SIZE, f);
	fclose(f);

	if(read == 0)
		return;

	if(command_line != nullptr)
	{
		size_t i = 0;
		do {
			*command_line = data[i];
			command_line++;
			for(; i < read; ++i)
			{
				if(data[i] == '\0')
					break;
			}
		} while(++i < read);
	}
	else
	{
		count = 0;
		for(size_t i = 0; i < read; ++i)
		{
			if(data[i] == '\0')
				count++;
		}
	}

#endif
}

}

}
