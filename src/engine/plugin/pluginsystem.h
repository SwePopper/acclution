/*
   Acclution
   Copyright (c) 2016 Christoffer Lindqvist

   This software is provided 'as-is', without any express or implied
   warranty. In no event will the authors be held liable for any
   damages arising from the use of this software.

   Permission is granted to anyone to use this software for any
   purpose, including commercial applications, and to alter it and
   redistribute it freely, subject to the following restrictions:

   1. The origin of this software must not be misrepresented; you
      must not claim that you wrote the original software. If you use
      this software in a product, an acknowledgment in the product
      documentation would be appreciated but is not required.

   2. Altered source versions must be plainly marked as such, and
      must not be misrepresented as being the original software.

   3. This notice may not be removed or altered from any source
      distribution.

   Christoffer Lindqvist
   swepopper@gmail.com
*/

#ifndef PLUGINSYSTEM_H
#define PLUGINSYSTEM_H
#pragma once

namespace Acclution {

class PluginSystem
{
public:
	class PluginApi;
	typedef void (*SetupPluginApi)(std::shared_ptr<PluginApi> api);

	class PluginApi
	{
	public:
		PluginApi() {}

	private:
	};

	void Release();

	bool LoadModule(const char* path);
	void UnloadModule(const char* path);

private:
	struct Module
	{
		Module() : m_module(nullptr) {}

		void*			m_module;
		SetupPluginApi	m_setup_plugin_api;
	};

	void* PlatformLoadModule(const char *path);
	void PlatformUnloadModule(void* module);
	void* PlatformGetSymbol(void* module, const char* symbol);

	typedef astd::map<astd::string, Module> ModuleContainer;
	ModuleContainer		m_modules;
};

}

#endif // PLUGINSYSTEM_H
