/*
   Acclution
   Copyright (c) 2016 Christoffer Lindqvist

   This software is provided 'as-is', without any express or implied
   warranty. In no event will the authors be held liable for any
   damages arising from the use of this software.

   Permission is granted to anyone to use this software for any
   purpose, including commercial applications, and to alter it and
   redistribute it freely, subject to the following restrictions:

   1. The origin of this software must not be misrepresented; you
      must not claim that you wrote the original software. If you use
      this software in a product, an acknowledgment in the product
      documentation would be appreciated but is not required.

   2. Altered source versions must be plainly marked as such, and
      must not be misrepresented as being the original software.

   3. This notice may not be removed or altered from any source
      distribution.

   Christoffer Lindqvist
   swepopper@gmail.com
*/

#include "engine/platform.h"
#include "pluginsystem.h"
#include "engine/utilities.h"

namespace Acclution {

void PluginSystem::Release()
{
	for(ModuleContainer::iterator it = m_modules.begin(); it != m_modules.end(); ++it)
	{
		Module& module = it->second;

		PlatformUnloadModule(module.m_module);
	}
	m_modules.clear();
}

bool PluginSystem::LoadModule(const char* path)
{
	if(path == nullptr || !Utilities::FileExist(path) || m_modules.find(astd::string(path)) == m_modules.end())
	{
		ALogError("Failed to load plugin: %s", path);
		return false;
	}

	Module module;
	module.m_module = PlatformLoadModule(path);
	if(module.m_module == nullptr)
	{
		ALogError("Failed to load plugin: %s", path);
		return false;
	}

	module.m_setup_plugin_api = (SetupPluginApi)PlatformGetSymbol(module.m_module, "SetupPluginApi");
	if(module.m_setup_plugin_api == nullptr)
	{
		ALogError("Failed to load plugin: %s", path);
		PlatformUnloadModule(module.m_module);
		return false;
	}

	module.m_setup_plugin_api(std::make_shared<PluginApi>());
	m_modules[astd::string(path)] = module;
	return true;
}

void PluginSystem::UnloadModule(const char* path)
{
	if(path == nullptr)
		return;

	ModuleContainer::iterator it = m_modules.find(astd::string(path));
	if(it != m_modules.end())
	{
		Module& module = it->second;

		// release all scripts

		PlatformUnloadModule(module.m_module);

		m_modules.erase(it);
	}
}

void* PluginSystem::PlatformLoadModule(const char *path)
{
#if defined(PLATFORM_WINDOWS)
	return (void*)LoadLibrary(path);
#elif defined(PLATFORM_LINUX)
	return (void*)dlopen(path, RTLD_LAZY);
#else
#error Implement!
	return nullptr;
#endif
}

void PluginSystem::PlatformUnloadModule(void* module)
{
#if defined(PLATFORM_WINDOWS)
	FreeLibrary((HMODULE)module);
#elif defined(PLATFORM_LINUX)
	dlclose(module);
#else
#error Implement!
#endif
}

void* PluginSystem::PlatformGetSymbol(void* module, const char* symbol)
{
#if defined(PLATFORM_WINDOWS)
	return (void*)GetProcAddress((HMODULE)module, symbol);
#elif defined(PLATFORM_LINUX)
	return dlsym((void*)module, symbol);
#else
#error Implement!
	return nullptr;
#endif
}

}
