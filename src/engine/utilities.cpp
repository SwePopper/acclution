/*
   Acclution
   Copyright (c) 2016 Christoffer Lindqvist

   This software is provided 'as-is', without any express or implied
   warranty. In no event will the authors be held liable for any
   damages arising from the use of this software.

   Permission is granted to anyone to use this software for any
   purpose, including commercial applications, and to alter it and
   redistribute it freely, subject to the following restrictions:

   1. The origin of this software must not be misrepresented; you
      must not claim that you wrote the original software. If you use
      this software in a product, an acknowledgment in the product
      documentation would be appreciated but is not required.

   2. Altered source versions must be plainly marked as such, and
      must not be misrepresented as being the original software.

   3. This notice may not be removed or altered from any source
      distribution.

   Christoffer Lindqvist
   swepopper@gmail.com
*/

#include "engine/platform.h"
#include "engine/utilities.h"

#include <sys/stat.h>

#if defined(PLATFORM_WINDOWS)
#define STAT_IF_DIR _S_IFDIR
#else
#define STAT_IF_DIR S_IFDIR
#endif

namespace Acclution {

namespace Utilities
{

bool FileExist(const char* name)
{
	struct stat buffer;
	return (stat (name, &buffer) == 0);
}

bool DirExist(const char* name)
{
	struct stat buffer;
	return (stat (name, &buffer) == 0) && buffer.st_mode & STAT_IF_DIR;
}

astd::string GetFileName(const astd::string& file_path)
{
	astd::string name = file_path;
	name.replace(name.begin(), name.end(), DIRECTORY_SEPARATOR, '/');
	size_t pos = name.find_last_of('/');
	if(pos != astd::string::npos)
	{
		name = name.substr(pos);
	}
	pos = name.find_first_of('.');
	if(pos != astd::string::npos)
	{
		name = name.substr(0, pos);
	}
	return name;
}

astd::string GetDirectory(const astd::string& file_path)
{
	size_t pos = file_path.find_last_of('/');
	if (pos != astd::string::npos)
	{
		return file_path.substr(0, pos+1);
	}
	pos = file_path.find_last_of('\\');
	if (pos != astd::string::npos)
	{
		return file_path.substr(0, pos+1);
	}
	return file_path;
}

void StackTrace(size_t count, char* trace, size_t trace_size)
{
#if defined(PLATFORM_WINDOWS)
	HANDLE process = GetCurrentProcess();
	SymInitialize(process, nullptr, true);

	void** stack = (void**)alloca(sizeof(void*)*count);

	unsigned short frames = CaptureStackBackTrace(1, (DWORD)count, stack, nullptr);
	ULONG max_name_len = (ULONG)(trace_size / count);
	SYMBOL_INFO* symbol = (SYMBOL_INFO*)malloc(sizeof(SYMBOL_INFO) + max_name_len);
	symbol->MaxNameLen = max_name_len;
	symbol->SizeOfStruct = sizeof(SYMBOL_INFO);

	uint32_t pos = 0;
	for (unsigned short i = 0; i < frames; ++i)
	{
		SymFromAddr(process, (DWORD64)stack[i], 0, symbol);
		pos += snprintf(&trace[pos], trace_size - pos, "%s - %p\n", symbol->Name, (void*)symbol->Address);
	}

	free(symbol);
#else
	// storage array for stack trace address data
	void* addrlist = alloca(count+1);

	// retrieve current stack addresses
	int addrlen = backtrace(&addrlist, sizeof(addrlist) / sizeof(void*));
	if(addrlen == 0)
		return;

	// create readable strings to each frame.
	char** symbollist = backtrace_symbols(&addrlist, addrlen);

	// copy the stack trace.
	count = 0;
	for(int i = 4; i < addrlen; i++)
	{
		snprintf(&trace[count], trace_size, "%s\n", symbollist[i]);
		count += strlen(symbollist[i])+2;
	}

	free(symbollist);
#endif
}

void GenerateUUID(Uuid& out)
{
#if defined(PLATFORM_WINDOWS)
	UuidCreate(&out);
#elif defined(PLATFORM_LINUX)
	uuid_generate_time_safe(out);
#endif
}

void UUIDToString(Uuid& uuid, char* out, size_t size)
{
	size = size < UUID_SIZE ? size : UUID_SIZE;
#if defined(PLATFORM_WINDOWS)
	unsigned char* str;
	UuidToStringA(&uuid, &str);
	strncpy(out, (char*)str, size);
	RpcStringFreeA(&str);
#elif defined(PLATFORM_LINUX)
	char s[37] = { 0 };
	uuid_unparse(uuid, s);
	strncpy(out, s, size);
#endif
}

bool StringToUUID(const char* in, Uuid& uuid)
{
#if defined(PLATFORM_WINDOWS)
	return UuidFromString((RPC_CSTR)in, &uuid) == RPC_S_OK;
#elif defined(PLATFORM_LINUX)
	return uuid_parse(in, uuid) == 0;
#endif
}

int CompareUUID(const Uuid& a, const Uuid& b)
{
#if defined(PLATFORM_WINDOWS)
	RPC_STATUS status;
	return UuidCompare((UUID*)&a, (UUID*)&b, &status);
#elif defined(PLATFORM_LINUX)
	return uuid_compare(a, b);
#endif
}

}

}
