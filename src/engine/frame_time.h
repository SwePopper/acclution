/*
   Acclution
   Copyright (c) 2016 Christoffer Lindqvist

   This software is provided 'as-is', without any express or implied
   warranty. In no event will the authors be held liable for any
   damages arising from the use of this software.

   Permission is granted to anyone to use this software for any
   purpose, including commercial applications, and to alter it and
   redistribute it freely, subject to the following restrictions:

   1. The origin of this software must not be misrepresented; you
      must not claim that you wrote the original software. If you use
      this software in a product, an acknowledgment in the product
      documentation would be appreciated but is not required.

   2. Altered source versions must be plainly marked as such, and
      must not be misrepresented as being the original software.

   3. This notice may not be removed or altered from any source
      distribution.

   Christoffer Lindqvist
   swepopper@gmail.com
*/

#ifndef FRAME_TIME_H
#define FRAME_TIME_H
#pragma once

namespace Acclution {

// http://bitsquid.blogspot.se/2010/10/time-step-smoothing.html

class FrameTime
{
public:
	FrameTime();

	const double& GetDeltaTime() const { return m_delta_time; }
	float GetDeltaTimeF() const { return (float)m_delta_time; }
	const int64_t& GetDeltaTimeNano() const { return m_delta_time_nano; }

	const double& GetSmoothedDeltaTime() const { return m_smoothed_delta_time; }
	float GetSmoothedDeltaTimeF() const { return (float)m_smoothed_delta_time; }
	const int64_t& GetSmoothedDeltaTimeNano() const { return m_smoothed_delta_time_nano; }

private:
	void Update(std::chrono::high_resolution_clock::time_point& last_time, int64_t last_smoothed_delta_time_nano);

	std::chrono::high_resolution_clock::time_point m_current_time;
	double		m_delta_time;
	int64_t		m_delta_time_nano;
	double		m_smoothed_delta_time;
	int64_t		m_smoothed_delta_time_nano;

	friend class Engine;
};

}

#endif // TIME_H
