#ifndef PLATFORM_EVENTS_H
#define PLATFORM_EVENTS_H

namespace Acclution
{

enum class PlatformEventType : uint32_t
{
	kWindowResize,
	kMouseMove,
	kMouseButton,
	kKeyboard
};

class PlatformEvent
{
public:
	virtual ~PlatformEvent() {}

	const PlatformEventType& GetEventType() const { return m_platform_event_type; }

protected:
	PlatformEvent(const PlatformEventType& type) : m_platform_event_type(type) {}

private:
	PlatformEventType		m_platform_event_type;
};

namespace PlatformEvents
{

class WindowResize : public PlatformEvent
{
public:
	WindowResize(const uint32_t& width, const uint32_t& height) : PlatformEvent(PlatformEventType::kWindowResize), m_width(width), m_height(height) {}
	virtual ~WindowResize() {}

	const uint32_t& GetWidth() const { return m_width; }
	const uint32_t& GetHeight() const { return m_height; }

private:
	uint32_t		m_width;
	uint32_t		m_height;
};

class MouseMove : public PlatformEvent
{
public:
	MouseMove(const float& x, const float& y) : PlatformEvent(PlatformEventType::kMouseMove), m_position(x, y) {}
	virtual ~MouseMove() {}

	const float& GetPositionX() const { return m_position.x; }
	const float& GetPositionY() const { return m_position.y; }
	const glm::vec2& GetPosition() const { return m_position; }

private:
	glm::vec2		m_position;
};

class MouseButton : public PlatformEvent
{
public:
	MouseButton(const float& x, const float& y, const uint8_t& index, const bool& state) : PlatformEvent(PlatformEventType::kMouseButton), m_position(x, y), m_button_index(index), m_state(state) {}
	virtual ~MouseButton() {}

	const float& GetPositionX() const { return m_position.x; }
	const float& GetPositionY() const { return m_position.y; }
	const glm::vec2& GetPosition() const { return m_position; }

	const uint8_t& GetButtonIndex() const { return m_button_index; }
	const bool& GetState() const { return m_state; }

private:
	glm::vec2		m_position;
	uint8_t			m_button_index;
	bool			m_state;
};

class Keyboard : public PlatformEvent
{
public:
	Keyboard(const int32_t& index, const bool& state) : PlatformEvent(PlatformEventType::kKeyboard), m_button_index(index), m_state(state) {}
	virtual ~Keyboard() {}

	const int32_t& GetButtonIndex() const { return m_button_index; }
	const bool& GetState() const { return m_state; }

private:
	int32_t			m_button_index;
	bool			m_state;
};

}

}

#endif // PLATFORM_EVENTS_H
