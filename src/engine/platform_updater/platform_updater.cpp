#include "engine/platform.h"
#include "platform_updater.h"

#if defined(PLATFORM_WINDOWS) || defined(PLATFORM_LINUX)
#include "engine/platform_updater/SDL/platform_updater_sdl.h"
using PlatformUpdaterType = Acclution::PlatformUpdaterSDL;
#endif

namespace Acclution
{

PlatformUpdater* PlatformUpdater::Create()
{
	PlatformUpdaterType* updater = new PlatformUpdaterType;
	if(updater == nullptr || !updater->Initialize())
	{
		ALogError("Failed to initialize PlatformUpdater");
		delete updater;
		updater = nullptr;
	}
	return updater;
}

void PlatformUpdater::AddEventCallback(EventCallback callback, void* user_data)
{
	RemoveEventCallback(callback, user_data);
	m_event_callbacks.push_back(astd::pair<EventCallback, void*>(callback, user_data));
}

void PlatformUpdater::RemoveEventCallback(EventCallback callback, void* user_data)
{
	if(m_event_callbacks.empty())
		return;
	astd::EraseRemove(m_event_callbacks, astd::pair<EventCallback, void*>(callback, user_data));
}

}
