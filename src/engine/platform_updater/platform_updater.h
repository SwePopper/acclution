#ifndef PLATFORM_UPDATER_H
#define PLATFORM_UPDATER_H
#pragma once

namespace Acclution
{

class PlatformEvent;

class PlatformUpdater
{
public:
	using EventCallback = void(*)(const PlatformEvent* event, void* user_data);

	static PlatformUpdater* Create();
	virtual ~PlatformUpdater() { Release(); }

	virtual bool Update() { return true; }

	virtual void SetEventBased(bool event_based) { ASTD_UNUSED(event_based); }

	virtual astd::string GetAssetsPath() { return ""; }
	virtual astd::string GetSavePath(const char* organization, const char* name) { ASTD_UNUSED(organization); ASTD_UNUSED(name);  return ""; }

	void AddEventCallback(EventCallback callback, void* user_data);
	void RemoveEventCallback(EventCallback callback, void* user_data);

protected:
	using EventCallbacks = astd::vector<astd::pair<EventCallback, void*> >;

	PlatformUpdater() {}

	virtual bool Initialize()=0;
	virtual void Release() {}

	EventCallbacks			m_event_callbacks;
};

}

#endif
