#include "engine/platform.h"
#include "platform_updater_sdl.h"
#include "engine/platform_updater/platform_events.h"

#include <SDL.h>

#ifdef STEAMWORKS
#include <steam/steam_api.h>
#endif

namespace Acclution
{

using namespace PlatformEvents;

PlatformUpdaterSDL::PlatformUpdaterSDL()
	: PlatformUpdater(),
	  m_event_buffer(nullptr)
{
}

PlatformUpdaterSDL::~PlatformUpdaterSDL()
{
}

bool PlatformUpdaterSDL::Initialize()
{
	if(SDL_Init(SDL_INIT_EVENTS | SDL_INIT_JOYSTICK | SDL_INIT_HAPTIC | SDL_INIT_GAMECONTROLLER | SDL_INIT_VIDEO) != 0)
	{
		ALogError("SDL Error: %s", SDL_GetError());
		return false;
	}

#ifdef STEAMWORKS
	if(!SteamAPI_Init())
	{
		ALogError("SteamAPI_Init failed!");
		return false;
	}

	if(SteamAPI_RestartAppIfNecessary(480))
	{
		ALogInfo("Steam needs to relaunch the application");
		return false;
	}
#endif

	constexpr size_t event_buffer_size = astd::multi_max(sizeof(WindowResize),
														 sizeof(MouseMove),
														 sizeof(MouseButton),
														 sizeof(Keyboard));

	m_event_buffer = malloc(event_buffer_size);

	return m_event_buffer != nullptr;
}

void PlatformUpdaterSDL::Release()
{
	if(m_event_buffer != nullptr)
		free(m_event_buffer);
	m_event_buffer = nullptr;

#ifdef STEAMWORKS
	SteamAPI_Shutdown();
#endif

	SDL_Quit();
}

astd::string PlatformUpdaterSDL::GetAssetsPath()
{
	char* path = SDL_GetBasePath();
	astd::string asset_path = astd::string(path);
	SDL_free(path);

	return asset_path;
}

astd::string PlatformUpdaterSDL::GetSavePath(const char* organization, const char* name)
{
	char* path = SDL_GetPrefPath(organization, name);
	astd::string save_path = astd::string(path);
	SDL_free(path);

	return save_path;
}

bool PlatformUpdaterSDL::Update()
{
	SDL_Event event;
	if(m_event_based && SDL_WaitEvent(&event) != 0)
	{
		if(!RunEvent(&event))
			return false;
	}

	while(SDL_PollEvent(&event))
	{
		if(!RunEvent(&event))
			return false;
	}

	return true;
}

bool PlatformUpdaterSDL::RunEvent(SDL_Event* event)
{
	if(event->type == SDL_QUIT)
	{
		return false;
	}

	PlatformEvent* platform_event = nullptr;
	switch(event->type)
	{
	case SDL_WINDOWEVENT: {
			platform_event = event->window.event == SDL_WINDOWEVENT_RESIZED ? new (m_event_buffer) WindowResize(static_cast<uint32_t>(event->window.data1), static_cast<uint32_t>(event->window.data2))
																			: nullptr;
		} break;

	case SDL_MOUSEMOTION: {
			int window_width = 0;
			int window_height = 0;
			SDL_GetWindowSize(SDL_GetWindowFromID(event->motion.windowID), &window_width, &window_height);

			float x = (float)event->motion.x / (float)window_width;
			float y = 1.0f - (float)event->motion.y / (float)window_height;

			platform_event = new (m_event_buffer) MouseMove(x, y);
		} break;

	case SDL_MOUSEBUTTONDOWN:
	case SDL_MOUSEBUTTONUP: {
			int window_width = 0;
			int window_height = 0;
			SDL_GetWindowSize(SDL_GetWindowFromID(event->button.windowID), &window_width, &window_height);

			float x = (float)event->button.x / (float)window_width;
			float y = 1.0f - (float)event->button.y / (float)window_height;

			platform_event = new (m_event_buffer) MouseButton(x, y, event->button.button - 1, event->button.state == SDL_PRESSED);
		} break;

	case SDL_KEYDOWN:
	case SDL_KEYUP: {
			platform_event = new (m_event_buffer) Keyboard(event->key.keysym.sym, event->key.state == SDL_PRESSED);
		} break;

	default: return true;
	}

	if(platform_event == nullptr)
		return true;

	for(EventCallbacks::const_iterator it = m_event_callbacks.begin(); it != m_event_callbacks.end(); ++it)
	{
		it->first(platform_event, it->second);
	}

	platform_event->~PlatformEvent();
	return true;
}

}
