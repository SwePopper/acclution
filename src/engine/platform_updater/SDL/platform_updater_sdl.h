#ifndef PLATFORM_UPDATER_SDL_H
#define PLATFORM_UPDATER_SDL_H
#pragma once

#include "engine/platform_updater/platform_updater.h"
#include <SDL_events.h>

namespace Acclution
{

class PlatformUpdaterSDL : public PlatformUpdater
{
public:
	PlatformUpdaterSDL();
	virtual ~PlatformUpdaterSDL();

	virtual bool Initialize();
	virtual void Release();

	virtual bool Update();

	virtual void SetEventBased(bool event_based) { m_event_based = event_based; }

	virtual astd::string GetAssetsPath();
	virtual astd::string GetSavePath(const char* organization, const char* name);

private:
	bool RunEvent(SDL_Event* event);

	bool		m_event_based;
	void*		m_event_buffer;
};

}

#endif
