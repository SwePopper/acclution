/*
   Acclution
   Copyright (c) 2016 Christoffer Lindqvist

   This software is provided 'as-is', without any express or implied
   warranty. In no event will the authors be held liable for any
   damages arising from the use of this software.

   Permission is granted to anyone to use this software for any
   purpose, including commercial applications, and to alter it and
   redistribute it freely, subject to the following restrictions:

   1. The origin of this software must not be misrepresented; you
      must not claim that you wrote the original software. If you use
      this software in a product, an acknowledgment in the product
      documentation would be appreciated but is not required.

   2. Altered source versions must be plainly marked as such, and
      must not be misrepresented as being the original software.

   3. This notice may not be removed or altered from any source
      distribution.

   Christoffer Lindqvist
   swepopper@gmail.com
*/

#include "engine/platform.h"

#if defined(USE_EASTL)
#ifndef EASTL_USER_DEFINED_ALLOCATOR

inline void* AlignedMalloc(size_t size, size_t align = 1)
{
	void* mem = nullptr;
#if defined(_ISOC11_SOURCE)
	mem = aligned_alloc(align, size);
#elif defined(_MSC_VER)
	mem = _aligned_malloc(size, align);
#elif _POSIX_C_SOURCE >= 200112L
	if(posix_memalign(&mem, align, size)) mem = nullptr;
#else
	mem = _mm_malloc(size, align);
#endif
	return mem;
}

inline void AlignedFree(void *ptr)
{
#if defined(_ISOC11_SOURCE)
	free(ptr);
#elif defined(_MSC_VER)
	_aligned_free(ptr);
#elif _POSIX_C_SOURCE >= 200112L
	free(ptr);
#else
	_mm_free(ptr);
#endif
}

// TODO popper: find a way to use the debug info (memory profiler?)
void* operator new[](size_t size, const char* /*pName*/, int /*flags*/, unsigned /*debugFlags*/, const char* /*file*/, int /*line*/)
{
	return AlignedMalloc(size);
}

void* operator new[](size_t size, size_t alignment, size_t /*alignmentOffset*/, const char* /*pName*/, int /*flags*/, unsigned /*debugFlags*/, const char* /*file*/, int /*line*/)
{
	return AlignedMalloc(size, alignment);
}

#endif
#endif

#if defined(PLATFORM_WINDOWS)
// Disabling windows telemetry stuff.. more info: https://www.reddit.com/r/cpp/comments/4ibauu/visual_studio_adding_telemetry_function_calls_to/
extern "C"
{
	void _cdecl __vcrt_initialize_telemetry_provider() {}
	void _cdecl __telemetry_main_invoke_trigger() {}
	void _cdecl __telemetry_main_return_trigger() {}
	void _cdecl __vcrt_uninitialize_telemetry_provider() {}
};
#endif
