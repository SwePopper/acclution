/*
   Acclution
   Copyright (c) 2016 Christoffer Lindqvist

   This software is provided 'as-is', without any express or implied
   warranty. In no event will the authors be held liable for any
   damages arising from the use of this software.

   Permission is granted to anyone to use this software for any
   purpose, including commercial applications, and to alter it and
   redistribute it freely, subject to the following restrictions:

   1. The origin of this software must not be misrepresented; you
      must not claim that you wrote the original software. If you use
      this software in a product, an acknowledgment in the product
      documentation would be appreciated but is not required.

   2. Altered source versions must be plainly marked as such, and
      must not be misrepresented as being the original software.

   3. This notice may not be removed or altered from any source
      distribution.

   Christoffer Lindqvist
   swepopper@gmail.com
*/

#include "engine/platform.h"
#include "scenerenderer.h"
#include "engine/engine.h"
#include "engine/components/component.h"
#include "engine/asset/model.h"
#include "engine/asset/texture.h"
#include "engine/graphics/shader.h"
#include "engine/components/componentmanager.h"
#include "engine/task/taskscheduler.h"
#include "engine/task/task.h"
#include "scenecomponents.h"
#include "engine/graphics/commandbuffer.h"

#include <glm/glm.hpp>

namespace Acclution {

SceneRenderer::SceneRenderer()
{
}

SceneRenderer::~SceneRenderer()
{
}

bool SceneRenderer::Initialize()
{
	return true;
}

void SceneRenderer::Release()
{
}

void SceneRenderer::BeginScene(CommandBuffer* command)
{
	ASTD_UNUSED(command);
}

void SceneRenderer::EndScene(CommandBuffer* command)
{
	ASTD_UNUSED(command);
}

void SceneRenderer::Add(SpriteRenderer* sprite_renderer)
{
	ASTD_UNUSED(sprite_renderer);
}

void SceneRenderer::Add(Camera* camera)
{
	ASTD_UNUSED(camera);
}

}
