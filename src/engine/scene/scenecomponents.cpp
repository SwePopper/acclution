/*
   Acclution
   Copyright (c) 2016 Christoffer Lindqvist

   This software is provided 'as-is', without any express or implied
   warranty. In no event will the authors be held liable for any
   damages arising from the use of this software.

   Permission is granted to anyone to use this software for any
   purpose, including commercial applications, and to alter it and
   redistribute it freely, subject to the following restrictions:

   1. The origin of this software must not be misrepresented; you
      must not claim that you wrote the original software. If you use
      this software in a product, an acknowledgment in the product
      documentation would be appreciated but is not required.

   2. Altered source versions must be plainly marked as such, and
      must not be misrepresented as being the original software.

   3. This notice may not be removed or altered from any source
      distribution.

   Christoffer Lindqvist
   swepopper@gmail.com
*/

#include "engine/platform.h"
#include "scenecomponents.h"
#include "engine/engine.h"
#include "engine/asset/contentmanager.h"
#include "engine/asset/model.h"
#include "engine/asset/texture.h"
#include "scenerenderer.h"
#include "engine/graphics/shader.h"
#include "engine/graphics/commandbuffer.h"
#include "engine/graphics/video.h"

namespace Acclution
{

Component* Camera::CreateComponent()
{
	Camera* cam = new Camera;
	Engine::GetInstance()->GetSceneRenderer()->Add(cam);
	return cam;
}

void Camera::Initialize(const char **attributes, ContentManager* content_manager, Entity* entity)
{
	RenderComponent::Initialize(attributes, content_manager, entity);

	for(uint32_t i = 0; attributes[i]; i += 2)
	{
		if(strncmp(attributes[i], "width", 6) == 0)
			m_width = static_cast<uint32_t>(strtoul(attributes[i+1], nullptr, 10));
		else if(strncmp(attributes[i], "height", 7) == 0)
			m_height = static_cast<uint32_t>(strtoul(attributes[i+1], nullptr, 10));
	}
}

void Camera::Update()
{
	if(m_entity != nullptr)
	{

	}
	else
	{
		m_view = glm::mat3x4();
	}
}

Component* SpriteRenderer::CreateComponent()
{
	SpriteRenderer* sr = new SpriteRenderer;
	Engine::GetInstance()->GetSceneRenderer()->Add(sr);
	return sr;
}

SpriteRenderer::SpriteRenderer()
	: m_texture(nullptr),
	  m_position_buffer(UINT32_MAX),
	  m_uv_buffer(UINT32_MAX),
	  m_color_buffer(UINT32_MAX),
	  m_index_buffer(UINT32_MAX) {}

void SpriteRenderer::Initialize(const char** attributes, ContentManager* content_manager, Entity* entity)
{
	RenderComponent::Initialize(attributes, content_manager, entity);

	for(uint32_t i = 0; attributes[i]; i += 2)
	{
		if(strncmp(attributes[i], "texture", 8) == 0)
		{
			m_texture = content_manager->LoadAsset<Texture>(attributes[i+1]);
		}
	}
}

void SpriteRenderer::CreateRenderData(CommandBuffer*)
{
	if(m_texture == nullptr)
		return;

	float aspect = m_texture->GetAspect();
	float vertex_width = 1.0f;
	float vertex_height = vertex_width * aspect;

	m_positions[0] = glm::vec3(vertex_width, -vertex_height, 0.0f);
	m_positions[1] = glm::vec3(-vertex_width, -vertex_height, 0.0f);
	m_positions[2] = glm::vec3(-vertex_width, vertex_height, 0.0f);
	m_positions[3] = glm::vec3(vertex_width, vertex_height, 0.0f);

	m_uvs[0] = glm::vec2(1.0f, 0.0f);
	m_uvs[1] = glm::vec2(0.0f, 0.0f);
	m_uvs[2] = glm::vec2(0.0f, 1.0f);
	m_uvs[3] = glm::vec2(1.0f, 1.0f);

	m_colors[0] = glm::vec4(1.0f);
	m_colors[1] = glm::vec4(1.0f);
	m_colors[2] = glm::vec4(1.0f);
	m_colors[3] = glm::vec4(1.0f);

	m_indices[0] = 0;
	m_indices[1] = 1;
	m_indices[2] = 2;
	m_indices[3] = 0;
	m_indices[4] = 2;
	m_indices[5] = 3;

	if(m_position_buffer == UINT32_MAX)
		m_position_buffer = Engine::GetInstance()->GetVideo()->CreateVertexBuffer(m_positions, sizeof(m_positions), BufferType::kVertex, BufferUsage::kStatic);
	if(m_uv_buffer == UINT32_MAX)
		m_uv_buffer = Engine::GetInstance()->GetVideo()->CreateVertexBuffer(m_uvs, sizeof(m_uvs), BufferType::kVertex, BufferUsage::kStatic);
	if(m_color_buffer == UINT32_MAX)
		m_color_buffer = Engine::GetInstance()->GetVideo()->CreateVertexBuffer(m_colors, sizeof(m_colors), BufferType::kVertex, BufferUsage::kStatic);
	if(m_index_buffer == UINT32_MAX)
		m_index_buffer = Engine::GetInstance()->GetVideo()->CreateVertexBuffer(m_indices, sizeof(m_indices), BufferType::kIndex, BufferUsage::kStatic);
}

void SpriteRenderer::RemoveRenderData(CommandBuffer*)
{
	if(m_position_buffer != UINT32_MAX)
	{
		Engine::GetInstance()->GetVideo()->DestroyVertexBuffer(m_position_buffer);
		m_position_buffer = UINT32_MAX;
	}

	if(m_uv_buffer != UINT32_MAX)
	{
		Engine::GetInstance()->GetVideo()->DestroyVertexBuffer(m_uv_buffer);
		m_uv_buffer = UINT32_MAX;
	}

	if(m_color_buffer != UINT32_MAX)
	{
		Engine::GetInstance()->GetVideo()->DestroyVertexBuffer(m_color_buffer);
		m_color_buffer = UINT32_MAX;
	}

	if(m_index_buffer != UINT32_MAX)
	{
		Engine::GetInstance()->GetVideo()->DestroyVertexBuffer(m_index_buffer);
		m_index_buffer = UINT32_MAX;
	}
}

void SpriteRenderer::Draw(CommandBuffer* /*command*/)
{
	/*Shader* shader = Engine::GetInstance()->GetVideo()->GetShader(s_default_shader);
	if(shader != nullptr)
	{
		glm::mat4 transform = glm::perspective(glm::radians(90.0f), 4.0f / 3.0f, 0.1f, 100.0f) * glm::lookAt(glm::vec3(0.0f, 0.0f, -3.0f), glm::vec3(), glm::vec3(0.0f, 1.0f, 0.0f));
		command->UseShader(s_default_shader);

		static uint32_t modelViewProjection = Utilities::Hash32("modelViewProjection");

		for(int i = 0; i < shader->GetUniformCount(); ++i)
		{
			if(shader->GetUniform(i).name_hash == modelViewProjection)
			{
				command->Uniform(shader->GetUniform(i).index, Types::kMatrix, 4, 1, &transform);
			}
		}

		Texture* texture = dynamic_cast<Texture*>(m_texture.get());
		command->BindTexture(texture->GetTextureObject(), 0);

		command->EnableVertexAttrib(m_position_buffer, 0, 3, Types::kFloat, false, 0, nullptr);
		command->EnableVertexAttrib(m_uv_buffer, 1, 2, Types::kFloat, false, 0, nullptr);
		//command->EnableVertexAttrib(m_color_buffer, 2, 4, Types::kFloat, false, 0, nullptr);
		command->DrawElements(Modes::kTriangles, 6, Types::kUByte, m_index_buffer);
		command->DisableVertexAttrib(0);
		command->DisableVertexAttrib(1);
		//command->DisableVertexAttrib(2);

		command->UnBindTexture(0);
	}*/
}

}
