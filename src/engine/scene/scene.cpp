/*
   Acclution
   Copyright (c) 2016 Christoffer Lindqvist

   This software is provided 'as-is', without any express or implied
   warranty. In no event will the authors be held liable for any
   damages arising from the use of this software.

   Permission is granted to anyone to use this software for any
   purpose, including commercial applications, and to alter it and
   redistribute it freely, subject to the following restrictions:

   1. The origin of this software must not be misrepresented; you
      must not claim that you wrote the original software. If you use
      this software in a product, an acknowledgment in the product
      documentation would be appreciated but is not required.

   2. Altered source versions must be plainly marked as such, and
      must not be misrepresented as being the original software.

   3. This notice may not be removed or altered from any source
      distribution.

   Christoffer Lindqvist
   swepopper@gmail.com
*/

#include "engine/platform.h"
#include "scene.h"
#include "engine/engine.h"
#include "engine/utilities.h"
#include "engine/entity/entity.h"
#include "engine/components/component.h"
#include "engine/config/config.h"
#include "engine/components/componentmanager.h"
#include "engine/asset/contentmanager.h"

#ifdef XML_LARGE_SIZE
#if defined(XML_USE_MSC_EXTENSIONS) && _MSC_VER < 1400
#define XML_FMT_INT_MOD "I64"
#else
#define XML_FMT_INT_MOD "ll"
#endif
#else
#define XML_FMT_INT_MOD "l"
#endif

namespace Acclution {

class LoadingData
{
public:
	LoadingData() : m_failed(false) {}
	bool			m_load_additive;
	astd::string	m_scene;
	ContentManager*	m_content_manager;
	Entity*			m_current_entity;
	bool			m_failed;
};

Scene::Scene()
	: m_content_manager(new ContentManager),
	  m_root_entity(nullptr),
	  m_loading(false)
{
}

Scene::~Scene()
{
	delete m_content_manager;
	delete m_root_entity;
}

void Scene::ClearScene()
{
	delete m_root_entity;
	m_root_entity = nullptr;
	m_content_manager->RemoveAllAssets();
}

void Scene::LoadScene(const char* scene, bool additive)
{
	if(m_loading)
	{
		ALogError("Allready loading scene!");
		return;
	}

	astd::string scene_path = Engine::GetInstance()->GetConfig()->GetAssetPath();
	if(!m_content_manager->Initialize(scene_path.c_str()))
		return;

	scene_path.append(scene);

	if(!Utilities::FileExist(scene_path.c_str()))
	{
		ALogError("Scene: %s does not exist!", scene_path.c_str());
		return;
	}

	LoadingData* async_operation = new LoadingData;
	async_operation->m_load_additive = additive;
	async_operation->m_scene = scene;
	async_operation->m_content_manager = m_content_manager;
	IO::ReadFile(&Scene::ReadScene, this, async_operation, scene_path);
}

class ScopedParser
{
public:
	~ScopedParser()
	{
		if(parser != nullptr)
			XML_ParserFree(parser);
	}

	XML_Parser parser;
};

bool Scene::ReadScene(IO::FileDeserializer* deserializer, LoadingData* data)
{
	if(!data->m_load_additive)
	{
		ALogInfo("Removing old entities");
		delete m_root_entity;
		m_root_entity = nullptr;

		m_content_manager->RemoveAllAssets();
	}

	if(m_root_entity == nullptr)
	{
		m_root_entity = new Entity;
		m_root_entity->SetName("Root");
		m_root_entity->SetEnabled(true);
	}

	data->m_current_entity = m_root_entity;

	if(deserializer->GetData() == nullptr)
	{
		ALogError("Failed to read header from scene file %s", data->m_scene.c_str());
		delete data;
		return false;
	}


	if(deserializer->GetData()[0] == 's' && deserializer->GetData()[1] == 'c' && deserializer->GetData()[2] == 'e')
	{
		if(!ReadSceneBin(deserializer, data))
		{
			ALogError("Failed to read binary scene file %s", data->m_scene.c_str());
			delete data;
			return false;
		}
	}
	else
	{
		if(!ReadSceneXML(deserializer, data))
		{
			ALogError("Failed to read xml scene file %s", data->m_scene.c_str());
			delete data;
			return false;
		}
	}

	ALogInfo("Done loading scene: %s", data->m_scene.c_str());
	delete data;
	return true;
}

bool Scene::ReadSceneBin(IO::FileDeserializer* /*deserializer*/, LoadingData* /*data*/)
{
	return false;
}

bool Scene::ReadSceneXML(IO::FileDeserializer* deserializer, LoadingData* data)
{
	ScopedParser sp;
	sp.parser = XML_ParserCreate(nullptr);
	if(sp.parser == nullptr)
	{
		ALogError("Failed to xml parser!");
		return false;
	}

	XML_SetElementHandler(sp.parser, &Scene::StartElementHandler, &Scene::EndElementHandler);
	XML_SetUserData(sp.parser, data);

	deserializer->GetData();
	deserializer->SkipDeserialize<uint8_t>(deserializer->GetDataSize());

	int done = 0;
	while(done == 0)
	{
		done = deserializer->IsEOF();
		if(XML_Parse(sp.parser, (const char*)deserializer->GetData(), (int)deserializer->GetDataSize(), done) == XML_STATUS_ERROR)
		{
			ALogError("XML ERROR: %s at line %" XML_FMT_INT_MOD "u",
					 XML_ErrorString(XML_GetErrorCode(sp.parser)),
					 XML_GetCurrentLineNumber(sp.parser));
			return false;
		}

		if(data->m_failed)
			return false;

		deserializer->StreamNext();
	}

	return true;
}

void Scene::StartElementHandler(void* user_data, const XML_Char* name, const XML_Char** atts)
{
	LoadingData* data = (LoadingData*)user_data;

	if(strncmp(name, "Model", 6) == 0 || strncmp(name, "Texture", 8) == 0)
	{
		/*const char* model_name = nullptr;
		for(uint32_t i = 0; atts[i]; i += 2)
		{
			if(strncmp(atts[i], "name", 0) == 0)
				model_name = atts[i+1];
		}*/

		/*std::shared_ptr<Asset> asset = pThis->m_content_manager->LoadAsset<Model>(name);
		if(asset == nullptr)
		{
			pThis->m_async_operation = nullptr;
			return;
		}*/
		return;
	}
	else if(strncmp(name, "Entity", 7) == 0)
	{
		Entity* entity = new Entity;
		for(uint32_t i = 0; atts[i]; i += 2)
		{
			if(strncmp(atts[i], "name", 5) == 0)
				entity->SetName(atts[i+1]);
			else if(strncmp(atts[i], "enabled", 8) == 0)
				entity->SetEnabled(atoi(atts[i+1]) != 0);
		}

		entity->SetParent(data->m_current_entity);
		data->m_current_entity = entity;
	}
	else if(strncmp(name, "Component", 10) == 0)
	{
		const char* component_type = nullptr;
		for(uint32_t i = 0; atts[i]; i += 2)
		{
			if(strncmp(atts[i], "type", 5) == 0)
			{
				component_type = atts[i+1];
				break;
			}
		}

		Component* component = Engine::GetInstance()->GetComponentManager()->Create(component_type);
		if(component == nullptr)
		{
			ALogError("Failed to create component: %s", component_type);
			return;
		}

		component->Initialize(atts, data->m_content_manager, data->m_current_entity);
	}
}

void Scene::EndElementHandler(void* user_data, const XML_Char* name)
{
	LoadingData* data = (LoadingData*)user_data;

	if(strncmp(name, "Entity", 7) == 0)
	{
		if(data->m_current_entity != nullptr)
			data->m_current_entity = data->m_current_entity->GetParent();
	}
}

}
