/*
   Acclution
   Copyright (c) 2016 Christoffer Lindqvist

   This software is provided 'as-is', without any express or implied
   warranty. In no event will the authors be held liable for any
   damages arising from the use of this software.

   Permission is granted to anyone to use this software for any
   purpose, including commercial applications, and to alter it and
   redistribute it freely, subject to the following restrictions:

   1. The origin of this software must not be misrepresented; you
      must not claim that you wrote the original software. If you use
      this software in a product, an acknowledgment in the product
      documentation would be appreciated but is not required.

   2. Altered source versions must be plainly marked as such, and
      must not be misrepresented as being the original software.

   3. This notice may not be removed or altered from any source
      distribution.

   Christoffer Lindqvist
   swepopper@gmail.com
*/

#ifndef SCENECOMPONENTS_H
#define SCENECOMPONENTS_H
#pragma once

#include "engine/asset/asset.h"
#include "engine/components/rendercomponent.h"

namespace Acclution
{

class CommandBuffer;
class Texture;

class Camera : public RenderComponent
{
public:
	static Component* CreateComponent();

	Camera() : m_width(1024), m_height(768) {}
	virtual ~Camera() {}

	virtual void Initialize(const char **attributes, ContentManager* content_manager, Entity* entity);

	virtual void Update();

	virtual void CreateRenderData(CommandBuffer* /*command*/) {}
	virtual void RemoveRenderData(CommandBuffer* /*command*/) {}

	glm::mat3x4		m_view;
	uint32_t		m_width;
	uint32_t		m_height;

private:
};

class SpriteRenderer : public RenderComponent
{
public:
	static Component* CreateComponent();

	SpriteRenderer();
	virtual ~SpriteRenderer() {}

	virtual void Initialize(const char** attributes, ContentManager* content_manager, Entity* entity);

	virtual void CreateRenderData(CommandBuffer* command);
	virtual void RemoveRenderData(CommandBuffer* command);

	virtual void Draw(CommandBuffer* command);

	Texture*	m_texture;
	uint32_t	m_position_buffer;
	uint32_t	m_uv_buffer;
	uint32_t	m_color_buffer;
	uint32_t	m_index_buffer;

	glm::vec3	m_positions[4];
	glm::vec2	m_uvs[4];
	glm::vec4	m_colors[4];
	uint8_t		m_indices[6];
};

}

#endif // SCENECOMPONENTS_H
