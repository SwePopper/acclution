/*
   Acclution
   Copyright (c) 2016 Christoffer Lindqvist

   This software is provided 'as-is', without any express or implied
   warranty. In no event will the authors be held liable for any
   damages arising from the use of this software.

   Permission is granted to anyone to use this software for any
   purpose, including commercial applications, and to alter it and
   redistribute it freely, subject to the following restrictions:

   1. The origin of this software must not be misrepresented; you
      must not claim that you wrote the original software. If you use
      this software in a product, an acknowledgment in the product
      documentation would be appreciated but is not required.

   2. Altered source versions must be plainly marked as such, and
      must not be misrepresented as being the original software.

   3. This notice may not be removed or altered from any source
      distribution.

   Christoffer Lindqvist
   swepopper@gmail.com
*/

#ifndef SCENE_H
#define SCENE_H
#pragma once

#include "engine/io/readtask.h"

#include <expat.h>

namespace Acclution {

class ContentManager;
class Entity;

class Scene
{
public:
	Scene();
	~Scene();

	void ClearScene();

	void LoadScene(const char* scene, bool additive = false);

private:
	bool ReadScene(IO::FileDeserializer* deserializer, class LoadingData* data);

	bool ReadSceneBin(IO::FileDeserializer* deserializer, class LoadingData* data);
	bool ReadSceneXML(IO::FileDeserializer* deserializer, class LoadingData* data);
	static void StartElementHandler(void* user_data, const XML_Char* name, const XML_Char** atts);
	static void EndElementHandler(void* user_data, const XML_Char* name);
	//static void CharacterDataHandler(void* user_data, const XML_Char* s, int len);

	ContentManager*		m_content_manager;
	Entity*				m_root_entity;
	std::atomic_bool	m_loading;
};

}

#endif // SCENE_H
