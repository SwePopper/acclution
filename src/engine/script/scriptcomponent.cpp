/*
   Acclution
   Copyright (c) 2016 Christoffer Lindqvist

   This software is provided 'as-is', without any express or implied
   warranty. In no event will the authors be held liable for any
   damages arising from the use of this software.

   Permission is granted to anyone to use this software for any
   purpose, including commercial applications, and to alter it and
   redistribute it freely, subject to the following restrictions:

   1. The origin of this software must not be misrepresented; you
      must not claim that you wrote the original software. If you use
      this software in a product, an acknowledgment in the product
      documentation would be appreciated but is not required.

   2. Altered source versions must be plainly marked as such, and
      must not be misrepresented as being the original software.

   3. This notice may not be removed or altered from any source
      distribution.

   Christoffer Lindqvist
   swepopper@gmail.com
*/

#include "engine/platform.h"
#include "scriptcomponent.h"
#include "engine/engine.h"
#include "scriptmanager.h"

namespace Acclution {

std::shared_ptr<Component> ScriptComponent::CreateComponent()
{
	std::shared_ptr<ScriptComponent> script = std::make_shared<ScriptComponent>();
	return script;
}

void ScriptComponent::Initialize(const char** attributes, ContentManager* content_manager, Entity* entity)
{
	Component::Initialize(attributes, content_manager, entity);
	for(uint32_t i = 0; attributes[i]; i += 2)
	{
	}
}

void ScriptComponent::Update()
{
}

}
