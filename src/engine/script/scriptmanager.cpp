/*
   Acclution
   Copyright (c) 2016 Christoffer Lindqvist

   This software is provided 'as-is', without any express or implied
   warranty. In no event will the authors be held liable for any
   damages arising from the use of this software.

   Permission is granted to anyone to use this software for any
   purpose, including commercial applications, and to alter it and
   redistribute it freely, subject to the following restrictions:

   1. The origin of this software must not be misrepresented; you
      must not claim that you wrote the original software. If you use
      this software in a product, an acknowledgment in the product
      documentation would be appreciated but is not required.

   2. Altered source versions must be plainly marked as such, and
      must not be misrepresented as being the original software.

   3. This notice may not be removed or altered from any source
      distribution.

   Christoffer Lindqvist
   swepopper@gmail.com
*/

#include "engine/platform.h"
#include "scriptmanager.h"
#include "scriptcomponent.h"
#include "engine/engine.h"
#include "engine/components/componentmanager.h"

#include <RuntimeObjectSystem/RuntimeObjectSystem.h>
#include <RuntimeObjectSystem/IObjectFactorySystem.h>
#include <RuntimeObjectSystem/ObjectInterface.h>
#include <RuntimeObjectSystem/IObject.h>

namespace Acclution {

class CompileLogger : public ICompilerLogger
{
public:
	virtual void LogError(const char* format, ...)
	{
		va_list args;
		va_start(args, format);
		LogInternal(format, args);
		ALogError("SCRIPT ERROR: %s", m_buff);
	}

	virtual void LogWarning(const char* format, ...)
	{
		va_list args;
		va_start(args, format);
		LogInternal(format, args);
		ALogWarning("SCRIPT WARNING: %s", m_buff);
	}

	virtual void LogInfo(const char* format, ...)
	{
		va_list args;
		va_start(args, format);
		LogInternal(format, args);
		ALogInfo("SCRIPT INFO: %s", m_buff);
	}

	void LogInternal(const char * format, va_list args)
	{
#ifdef PLATFORM_LINUX
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wformat-nonliteral"
#endif

		vsnprintf(m_buff, BUF_SIZE, format, args);

#ifdef PLATFORM_LINUX
#pragma GCC diagnostic pop
#endif

		va_end(args);
		m_buff[BUF_SIZE-1] = '\0';
	}

private:
	char		m_buff[BUF_SIZE];
};

ScriptManager::ScriptManager()
	: m_compiler_logger(nullptr),
	  m_runtime_object_system(nullptr)
{
}

bool ScriptManager::Initialize()
{
	m_runtime_object_system = new RuntimeObjectSystem;
	m_compiler_logger = new CompileLogger;
	if(!m_runtime_object_system->Initialise(m_compiler_logger, 0))
	{
		delete m_runtime_object_system;
		m_runtime_object_system = nullptr;
		return false;
	}

	m_runtime_object_system->SetAdditionalCompileOptions("-std=c++11");

	// Add include directories (TODO: make this "prittier")
	FileSystemUtils::Path path = m_runtime_object_system->FindFile(__FILE__);
	path = path.ParentPath().ParentPath().ParentPath();
	m_runtime_object_system->AddIncludeDir(path.c_str());

	path = path.ParentPath();
	path.m_string += "/external/randutils";
	m_runtime_object_system->AddIncludeDir(path.c_str());

	path = path.ParentPath();
	path.m_string += "/concurrentqueue";
	m_runtime_object_system->AddIncludeDir(path.c_str());

	path = path.ParentPath();
	path.m_string += "/RuntimeCompiledCPlusPlus/Aurora";
	m_runtime_object_system->AddIncludeDir(path.c_str());

	return true;
}

void ScriptManager::Release()
{
	if(m_runtime_object_system != nullptr)
	{
		m_runtime_object_system->CleanObjectFiles();

		delete m_runtime_object_system;
		m_runtime_object_system = nullptr;
	}

	delete m_compiler_logger;
	m_compiler_logger = nullptr;
}

void ScriptManager::Update()
{
	if(m_runtime_object_system->GetIsCompiledComplete())
	{
		m_runtime_object_system->LoadCompiledModule();
	}

	if(!m_runtime_object_system->GetIsCompiling())
	{
		m_runtime_object_system->GetFileChangeNotifier()->Update(0.0166f);
	}
}

ScriptObjectID ScriptManager::ConstructObject(const char* type)
{
	IObjectConstructor* constructor = m_runtime_object_system->GetObjectFactorySystem()->GetConstructor(type);
	if(constructor != nullptr)
	{
		IObject* object = constructor->Construct();
		ObjectId scriptId = object->GetObjectId();
		return ScriptObjectID(scriptId.m_PerTypeId, scriptId.m_ConstructorId);
	}
	return ScriptObjectID(InvalidId, InvalidId);
}

IObject* ScriptManager::GetScriptObject(const ScriptObjectID& object_id)
{
	ObjectId scriptId;
	scriptId.m_PerTypeId = object_id.first;
	scriptId.m_ConstructorId = object_id.second;
	return m_runtime_object_system->GetObjectFactorySystem()->GetObject(scriptId);
}

}
