/*
   Acclution
   Copyright (c) 2016 Christoffer Lindqvist

   This software is provided 'as-is', without any express or implied
   warranty. In no event will the authors be held liable for any
   damages arising from the use of this software.

   Permission is granted to anyone to use this software for any
   purpose, including commercial applications, and to alter it and
   redistribute it freely, subject to the following restrictions:

   1. The origin of this software must not be misrepresented; you
      must not claim that you wrote the original software. If you use
      this software in a product, an acknowledgment in the product
      documentation would be appreciated but is not required.

   2. Altered source versions must be plainly marked as such, and
      must not be misrepresented as being the original software.

   3. This notice may not be removed or altered from any source
      distribution.

   Christoffer Lindqvist
   swepopper@gmail.com
*/

#ifndef SCRIPTMANAGER_H
#define SCRIPTMANAGER_H
#pragma once

struct ICompilerLogger;
struct IRuntimeObjectSystem;
struct IObject;

namespace Acclution {

class ScriptManager
{
public:
	ScriptManager();
	~ScriptManager() { Release(); }

	bool Initialize();
	void Release();

	void Update();

	ScriptObjectID Construct(const char* type)
	{
		return ConstructObject(type);
	}

	template<typename T>
	T* Get(const ScriptObjectID& object_id)
	{
		return (T*)GetScriptObject(object_id);
	}

private:
	ScriptObjectID ConstructObject(const char* type);
	IObject* GetScriptObject(const ScriptObjectID& object_id);

	ICompilerLogger*		m_compiler_logger;
	IRuntimeObjectSystem*	m_runtime_object_system;
};

}

#endif // SCRIPTMANAGER_H
