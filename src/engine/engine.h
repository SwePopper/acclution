/*
   Acclution
   Copyright (c) 2016 Christoffer Lindqvist

   This software is provided 'as-is', without any express or implied
   warranty. In no event will the authors be held liable for any
   damages arising from the use of this software.

   Permission is granted to anyone to use this software for any
   purpose, including commercial applications, and to alter it and
   redistribute it freely, subject to the following restrictions:

   1. The origin of this software must not be misrepresented; you
      must not claim that you wrote the original software. If you use
      this software in a product, an acknowledgment in the product
      documentation would be appreciated but is not required.

   2. Altered source versions must be plainly marked as such, and
      must not be misrepresented as being the original software.

   3. This notice may not be removed or altered from any source
      distribution.

   Christoffer Lindqvist
   swepopper@gmail.com
*/

#ifndef ENGINE_H
#define ENGINE_H
#pragma once

#include <future>

#include "frame_time.h"

union SDL_Event;

namespace Acclution {

class ApplicationEventRunner;
class CommandBuffer;
class ComponentManager;
class Config;
class ContentManager;
class FrameAllocator;
class GUI;
class Input;
class Log;
class Physics;
class PlatformUpdater;
class PluginSystem;
class Scene;
class SceneRenderer;
class ScriptManager;
class TaskScheduler;
class Video;

/*!
 * \brief This is the main class. Here most of our classes are initialized.
 *
 * Engine hold all the important classes that makes the Acclution Engine.
 * You can access Engine from the static GetInstance(); funciton and from there
 * you gain access to the AssetManager, ComponentManager, Config, FiberScheduler,
 * Input, IOThreads, Physics, SceneRenderer, ScriptManager and Video.
 *
 * Engine has a Singleton behaviour so you can only Initialize one instance at
 * a time.
 */
class Engine
{
public:
	Engine();
	~Engine();

	/*!
	 * \brief This function will initialize all the main classes of the engine.
	 *
	 * When this function returns then the enging will want to shutdown.
	 * You can call Release(); and then Initalize(); right after if you
	 * want to restart the engine.
	 * Only one instance of Engine can be Initialized at a time.
	 * \param event_based if true it will run the game loop only window after events have run
	 * otherwise it will run the loop even if there is no window events
	 * \return Returns true if succeded.
	 */
	bool Initialize(const UpdateType& update_type);

	/*!
	 * \brief This function will release and cleanup all the main classes.
	 */
	void Release();

	/*!
	 * \brief If initialize whent well then we can run the engine.
	 */
	void Run();

	/*!
	 * \brief This will get the main instance of the Engine class.
	 * \return The last engine Instance successfully calling Initialize();
	 *         Otherwise it returns nullptr.
	 */
	static Engine* GetInstance() { return s_instance; }

	/*!
	 * \brief Gets the ComponentManager that the Engine is using.
	 * \return A unique pointer of the ComponentManager
	 */
	const std::unique_ptr<ComponentManager>& GetComponentManager()	const { return m_component_manager; }

	/*!
	 * \brief Gets the Config that the Engine is using.
	 * \return A unique pointer of the Config
	 */
	const std::unique_ptr<Config>& GetConfig()						const { return m_config; }

	/*!
	 * \brief Gets the ContentManager that the Engine is using.
	 * \return A unique pointer of the ContentManager
	 */
	const std::unique_ptr<ContentManager>& GetContentManager()		const { return m_content_manager; }

	/*!
	 * \brief Gets the Input that the Engine is using.
	 * \return A unique pointer of the Input
	 */
	const std::unique_ptr<Input>& GetInput()						const { return m_input; }

	/*!
	 * \brief Gets the Log that the Engine is using.
	 * \return A unique pointer of the Log
	 */
	const std::unique_ptr<Log>& GetLog()							const { return m_log; }
	std::unique_ptr<Log>& GetLog()										  { return m_log; }

	/*!
	 * \brief Gets the Physics that the Engine is using.
	 * \return A unique pointer of the Physics
	 */
	const std::unique_ptr<Physics>& GetPhysics()					const { return m_physics; }

	/*!
	 * \brief Gets the PlatformUpdater
	 * \return A pointer to the platform updater
	 */
	PlatformUpdater* GetPlatformUpdater()							const { return m_platform_updater; }

	/*!
	 * \brief Gets the SceneRenderer that the Engine is using.
	 * \return A unique pointer of the SceneRenderer
	 */
	const std::unique_ptr<SceneRenderer>& GetSceneRenderer()		const { return m_scene_renderer; }

	/*!
	 * \brief Gets the ScriptManager that the Engine is using.
	 * \return A unique pointer of the ScriptManager
	 */
	const std::unique_ptr<ScriptManager>& GetScriptManager()		const { return m_script_manager; }

	/*!
	 * \brief Gets the TaskScheduler that the Engine is using.
	 * \return A unique pointer of the TaskScheduler
	 */
	const std::unique_ptr<TaskScheduler>& GetTaskScheduler()		const { return m_task_scheduler; }

	/*!
	 * \brief Gets the Video that the Engine is using.
	 * \return A unique pointer of the Video
	 */
	const std::unique_ptr<Video>& GetVideo()						const { return m_video; }

	/*!
	 * \brief GetCurrentGamePlayCommand
	 * Useful for when you want to add creation or destruction commands to the render pipeline before the render stage begins.
	 * \return Returns the reference for the current game play stages video command buffer
	 */
	CommandBuffer* GetCurrentGamePlayCommand()
	{
		int index = m_next_game_play_frame;
		return index >= 0 && index < 3 ? m_frames[index].m_command_buffer : nullptr;
	}

	/*!
	 * \brief Gets FrameAllocator for the current GamePlay frame
	 * If you know that you are in the GamePlay stage then use this frame allocator. Useful for stuff you want
	 * to be alive the entire frame (ex: Vertex Position Update).
	 * \return Returns the reference for the current game play stages video command buffer
	 */
	FrameAllocator* GetGamePlayFrameAllocator()
	{
		int index = m_next_game_play_frame;
		return index >= 0 && index < 3 ? m_frames[index].m_frame_allocator : nullptr;
	}

	/*!
	 * \brief Gets FrameAllocator for the current Render frame
	 * If you know that you are in the Render stage then use this frame allocator. Useful for stuff you want
	 * to be alive the entire frame (ex: Vertex Position Update).
	 * \return Returns the reference for the current game play stages video command buffer
	 */
	FrameAllocator* GetRenderFrameAllocator()
	{
		int index = m_next_command_buffer_frame;
		return index >= 0 && index < 3 ? m_frames[index].m_frame_allocator : nullptr;
	}

private:
	std::future<int> Update(std::promise<int>* promise);
	std::future<int> Draw(std::promise<int>* promise, int frame_index);

	void GamePlayStage(std::promise<int>* promise, int frame);

	void RenderCommandBufferStageStart(std::promise<int>* promise, int frame_index);
	void RenderCommandBufferStageEnd(std::promise<int>* promise);

	struct FrameData
	{
		uint64_t			m_count;
		FrameTime			m_frame_time;

		CommandBuffer*		m_command_buffer;
		FrameAllocator*		m_frame_allocator;
	};

	static Engine*			s_instance;
	UpdateType				m_update_type;
	std::atomic_ullong		m_frame_count;
	FrameData				m_frames[3];
	std::atomic<int>		m_next_game_play_frame;
	std::atomic<int>		m_next_command_buffer_frame;
	std::atomic<int>		m_next_frame_render_frame;
	std::chrono::high_resolution_clock::time_point m_last_time;
	int64_t					m_last_smoothed_delta_time_nano;

	Allocator*								m_default_allocator;
	std::unique_ptr<ComponentManager>		m_component_manager;
	std::unique_ptr<Config>					m_config;
	std::unique_ptr<ContentManager>			m_content_manager;
	std::unique_ptr<GUI>					m_gui;
	std::unique_ptr<Input>					m_input;
	std::unique_ptr<Log>					m_log;
	std::unique_ptr<Physics>				m_physics;
	PlatformUpdater*						m_platform_updater;
	std::unique_ptr<PluginSystem>			m_plugin_system;
	std::unique_ptr<Scene>					m_scene;
	std::unique_ptr<SceneRenderer>			m_scene_renderer;
	std::unique_ptr<ScriptManager>			m_script_manager;
	std::unique_ptr<TaskScheduler>			m_task_scheduler;
	std::unique_ptr<Video>					m_video;
};

}

#endif // ENGINE_H
