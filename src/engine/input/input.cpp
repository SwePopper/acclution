/*
   Acclution
   Copyright (c) 2016 Christoffer Lindqvist

   This software is provided 'as-is', without any express or implied
   warranty. In no event will the authors be held liable for any
   damages arising from the use of this software.

   Permission is granted to anyone to use this software for any
   purpose, including commercial applications, and to alter it and
   redistribute it freely, subject to the following restrictions:

   1. The origin of this software must not be misrepresented; you
      must not claim that you wrote the original software. If you use
      this software in a product, an acknowledgment in the product
      documentation would be appreciated but is not required.

   2. Altered source versions must be plainly marked as such, and
      must not be misrepresented as being the original software.

   3. This notice may not be removed or altered from any source
      distribution.

   Christoffer Lindqvist
   swepopper@gmail.com
*/

#include "engine/platform.h"
#include "input.h"
#include "engine/engine.h"
#include "engine/platform_updater/platform_updater.h"
#include "engine/platform_updater/platform_events.h"

namespace Acclution {

Input::Input()
{
}

bool Input::Initialize()
{
	Engine::GetInstance()->GetPlatformUpdater()->AddEventCallback([](const PlatformEvent* event, void* user_data) {
			Input* input = reinterpret_cast<Input*>(user_data);
			if(input != nullptr && event != nullptr)
				input->OnInputEvent(event);
		}, this);

	return true;
}

void Input::Release()
{
}

void Input::AddActionCallback(const uint32_t& context_id, const uint32_t& action_id, InputActionCallback callback, void* user_data)
{
	uint32_t context = GetContextIndex(context_id);
	m_contexts[context].m_action_callbacks[action_id].push_back(astd::pair<InputActionCallback, void*>(callback, user_data));
}

void Input::RemoveActionCallback(const uint32_t& context_id, const uint32_t& action_id, InputActionCallback callback, void* user_data)
{
	uint32_t context = GetContextIndex(context_id);
	ActionCallbacks::iterator it = m_contexts[context].m_action_callbacks.find(action_id);
	if(it == m_contexts[context].m_action_callbacks.end() || it->second.empty())
		return;
	astd::EraseRemove(it->second, astd::pair<InputActionCallback, void*>(callback, user_data));
}

void Input::OnInputEvent(const PlatformEvent* event)
{
	InputActionData data(m_contexts[m_current_context].m_context_id);

	switch(event->GetEventType())
	{
	case PlatformEventType::kMouseMove: {
			const PlatformEvents::MouseMove* mouse_move = dynamic_cast<const PlatformEvents::MouseMove*>(event);
			if(mouse_move != nullptr)
			{
				data.m_action_id = 0;
				data.m_value1 = mouse_move->GetPositionX();
				data.m_value2 = mouse_move->GetPositionY();
			}
		} break;

	case PlatformEventType::kMouseButton: {
			const PlatformEvents::MouseButton* mouse_button = dynamic_cast<const PlatformEvents::MouseButton*>(event);
			if(mouse_button != nullptr)
			{
				data.m_action_id = 0;
				data.m_value1 = mouse_button->GetState() ? 1.0f : 0.0f;
			}
		} break;

	case PlatformEventType::kKeyboard: {
			const PlatformEvents::Keyboard* keyboard = dynamic_cast<const PlatformEvents::Keyboard*>(event);
			if(keyboard != nullptr)
			{
				data.m_action_id = 0;
				data.m_value1 = keyboard->GetState() ? 1.0f : 0.0f;
			}
		} break;

	case PlatformEventType::kWindowResize:
	default: return;
	}

	const ActionCallbackData& action_callback_data = m_contexts[m_current_context].m_action_callbacks[data.m_action_id];
	for(ActionCallbackData::const_iterator it = action_callback_data.begin(); it != action_callback_data.end(); ++it)
	{
		data.m_user_data = it->second;
		it->first(&data);
	}
}

uint32_t Input::GetContextIndex(const uint32_t& context_id)
{
	uint32_t size = static_cast<uint32_t>(m_contexts.size());
	for(uint32_t i = 0; i < size; ++i)
	{
		if(m_contexts[i].m_context_id == context_id)
			return i;
	}

	m_contexts.push_back(Context());
	m_contexts.back().m_context_id = context_id;
	return size;
}

}
