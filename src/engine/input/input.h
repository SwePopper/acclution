/*
   Acclution
   Copyright (c) 2016 Christoffer Lindqvist

   This software is provided 'as-is', without any express or implied
   warranty. In no event will the authors be held liable for any
   damages arising from the use of this software.

   Permission is granted to anyone to use this software for any
   purpose, including commercial applications, and to alter it and
   redistribute it freely, subject to the following restrictions:

   1. The origin of this software must not be misrepresented; you
      must not claim that you wrote the original software. If you use
      this software in a product, an acknowledgment in the product
      documentation would be appreciated but is not required.

   2. Altered source versions must be plainly marked as such, and
      must not be misrepresented as being the original software.

   3. This notice may not be removed or altered from any source
      distribution.

   Christoffer Lindqvist
   swepopper@gmail.com
*/

#ifndef INPUT_H
#define INPUT_H
#pragma once

#include <glm/glm.hpp>

namespace Acclution
{

class PlatformEvent;

class InputActionData
{
public:
	InputActionData(const uint32_t& context_id) : m_context_id(context_id), m_action_id(static_cast<uint32_t>(-1)), m_value1(0.0f), m_value2(0.0f), m_user_data(nullptr) {}

	const uint32_t& GetContextID() const { return m_context_id; }
	const uint32_t& GetActionID() const { return m_action_id; }

	bool GetState() const { return m_value1 > 0.0f; }

	const float& GetValue1() const { return m_value1; }
	const float& GetValue2() const { return m_value2; }
	void GetValues(glm::vec2& values) const { values.x = m_value1; values.y = m_value2; }

	void* GetUserData() const { return m_user_data; }

private:
	uint32_t	m_context_id;
	uint32_t	m_action_id;
	float		m_value1;
	float		m_value2;
	void*		m_user_data;

	friend class Input;
};

class Input
{
public:
	using InputActionCallback = void (*)(const InputActionData* input_action_data);

	Input();

	bool Initialize();
	void Release();

	void AddActionCallback(const uint32_t& context_id, const uint32_t& action_id, InputActionCallback callback, void* user_data);
	void RemoveActionCallback(const uint32_t& context_id, const uint32_t& action_id, InputActionCallback callback, void* user_data);

private:
	using ActionCallbackData = astd::vector<astd::pair<InputActionCallback, void*> >;
	using ActionCallbacks = astd::map<uint32_t, ActionCallbackData>;

	struct Context
	{
		uint32_t			m_context_id;
		ActionCallbacks		m_action_callbacks;
	};

	void OnInputEvent(const PlatformEvent* event);
	uint32_t GetContextIndex(const uint32_t& context_id);

	astd::vector<Context>		m_contexts;
	uint32_t					m_current_context;
};

}

#endif // INPUT_H
