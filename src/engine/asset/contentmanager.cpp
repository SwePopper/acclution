#include "engine/platform.h"
#include "contentmanager.h"

#include "engine/utilities.h"
#include "engine/io/readtask.h"

#include "asset.h"
#include "content_readers/texture2dreader.h"
#include "content_readers/fontreader.h"
#include "content_readers/spirvreader.h"
#include "content_readers/modelreader.h"

namespace Acclution {
	
enum class ContentReaderTypes : uint32_t {
	kSpirVReader = Utilities::Hash32("SpirVShaderProcessor"),
	kTexture2DReader = Utilities::Hash32("TextureProcessor"),
	kFontReader = Utilities::Hash32("FontProcessor"),
	kModelReader = Utilities::Hash32("ModelProcessor")
};

enum class Platform : uint8_t
{
	kDesktop,
	kCount
};

ContentManager::ContentManager()
{}

bool ContentManager::Initialize(const char* root_directory)
{
	if(Initialized())
		return true;

	m_root_directory = root_directory;
	astd::replace(m_root_directory.begin(), m_root_directory.end(), '\\', '/');
	return true;
}

void ContentManager::Release()
{
	RemoveAllAssets();
	m_root_directory = "";
}

void ContentManager::RemoveAllAssets()
{
	for(LoadedContent::iterator it = m_loaded_content.begin(); it != m_loaded_content.end(); ++it)
	{
		delete it->second;
	}
	m_loaded_content.clear();
}

Asset* ContentManager::LoadAsset(const astd::string& asset_name)
{
	astd::string path = asset_name;
	astd::replace(path.begin(), path.end(), '\\', '/');

	LoadedContent::const_iterator it = m_loaded_content.find(path);
	if(it != m_loaded_content.end())
	{
		return it->second;
	}

	Asset* asset = ReadAsset(path);
	if(asset != nullptr)
	{
		m_loaded_content[path] = asset;
	}
	return asset;
}

struct ReadData
{
	Asset* asset;
	astd::string directory;
	astd::string asset_name;
	astd::string path;
};

Asset* ContentManager::ReadAsset(const astd::string& asset_name)
{
	ReadData* data = new ReadData();

	data->asset_name = asset_name;
	data->directory = Utilities::GetDirectory(asset_name);
	data->path = m_root_directory + asset_name;
	astd::replace(data->path.begin(), data->path.end(), '/', DIRECTORY_SEPARATOR);

	data->asset = nullptr;

	std::future<bool> result = IO::ReadFile(&ContentManager::AsyncReadAsset, this, reinterpret_cast<void*>(data), data->path + ".adf", true, 0, 0, 8192);
	if(!result.get())
	{
		delete data;
		ALogError("Failed to load asset: %s", asset_name.c_str());
		return nullptr;
	}

	Asset* asset = data->asset;
	delete data;
	asset->SetLoadingResult(Asset::LoadingResult::kSuccess);
	return asset;
}

bool ContentManager::AsyncReadAsset(IO::FileDeserializer* deserializer, void* param)
{
	ReadData* read_data = reinterpret_cast<ReadData*>(param);

	// The first 4 bytes should be the "XNB" header.
	uint8_t platform = 0;
	if(!deserializer->Deserialize(platform) || platform != 'A') { return false; }
	if(!deserializer->Deserialize(platform) || platform != 'D') { return false; }
	if(!deserializer->Deserialize(platform) || platform != 'F') { return false; }
	if(!deserializer->Deserialize(platform)) { return false; }

	// Check the platform and see if we are supported
	if(platform >= static_cast<uint8_t>(Platform::kCount))
		return false;

	uint8_t version = 0;
	if(!deserializer->Deserialize(version)) { return false; }

	// Check for valid version
	if(version != 1) { return false; }

	uint8_t flags = 0;
	if(!deserializer->Deserialize(flags)) { return false; }

	bool compressed = !!(flags & static_cast<uint8_t>(Flags::kCompressed));

	uint64_t adf_size = 0;
	if(!deserializer->Deserialize(adf_size)) { return false; }

	if(compressed)
	{
		uint64_t decompressed_size = 0;
		if(!deserializer->Deserialize(decompressed_size)) { return false; }

		// Unsupported
		return false;
	}

	uint64_t reader_name_length = 0;
	if(!deserializer->Deserialize(reader_name_length) || reader_name_length == 0)
		return false;

	char* reader_name = reinterpret_cast<char*>(alloca(static_cast<uint32_t>(reader_name_length)+1));
	if(!deserializer->Deserialize(reader_name, reader_name_length))
		return false;
	reader_name[reader_name_length] = '\0';

	ContentReaderType* content_reader = nullptr;

	uint32_t reader_name_hash = Utilities::Hash32(reader_name);
	switch(reader_name_hash)
	{
	case static_cast<uint32_t>(ContentReaderTypes::kTexture2DReader): content_reader = new Texture2DReader(this); break;
	case static_cast<uint32_t>(ContentReaderTypes::kFontReader): content_reader = new FontReader(this); break;
	case static_cast<uint32_t>(ContentReaderTypes::kSpirVReader): content_reader = new SpirVReader(this); break;
	case static_cast<uint32_t>(ContentReaderTypes::kModelReader): content_reader = new ModelReader(this); break;
	default: ALogError("Could not find content reader type for %s", reader_name); return false;
	}

	read_data->asset = content_reader->Read(deserializer);

	delete content_reader;
	return read_data->asset != nullptr;
}

}
