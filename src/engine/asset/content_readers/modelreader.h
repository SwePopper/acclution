#ifndef MODELREADER_H
#define MODELREADER_H
#pragma once

#include "engine/asset/contentreadertype.h"

namespace Acclution {

struct Mesh;
struct Material;
struct Node;

class ModelReader : public ContentReaderType
{
public:
	ModelReader(ContentManager* manager) : ContentReaderType(manager) {}
	virtual Asset* Read(IO::FileDeserializer* deserializer);

private:
	bool Deserialize(IO::FileDeserializer* deserializer, Mesh* mesh);
	bool Deserialize(IO::FileDeserializer* deserializer, Material* material);
	bool Deserialize(IO::FileDeserializer *deserializer, Node* node);

	void GenerateVertexLayout(uint32_t vertex_type, Mesh* mesh);
};

}

#endif // TEXTURE2DREADER_H
