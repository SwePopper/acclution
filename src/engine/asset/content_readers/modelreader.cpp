#include "engine/platform.h"
#include "modelreader.h"

#include "engine/io/readtask.h"

#include "engine/engine.h"
#include "engine/graphics/video.h"
#include "engine/graphics/commandbuffer.h"
#include "engine/memory/frameallocator.h"

#include "engine/asset/model.h"

namespace {
	template<typename T>
	class AutoDelete
	{
	public:
		T* type;
		AutoDelete(T* t) : type(t) {}
		~AutoDelete() { delete type; }
	};

	enum class VertexType : uint32_t
	{
		kPosition =		0x1,
		kNormal =		0x2,
		kTangents =		0x4,
		kTexture1 =		0x10,
		kTexture2 =		0x20,
		kTexture3 =		0x40,
		kTexture4 =		0x80,
		kColor1 =		0x100,
		kColor2 =		0x200,
		kColor3 =		0x400,
		kColor4 =		0x800,
	};

	constexpr uint32_t kMaxVertexTextureCoords = 4;
	constexpr uint32_t kMaxVertexColors = 4;

	template<typename T>
	void* DeserializeIndices(Acclution::IO::FileDeserializer* deserializer, uint32_t count)
	{
		T* buffer = static_cast<T*>(Acclution::Engine::GetInstance()->GetGamePlayFrameAllocator()->AllocateBig(sizeof(T) * count));
		for(uint32_t i = 0; i < count; ++i)
		{
			if(!deserializer->Deserialize(buffer[i]))
			{
				return nullptr;
			}
		}
		return buffer;
	}
}

namespace Acclution {

Asset* ModelReader::Read(IO::FileDeserializer* deserializer)
{
	Model* model = new Model;
	AutoDelete<Model> deleter(model);

	uint32_t num_meshes = 0;
	if(!deserializer->Deserialize(num_meshes))
	{
		ALogError("Failed to deserialize num meshes!");
		return nullptr;
	}

	uint32_t num_materials = 0;
	if(!deserializer->Deserialize(num_materials))
	{
		ALogError("Failed to deserialize num materials!");
		return nullptr;
	}

	model->m_meshes.resize(num_meshes);
	model->m_materials.resize(num_materials);

	for(uint32_t i = 0; i < num_meshes; ++i)
	{
		if(!Deserialize(deserializer, &model->m_meshes[i]))
		{
			ALogError("Failed to deserialize mesh");
			return nullptr;
		}
	}

	for(uint32_t i = 0; i < num_materials; ++i)
	{
		if(!Deserialize(deserializer, &model->m_materials[i]))
		{
			ALogError("Failed to deserialize mesh");
			return nullptr;
		}
	}

	if(!Deserialize(deserializer, &model->m_root))
	{
		ALogError("Failed to deserialize root node");
		return nullptr;
	}

	// prevent deletion
	deleter.type = nullptr;
	return model;
}

bool ModelReader::Deserialize(IO::FileDeserializer* deserializer, Mesh* mesh)
{
	if(!deserializer->Deserialize(mesh->m_name))
	{
		ALogError("Failed to deserialize mesh name");
		return false;
	}

	if(!deserializer->Deserialize(mesh->m_material_index))
	{
		ALogError("Failed to deserialize mesh material index");
		return false;
	}

	uint32_t primitive_type = 0;
	if(!deserializer->Deserialize(primitive_type))
	{
		ALogError("Failed to deserialize mesh material index");
		return false;
	}

	mesh->m_primitive_type = primitive_type & 0x1 ? PrimitiveType::kPoint :
							primitive_type & 0x2 ? PrimitiveType::kLine :
							primitive_type & 0x4 ? PrimitiveType::kTriangle :
												   PrimitiveType::kPolygon;

	if(!deserializer->Deserialize(mesh->m_vertex_type))
	{
		ALogError("Failed to deserialize vertex type");
		return false;
	}

	GenerateVertexLayout(mesh->m_vertex_type, mesh);

	if(!deserializer->Deserialize(mesh->m_vertex_count))
	{
		ALogError("Failed to deserialize vertex count");
		return false;
	}

	void* vertex_data = Engine::GetInstance()->GetGamePlayFrameAllocator()->AllocateBig(mesh->m_vertex_size * mesh->m_vertex_count);

	// I could read in everything to the buffer but I am worried about endianness.. so I tead it manually
	char* ptr = static_cast<char*>(vertex_data);
	for(uint32_t i = 0; i < mesh->m_vertex_count; ++i)
	{
		for(uint32_t j = 0; j < mesh->m_vertex_declaration.size(); ++j)
		{
			uint32_t type_size = 0;
			switch(mesh->m_vertex_declaration[j].m_data_type)
			{
			case VertexDataType::kFloat: // same size
			case VertexDataType::kInt: {
					type_size = sizeof(uint32_t);
					uint32_t* iptr = reinterpret_cast<uint32_t*>(ptr);
					if(!deserializer->Deserialize(iptr, mesh->m_vertex_declaration[j].m_count))
					{
						ALogError("Failed to read vertex data");
						return false;
					}
				} break;
			}

			ptr += type_size * mesh->m_vertex_declaration[j].m_count;
		}
	}

	mesh->m_vertex_buffer = Engine::GetInstance()->GetVideo()->CreateVertexBuffer(
				vertex_data, mesh->m_vertex_size * mesh->m_vertex_count, BufferType::kVertex, BufferUsage::kStatic);

	if(!deserializer->Deserialize(mesh->m_index_count))
	{
		ALogError("Failed to deserialize index count");
		return false;
	}

	void* index_data = nullptr;
	if(mesh->m_vertex_count <= UCHAR_MAX)
	{
		mesh->m_index_size = sizeof(uint8_t);
		index_data = DeserializeIndices<uint8_t>(deserializer, mesh->m_index_count);
	}
	else if(mesh->m_vertex_count <= USHRT_MAX)
	{
		mesh->m_index_size = sizeof(uint16_t);
		index_data = DeserializeIndices<uint16_t>(deserializer, mesh->m_index_count);
	}
	else
	{
		mesh->m_index_size = sizeof(uint32_t);
		index_data = DeserializeIndices<uint32_t>(deserializer, mesh->m_index_count);
	}

	if(index_data == nullptr)
	{
		ALogError("Failed to load index data");
		return false;
	}

	mesh->m_index_buffer = Engine::GetInstance()->GetVideo()->CreateVertexBuffer(
				index_data, mesh->m_index_size * mesh->m_index_count, BufferType::kIndex, BufferUsage::kStatic);

	return true;
}

bool ModelReader::Deserialize(IO::FileDeserializer* deserializer, Material* material)
{
	if(!deserializer->Deserialize(material->m_name))
	{
		ALogError("Failed to deserialize material name");
		return false;
	}

	if(!deserializer->Deserialize(material->m_two_sided))
	{
		ALogError("Failed to deserialize material two sided");
		return false;
	}

	if(!deserializer->Deserialize(material->m_wire_frame))
	{
		ALogError("Failed to deserialize material wire frame");
		return false;
	}

	uint8_t uivalue = 0;
	if(!deserializer->Deserialize(uivalue))
	{
		ALogError("Failed to deserialize material shading mode");
		return false;
	}
	material->m_shading_mode = static_cast<ShadingMode>(uivalue);

	if(!deserializer->Deserialize(uivalue))
	{
		ALogError("Failed to deserialize material blend mode");
		return false;
	}
	material->m_blend_mode = static_cast<BlendMode>(uivalue);

	if(!deserializer->Deserialize(material->m_opacity))
	{
		ALogError("Failed to deserialize material opacity");
		return false;
	}

	if(!deserializer->Deserialize(material->m_bump_scaling))
	{
		ALogError("Failed to deserialize material bump scaling");
		return false;
	}

	if(!deserializer->Deserialize(material->m_shininess))
	{
		ALogError("Failed to deserialize material shininess");
		return false;
	}

	if(!deserializer->Deserialize(material->m_shininess_strength))
	{
		ALogError("Failed to deserialize material shininess strength");
		return false;
	}

	if(!deserializer->Deserialize(material->m_reflectivity))
	{
		ALogError("Failed to deserialize material reflectivity");
		return false;
	}

	if(!deserializer->Deserialize(material->m_refracti))
	{
		ALogError("Failed to deserialize material refracti");
		return false;
	}

	if(!deserializer->Deserialize(material->m_diffuse))
	{
		ALogError("Failed to deserialize material diffuse");
		return false;
	}

	if(!deserializer->Deserialize(material->m_ambient))
	{
		ALogError("Failed to deserialize material ambient");
		return false;
	}

	if(!deserializer->Deserialize(material->m_specular))
	{
		ALogError("Failed to deserialize material specular");
		return false;
	}

	if(!deserializer->Deserialize(material->m_emissive))
	{
		ALogError("Failed to deserialize material emissive");
		return false;
	}

	if(!deserializer->Deserialize(material->m_transparent))
	{
		ALogError("Failed to deserialize material transparent");
		return false;
	}

	if(!deserializer->Deserialize(material->m_reflective))
	{
		ALogError("Failed to deserialize material reflective");
		return false;
	}

	for(uint32_t i = static_cast<uint32_t>(TextureType::kDiffuse); i < static_cast<uint32_t>(TextureType::kUnknown); ++i)
	{
		uint32_t count = 0;
		if(!deserializer->Deserialize(count))
		{
			ALogError("Failed to deserialize texture count");
			return false;
		}

		material->m_texture_slots[i].resize(count);
		for(uint32_t j = 0; j < count; ++j)
		{
			if(!deserializer->Deserialize(material->m_texture_slots[i][j].m_texture_name))
			{
				ALogError("Failed to deserialize material texture name");
				return false;
			}

			if(!deserializer->Deserialize(material->m_texture_slots[i][j].m_mapping))
			{
				ALogError("Failed to deserialize material texture mapping");
				return false;
			}

			if(!deserializer->Deserialize(material->m_texture_slots[i][j].m_uvindex))
			{
				ALogError("Failed to deserialize material texture uvindex");
				return false;
			}

			if(!deserializer->Deserialize(material->m_texture_slots[i][j].m_blend))
			{
				ALogError("Failed to deserialize material texture blend");
				return false;
			}

			if(!deserializer->Deserialize(material->m_texture_slots[i][j].m_texture_op))
			{
				ALogError("Failed to deserialize material texture op");
				return false;
			}

			if(!deserializer->Deserialize(material->m_texture_slots[i][j].m_mapmode[0]))
			{
				ALogError("Failed to deserialize material texture mapmode");
				return false;
			}

			if(!deserializer->Deserialize(material->m_texture_slots[i][j].m_mapmode[1]))
			{
				ALogError("Failed to deserialize material texture mapmode");
				return false;
			}
		}
	}

	return true;
}

bool ModelReader::Deserialize(IO::FileDeserializer *deserializer, Node* node)
{
	if(!deserializer->Deserialize(node->m_name))
	{
		ALogError("Failed to deserialize node name");
		return false;
	}

	if(!deserializer->Deserialize(node->m_transform))
	{
		ALogError("Failed to deserialize node transform");
		return false;
	}

	uint32_t count = 0;
	if(!deserializer->Deserialize(count))
	{
		ALogError("Failed to deserialize node mesh count");
		return false;
	}

	node->m_meshes.resize(count);
	for(uint32_t i = 0; i < count; ++i)
	{
		if(!deserializer->Deserialize(node->m_meshes[i]))
		{
			ALogError("Failed to deserialize node mesh");
			return false;
		}
	}

	if(!deserializer->Deserialize(count))
	{
		ALogError("Failed to deserialize node child count");
		return false;
	}

	node->m_children.resize(count);
	for(uint32_t i = 0; i < count; ++i)
	{
		if(!Deserialize(deserializer, &node->m_children[i]))
		{
			ALogError("Failed to deserialize node child");
			return false;
		}
	}

	return true;
}

void ModelReader::GenerateVertexLayout(uint32_t vertex_type, Mesh* mesh)
{
	mesh->m_vertex_size = 0;

	if(vertex_type & static_cast<uint32_t>(VertexType::kPosition))
	{
		VertexDeclaration declaration = { mesh->m_vertex_size, VertexDataType::kFloat, 3 };
		mesh->m_vertex_size += sizeof(float) * declaration.m_count;
		mesh->m_vertex_declaration.push_back(declaration);
	}

	if(vertex_type & static_cast<uint32_t>(VertexType::kNormal))
	{
		VertexDeclaration declaration = { mesh->m_vertex_size, VertexDataType::kFloat, 3 };
		mesh->m_vertex_size += sizeof(float) * declaration.m_count;
		mesh->m_vertex_declaration.push_back(declaration);
	}

	if(vertex_type & static_cast<uint32_t>(VertexType::kTangents))
	{
		VertexDeclaration declaration = { mesh->m_vertex_size, VertexDataType::kFloat, 6 };
		mesh->m_vertex_size += sizeof(float) * declaration.m_count;
		mesh->m_vertex_declaration.push_back(declaration);
	}

	for(uint32_t i = 0; i < kMaxVertexTextureCoords; ++i)
	{
		if(vertex_type & (1 << (i + 5)))
		{
			VertexDeclaration declaration = { mesh->m_vertex_size, VertexDataType::kFloat, 2 };
			mesh->m_vertex_size += sizeof(float) * declaration.m_count;
			mesh->m_vertex_declaration.push_back(declaration);
		}
	}

	for(uint32_t i = 0; i < kMaxVertexColors; ++i)
	{
		if(vertex_type & (1 << (i + 9)))
		{
			VertexDeclaration declaration = { mesh->m_vertex_size, VertexDataType::kFloat, 4 };
			mesh->m_vertex_size += sizeof(float) * declaration.m_count;
			mesh->m_vertex_declaration.push_back(declaration);
		}
	}
}

}
