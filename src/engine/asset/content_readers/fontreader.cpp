#include "engine/platform.h"
#include "fontreader.h"

#include "engine/io/readtask.h"

#include "engine/engine.h"
#include "engine/graphics/commandbuffer.h"
#include "engine/memory/frameallocator.h"
#include "engine/asset/texture.h"
#include "engine/asset/font.h"

#include "texture2dreader.h"

namespace Acclution {

Asset* FontReader::Read(IO::FileDeserializer* deserializer)
{
	uint64_t data_size = 0;
	if(!deserializer->Deserialize(data_size))
	{
		ALogError("Failed to deserialize font data size!\n");
		return nullptr;
	}

	char* data = new char[data_size+1];
	if(!deserializer->Deserialize(data, data_size))
	{
		delete[] data;
		ALogError("Failed to deserialize font data!\n");
		return nullptr;
	}
	data[data_size] = '\0';

	Font* font = new Font;
	if(!font->Initialize(data, data_size))
	{
		delete font;
		ALogError("Failed to initialize Font!");
		return nullptr;
	}

	return font;
}

}
