#ifndef SPIRVREADER_H
#define SPIRVREADER_H
#pragma once

#include "engine/asset/contentreadertype.h"

namespace Acclution {

class SpirVReader : public ContentReaderType
{
public:
	SpirVReader(ContentManager* manager) : ContentReaderType(manager) {}
	virtual Asset* Read(IO::FileDeserializer* deserializer);
};

}

#endif // TEXTURE2DREADER_H
