#include "engine/platform.h"
#include "spirvreader.h"

#include "engine/asset/shaderdata.h"

#include "engine/io/readtask.h"

namespace Acclution {

Asset* SpirVReader::Read(IO::FileDeserializer* deserializer)
{
	ShaderData* shader_data = new ShaderData;

	if(!deserializer->Deserialize(shader_data->m_name))
	{
		delete shader_data;
		return nullptr;
	}

	uint8_t shader_type = 0;
	if(!deserializer->Deserialize(shader_type))
	{
		ALogError("Failed to deserialize shader type!");
		delete shader_data;
		return nullptr;
	}
	shader_data->m_type = static_cast<ShaderData::ShaderType>(shader_type);

	if(!deserializer->Deserialize(shader_data->m_glsl_shader))
	{
		delete shader_data;
		return nullptr;
	}

	if(!deserializer->Deserialize(shader_data->m_spirv_vulkan_size))
	{
		ALogError("Failed to deserialize vulkan shader size!");
		delete shader_data;
		return nullptr;
	}

	if(shader_data->m_spirv_vulkan_size > 0)
	{
		shader_data->m_spirv_vulkan = malloc(static_cast<uint32_t>(shader_data->m_spirv_vulkan_size));
		if(!deserializer->Read(shader_data->m_spirv_vulkan, shader_data->m_spirv_vulkan_size))
		{
			ALogError("Failed to deserialize vulkan shader data!");
			delete shader_data;
			return nullptr;
		}
	}
	return shader_data;
}

}
