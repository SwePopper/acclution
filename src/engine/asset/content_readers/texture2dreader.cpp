#include "engine/platform.h"
#include "texture2dreader.h"

#include "engine/io/readtask.h"

#include "engine/engine.h"
#include "engine/graphics/video.h"
#include "engine/memory/frameallocator.h"
#include "engine/asset/texture.h"

#define GL3_PROTOTYPES 1
#define GL_GLEXT_PROTOTYPES
#include <GL/glew.h>
#include <SDL_opengl.h>

namespace Acclution {

Asset* Texture2DReader::Read(IO::FileDeserializer* deserializer)
{
	uint32_t width = 0;
	if(!deserializer->Deserialize(width))
		return nullptr;

	uint32_t height = 0;
	if(!deserializer->Deserialize(height))
		return nullptr;

	bool generate_mipmap = false;
	if(!deserializer->Deserialize(generate_mipmap))
		return nullptr;

	Texture* texture = new Texture;

	uint32_t data_size = 0;
	if(!deserializer->Deserialize(data_size))
	{
		delete texture;
		return nullptr;
	}

	uint32_t bpp = data_size / width / height;

	GLenum internal_format = GL_RGBA8;
	GLenum format = GL_BGRA;

	if(bpp == 24)
	{
		internal_format = GL_RGB8;
		format = GL_BGRA;
	}
	else if(bpp == 8)
	{
		internal_format = GL_RED;
		format = GL_RED;
	}

	void* data = Engine::GetInstance()->GetGamePlayFrameAllocator()->AllocateBig(data_size);
	if(!deserializer->Read(data, data_size))
	{
		delete texture;
		return nullptr;
	}

	texture->m_width = static_cast<uint16_t>(width);
	texture->m_height = static_cast<uint16_t>(height);
	texture->m_texture_object = Engine::GetInstance()->GetVideo()->CreateTexture(texture->m_width, texture->m_height,
											generate_mipmap, format, internal_format, GL_UNSIGNED_BYTE, data);

	return texture;
}

}
