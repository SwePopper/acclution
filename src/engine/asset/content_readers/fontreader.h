#ifndef FONTREADER_H
#define FONTREADER_H
#pragma once

#include "engine/asset/contentreadertype.h"

namespace Acclution {

class FontReader : public ContentReaderType
{
public:
	FontReader(ContentManager* manager) : ContentReaderType(manager) {}
	virtual Asset* Read(IO::FileDeserializer* deserializer);
};

}

#endif // TEXTURE2DREADER_H
