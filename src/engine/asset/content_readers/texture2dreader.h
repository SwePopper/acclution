#ifndef TEXTURE2DREADER_H
#define TEXTURE2DREADER_H
#pragma once

#include "engine/asset/contentreadertype.h"

namespace Acclution {

class Texture2DReader : public ContentReaderType
{
public:
	Texture2DReader(ContentManager* manager) : ContentReaderType(manager) {}
	virtual Asset* Read(IO::FileDeserializer* deserializer);
};

}

#endif // TEXTURE2DREADER_H
