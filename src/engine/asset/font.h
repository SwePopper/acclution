#ifndef SPRITEFONT_H
#define SPRITEFONT_H
#pragma once

#include "asset.h"

struct FT_LibraryRec_;
struct FT_FaceRec_;

namespace Acclution
{

class Texture;

class Font : public Asset
{
public:
	Font();
	virtual ~Font();

	bool Initialize(char* font_data, uint64_t font_data_size);

	const bool& HasKerning() const { return m_has_kerning; }

	bool SetCharacterSize(uint32_t size);
	bool SetCharacterPizelSize(uint32_t size);

private:
	char*				m_font_data;
	uint64_t			m_font_data_size;

	FT_LibraryRec_*		m_library;
	FT_FaceRec_*		m_font_face;
	bool				m_has_kerning;
};

}

#endif
