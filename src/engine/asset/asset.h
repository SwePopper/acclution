#ifndef ASSET_H
#define ASSET_H
#pragma once

namespace Acclution {

class Asset
{
public:
	enum class AssetType
	{
		kTexture,
		kModel,
		kShaderData,
		kFont,
		kList,
		kInternal
	};

	enum class LoadingResult
	{
		kFailed = -1,
		kNotDone = 0,
		kSuccess = 1
	};

	Asset(AssetType type) : m_loading_result(LoadingResult::kNotDone), m_path(""), m_persistent(false), m_asset_type(type) {}
	virtual ~Asset() {}

	bool IsPersistent() const { return m_persistent; }
	void SetPersistent(bool persistent) { m_persistent = persistent; }

	const LoadingResult& GetLoadingResult() const { return m_loading_result; }
	void SetLoadingResult(const LoadingResult& res) { m_loading_result = res; }

	const astd::string& GetPath() const { return m_path; }
	void SetPath(const astd::string& path) { m_path = path; }

	const AssetType& GetAssetType() const { return m_asset_type; }

private:
	LoadingResult	m_loading_result;
	astd::string	m_path;
	bool			m_persistent;
	AssetType		m_asset_type;
};

}

#endif // ASSET_H
