/*
   Acclution
   Copyright (c) 2016 Christoffer Lindqvist

   This software is provided 'as-is', without any express or implied
   warranty. In no event will the authors be held liable for any
   damages arising from the use of this software.

   Permission is granted to anyone to use this software for any
   purpose, including commercial applications, and to alter it and
   redistribute it freely, subject to the following restrictions:

   1. The origin of this software must not be misrepresented; you
      must not claim that you wrote the original software. If you use
      this software in a product, an acknowledgment in the product
      documentation would be appreciated but is not required.

   2. Altered source versions must be plainly marked as such, and
      must not be misrepresented as being the original software.

   3. This notice may not be removed or altered from any source
      distribution.

   Christoffer Lindqvist
   swepopper@gmail.com
*/

#ifndef SHADER_DATA_H
#define SHADER_DATA_H
#pragma once

#include "asset.h"

namespace Acclution {

class ShaderData : public Asset
{
public:
	enum class ShaderType : uint8_t
	{
		kUnknown,
		kVertex,
		kTessellationControl,
		kTesselationEvaluation,
		kGeometry,
		kFragment,
		kCompute
	};

	ShaderData() : Asset(AssetType::kShaderData),
		m_spirv_vulkan_size(0),
		m_spirv_vulkan(nullptr)
	{}

	virtual ~ShaderData()
	{
		if(m_spirv_vulkan != nullptr)
			free(m_spirv_vulkan);
	}

	astd::string	m_name;
	ShaderType		m_type;

	astd::string	m_glsl_shader;

	uint64_t		m_spirv_vulkan_size;
	void*			m_spirv_vulkan;
};

}

#endif // MODEL_H
