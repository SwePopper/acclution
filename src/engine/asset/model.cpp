#include "engine/platform.h"
#include "model.h"

#include "engine/engine.h"
#include "engine/graphics/video.h"

namespace Acclution
{

Model::~Model()
{
	Engine* engine = Engine::GetInstance();
	if(engine != nullptr)
	{
		for(astd::vector<Mesh>::iterator it = m_meshes.begin(); it != m_meshes.end(); ++it)
		{
			engine->GetVideo()->DestroyVertexBuffer(it->m_vertex_buffer);
			engine->GetVideo()->DestroyVertexBuffer(it->m_index_buffer);
		}
	}
}

}
