/*
   Acclution
   Copyright (c) 2016 Christoffer Lindqvist

   This software is provided 'as-is', without any express or implied
   warranty. In no event will the authors be held liable for any
   damages arising from the use of this software.

   Permission is granted to anyone to use this software for any
   purpose, including commercial applications, and to alter it and
   redistribute it freely, subject to the following restrictions:

   1. The origin of this software must not be misrepresented; you
      must not claim that you wrote the original software. If you use
      this software in a product, an acknowledgment in the product
      documentation would be appreciated but is not required.

   2. Altered source versions must be plainly marked as such, and
      must not be misrepresented as being the original software.

   3. This notice may not be removed or altered from any source
      distribution.

   Christoffer Lindqvist
   swepopper@gmail.com
*/

#ifndef TEXTURE_H
#define TEXTURE_H
#pragma once

#include "asset.h"

namespace Acclution {

class Texture : public Asset
{
public:
	Texture();
	virtual ~Texture();

	const uint32_t& GetTextureObject() const { return m_texture_object; }

	const uint16_t& GetWidth() const { return m_width; }
	const uint16_t& GetHeight() const { return m_height; }
	float GetAspect() const { return (float)m_height / (float)m_width; }

private:
	uint16_t		m_width;
	uint16_t		m_height;

	uint32_t		m_texture_object;

	friend class Texture2DReader;
};

}

#endif // TEXTURE_H
