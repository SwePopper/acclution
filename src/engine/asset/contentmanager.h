#ifndef CONTENTMANAGER_H
#define CONTENTMANAGER_H
#pragma once

namespace Acclution {

class Asset;

// Tried to port MonoGame's ContentManager https://github.com/mono/MonoGame/blob/develop/MonoGame.Framework/Content/ContentManager.cs
class ContentManager
{
public:
	ContentManager();
	~ContentManager() { Release(); }

	bool Initialize(const char* root_directory);
	void Release();

	bool Initialized() const { return !m_root_directory.empty(); }

	void RemoveAllAssets();

	template<typename T>
	T* LoadAsset(const astd::string& asset_name)
	{
		Asset* asset = LoadAsset(asset_name);
		return dynamic_cast<T*>(asset);
	}

private:
	enum class Flags : uint8_t
	{
		kNone			= 0x0,
		kCompressed		= 0x1
	};

	Asset* LoadAsset(const astd::string& asset_name);
	Asset* ReadAsset(const astd::string& asset_name);
	bool AsyncReadAsset(IO::FileDeserializer* deserializer, void* param);

	using LoadedContent = astd::unordered_map<astd::string, Asset*>;

	astd::string		m_root_directory;
	LoadedContent		m_loaded_content;
};

}

#endif // CONTENTMANAGER_H
