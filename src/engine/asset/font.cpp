#include "engine/platform.h"
#include "font.h"
#include "texture.h"

#include <ft2build.h>
#include FT_FREETYPE_H
#include FT_GLYPH_H

// https://www.freetype.org/freetype2/docs/tutorial/example2.cpp
// http://freetype.sourceforge.net/freetype2/docs/tutorial/step2.html

namespace Acclution
{

Font::Font()
	: Asset(AssetType::kFont),
	  m_font_data(nullptr),
	  m_font_data_size(0),
	  m_library(nullptr),
	  m_font_face(nullptr)
{}

Font::~Font()
{
	if(m_font_face != nullptr)
	{
		FT_Done_Face(m_font_face);
		m_font_face = nullptr;
	}

	if(m_library != nullptr)
	{
		FT_Done_FreeType(m_library);
		m_library = nullptr;
	}

	delete[] m_font_data;
	m_font_data = nullptr;
	m_font_data_size = 0;
}

bool Font::Initialize(char* font_data, uint64_t font_data_size)
{
	if(m_library != nullptr)
	{
		ALogError("Initializing font multiple times!\n");
		return false;
	}

	m_font_data = font_data;
	m_font_data_size = font_data_size;

	FT_Error error = FT_Init_FreeType(&m_library);
	if(error != 0)
	{
		ALogError("Failed to initialize freetype library! %i\n", error);
		return false;
	}

	error = FT_New_Memory_Face(m_library, (unsigned char*)m_font_data, (uint32_t)m_font_data_size, 0, &m_font_face);
	if(error != 0)
	{
		ALogError("Failed to create font face! %i\n", error);
		return false;
	}

	m_has_kerning = !!FT_HAS_KERNING(m_font_face);

	return SetCharacterSize(16);
}

bool Font::SetCharacterSize(uint32_t size)
{
	FT_Error error = FT_Set_Char_Size(m_font_face, 0, size * 64, 0, 0);
	if(error != 0)
	{
		ALogError("Failed to set font character size! %i\n", error);
		return false;
	}
	return true;
}

bool Font::SetCharacterPizelSize(uint32_t size)
{
	FT_Error error = FT_Set_Pixel_Sizes(m_font_face, 0, size);
	if(error != 0)
	{
		ALogError("Failed to set font character pixel size! %i\n", error);
		return false;
	}
	return true;
}

}
