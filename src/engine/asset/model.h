/*
   Acclution
   Copyright (c) 2016 Christoffer Lindqvist

   This software is provided 'as-is', without any express or implied
   warranty. In no event will the authors be held liable for any
   damages arising from the use of this software.

   Permission is granted to anyone to use this software for any
   purpose, including commercial applications, and to alter it and
   redistribute it freely, subject to the following restrictions:

   1. The origin of this software must not be misrepresented; you
      must not claim that you wrote the original software. If you use
      this software in a product, an acknowledgment in the product
      documentation would be appreciated but is not required.

   2. Altered source versions must be plainly marked as such, and
      must not be misrepresented as being the original software.

   3. This notice may not be removed or altered from any source
      distribution.

   Christoffer Lindqvist
   swepopper@gmail.com
*/

#ifndef MODEL_H
#define MODEL_H
#pragma once

#include "asset.h"

namespace Acclution {

enum class PrimitiveType
{
	kPoint,
	kLine,
	kTriangle,
	kPolygon,
};

enum class VertexDataType
{
	kFloat,
	kInt
};

enum class ShadingMode : uint8_t
{
	kFlat,
	kGouraud,
	kPhong,
	kBlinn,
	kToon,
	kOrenNayar,
	kMinnaert,
	kCookTorrance,
	kNoShading,
	kFresnel
};

enum class BlendMode : uint8_t
{
	kDefault,
	kAdditive
};

enum class TextureMapping : uint32_t
{
	kUV,
	kSphere,
	kCylinder,
	kBox,
	kPlane,
	kOther
};

enum class TextureOp : uint32_t
{
	kMultiply,
	kAdd,
	kSubtract,
	kDivide,
	kSmoothAdd,
	kSignedAdd
};

enum class TextureMapMode : uint32_t
{
	kWrap,
	kClamp,
	kMirror,
	kDecal
};

enum class TextureType : uint32_t
{
	kNone,
	kDiffuse,
	Specular,
	kAmbient,
	kEmissive,
	kHeight,
	kNormals,
	kShininess,
	kOpacity,
	kDisplacement,
	kLightmap,
	kReflection,
	kUnknown
};

struct VertexDeclaration
{
	uint32_t		m_offset;
	VertexDataType	m_data_type;
	uint32_t		m_count;
};

struct Mesh
{
	astd::string	m_name;
	uint32_t		m_material_index;
	PrimitiveType	m_primitive_type;
	uint32_t		m_vertex_type;
	uint32_t		m_vertex_count;
	uint32_t		m_index_count;

	uint32_t		m_vertex_buffer;
	uint32_t		m_index_buffer;

	uint32_t		m_vertex_size;
	uint32_t		m_index_size;
	astd::vector<VertexDeclaration>		m_vertex_declaration;
};

struct TextureSlot
{
	astd::string		m_texture_name;
	TextureMapping		m_mapping;
	uint32_t			m_uvindex;
	float				m_blend;
	TextureOp			m_texture_op;
	TextureMapMode		m_mapmode[2];

	TextureType			m_texture_type;
};

struct Material
{
	astd::string	m_name;
	bool			m_two_sided;
	bool			m_wire_frame;
	ShadingMode		m_shading_mode;
	BlendMode		m_blend_mode;
	float			m_opacity;
	float			m_bump_scaling;
	float			m_shininess;
	float			m_shininess_strength;
	float			m_reflectivity;
	float			m_refracti;
	glm::vec3		m_diffuse;
	glm::vec3		m_ambient;
	glm::vec3		m_specular;
	glm::vec3		m_emissive;
	glm::vec3		m_transparent;
	glm::vec3		m_reflective;

	typedef astd::vector<TextureSlot> TextureSlots;
	TextureSlots	m_texture_slots[(uint32_t)TextureType::kUnknown + 1];
};

struct Node
{
	astd::string			m_name;
	glm::mat4				m_transform;
	astd::vector<uint32_t>	m_meshes;
	astd::vector<Node>		m_children;
};

class Model : public Asset
{
public:
	Model() : Asset(AssetType::kModel) {}
	virtual ~Model();

	astd::vector<Mesh>			m_meshes;
	astd::vector<Material>		m_materials;

	Node	m_root;
};

}

#endif // MODEL_H
