#ifndef CONTENTREADERTYPE_H
#define CONTENTREADERTYPE_H
#pragma once

namespace Acclution {

class Asset;
class ContentManager;

class ContentReaderType
{
public:
	using SharedResourceCallbackFunction = void(*)(Asset*, void* user_data);
	using SharedResourceCallback = astd::vector<astd::pair<SharedResourceCallbackFunction, void*> >;
	using SharedResourceCallbacks = astd::map<int32_t, SharedResourceCallback>;

	ContentReaderType(ContentManager* manager) : m_content_manager(manager) {}
	virtual ~ContentReaderType() {}
	virtual Asset* Read(IO::FileDeserializer* deserializer)=0;

	const SharedResourceCallbacks& GetSharedResourceCallbacks() const { return m_shared_resource_callback; }

protected:
	ContentManager*			m_content_manager;

private:
	SharedResourceCallbacks	m_shared_resource_callback;
};

}

#endif // CONTENTREADERTYPE_H
