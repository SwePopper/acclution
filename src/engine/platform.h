/*
   Acclution
   Copyright (c) 2016 Christoffer Lindqvist

   This software is provided 'as-is', without any express or implied
   warranty. In no event will the authors be held liable for any
   damages arising from the use of this software.

   Permission is granted to anyone to use this software for any
   purpose, including commercial applications, and to alter it and
   redistribute it freely, subject to the following restrictions:

   1. The origin of this software must not be misrepresented; you
      must not claim that you wrote the original software. If you use
      this software in a product, an acknowledgment in the product
      documentation would be appreciated but is not required.

   2. Altered source versions must be plainly marked as such, and
      must not be misrepresented as being the original software.

   3. This notice may not be removed or altered from any source
      distribution.

   Christoffer Lindqvist
   swepopper@gmail.com
*/

#ifndef PLATFORM_H
#define PLATFORM_H
#pragma once

#include "defines.h"

#include <stdio.h>
#include <stddef.h>
#include <stdint.h>
#include <stdlib.h>
#include <stdarg.h>
#include <assert.h>
#include <string.h>
#include <time.h>
#include <malloc.h>
#include <new>
#include <stdexcept>
#include <limits.h>
#include <float.h>
#include <type_traits>
#include <memory>
#include <cmath>
#include <ctype.h>
#include <fstream>

#include <thread>
#include <atomic>
#include <mutex>
#include <condition_variable>
#include <future>

#if defined(PLATFORM_WINDOWS)

#ifdef __MINGW32__
#pragma GCC diagnostic ignored "-Wunknown-pragmas"
#pragma GCC diagnostic ignored "-Wunused-function"
#else
#pragma warning(disable : 4091) // warning C4091: 'typedef ': ignored on left of '' when no variable is declared  from DbgHelp.h
#pragma warning(disable : 4100) // warning C4100: 't': unreferenced formal parameter
#pragma warning(disable : 4127) // warning C4127 : conditional expression is constant
#pragma warning(disable : 4201) // warning C4201: nonstandard extension used: nameless struct/union
#pragma warning(disable : 4996) // warning C4996: 'x': This function or variable may be unsafe. Consider using x_s instead. To disable deprecation, use _CRT_SECURE_NO_WARNINGS. See online help for details.
#endif

#include <windows.h>
#include <intrin.h>
#include <dbghelp.h>
#include <direct.h>

#elif defined(PLATFORM_LINUX)

#include <alloca.h>
#include <sys/time.h>
#include <sys/types.h>
#include <dlfcn.h>
#include <ucontext.h>
#include <dlfcn.h>
#include <pthread.h>
#include <execinfo.h>

#endif

#if defined(USE_EASTL)

#include <EASTL/allocator.h>
#include <EASTL/allocator_malloc.h>
#include <EASTL/fixed_allocator.h>
#include <EASTL/core_allocator_adapter.h>
#include <EASTL/iterator.h>

#include <EASTL/algorithm.h>
#include <EASTL/functional.h>
#include <EASTL/tuple.h>

#include <EASTL/vector.h>
#include <EASTL/map.h>
#include <EASTL/string.h>
#include <EASTL/list.h>
#include <EASTL/set.h>
#include <EASTL/queue.h>
#include <EASTL/hash_map.h>

#else

#include <algorithm>
#include <functional>
#include <tuple>

#include <vector>
#include <map>
#include <string>
#include <list>
#include <set>
#include <queue>
#include <unordered_map>
#include <bitset>

#endif

#ifdef PLATFORM_WINDOWS
#pragma warning ( push )
#pragma warning (disable : 4245)
#else
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wshadow"
#pragma GCC diagnostic ignored "-Wunused-variable"
#endif

#include <randutils.hpp>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

#include <concurrentqueue.h>

#ifdef PLATFORM_WINDOWS
#pragma warning ( pop )
#else
#pragma GCC diagnostic pop
#endif

#include "memory/astd_allocator.h"
#include "types/types.h"
#include "functions.h"
#include "debug/log.h"

#endif // PLATFORM_H
