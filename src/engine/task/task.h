/*
   Acclution
   Copyright (c) 2016 Christoffer Lindqvist

   This software is provided 'as-is', without any express or implied
   warranty. In no event will the authors be held liable for any
   damages arising from the use of this software.

   Permission is granted to anyone to use this software for any
   purpose, including commercial applications, and to alter it and
   redistribute it freely, subject to the following restrictions:

   1. The origin of this software must not be misrepresented; you
      must not claim that you wrote the original software. If you use
      this software in a product, an acknowledgment in the product
      documentation would be appreciated but is not required.

   2. Altered source versions must be plainly marked as such, and
      must not be misrepresented as being the original software.

   3. This notice may not be removed or altered from any source
      distribution.

   Christoffer Lindqvist
   swepopper@gmail.com
*/

#ifndef TASK_H
#define TASK_H
#pragma once

namespace Acclution {

namespace Task
{

/*!
 * \brief Task Interface.
 *
 * This class is an interface for tasks. It is like std::bind and std::function but
 * now I can store those functions in a list or vector without the need to have templated
 * stuff in the way.
 */
class Task
{
public:
	virtual ~Task() {}

	/*!
	 * \brief Runs the stored std::function with all the arguments.
	 */
	virtual void Execute()=0;

	/*!
	 * \brief Checks if this task has run.
	 * \return True if the task have been run.
	 */
	bool HasRun() const { return m_has_run; }

	/*!
	 * \brief Resets the has run flag to false.
	 */
	void Reset() { m_has_run = false; }

protected:
	Task() : m_has_run(false) {}

	// Can't use atomic_bool because of copy stuff..
	// so hopefully volatile will be enough
	volatile bool m_has_run;
};

namespace detail
{

/*!
 * \brief Template Magic.
 *
 * I think it does something similar to this:
 *     http://en.cppreference.com/w/cpp/utility/integer_sequence
 */
template <std::size_t... Ts>
struct Index {};

/*!
 * \brief Template Magic.
 *
 * I think it does something similar to this:
 *     http://en.cppreference.com/w/cpp/utility/integer_sequence
 */
template <std::size_t N, std::size_t... Ts>
struct GenerateSequence : GenerateSequence<N - 1, N - 1, Ts...> {};

/*!
 * \brief Template Magic.
 *
 * I think it does something similar to this:
 *     http://en.cppreference.com/w/cpp/utility/integer_sequence
 */
template <std::size_t... Ts>
struct GenerateSequence<0, Ts...> : Index<Ts...> {};

/*!
 * \brief Helper bind functions.
 *
 * Adds the amount of placeholders needed automatically
 */
namespace bind
{

/*!
 * \brief Binds function with no arguments.
 */
template<class Function, typename Object>
std::function<void()> BindFunction(Function function, Object object)
{
	return std::bind(function, object);
}

/*!
 * \brief Binds function with one argument.
 */
template<class Function, typename Object, typename Arg1>
std::function<void(Arg1)> BindFunction(Function function, Object object, Arg1 /*arg1*/)
{
	return std::bind(function, object, std::placeholders::_1);
}

/*!
 * \brief Binds function with two arguments.
 */
template<class Function, typename Object, typename Arg1, typename Arg2>
std::function<void(Arg1, Arg2)> BindFunction(Function function, Object object, Arg1 /*arg1*/, Arg2 /*arg2*/)
{
	return std::bind(function, object, std::placeholders::_1, std::placeholders::_2);
}

/*!
 * \brief Binds function with three arguments.
 */
template<class Function, typename Object, typename Arg1, typename Arg2, typename Arg3>
std::function<void(Arg1, Arg2, Arg3)> BindFunction(Function function, Object object, Arg1 /*arg1*/, Arg2 /*arg2*/, Arg3 /*arg3*/)
{
	return std::bind(function, object, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3);
}

/*!
 * \brief Binds function with four arguments.
 */
template<class Function, typename Object, typename Arg1, typename Arg2, typename Arg3, typename Arg4>
std::function<void(Arg1, Arg2, Arg3, Arg4)> BindFunction(Function function, Object object, Arg1 /*arg1*/, Arg2 /*arg2*/, Arg3 /*arg3*/, Arg4 /*arg4*/)
{
	return std::bind(function, object, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3, std::placeholders::_4);
}

}

/*!
 * \brief Task Implementation.
 *
 * This class does all the magic of calling the task's std::function with all the arguments.
 * The arguments are stured in a tuple and when the user calls Execute the tuple will be
 * sequensed.. via template magic.
 */
template<typename... Args>
class TaskImpl : public Task
{
public:
	/*!
	 * \brief Constructor ot the TaskImpl class.
	 *
	 * Sets the std::function and makes a tuple of all the arguments.
	 */
	template<typename Function>
	TaskImpl(Function function, Args... args)
		: m_function(std::forward<Function>(function)),
		  m_args(std::make_tuple(std::forward<Args>(args)...))
	{}

	/*!
	 * \brief Executes the task
	 *
	 * Runs the std::function with all the arguments stored in the tuple.
	 */
	virtual void Execute()
	{
		func(m_args, detail::GenerateSequence<sizeof...(Args)>{});
		m_has_run = true;
	}

private:
	template <std::size_t... Is>
	void func(std::tuple<Args...>& tup, detail::Index<Is...>)
	{
		m_function(std::get<Is>(tup)...);
	}

	std::function<void (Args...)>	m_function;
	std::tuple<Args...>				m_args;
};

}

/*!
 * \brief Makes a Task from a function pointer or lambda and stores it in a shared_ptr.
 *
 * Example of creating a lambda Task:
 *     Task::make_task([] (int a, int b) { fprintf(stderr, "%i\n", a + b); }, 2, 3);
 *
 * Example of creating an ordinary function Task:
 *     void Function(int i) { printf("%i\n", i); }
 *      ...
 *     Task::make_task(Function, 6);
 *
 * \return Returns a std::shared_ptr<Task::Task>
 */
template <typename Function, typename... Args>
std::shared_ptr<detail::TaskImpl<Args...> > make_task(Function f, Args... args)
{
	return std::make_shared<detail::TaskImpl<Args...>>(detail::TaskImpl<Args...>(std::forward<Function>(f), std::forward<Args>(args)...));
}

/*!
 * \brief Makes a Task from a member function and stores it in a shared_ptr
 *
 * Example:
 *     class Object { public: void Function(float x, float y) { printf("%f %f\n", x, y); } };
 *      ...
 *     Object object;
 *     Task::make_taskmf(&Object::Function, &object, 1.0f, 5.0f)
 *
 * \return Returns a std::shared_ptr<Task::Task>
 */
template <typename Function, typename Object, typename... Args>
std::shared_ptr<detail::TaskImpl<Args...> > make_taskmf(Function f, Object object, Args... args)
{
	auto bound = detail::bind::BindFunction(f, object, std::forward<Args>(args)...);
	return std::make_shared<detail::TaskImpl<Args...>>(detail::TaskImpl<Args...>(bound, std::forward<Args>(args)...));
}

}

}

#endif // TASK_H
