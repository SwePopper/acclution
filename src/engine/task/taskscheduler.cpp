/*
   Acclution
   Copyright (c) 2016 Christoffer Lindqvist

   This software is provided 'as-is', without any express or implied
   warranty. In no event will the authors be held liable for any
   damages arising from the use of this software.

   Permission is granted to anyone to use this software for any
   purpose, including commercial applications, and to alter it and
   redistribute it freely, subject to the following restrictions:

   1. The origin of this software must not be misrepresented; you
      must not claim that you wrote the original software. If you use
      this software in a product, an acknowledgment in the product
      documentation would be appreciated but is not required.

   2. Altered source versions must be plainly marked as such, and
      must not be misrepresented as being the original software.

   3. This notice may not be removed or altered from any source
      distribution.

   Christoffer Lindqvist
   swepopper@gmail.com
*/

#include "engine/platform.h"
#include "taskscheduler.h"
#include "task.h"

namespace Acclution {

const uint32_t TaskScheduler::sAnyCore = UINT32_MAX;
const float TaskScheduler::sHighestPriority = 0.0f;
const float TaskScheduler::sNormalPriority = 0.5f;
const float TaskScheduler::sLowestPriority = 1.0f;

TaskScheduler::TaskScheduler()
	: m_sleep_on_empty(false),
	  m_threads(nullptr),
	  m_thread_count(0),
	  m_run_threads(false),
	  m_sync_counter(0),
	  m_thread_data(nullptr),
	  m_tasks((uint32_t)kMaxTaskCount),
	  m_job_queue((uint32_t)kMaxTaskCount),
	  m_free_queue((uint32_t)kMaxTaskCount)
{
}

bool TaskScheduler::Initialize(bool sleep_when_empty)
{
	m_sleep_on_empty = sleep_when_empty;
	for(uint32_t i = 0; i < (uint32_t)kMaxTaskCount; ++i)
	{
		m_free_queue.enqueue(i);
	}

	m_thread_count = std::thread::hardware_concurrency();
	if (m_thread_count < 2)
		return false;
	m_thread_count = astd::max(m_thread_count-1, 1u);

	SetCoreAffinity(0);

	m_thread_data = new ThreadData[m_thread_count];
	for(uint32_t i = 0; i < m_thread_count; ++i)
	{
		m_thread_data[i].m_scheduler = this;
	}

	m_run_threads.store(true);
	m_sync_counter.store(0);
	m_threads = new std::thread[m_thread_count];
	for(uint32_t i = 0; i < m_thread_count; ++i)
	{
		m_threads[i] = std::thread(&TaskScheduler::WorkThreadFunction, this, i);
	}

	ALogInfo("Task Scheduler initialized");
	return true;
}

void TaskScheduler::Release()
{
	m_run_threads.store(false);
	if(m_sleep_on_empty)
	{
		m_sleep_condition.notify_all();
	}

	if(m_threads != nullptr)
	{
		for(uint32_t i = 0; i < m_thread_count; ++i)
		{
			m_threads[i].join();
		}
		delete[] m_threads;
		m_threads = nullptr;
		m_thread_count = 0;
	}

	delete[] m_thread_data;
	m_thread_data = nullptr;

	m_tasks.clear();

	ALogInfo("Task Scheduler released");
}

void TaskScheduler::Stop()
{
	m_run_threads.store(false);
	if(m_sleep_on_empty)
	{
		m_sleep_condition.notify_all();
	}
}

void TaskScheduler::AddTask(std::shared_ptr<Task::Task> task, uint32_t core_id)
{
	task->Reset();

	uint32_t free_id = UINT32_MAX;
	// Try to get a free task slot until we succeed
	while(!m_free_queue.try_dequeue(free_id))
		;

	m_tasks[free_id] = std::pair<int, std::shared_ptr<Task::Task> >(core_id, task);
	m_job_queue.enqueue(free_id);

	if(m_sleep_on_empty)
	{
		m_sleep_condition.notify_all();
	}
}

void TaskScheduler::WorkThreadFunction(TaskScheduler* pThis, uint32_t thread_index)
{
	ALogInfo("Started Task Thread: %u", thread_index);

	SetCoreAffinity(static_cast<int>(thread_index+1));
	std::this_thread::yield();

	ThreadData* thread_data = &pThis->m_thread_data[thread_index];
	uint32_t task_index = UINT32_MAX;
	while(pThis->m_run_threads)
	{
		if(!thread_data->m_job_queue.try_dequeue(task_index))
		{
			if(!pThis->m_job_queue.try_dequeue(task_index))
			{
				if(pThis->m_sleep_on_empty)
				{
					std::unique_lock<std::mutex> lock(pThis->m_sleep_lock);
					pThis->m_sleep_condition.wait(lock);
				}
				continue;
			}
		}

		uint32_t affinity = pThis->m_tasks[task_index].first;
		if(affinity >= pThis->m_thread_count || affinity == thread_index)
		{
			pThis->m_tasks[task_index].second->Execute();
			pThis->m_free_queue.enqueue(task_index);
		}
		else if(affinity < pThis->m_thread_count)
		{
			pThis->m_thread_data[affinity].m_job_queue.enqueue(task_index);
		}

		task_index = UINT32_MAX;
	}
}

int TaskScheduler::SetCoreAffinity(int core_id)
{
#if defined(PLATFORM_WINDOWS)
	SYSTEM_INFO sysinfo;
	GetSystemInfo(&sysinfo);

	if (core_id < 0 || core_id >= (int)sysinfo.dwNumberOfProcessors)
		return EINVAL;

	return SetThreadAffinityMask(GetCurrentThread(), static_cast<uint32_t>(1 << core_id)) != 0 ? 0 : GetLastError();
#else
	long num_cores = sysconf(_SC_NPROCESSORS_ONLN);
	if (core_id < 0 || core_id >= num_cores)
	  return EINVAL;

	cpu_set_t cpuset;
	CPU_ZERO(&cpuset);
	CPU_SET(core_id, &cpuset);

	pthread_t current_thread = pthread_self();
	return pthread_setaffinity_np(current_thread, sizeof(cpu_set_t), &cpuset);
#endif
}

}

