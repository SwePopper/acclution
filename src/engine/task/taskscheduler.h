/*
   Acclution
   Copyright (c) 2016 Christoffer Lindqvist

   This software is provided 'as-is', without any express or implied
   warranty. In no event will the authors be held liable for any
   damages arising from the use of this software.

   Permission is granted to anyone to use this software for any
   purpose, including commercial applications, and to alter it and
   redistribute it freely, subject to the following restrictions:

   1. The origin of this software must not be misrepresented; you
      must not claim that you wrote the original software. If you use
      this software in a product, an acknowledgment in the product
      documentation would be appreciated but is not required.

   2. Altered source versions must be plainly marked as such, and
      must not be misrepresented as being the original software.

   3. This notice may not be removed or altered from any source
      distribution.

   Christoffer Lindqvist
   swepopper@gmail.com
*/

#ifndef TASKSCHEDULER_H
#define TASKSCHEDULER_H
#pragma once

namespace Acclution {

namespace Task
{
class Task;
}

class TaskScheduler
{
public:
	static const uint32_t sAnyCore;
	static const float sHighestPriority;
	static const float sNormalPriority;
	static const float sLowestPriority;

	TaskScheduler();

	bool Initialize(bool sleep_when_empty = false);
	void Release();

	void Stop();
	bool ShouldStop() { return !m_run_threads; }

	void AddTask(std::shared_ptr<Task::Task> task, uint32_t core_id = sAnyCore);

	static int SetCoreAffinity(int core_id);

private:
	enum
	{
		kMaxTaskCount = 4096
	};

	typedef moodycamel::ConcurrentQueue<uint32_t> JobQueue;
	typedef astd::vector<std::pair<uint32_t, std::shared_ptr<Task::Task> > > Tasks;

	class ThreadData
	{
	public:
		ThreadData() : m_scheduler(nullptr), m_job_queue(30) {}

		TaskScheduler*		m_scheduler;
		JobQueue			m_job_queue;
	};

	static void WorkThreadFunction(TaskScheduler* pThis, uint32_t thread_index = 0);

	bool					m_sleep_on_empty;
	std::condition_variable	m_sleep_condition;
	std::mutex				m_sleep_lock;

	std::thread*			m_threads;
	uint32_t				m_thread_count;
	std::atomic_bool		m_run_threads;
	std::atomic_int			m_sync_counter;

	ThreadData*				m_thread_data;

	Tasks					m_tasks;
	JobQueue				m_job_queue;
	JobQueue				m_free_queue;
};

}

#endif // TASKSCHEDULER_H
