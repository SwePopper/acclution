/*
   Acclution
   Copyright (c) 2016 Christoffer Lindqvist

   This software is provided 'as-is', without any express or implied
   warranty. In no event will the authors be held liable for any
   damages arising from the use of this software.

   Permission is granted to anyone to use this software for any
   purpose, including commercial applications, and to alter it and
   redistribute it freely, subject to the following restrictions:

   1. The origin of this software must not be misrepresented; you
      must not claim that you wrote the original software. If you use
      this software in a product, an acknowledgment in the product
      documentation would be appreciated but is not required.

   2. Altered source versions must be plainly marked as such, and
      must not be misrepresented as being the original software.

   3. This notice may not be removed or altered from any source
      distribution.

   Christoffer Lindqvist
   swepopper@gmail.com
*/

#include "engine/platform.h"
#include "config.h"
#include "engine/utilities.h"
#include "engine/engine.h"
#include "engine/platform_updater/platform_updater.h"
#include "engine/script/scriptmanager.h"

#include "engine/io/readtask.h"
#include "engine/types/tokenizer.h"

namespace Acclution {

static const uint32_t skGlobalSection = Utilities::Hash32("__GLOBAL");

Config::Config()
{
}

bool Config::Initialize()
{
	astd::string config_path = m_asset_path = Engine::GetInstance()->GetPlatformUpdater()->GetAssetsPath();

	config_path.append("Assets/config.ini");
	astd::replace(config_path.begin(), config_path.end(), '/', DIRECTORY_SEPARATOR);

	if(!Utilities::FileExist(config_path.c_str()))
	{
		ALogFatal("Failed to find config.ini file!\n");
		return false;
	}
	else
	{
		m_asset_path.append("Assets/");
		astd::replace(m_asset_path.begin(), m_asset_path.end(), '/', DIRECTORY_SEPARATOR);
	}

	std::future<bool> result = IO::ReadFile(&Config::ReadConfig, this, config_path, false, 0, 0, 0);
	if(!result.get())
	{
		ALogError("Failed to read the config file %s", config_path.c_str());
		return false;
	}

	m_save_path = Engine::GetInstance()->GetPlatformUpdater()->GetSavePath(GetOrganization().c_str(), GetGameName().c_str());

	return true;
}

bool Config::ReadConfig(IO::FileDeserializer* deserializer)
{
	m_sections[skGlobalSection] = Section();
	Section* current_section = &m_sections.begin()->second;

	const char* delimiter = "\n";
	Tokenizer tokenizer((char*)deserializer->GetData(), delimiter);
	while(!tokenizer.IsEOF())
	{
		astd::string token = tokenizer.GetNextToken();
		if(token.empty() || token[0] == ';' || token[0] == '#')
			continue;

		if(token[0] == '[')
		{
			size_t pos = token.find(']');
			if(pos != astd::string::npos)
			{
				astd::string section_name = token.substr(1, pos-1);
				uint32_t name_id = Utilities::Hash32(section_name.c_str());
				m_sections[name_id] = Section();
				current_section = &m_sections[name_id];
			}
		}
		else
		{
			// New Key Value
			size_t pos = token.find('=');
			if(pos != astd::string::npos)
			{
				astd::string key = token.substr(0, pos);
				uint32_t key_id = Utilities::Hash32(key.c_str());
				(*current_section)[key_id] = token.substr(pos+1);
			}
		}
	}
	return true;
}

const astd::string& Config::GetValue(const uint32_t& section, const uint32_t& key) const
{
	static const astd::string empty = "";
	SectionContainer::const_iterator sec = m_sections.find(section);
	if(sec == m_sections.end())
	{
		sec = m_sections.find(skGlobalSection);
		if(sec == m_sections.end())
		{
			return empty;
		}
	}

	Section::const_iterator keyval = sec->second.find(key);
	if(keyval == sec->second.end())
		return empty;
	return keyval->second;
}

const astd::string& Config::GetOrganization() const
{
	return GetValue(Utilities::Hash32("PublishingInfo"), Utilities::Hash32("organization"));
}

const astd::string& Config::GetGameName() const
{
	return GetValue(Utilities::Hash32("PublishingInfo"), Utilities::Hash32("game_name"));
}

const astd::string& Config::GetAssetPath() const
{
	return m_asset_path;
}

const astd::string& Config::GetSavePath() const
{
	return m_save_path;
}

const astd::string& Config::GetStartupScene() const
{
	return GetValue(Utilities::Hash32("GameConfig"), Utilities::Hash32("startup_scene"));
}

}

