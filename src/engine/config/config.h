/*
   Acclution
   Copyright (c) 2016 Christoffer Lindqvist

   This software is provided 'as-is', without any express or implied
   warranty. In no event will the authors be held liable for any
   damages arising from the use of this software.

   Permission is granted to anyone to use this software for any
   purpose, including commercial applications, and to alter it and
   redistribute it freely, subject to the following restrictions:

   1. The origin of this software must not be misrepresented; you
      must not claim that you wrote the original software. If you use
      this software in a product, an acknowledgment in the product
      documentation would be appreciated but is not required.

   2. Altered source versions must be plainly marked as such, and
      must not be misrepresented as being the original software.

   3. This notice may not be removed or altered from any source
      distribution.

   Christoffer Lindqvist
   swepopper@gmail.com
*/

#ifndef CONFIG_H
#define CONFIG_H
#pragma once

namespace Acclution
{

class Config
{
public:
	Config();

	bool Initialize();

	const astd::string& GetValue(const uint32_t& section, const uint32_t& key) const;

	const astd::string& GetOrganization() const;
	const astd::string& GetGameName() const;
	const astd::string& GetAssetPath() const;
	const astd::string& GetSavePath() const;
	const astd::string& GetStartupScene() const;

private:
	bool ReadConfig(IO::FileDeserializer* deserializer);

	typedef astd::map<uint32_t, astd::string> Section;
	typedef astd::map<uint32_t, Section> SectionContainer;

	SectionContainer	m_sections;

	astd::string		m_asset_path;
	astd::string		m_save_path;
};

}

#endif // CONFIG_H
