#ifndef TYPES_H
#define TYPES_H
#pragma once

#include <glm/detail/setup.hpp>

namespace Acclution
{

enum class BufferType : uint8_t
{
	kVertex,
	kIndex,
	kUniform
};

enum class BufferUsage : uint8_t
{
	kStatic,
	kDynamic
};

// glm removed the detail namespace after GLM_VERSION 0.9.5 (and debian is slow to update)
#if GLM_VERSION_MAJOR > 0 || GLM_VERSION_MINOR > 9 || (GLM_VERSION_MINOR == 9 && GLM_VERSION_PATCH > 5)
typedef glm::tvec2<int> Point;
#else
typedef glm::detail::tvec2<int, glm::mediump> Point;
#endif

namespace IO
{
namespace detail
{
class ReadMemoryStream;
class ReadDataStream;
}

template<typename T>
class Deserializer;

using MemoryDeserializer = Deserializer<detail::ReadMemoryStream>;
using FileDeserializer = Deserializer<detail::ReadDataStream>;
}

namespace astd
{

#if defined(USE_EASTL)

template <typename T, typename Allocator = AstdAllocatorBase>
using vector = eastl::vector<T, Allocator>;

template <typename T1, typename T2>
using pair = eastl::pair<T1, T2>;

template <typename Key, typename T, typename Compare = eastl::less<Key>, typename Allocator = AstdAllocatorBase>
using map = eastl::map<Key, T, Compare, Allocator>;

template <typename T, typename Allocator = AstdAllocatorBase>
using list = eastl::list<T, Allocator>;

template <typename Key, typename Compare = eastl::less<Key>, typename Allocator = AstdAllocatorBase>
using set = eastl::set<Key, Compare, Allocator>;

template <typename T, typename Container = eastl::deque<T, AstdAllocatorBase, DEQUE_DEFAULT_SUBARRAY_SIZE(T)> >
using queue = eastl::queue<T, Container>;

using string = eastl::basic_string<char, AstdAllocatorBase>;

template <typename Key, typename T, typename Hash = eastl::hash<Key>, typename Compare = eastl::less<Key>, typename Allocator = AstdAllocatorBase>
using unordered_map = eastl::map<Key, T, Compare, Allocator>;// TODO: use actual unordered map

#else

template<typename _Tp, typename _Alloc = AstdAllocator<_Tp> >
using vector = std::vector<_Tp, _Alloc>;

template<class _T1, class _T2>
using pair = std::pair<_T1, _T2>;

template <typename _Key, typename _Tp, typename _Compare = std::less<_Key>, typename _Alloc = AstdAllocator<std::pair<const _Key, _Tp> > >
using map = std::map<_Key, _Tp, _Compare, _Alloc>;

template<typename _Tp, typename _Alloc = AstdAllocator<_Tp> >
using list = std::list<_Tp, _Alloc>;

template<typename _Key, typename _Compare = std::less<_Key>, typename _Alloc = AstdAllocator<_Key> >
using set = std::set<_Key, _Compare, _Alloc>;

template<typename _Tp, typename _Sequence = std::deque<_Tp, AstdAllocator<_Tp>> >
using queue = std::queue<_Tp, _Sequence>;

using string = std::string;

template<class _Key, class _Tp,
	class _Hash = std::hash<_Key>,
	class _Pred = std::equal_to<_Key>,
	class _Alloc = AstdAllocator<std::pair<const _Key, _Tp> > >
using unordered_map = std::unordered_map<_Key, _Tp, _Hash, _Pred, _Alloc>;

template<size_t _Nb>
using bitset = std::bitset<_Nb>;

#endif

}

using ScriptObjectID = astd::pair<size_t, size_t>;

}

#include "rectangle.h"
#include "delegate.h"

#endif // TYPES_H
