#ifndef RECTANGLE_H
#define RECTANGLE_H
#pragma once

namespace Acclution {

class Rectangle
{
public:
	Rectangle() : m_position(), m_size() {}
	Rectangle(glm::vec2 position, glm::vec2 size) : m_position(position), m_size(size) {}
	Rectangle(float x, float y, float w, float h) : m_position(x, y), m_size(w, h) {}

	const float& GetLeft() const { return m_position.x; }
	void SetLeft(const float& left)	{ m_position.x = left; }

	const float& GetTop() const { return m_position.y; }
	void SetTop(const float& top) { m_position.y = top; }

	float GetRight() const { return m_position.x + m_size.x; }
	void SetRight(const float& right) { m_size.x = right - m_position.x; }

	float GetBottom() const { return m_position.y + m_size.y; }
	void SetBottom(const float& bottom) { m_size.y = bottom - m_position.y; }

	glm::vec2& Position() { return m_position; }
	const glm::vec2& Position() const { return m_position; }

	glm::vec2& Size() { return m_size; }
	const glm::vec2& Size() const { return m_size; }

	bool Contains(const glm::vec2& position) const
	{
		return position.x < m_position.x ? false :
			   position.y < m_position.y ? false :
			   position.x > (m_position.x + m_size.x) ? false :
			   position.y > (m_position.y + m_size.y) ? false :
														true;
	}

private:
	glm::vec2		m_position;
	glm::vec2		m_size;
};

}

#endif
