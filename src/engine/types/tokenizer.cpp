#include "engine/platform.h"
#include "tokenizer.h"

namespace Acclution
{

Tokenizer::Tokenizer(const char* text, const char* delimiters)
	: m_text(text),
	  m_text_length(std::char_traits<char>::length(text)),
	  m_delimiters(delimiters),
	  m_delimiter_count(strlen(delimiters)),
	  m_current_position(0)
{}

astd::string Tokenizer::GetNextToken()
{
	if(IsEOF())
		return "";
	size_t next_position = FindNextPosition();
	size_t old_position = m_current_position;
	m_current_position = next_position;
	return astd::string(&m_text[old_position], next_position - old_position);
}

size_t Tokenizer::FindNextPosition()
{
	size_t next_position = m_text_length;
	for(size_t i = 0; i < m_delimiter_count; ++i)
	{
		const char* ptr = strchr(&m_text[m_current_position], m_delimiters[i]);
		if(ptr == nullptr)
			continue;
		size_t pos = static_cast<size_t>(ptr - m_text);
		if(pos == m_current_position)
		{
			m_current_position += 1;
			i -= 1;
			continue;
		}
		next_position = astd::min(pos, next_position);
	}
	return next_position;
}

}
