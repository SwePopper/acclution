#ifndef DELEGATE_H
#define DELEGATE_H
#pragma once

namespace Acclution
{

template<typename... Args>
class Delegate
{
public:
	using Function = void (*)(Args...);

	void Invoke(Args... args) const
	{
		for(typename Functions::const_iterator it = m_functions.begin(); it != m_functions.end(); ++it)
		{
			(*it)(args...);
		}
	}

	void Add(Function function)
	{
		m_functions.push_back(function);
	}

	void Remove(Function function)
	{
		if(!m_functions.empty())
			m_functions.erase(std::remove(m_functions.begin(), m_functions.end(), function), m_functions.end());
	}

	void operator()(Args... args) const { Invoke(std::forward<Args>(args)...); }
	void operator+=(Function function) { Add(std::forward<Function>(function)); }
	void operator-=(Function function) { Remove(std::forward<Function>(function)); }

private:
	using Functions = astd::vector<Function>;
	Functions	m_functions;
};

}

#endif
