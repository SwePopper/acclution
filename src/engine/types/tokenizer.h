#ifndef TOKENIZER_H
#define TOKENIZER_H
#pragma once

namespace Acclution
{

class Tokenizer
{
public:
	Tokenizer(const char* text, const char* delimiters);

	bool IsEOF() const { return m_text == nullptr || m_current_position >= m_text_length; }
	astd::string GetNextToken();

private:
	size_t FindNextPosition();

	const char*	m_text;
	size_t		m_text_length;

	const char*	m_delimiters;
	size_t		m_delimiter_count;

	size_t		m_current_position;
};

}

#endif // TOKENIZER_H
