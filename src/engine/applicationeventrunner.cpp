/*
   Acclution
   Copyright (c) 2016 Christoffer Lindqvist

   This software is provided 'as-is', without any express or implied
   warranty. In no event will the authors be held liable for any
   damages arising from the use of this software.

   Permission is granted to anyone to use this software for any
   purpose, including commercial applications, and to alter it and
   redistribute it freely, subject to the following restrictions:

   1. The origin of this software must not be misrepresented; you
      must not claim that you wrote the original software. If you use
      this software in a product, an acknowledgment in the product
      documentation would be appreciated but is not required.

   2. Altered source versions must be plainly marked as such, and
      must not be misrepresented as being the original software.

   3. This notice may not be removed or altered from any source
      distribution.

   Christoffer Lindqvist
   swepopper@gmail.com
*/

#include "engine/platform.h"
#include "applicationeventrunner.h"
#include "engine/engine.h"
#include "engine/config/config.h"
#include "engine/utilities.h"

#include <SDL2/SDL.h>

namespace Acclution
{

ApplicationEventRunner::ApplicationEventRunner()
	: m_frame_rate(16),
	m_update_type(UpdateType::kFrameBased)
{
}

bool ApplicationEventRunner::Initialize(const UpdateType& update_type)
{
	const astd::string& frame_rate = Engine::GetInstance()->GetConfig()->GetValue(Utilities::Hash32("GameConfig"), Utilities::Hash32("frame_rate"));
	if (!frame_rate.empty())
	{
		m_frame_rate = static_cast<uint32_t>(strtoul(frame_rate.c_str(), nullptr, 10));
	}

	m_last_time = SDL_GetTicks();

	m_update_functions[0] = &ApplicationEventRunner::UpdateEventBased;
	m_update_functions[1] = &ApplicationEventRunner::UpdateFrameBased;
	m_update_type = update_type;
	return true;
}

void ApplicationEventRunner::Update()
{
	(this->*m_update_functions[(uint32_t)m_update_type])();
}

void ApplicationEventRunner::UpdateEventBased()
{
	SDL_Event event;
	if(SDL_WaitEvent(&event) != 0)
	{
		do {
			RunEvent(&event);
		} while (SDL_PollEvent(&event));
	}
}

void ApplicationEventRunner::UpdateFrameBased()
{
	/*if (m_frame_rate > 0)
	{
		uint32_t time_to_delay = m_frame_rate - SDL_GetTicks() - m_last_time;
		if(time_to_delay < m_frame_rate)
			SDL_Delay(time_to_delay);
		m_last_time = SDL_GetTicks();
	}*/

	// Pump Events
	SDL_Event event;
	while(SDL_PollEvent(&event))
	{
		RunEvent(&event);
	}
}

void ApplicationEventRunner::AddEventCallback(EventCallback callback, void* user_param)
{
	m_event_callbacks.push_back(EventCallbackPair(callback, user_param));
}

void ApplicationEventRunner::ClearAll()
{
	m_event_callbacks.clear();
}

void ApplicationEventRunner::RunEvent(SDL_Event* event)
{
	for(EventCallbacks::iterator it = m_event_callbacks.begin(); it != m_event_callbacks.end(); ++it)
	{
		it->first(event, it->second);
	}
}

}
