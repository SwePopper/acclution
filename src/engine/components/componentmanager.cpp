/*
   Acclution
   Copyright (c) 2016 Christoffer Lindqvist

   This software is provided 'as-is', without any express or implied
   warranty. In no event will the authors be held liable for any
   damages arising from the use of this software.

   Permission is granted to anyone to use this software for any
   purpose, including commercial applications, and to alter it and
   redistribute it freely, subject to the following restrictions:

   1. The origin of this software must not be misrepresented; you
      must not claim that you wrote the original software. If you use
      this software in a product, an acknowledgment in the product
      documentation would be appreciated but is not required.

   2. Altered source versions must be plainly marked as such, and
      must not be misrepresented as being the original software.

   3. This notice may not be removed or altered from any source
      distribution.

   Christoffer Lindqvist
   swepopper@gmail.com
*/

#include "engine/platform.h"
#include "componentmanager.h"

namespace Acclution {

Component* ComponentManager::Create(const char* name)
{
	if(name == nullptr || strlen(name) == 0)
	{
		ALogWarning("Invalid component name!");
		return nullptr;
	}

	ComponentRegister::iterator it = m_registered_components.find(name);
	if(it == m_registered_components.end())
	{
		ALogWarning("Could not find component %s in register!", name);
		return nullptr;
	}

	return (*it->second)();
}

void ComponentManager::RegisterComponent(const char* component_name, CreateComponentFunction func)
{
	if(component_name == nullptr || strlen(component_name) == 0 || func == nullptr)
		return;

	ALogInfo("Registering component: %s", component_name);

	astd::pair<ComponentRegister::iterator, bool> ret = m_registered_components.insert(ComponentRegister::value_type(component_name, func));
	if(!ret.second)
	{
		ALogError("Component already exist in the registry! %s", component_name);
	}
}

void ComponentManager::UnRegisterComponent(const char* component_name)
{
	if(component_name == nullptr || strlen(component_name) == 0)
		return;
	ComponentRegister::iterator it = m_registered_components.find(component_name);
	if(it != m_registered_components.end())
	{
		m_registered_components.erase(it);
	}
}

void ComponentManager::ClearRegister()
{
	m_registered_components.clear();
}

}
