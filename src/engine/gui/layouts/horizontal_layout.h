#ifndef HORIZONTAL_LAYOUT_H
#define HORIZONTAL_LAYOUT_H

#include "engine/gui/widget.h"

namespace Acclution
{

class HorizontalLayout : public Widget
{
public:
	HorizontalLayout(GUI* gui, Widget* parent, const char** attributes);

	virtual void UpdateLayout();
};

}

#endif // HORIZONTAL_LAYOUT_H
