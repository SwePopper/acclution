#ifndef VERTICAL_LAYOUT_H
#define VERTICAL_LAYOUT_H

#include "engine/gui/widget.h"

namespace Acclution
{

class VerticalLayout : public Widget
{
public:
	VerticalLayout(GUI* gui, Widget* parent, const char** attributes);

	virtual void UpdateLayout();
};

}

#endif // VERTICAL_LAYOUT_H
