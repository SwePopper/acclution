#include "engine/platform.h"
#include "horizontal_layout.h"

namespace Acclution
{

HorizontalLayout::HorizontalLayout(GUI *gui, Widget* parent, const char **attributes)
	: Widget(gui, parent, attributes)
{
}

void HorizontalLayout::UpdateLayout()
{
	size_t child_count = m_children.size();
	float step = m_rect.Size().x / child_count;

	for(size_t i = 0; i < child_count; ++i)
	{
		Rectangle rect(glm::vec2(m_rect.Position().x + step * i, m_rect.Position().y),
					   glm::vec2(step, m_rect.Size().y));

		m_children[i]->SetRect(rect);

		m_children[i]->UpdateLayout();
	}
}

}
