#include "engine/platform.h"
#include "vertical_layout.h"

namespace Acclution
{

VerticalLayout::VerticalLayout(GUI *gui, Widget* parent, const char **attributes)
	: Widget(gui, parent, attributes)
{
}

void VerticalLayout::UpdateLayout()
{
	size_t child_count = m_children.size();
	float step = m_rect.Size().y / child_count;

	for(size_t i = 0; i < child_count; ++i)
	{
		Rectangle rect(glm::vec2(m_rect.Position().x, m_rect.Position().y + step * i),
					   glm::vec2(m_rect.Size().x, step));

		m_children[i]->SetRect(rect);

		m_children[i]->UpdateLayout();
	}
}

}
