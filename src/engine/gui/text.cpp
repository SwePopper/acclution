#include "engine/platform.h"
#include "text.h"
#include "gui.h"

#include "engine/asset/contentmanager.h"
#include "engine/asset/font.h"
#include "engine/utilities.h"

namespace Acclution
{

enum class TextAttributeNameID : uint32_t
{
	kFont = Utilities::Hash32("font"),
	kR = Utilities::Hash32("r"),
	kG = Utilities::Hash32("g"),
	kB = Utilities::Hash32("b"),
	kA = Utilities::Hash32("a")
};

Text::Text(GUI* gui, Widget* parent, const char** attributes)
	: Widget(gui, parent, attributes),
	  m_font(nullptr),
	  m_text(""),
	  m_color(1.0f, 1.0f, 1.0f, 1.0f)
{
	for(uint32_t i = 0; attributes[i] != NULL; i += 2)
	{
		switch((TextAttributeNameID)Utilities::Hash32(attributes[i]))
		{
		case TextAttributeNameID::kFont: m_font = m_gui->GetContentManager()->LoadAsset<Font>(attributes[i+1]); continue;
		case TextAttributeNameID::kR: m_color.r = strtof(attributes[i+1], nullptr); continue;
		case TextAttributeNameID::kG: m_color.g = strtof(attributes[i+1], nullptr); continue;
		case TextAttributeNameID::kB: m_color.b = strtof(attributes[i+1], nullptr); continue;
		case TextAttributeNameID::kA: m_color.a = strtof(attributes[i+1], nullptr); continue;
		default: continue;
		}
	}
}

void Text::OnEnable()
{
	m_gui->AddDrawable(this);
	Widget::OnEnable();
}

void Text::OnDisable()
{
	m_gui->RemoveDrawable(this);
	Widget::OnDisable();
}

void Text::SetupQuad(glm::vec3* positions, glm::vec2* uvs, glm::vec4* colors, uint16_t* indices, uint16_t current_vertex_index, astd::vector<const Texture*>& textures)
{
	ASTD_UNUSED(positions);
	ASTD_UNUSED(uvs);
	ASTD_UNUSED(colors);
	ASTD_UNUSED(indices);
	ASTD_UNUSED(current_vertex_index);
	ASTD_UNUSED(textures);
	/*if (m_font == nullptr || m_text.empty())
		return;

	const Font::GlyphContainer& glyphs = m_font->GetGlyphs();
	Font::GlyphContainer::const_iterator default_glyph = m_font->GetDefaultGlyph();

	// Find texture index
	uint32_t texture_index = 0;
	for(; texture_index < textures.size(); ++texture_index)
	{
		if(textures[texture_index] == m_font->GetTexture())
			break;
	}

	// if not exist add the texture
	if(texture_index >= textures.size())
		textures.push_back(m_font->GetTexture());

	glm::vec2 offset;
	offset.y += m_font->GetLineSpacing();

	bool is_first_on_line = true;

	glm::vec2 text_size;
	text_size.y = offset.y;

	size_t text_length = m_text.size();
	for(size_t i = 0; i < text_length; ++i, current_vertex_index += 4)
	{
		// Setup quad indices
		uint16_t* current_indices = &indices[i * 6];
		current_indices[0] = current_vertex_index;
		current_indices[1] = current_vertex_index + 1;
		current_indices[2] = current_vertex_index + 2;
		current_indices[3] = current_vertex_index;
		current_indices[4] = current_vertex_index + 2;
		current_indices[5] = current_vertex_index + 3;

		glm::vec4* current_color = &colors[i * 4];
		current_color[0] = m_color;
		current_color[1] = m_color;
		current_color[2] = m_color;
		current_color[3] = m_color;

		glm::vec3* current_position = &positions[i * 4];
		glm::vec2* current_uvs = &uvs[i * 4];

		// Get Current Glyph
		Font::GlyphContainer::const_iterator current_glyph = glyphs.find(m_text[i]);
		if(m_text[i] == '\r' || m_text[i] == '\n' || (current_glyph == glyphs.end() && default_glyph == glyphs.end()))
		{
			if(m_text[i] == '\n')
			{
				offset.x = 0.0f;
				offset.y -= m_font->GetLineSpacing();
				text_size.y = text_size.y < offset.y ? offset.y : text_size.y;
				is_first_on_line = true;
			}

			// Hide the quad
			positions[0] = glm::vec3(-1000.0f, 0.0f, 0.0f);
			positions[1] = glm::vec3(-1000.0f, 0.0f, 0.0f);
			positions[2] = glm::vec3(-1000.0f, 0.0f, 0.0f);
			positions[3] = glm::vec3(-1000.0f, 0.0f, 0.0f);
			current_uvs[0] = glm::vec2(0.0f, 0.0f);
			current_uvs[1] = glm::vec2(0.0f, 0.0f);
			current_uvs[2] = glm::vec2(0.0f, 0.0f);
			current_uvs[3] = glm::vec2(0.0f, 0.0f);
			continue;
		}
		else if(current_glyph == glyphs.end())
		{
			current_glyph = default_glyph;
		}

		if(is_first_on_line)
		{
			is_first_on_line = false;
			offset.x = std::max(current_glyph->second.m_left_side_bearing, 0.0f);
		}
		else
		{
			offset.x += m_font->GetSpacing() + current_glyph->second.m_left_side_bearing;
		}

		glm::vec2 point = glm::vec2(offset.x + current_glyph->second.m_cropping.Position().x, offset.y - current_glyph->second.m_cropping.Position().y);
		point *= m_gui->PixelToScreen();

		glm::vec2 size = glm::vec2(current_glyph->second.m_bounds_in_texture.Size().x, -current_glyph->second.m_bounds_in_texture.Size().y);
		size *= m_gui->PixelToScreen();

		current_position[0] = glm::vec3(point.x + size.x,	point.y, 0.0f);
		current_position[1] = glm::vec3(point.x,			point.y, 0.0f);
		current_position[2] = glm::vec3(point.x,			point.y + size.y, 0.0f);
		current_position[3] = glm::vec3(point.x + size.x,	point.y + size.y, 0.0f);

		current_uvs[0] = glm::vec2((float)texture_index + current_glyph->second.m_bounds_in_texture_normalized.z, current_glyph->second.m_bounds_in_texture_normalized.y);
		current_uvs[1] = glm::vec2((float)texture_index + current_glyph->second.m_bounds_in_texture_normalized.x, current_glyph->second.m_bounds_in_texture_normalized.y);
		current_uvs[2] = glm::vec2((float)texture_index + current_glyph->second.m_bounds_in_texture_normalized.x, current_glyph->second.m_bounds_in_texture_normalized.w);
		current_uvs[3] = glm::vec2((float)texture_index + current_glyph->second.m_bounds_in_texture_normalized.z, current_glyph->second.m_bounds_in_texture_normalized.w);

		offset.x += current_glyph->second.m_width + current_glyph->second.m_right_side_bearing;

		text_size.x = text_size.x < offset.x ? offset.x : text_size.x;
	}

	text_size *= m_gui->PixelToScreen();

	m_content_rect.Size() = text_size;
	m_content_rect.Position() = GetAlignPosition(m_content_rect.Size());

	for(uint32_t i = 0; i < text_length; ++i)
	{
		glm::vec3* current_position = &positions[i * 4];
		current_position[0].x += m_content_rect.Position().x; current_position[0].y += m_content_rect.Position().y;
		current_position[1].x += m_content_rect.Position().x; current_position[1].y += m_content_rect.Position().y;
		current_position[2].x += m_content_rect.Position().x; current_position[2].y += m_content_rect.Position().y;
		current_position[3].x += m_content_rect.Position().x; current_position[3].y += m_content_rect.Position().y;
	}*/
}

}
