#ifndef BUTTON_H
#define BUTTON_H
#pragma once

#include "image.h"

namespace Acclution
{

class Button : public Image
{
public:
	Button(GUI* gui, Widget* parent, const char** attributes);
	virtual ~Button() {}

	virtual void OnEnable();
	virtual void OnDisable();

	virtual void OnAccept();
	virtual bool CanSelect() { return m_sprite != m_inactive_sprite; }
	virtual void OnSelect();
	virtual void OnDeselect();
	virtual void OnPressed();

private:
	uint32_t		m_normal_sprite;
	uint32_t		m_highlight_sprite;
	uint32_t		m_pressed_sprite;
	uint32_t		m_inactive_sprite;

	ScriptObjectID	m_on_accept;
};

}

#endif // BUTTON_H
