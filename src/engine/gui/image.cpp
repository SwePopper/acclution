#include "engine/platform.h"
#include "image.h"
#include "gui.h"
#include "engine/utilities.h"
#include "gui.h"
#include "textureatlas.h"
#include "engine/asset/texture.h"

namespace Acclution
{

enum class ImageAttributeNameID : uint32_t
{
	kSprite = Utilities::Hash32("sprite")
};

Image::Image(GUI* gui, Widget* parent, const char** attributes)
	: Widget(gui, parent, attributes),
	  m_sprite(UINT32_MAX)
{
	for(uint32_t i = 0; attributes[i] != NULL; i += 2)
	{
		switch((ImageAttributeNameID)Utilities::Hash32(attributes[i]))
		{
		case ImageAttributeNameID::kSprite: m_sprite = Utilities::Hash32(attributes[i+1]); continue;
		default: continue;
		}
	}

	UpdateBounds();
}

void Image::OnEnable()
{
	m_gui->AddDrawable(this);
	Widget::OnEnable();
}

void Image::OnDisable()
{
	m_gui->RemoveDrawable(this);
	Widget::OnDisable();
}

void Image::SetupQuad(glm::vec3* positions, glm::vec2* uvs, glm::vec4* colors, uint16_t* indices, uint16_t current_vertex_index, astd::vector<const Texture*>& /*textures*/)
{
	// Setup the indices
	indices[0] = current_vertex_index;
	indices[1] = current_vertex_index + 1;
	indices[2] = current_vertex_index + 2;
	indices[3] = current_vertex_index;
	indices[4] = current_vertex_index + 2;
	indices[5] = current_vertex_index + 3;

	// Setup the colors
	colors[0] = glm::vec4(1.0f, 1.0f, 1.0f, 1.0f);
	colors[1] = glm::vec4(1.0f, 1.0f, 1.0f, 1.0f);
	colors[2] = glm::vec4(1.0f, 1.0f, 1.0f, 1.0f);
	colors[3] = glm::vec4(1.0f, 1.0f, 1.0f, 1.0f);

	TextureAtlas::Sprite info;

	// count is for moving the uv in the x so that the shader can select the correct atlas.
	int count = 0;
	const GUI::AtlasContainer& atlases = m_gui->GetTextureAtlases();
	for(GUI::AtlasContainer::const_iterator it = atlases.begin(); it != atlases.end(); ++it, ++count)
	{
		if((*it)->GetSprite(m_sprite, info))
		{
			glm::vec2 texture_size((*it)->GetTexture()->GetWidth(), (*it)->GetTexture()->GetHeight());

			glm::vec2 uv1;
			uv1.x = ((float)info.m_rect.Position().x / texture_size.x) + count;
			uv1.y = (float)info.m_rect.Position().y / texture_size.y;

			glm::vec2 uv2;
			uv2.x = ((float)(info.m_rect.Position().x + info.m_rect.Size().x) / texture_size.x) + count;
			uv2.y = (float)(info.m_rect.Position().y + info.m_rect.Size().y) / texture_size.y;

			uvs[0] = glm::vec2(uv2.x, uv1.y);
			uvs[1] = glm::vec2(uv1.x, uv1.y);
			uvs[2] = glm::vec2(uv1.x, uv2.y);
			uvs[3] = glm::vec2(uv2.x, uv2.y);

			glm::vec2 size = info.m_rect.Size() / texture_size;
			glm::vec2 position = GetAlignPosition(size);

			positions[0] = glm::vec3(position.x + size.x, position.y, 0.0f);
			positions[1] = glm::vec3(position.x, position.y, 0.0f);
			positions[2] = glm::vec3(position.x, position.y + size.y, 0.0f);
			positions[3] = glm::vec3(position.x + size.x, position.y + size.y, 0.0f);

			return;
		}
	}

	ALogError("Failed to find gui sprite! %u", m_sprite);
}

void Image::UpdateBounds()
{
	TextureAtlas::Sprite info;
	const GUI::AtlasContainer& atlases = m_gui->GetTextureAtlases();
	for(GUI::AtlasContainer::const_iterator it = atlases.begin(); it != atlases.end(); ++it)
	{
		if((*it)->GetSprite(m_sprite, info))
		{
			glm::vec2 texture_size((*it)->GetTexture()->GetWidth(), (*it)->GetTexture()->GetHeight());

			m_content_rect.Size() = info.m_rect.Size() / texture_size;
			m_content_rect.Position() = GetAlignPosition(m_content_rect.Size());
			return;
		}
	}
}

}
