#ifndef ROOT_H
#define ROOT_H
#pragma once

#include "widget.h"

namespace Acclution
{

class Root : public Widget
{
public:
	Root(GUI* gui, Widget* parent, const char** attributes);
	virtual ~Root();

	virtual void Initialize() {}
	virtual void OnBack() {}

	virtual Rectangle GetRect() const { return Rectangle(0.0f, 0.0f, 1.0f, 1.0f); }
	virtual void SetRect(const Rectangle&) {}

	const char* GetName() const { return m_name; }
	const uint32_t& GetNameHash() const { return m_name_hash; }

private:
	char*			m_name;
	uint32_t		m_name_hash;
};

}

#endif // ROOT_H
