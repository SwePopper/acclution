#ifndef TEXT_H
#define TEXT_H
#pragma once

#include "widget.h"

namespace Acclution
{

class Font;

class Text : public Widget
{
public:
	Text(GUI* gui, Widget* parent, const char** attributes);
	virtual ~Text() {}

	virtual void OnEnable();
	virtual void OnDisable();

	const astd::string& GetText() const { return m_text; }
	void SetText(const char* text) { m_text = text; }

	virtual uint32_t NumberOfQuads() const { return (m_font == nullptr) ? 0 : static_cast<uint32_t>(m_text.size()); }
	virtual void SetupQuad(glm::vec3* positions, glm::vec2* uvs, glm::vec4* colors, uint16_t* indices, uint16_t current_vertex_index, astd::vector<const Texture*>& textures);

private:
	Font*			m_font;
	astd::string		m_text;
	glm::vec4			m_color;
};

}

#endif // TEXT_H
