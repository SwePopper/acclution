/*
   Acclution
   Copyright (c) 2016 Christoffer Lindqvist

   This software is provided 'as-is', without any express or implied
   warranty. In no event will the authors be held liable for any
   damages arising from the use of this software.

   Permission is granted to anyone to use this software for any
   purpose, including commercial applications, and to alter it and
   redistribute it freely, subject to the following restrictions:

   1. The origin of this software must not be misrepresented; you
      must not claim that you wrote the original software. If you use
      this software in a product, an acknowledgment in the product
      documentation would be appreciated but is not required.

   2. Altered source versions must be plainly marked as such, and
      must not be misrepresented as being the original software.

   3. This notice may not be removed or altered from any source
      distribution.

   Christoffer Lindqvist
   swepopper@gmail.com
*/

#include "engine/platform.h"
#include "gui.h"
#include "widget.h"
#include "root.h"
#include "button.h"
#include "image.h"
#include "text.h"
#include "layouts/horizontal_layout.h"
#include "layouts/vertical_layout.h"
#include "engine/engine.h"
#include "engine/platform_updater/platform_updater.h"
#include "engine/platform_updater/platform_events.h"
#include "engine/config/config.h"
#include "engine/utilities.h"
#include "engine/graphics/commandbuffer.h"
#include "engine/graphics/video.h"
#include "engine/memory/frameallocator.h"
#include "engine/asset/contentmanager.h"
#include "engine/asset/texture.h"
#include "engine/asset/shaderdata.h"
#include "textureatlas.h"
#include "engine/input/input.h"
#include "engine/io/readtask.h"

#include <SDL2/SDL_events.h>

namespace Acclution
{

template<typename T>
Widget* CreateWidget(GUI* gui, Widget* parent, const char** atts)
{
	return dynamic_cast<Widget*>(new T(gui, parent, atts));
}

class XMLParsingData
{
public:
	GUI*					m_gui;
	GUI::WidgetRegister*	m_widget_registry;
	Widget*					m_current_widget;

	astd::vector<Root*>		m_roots;
	uint32_t				m_start_root;

	astd::vector<TextureAtlas*> m_textures;
};

// I do not think that the GUI will need more than 2048 quads
const uint32_t kMaxQuads = USHRT_MAX / 4;

GUI::GUI()
	: m_content_manager(nullptr),
	  m_current_root(UINT32_MAX),
	  m_redraw(true),
	  m_current_selected(nullptr),
	  m_pressed_widget(nullptr),
	  m_screen_size(),
	  m_pixel_to_screen(),
	  m_shader(UINT32_MAX),
	  m_vertex_positions(UINT32_MAX),
	  m_vertex_uvs(UINT32_MAX),
	  m_vertex_colors(UINT32_MAX),
	  m_vertex_indices(UINT32_MAX),
	  m_uniform_buffer(UINT32_MAX)
{}

GUI::~GUI()
{
	Release();
}

bool GUI::Initialize()
{
	m_content_manager = new ContentManager;
	if(!m_content_manager->Initialize(Engine::GetInstance()->GetConfig()->GetAssetPath().c_str()))
		return false;

	// Register Widget creation functions
	m_widget_register[Utilities::Hash32(STRINGIFY(Root))] = &CreateWidget<Root>;
	m_widget_register[Utilities::Hash32(STRINGIFY(Image))] = &CreateWidget<Image>;
	m_widget_register[Utilities::Hash32(STRINGIFY(Button))] = &CreateWidget<Button>;
	m_widget_register[Utilities::Hash32(STRINGIFY(Text))] = &CreateWidget<Text>;
	m_widget_register[Utilities::Hash32(STRINGIFY(Widget))] = &CreateWidget<Widget>;
	m_widget_register[Utilities::Hash32(STRINGIFY(VerticalLayout))] = &CreateWidget<VerticalLayout>;
	m_widget_register[Utilities::Hash32(STRINGIFY(HorizontalLayout))] = &CreateWidget<HorizontalLayout>;

	astd::string agui_path = Engine::GetInstance()->GetConfig()->GetAssetPath();
	agui_path.append("gui/agui.gui");

	XMLParsingData user_data;
	user_data.m_widget_registry = &m_widget_register;
	user_data.m_current_widget = nullptr;
	user_data.m_gui = this;

	std::future<bool> gui_result = IO::ReadXml(&user_data, &GUI::StartElementHandler, &GUI::EndElementHandler, &GUI::CharacterData, agui_path);

	// Read the gui shader
	ShaderData* shader_datas[] = {
		m_content_manager->LoadAsset<ShaderData>("gui/agui_vshader.vert"),
		m_content_manager->LoadAsset<ShaderData>("gui/agui_fshader.frag")
	};

	m_shader = Engine::GetInstance()->GetVideo()->CreateShader(shader_datas, ASTD_ARRAY_COUNT(shader_datas));

	// Create the vertex buffers
	uint32_t max_vertices = kMaxQuads * 4;

	m_vertex_positions = Engine::GetInstance()->GetVideo()->CreateVertexBuffer(nullptr, max_vertices * sizeof(glm::vec3), BufferType::kVertex, BufferUsage::kDynamic);
	m_vertex_uvs = Engine::GetInstance()->GetVideo()->CreateVertexBuffer(nullptr, max_vertices * sizeof(glm::vec2), BufferType::kVertex, BufferUsage::kDynamic);
	m_vertex_colors = Engine::GetInstance()->GetVideo()->CreateVertexBuffer(nullptr, max_vertices * sizeof(glm::vec4), BufferType::kVertex, BufferUsage::kDynamic);
	m_vertex_indices = Engine::GetInstance()->GetVideo()->CreateVertexBuffer(nullptr, kMaxQuads * 6 * sizeof(uint16_t), BufferType::kIndex, BufferUsage::kDynamic);

	m_uniform_buffer = Engine::GetInstance()->GetVideo()->CreateVertexBuffer(nullptr, sizeof(glm::mat4), BufferType::kUniform, BufferUsage::kStatic);

	m_gui_projection = glm::ortho(0.0f, 1.0f, 0.0f, 1.0f);
	Engine::GetInstance()->GetVideo()->UpdateVertexBuffer(m_uniform_buffer, glm::value_ptr(m_gui_projection), sizeof(glm::mat4), BufferType::kUniform);

	if(!gui_result.get())
	{
		ALogError("Failed to read gui xml!");
		return false;
	}

	m_texture_atlases.clear();
	m_texture_atlases.insert(m_texture_atlases.begin(), user_data.m_textures.begin(), user_data.m_textures.end());

	// Get the created roots and initialize them.
	m_roots.clear();
	m_roots.insert(m_roots.begin(), user_data.m_roots.begin(), user_data.m_roots.end());

	m_current_root = 0;
	size_t roots_count = m_roots.size();
	for(size_t i = 0; i < roots_count; ++i)
	{
		if(user_data.m_start_root == m_roots[i]->GetNameHash())
		{
			m_current_root = static_cast<uint32_t>(i);
			break;
		}
	}

	m_roots[m_current_root]->OnEnable();

	const std::unique_ptr<Input>& input = Engine::GetInstance()->GetInput();
	input->AddActionCallback(Utilities::Hash32("GUI"), Utilities::Hash32("MouseMove"), &GUI::OnMouseMove, this);

	input->AddActionCallback(Utilities::Hash32("GUI"), Utilities::Hash32("Accept"), &GUI::OnAccept, this);
	input->AddActionCallback(Utilities::Hash32("GUI"), Utilities::Hash32("Back"), &GUI::OnBack, this);

	input->AddActionCallback(Utilities::Hash32("GUI"), Utilities::Hash32("Up"), &GUI::OnMoveUp, this);
	input->AddActionCallback(Utilities::Hash32("GUI"), Utilities::Hash32("Left"), &GUI::OnMoveLeft, this);
	input->AddActionCallback(Utilities::Hash32("GUI"), Utilities::Hash32("Down"), &GUI::OnMoveDown, this);
	input->AddActionCallback(Utilities::Hash32("GUI"), Utilities::Hash32("Right"), &GUI::OnMoveRight, this);

	Engine::GetInstance()->GetPlatformUpdater()->AddEventCallback(&GUI::OnSystemEvent, this);

	return true;
}

void GUI::Release()
{
	if(Engine::GetInstance() != nullptr)
	{
		const std::unique_ptr<Video>& video = Engine::GetInstance()->GetVideo();
		if(video.get() != nullptr)
		{
			Engine::GetInstance()->GetVideo()->DestroyVertexBuffer(m_uniform_buffer);
			Engine::GetInstance()->GetVideo()->DestroyVertexBuffer(m_vertex_indices);
			Engine::GetInstance()->GetVideo()->DestroyVertexBuffer(m_vertex_colors);
			Engine::GetInstance()->GetVideo()->DestroyVertexBuffer(m_vertex_uvs);
			Engine::GetInstance()->GetVideo()->DestroyVertexBuffer(m_vertex_positions);
			Engine::GetInstance()->GetVideo()->DestroyShader(m_shader);
		}
	}

	m_shader = m_uniform_buffer = m_vertex_indices = m_vertex_colors = m_vertex_uvs = m_vertex_positions = UINT32_MAX;

	for(AtlasContainer::iterator it = m_texture_atlases.begin(); it != m_texture_atlases.end(); ++it)
	{
		delete *it;
	}
	m_texture_atlases.clear();

	for(RootContainer::iterator it = m_roots.begin(); it != m_roots.end(); ++it)
	{
		delete *it;
	}
	m_roots.clear();
	m_widget_register.clear();
	m_unique_widget_register.clear();

	m_current_root = UINT32_MAX;

	if(m_content_manager != nullptr)
	{
		m_content_manager->Release();
		delete m_content_manager;
		m_content_manager = nullptr;
	}
}

void GUI::Update()
{
	if(m_current_root < m_roots.size())
	{
		// Update m_roots[m_current_root] interactables
	}
}

void GUI::Render(CommandBuffer* command)
{
	if(m_current_root < m_roots.size())
	{
		//if(m_redraw.exchange(false))
		{
			astd::vector<const Texture*> textures;
			for(uint32_t i = 0; i < m_texture_atlases.size(); ++i)
			{
				textures.push_back(m_texture_atlases[i]->GetTexture());
				// Wait until all texture atlases is loaded
				if(m_texture_atlases[i]->GetTexture()->GetLoadingResult() == Asset::LoadingResult::kNotDone)
				{
					m_redraw.store(true);
					return;
				}
			}

			const std::unique_ptr<Video>& video = Engine::GetInstance()->GetVideo();
			m_screen_size = glm::vec2(video->GetWindowWidth(), video->GetWindowHeight());
			m_pixel_to_screen = glm::vec2(1.0f, 1.0f) / m_screen_size;
			m_screen_aspect = m_screen_size.y / m_screen_size.x;

			m_roots[m_current_root]->UpdateLayout();

			uint32_t number_of_quads = 0;
			for(WidgetContainer::const_iterator it = m_drawables.begin(); it != m_drawables.end(); ++it)
			{
				number_of_quads += (*it)->NumberOfQuads();
			}

			if(number_of_quads > kMaxQuads)
			{
				ALogError("Number of needed quads for root (%s) exceeds %u (needed %u)", m_roots[m_current_root]->GetName(), kMaxQuads, number_of_quads);
			}

			uint32_t max_vertices = number_of_quads * 4;

			glm::vec3* positions = (glm::vec3*)Engine::GetInstance()->GetRenderFrameAllocator()->AllocateBig(max_vertices * sizeof(glm::vec3));
			glm::vec2* uvs = (glm::vec2*)Engine::GetInstance()->GetRenderFrameAllocator()->AllocateBig(max_vertices * sizeof(glm::vec2));
			glm::vec4* colors = (glm::vec4*)Engine::GetInstance()->GetRenderFrameAllocator()->AllocateBig(max_vertices * sizeof(glm::vec4));
			uint16_t* indices = (uint16_t*)Engine::GetInstance()->GetRenderFrameAllocator()->AllocateBig(number_of_quads * 6 * sizeof(uint16_t));

			// Update the position, uv and index buffers
			uint16_t current_quad = 0;
			for(WidgetContainer::iterator it = m_drawables.begin(); it != m_drawables.end(); ++it)
			{
				// TODO: should stop if textures is more than 4
				uint16_t current_vert = current_quad * 4;
				(*it)->SetupQuad(&positions[current_vert], &uvs[current_vert], &colors[current_vert], &indices[current_quad * 6], current_vert, textures);
				current_quad += (uint16_t)(*it)->NumberOfQuads();
			}

			// fix z
			float step = 1.98f / (float)number_of_quads;
			float cur = -0.99f;
			for(uint32_t i = 0; i < number_of_quads; ++i)
			{
				positions[i*4].z = cur;
				positions[i*4 + 1].z = cur;
				positions[i*4 + 2].z = cur;
				positions[i*4 + 3].z = cur;
				cur += step;
			}

			// upload data
			command->UpdateVertexBuffer(m_vertex_positions, positions, max_vertices * sizeof(glm::vec3), BufferType::kVertex);
			command->UpdateVertexBuffer(m_vertex_uvs, uvs, max_vertices * sizeof(glm::vec2), BufferType::kVertex);
			command->UpdateVertexBuffer(m_vertex_colors, colors, max_vertices * sizeof(glm::vec4), BufferType::kVertex);
			command->UpdateVertexBuffer(m_vertex_indices, indices, number_of_quads * 6 * sizeof(uint16_t), BufferType::kIndex);

			// Record the render commands
			command->ClearDepth();
			command->UseShader(m_shader);

			command->UniformBuffer(m_uniform_buffer, 0);

			constexpr uint32_t shaderTextureNames[] = {
				Utilities::Hash32("atlasOne"),
				Utilities::Hash32("atlasTwo"),
				Utilities::Hash32("atlasThree"),
				Utilities::Hash32("atlasFour")
			};

			for(uint8_t i = 0; i < textures.size(); ++i)
			{
				command->BindTexture(textures[i]->GetTextureObject(), i, shaderTextureNames[i]);
			}

			command->EnableVertexAttrib(m_vertex_positions, 0, 3, Types::kFloat, false, 0, NULL);
			command->EnableVertexAttrib(m_vertex_uvs, 1, 2, Types::kFloat, false, 0, NULL);
			command->EnableVertexAttrib(m_vertex_colors, 2, 4, Types::kFloat, false, 0, NULL);

			command->DrawElements(Modes::kTriangles, static_cast<int>(number_of_quads * 6), Types::kUShort, m_vertex_indices);

			command->DisableVertexAttrib(2);
			command->DisableVertexAttrib(1);
			command->DisableVertexAttrib(0);
		}

		// Redraw cached CommandBuffer
		//command->AddCommandLine(*m_command_buffer);

		// if rendering to render texture, draw it on screen.
	}
}

void GUI::OnSystemEvent(const PlatformEvent* event, void* user_param)
{
	ASTD_UNUSED(event);
	if(user_param != nullptr)
	{
		GUI* gui = reinterpret_cast<GUI*>(user_param);
		gui->m_redraw = true;
	}
}

void GUI::AddSelectable(Widget* selectable)
{
	if(selectable != nullptr)
	{
		m_selectables.push_back(selectable);
		astd::RemoveDuplicates(m_selectables);
		if(m_current_selected == nullptr && selectable->CanSelect())
		{
			m_current_selected = selectable;
			m_current_selected->OnSelect();
		}
	}
}

void GUI::RemoveSelectable(Widget* selectable)
{
	if(m_selectables.empty() || selectable == nullptr)
		return;
	astd::EraseRemove(m_selectables, selectable);
	if(m_current_selected == selectable)
	{
		SelectAny();
	}
}

void GUI::AddDrawable(Widget* drawable)
{
	if(drawable != nullptr)
	{
		m_drawables.push_back(drawable);
		astd::RemoveDuplicates(m_drawables);
	}
}

void GUI::RemoveDrawable(Widget* drawable)
{
	if(m_drawables.empty() || drawable == nullptr)
		return;
	astd::EraseRemove(m_drawables, drawable);
}

void GUI::RegisterWidgetUserID(Widget* widget, const uint32_t& user_id)
{
	const UniqueWidgetRegister::const_iterator it = m_unique_widget_register.find(user_id);
	if(it != m_unique_widget_register.cend())
	{
		ALogError("User id %u already taken for the widget register", user_id);
		return;
	}

	m_unique_widget_register[user_id] = widget;
}

void GUI::UnRegisterWidgetUserID(const uint32_t& user_id)
{
	m_unique_widget_register.erase(user_id);
}

Widget* GUI::GetWidgetFromUserID(const uint32_t& user_id)
{
	UniqueWidgetRegister::iterator it = m_unique_widget_register.find(user_id);
	if(it == m_unique_widget_register.end())
		return nullptr;
	return it->second;
}

void GUI::OnMouseMove(const InputActionData* input_action_data)
{
	GUI* gui = reinterpret_cast<GUI*>(input_action_data->GetUserData());
	if(gui != nullptr)
	{
		if(gui->m_pressed_widget != nullptr)
			return;

		glm::vec2 new_mouse_pos;
		input_action_data->GetValues(new_mouse_pos);

		for(WidgetContainer::const_iterator it = gui->m_selectables.begin(); it != gui->m_selectables.end(); ++it)
		{
			if(!(*it)->CanSelect())
				continue;

			Rectangle rect = (*it)->GetContentRect();
			if(!rect.Contains(new_mouse_pos))
				continue;

			if(*it == gui->m_current_selected)
				return;

			if(gui->m_current_selected != nullptr)
				gui->m_current_selected->OnDeselect();
			gui->m_current_selected = *it;
			gui->m_current_selected->OnSelect();
		}
	}
}

void GUI::OnAccept(const InputActionData* input_action_data)
{
	if(input_action_data->GetState())
		return;

	GUI* gui = reinterpret_cast<GUI*>(input_action_data->GetUserData());
	if(gui != nullptr)
	{
		if(gui->m_current_selected != nullptr)
			gui->m_current_selected->OnAccept();
	}
}

void GUI::OnBack(const InputActionData* input_action_data)
{
	if(input_action_data->GetState())
		return;

	GUI* gui = reinterpret_cast<GUI*>(input_action_data->GetUserData());
	if(gui != nullptr)
	{
		if(gui->m_current_root < gui->m_roots.size())
			gui->m_roots[gui->m_current_root]->OnBack();
	}
}

void GUI::OnMoveUp(const InputActionData* input_action_data)
{
	if(input_action_data->GetState())
		return;

	GUI* gui = reinterpret_cast<GUI*>(input_action_data->GetUserData());
	if(gui != nullptr)
	{
		if(gui->m_pressed_widget != nullptr)
			return;

		if(gui->m_current_selected == nullptr)
		{
			gui->SelectAny();
		}
		else
		{
			Widget* next_selection = gui->GetNextSelection([](Widget* cur, Widget* next) -> float {
					Rectangle cur_rect = cur->GetRect();
					Rectangle next_rect = next->GetRect();
					return glm::length(astd::sign(cur_rect.Position().y - next_rect.Position().y) * (next_rect.Position() - cur_rect.Position()));
			});
			if(next_selection != nullptr)
			{
				gui->m_current_selected->OnDeselect();
				gui->m_current_selected = next_selection;
				gui->m_current_selected->OnSelect();
			}
		}
	}
}

void GUI::OnMoveLeft(const InputActionData* input_action_data)
{
	if(input_action_data->GetState())
		return;

	GUI* gui = reinterpret_cast<GUI*>(input_action_data->GetUserData());
	if(gui != nullptr)
	{
		if(gui->m_pressed_widget != nullptr)
			return;

		if(gui->m_current_selected == nullptr)
		{
			gui->SelectAny();
		}
		else
		{
			Widget* next_selection = gui->GetNextSelection([](Widget* cur, Widget* next) -> float {
					Rectangle cur_rect = cur->GetRect();
					Rectangle next_rect = next->GetRect();
					return glm::length(astd::sign(next_rect.Position().x - cur_rect.Position().x) * (next_rect.Position() - cur_rect.Position()));
			});
			if(next_selection != nullptr)
			{
				gui->m_current_selected->OnDeselect();
				gui->m_current_selected = next_selection;
				gui->m_current_selected->OnSelect();
			}
		}
	}
}

void GUI::OnMoveDown(const InputActionData* input_action_data)
{
	if(input_action_data->GetState())
		return;

	GUI* gui = reinterpret_cast<GUI*>(input_action_data->GetUserData());
	if(gui != nullptr)
	{
		if(gui->m_pressed_widget != nullptr)
			return;

		if(gui->m_current_selected == nullptr)
		{
			gui->SelectAny();
		}
		else
		{
			Widget* next_selection = gui->GetNextSelection([](Widget* cur, Widget* next) -> float {
					Rectangle cur_rect = cur->GetRect();
					Rectangle next_rect = next->GetRect();
					return glm::length(astd::sign(next_rect.Position().y - cur_rect.Position().y) * (next_rect.Position() - cur_rect.Position()));
			});
			if(next_selection != nullptr)
			{
				gui->m_current_selected->OnDeselect();
				gui->m_current_selected = next_selection;
				gui->m_current_selected->OnSelect();
			}
		}
	}
}

void GUI::OnMoveRight(const InputActionData* input_action_data)
{
	if(input_action_data->GetState())
		return;

	GUI* gui = reinterpret_cast<GUI*>(input_action_data->GetUserData());
	if(gui != nullptr)
	{
		if(gui->m_pressed_widget != nullptr)
			return;

		if(gui->m_current_selected == nullptr)
		{
			gui->SelectAny();
		}
		else
		{
			Widget* next_selection = gui->GetNextSelection([](Widget* cur, Widget* next) -> float {
					Rectangle cur_rect = cur->GetRect();
					Rectangle next_rect = next->GetRect();
					return glm::length(astd::sign(cur_rect.Position().x - next_rect.Position().x) * (next_rect.Position() - cur_rect.Position()));
			});
			if(next_selection != nullptr)
			{
				gui->m_current_selected->OnDeselect();
				gui->m_current_selected = next_selection;
				gui->m_current_selected->OnSelect();
			}
		}
	}
}

Widget* GUI::GetNextSelection(float (*Comparer)(Widget*, Widget*))
{
	Widget* next_selection = nullptr;
	float current_distance = FLT_MAX;

	for(WidgetContainer::const_iterator it = m_selectables.begin(); it != m_selectables.end(); ++it)
	{
		if((*it)->CanSelect() && *it != m_current_selected)
		{
			float distance = Comparer(m_current_selected, *it);
			if(distance > 0 && distance < current_distance)
			{
				current_distance = distance;
				next_selection = *it;
			}
		}
	}
	return next_selection;
}

void GUI::SelectAny()
{
	if(m_current_selected != nullptr)
		m_current_selected->OnDeselect();

	if(m_selectables.empty())
	{
		m_current_selected = nullptr;
	}
	else
	{
		for(WidgetContainer::iterator it = m_selectables.begin(); it != m_selectables.end(); ++it)
		{
			if((*it)->CanSelect())
			{
				m_current_selected = *it;
				m_current_selected->OnSelect();
				break;
			}
		}
	}
}

void GUI::StartElementHandler(void* user_data, const char* name, const char** atts)
{
	uint32_t name_id = Utilities::Hash32(name);
	XMLParsingData* data = reinterpret_cast<XMLParsingData*>(user_data);

	GUI::WidgetRegister::iterator it = data->m_widget_registry->find(name_id);
	if(it != data->m_widget_registry->end())
	{
		Widget* widget = it->second(data->m_gui, data->m_current_widget, atts);
		data->m_current_widget = widget;

		if(name_id == Utilities::Hash32(STRINGIFY(Root)))
		{
			data->m_roots.push_back(dynamic_cast<Root*>(widget));
		}
	}
	else if(name_id == Utilities::Hash32("AGUI"))
	{
		for(uint32_t i = 0; atts[i] != NULL; i += 2)
		{
			if(strncmp(atts[i], "start_root", 11) == 0)
			{
				data->m_start_root = strlen(atts[i+1]) > 0 ? Utilities::Hash32(atts[i+1]) : UINT32_MAX;
			}
		}
	}
	else if(name_id == Utilities::Hash32("Atlas"))
	{
		for(uint32_t i = 0; atts[i] != NULL; i += 2)
		{
			if(strncmp(atts[i], "path", 5) == 0)
			{
				TextureAtlas* atlas = new TextureAtlas;
				if(atlas == nullptr || !atlas->Load(atts[i+1], data->m_gui->GetContentManager()))
				{
					ALogError("Failed to load gui atlas: %s", atts[i+1]);
				}
				else
				{
					data->m_textures.push_back(atlas);
				}
			}
		}
	}
	else if(name_id != Utilities::Hash32("Atlases")) // Ignering atlases
	{
		ALogWarning("Unknown GUI Widget: %s", name);
	}
}

void GUI::EndElementHandler(void* user_data, const char* name)
{
	uint32_t name_id = Utilities::Hash32(name);
	XMLParsingData* data = reinterpret_cast<XMLParsingData*>(user_data);
	GUI::WidgetRegister::iterator it = data->m_widget_registry->find(name_id);
	if(data->m_current_widget != NULL && it != data->m_widget_registry->end())
	{
		data->m_current_widget = data->m_current_widget->GetParent();
	}
}

void GUI::CharacterData(void* user_data, const char* str, int length)
{
	XMLParsingData* data = reinterpret_cast<XMLParsingData*>(user_data);
	if(length > 0 && data != nullptr && data->m_current_widget != nullptr)
	{
		Text* text = dynamic_cast<Text*>(data->m_current_widget);
		if(text != nullptr)
		{
			astd::string text_data(str, static_cast<astd::string::size_type>(length));
			text_data = text->GetText() + text_data; // Seems like we dont get all the data so we need to append
			text->SetText(text_data.c_str());
		}
	}
}

}
