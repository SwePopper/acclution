#include "engine/platform.h"
#include "engine/engine.h"
#include "engine/config/config.h"
#include "engine/script/scriptmanager.h"
#include "button.h"
#include "engine/utilities.h"
#include "gui.h"
#include "action.h"

namespace Acclution
{

enum class ButtonAttributeNameID : uint32_t
{
	kNormal = Utilities::Hash32("normal"),
	kHighlight = Utilities::Hash32("highlight"),
	kPressed = Utilities::Hash32("pressed"),
	kInactive = Utilities::Hash32("inactive"),
	kScript = Utilities::Hash32("script")
};

Button::Button(GUI *gui, Widget* parent, const char **attributes)
	: Image(gui, parent, attributes),
	  m_normal_sprite(UINT32_MAX),
	  m_highlight_sprite(UINT32_MAX),
	  m_pressed_sprite(UINT32_MAX),
	  m_inactive_sprite(UINT32_MAX),
	  m_on_accept(UINTPTR_MAX, UINTPTR_MAX)
{
	for(uint32_t i = 0; attributes[i] != NULL; i += 2)
	{
		uint32_t id = Utilities::Hash32(attributes[i+1]);
		switch((ButtonAttributeNameID)Utilities::Hash32(attributes[i]))
		{
		case ButtonAttributeNameID::kNormal: m_normal_sprite = id; continue;
		case ButtonAttributeNameID::kHighlight: m_highlight_sprite = id; continue;
		case ButtonAttributeNameID::kPressed: m_pressed_sprite = id; continue;
		case ButtonAttributeNameID::kInactive: m_inactive_sprite = id; continue;
		case ButtonAttributeNameID::kScript: m_on_accept = Engine::GetInstance()->GetScriptManager()->Construct(attributes[i+1]); continue;
		default: continue;
		}
	}

	m_sprite = m_normal_sprite;
	UpdateBounds();
}

void Button::OnEnable()
{
	m_gui->AddDrawable(this);
	m_gui->AddSelectable(this);
	Widget::OnEnable();
	m_gui->ReDraw();
}

void Button::OnDisable()
{
	m_gui->RemoveDrawable(this);
	m_gui->RemoveSelectable(this);
	Widget::OnDisable();
	m_gui->ReDraw();
}

void Button::OnAccept()
{
	Action* action = Engine::GetInstance()->GetScriptManager()->Get<Action>(m_on_accept);
	if(action != nullptr)
		action->OnAccept();
}

void Button::OnSelect()
{
	if(m_sprite != m_inactive_sprite)
	{
		m_sprite = m_highlight_sprite;
		UpdateBounds();
		m_gui->ReDraw();
	}
}

void Button::OnDeselect()
{
	if(m_sprite != m_inactive_sprite)
	{
		m_sprite = m_normal_sprite;
		UpdateBounds();
		m_gui->ReDraw();
	}
}

void Button::OnPressed()
{
	if(m_sprite != m_inactive_sprite)
	{
		m_sprite = m_pressed_sprite;
		UpdateBounds();
		m_gui->ReDraw();
	}
}

}
