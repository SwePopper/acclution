#include "engine/platform.h"
#include "textureatlas.h"
#include "engine/asset/contentmanager.h"
#include "engine/io/readtask.h"
#include "engine/engine.h"
#include "engine/config/config.h"
#include "engine/utilities.h"
#include "engine/asset/texture.h"

namespace Acclution
{

class XMLParsingData
{
public:
	TextureAtlas*			m_atlas;
	ContentManager*			m_content_manager;
};

bool TextureAtlas::Load(const char* path, ContentManager* content_manager)
{
	astd::string xml_path = Engine::GetInstance()->GetConfig()->GetAssetPath();
	xml_path.append(path);
	xml_path.append(".xml");

	XMLParsingData user_data;
	user_data.m_atlas = this;
	user_data.m_content_manager = content_manager;

	std::future<bool> result = IO::ReadXml(&user_data, &TextureAtlas::StartElementHandler, nullptr, nullptr, xml_path);
	return result.get();
}

void TextureAtlas::SetTexture(Texture* texture)
{
	m_texture = texture;
	m_sprites.clear();
}

bool TextureAtlas::GetSprite(const uint32_t& name_id, Sprite& info) const
{
	for(SpriteContainer::const_iterator it = m_sprites.begin(); it != m_sprites.end(); ++it)
	{
		if(name_id == it->m_name_id)
		{
			info = *it;
			return true;
		}
	}
	return false;
}

void TextureAtlas::StartElementHandler(void* user_data, const char* name, const char** atts)
{
	uint32_t name_id = Utilities::Hash32(name);
	XMLParsingData* data = (XMLParsingData*)user_data;

	if(name_id == Utilities::Hash32("SubTexture"))
	{
		TextureAtlas::Sprite sprite;
		for(uint32_t i = 0; atts[i] != NULL; i += 2)
		{
			switch(Utilities::Hash32(atts[i]))
			{
			case Utilities::Hash32("name"): sprite.m_name_id = Utilities::Hash32(atts[i+1]); continue;
			case Utilities::Hash32("x"): sprite.m_rect.Position().x = (float)atof(atts[i+1]); continue;
			case Utilities::Hash32("y"): sprite.m_rect.Position().y = (float)atof(atts[i+1]); continue;
			case Utilities::Hash32("width"): sprite.m_rect.Size().x = (float)atof(atts[i+1]); continue;
			case Utilities::Hash32("height"): sprite.m_rect.Size().y = (float)atof(atts[i+1]); continue;
			default: continue;
			}
		}
		data->m_atlas->AddSprite(sprite);
	}
	else if(name_id == Utilities::Hash32("TextureAtlas"))
	{
		for(uint32_t i = 0; atts[i] != NULL; i += 2)
		{
			switch(Utilities::Hash32(atts[i]))
			{
			case Utilities::Hash32("imagePath"): {
					Texture* texture = data->m_content_manager->LoadAsset<Texture>(atts[i+1]);
					if(texture != nullptr && texture->GetAssetType() == Asset::AssetType::kTexture)
					{
						data->m_atlas->SetTexture(texture);
					}
					else
					{
						ALogError("Failed to load texture: %s", atts[i+1]);
					}
				} continue;
			default: continue;
			}
		}
	}
}


}
