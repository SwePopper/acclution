#ifndef IMAGE_H
#define IMAGE_H
#pragma once

#include "widget.h"

namespace Acclution
{

class Image : public Widget
{
public:
	Image(GUI* gui, Widget* parent, const char** attributes);
	virtual ~Image() {}

	virtual void OnEnable();
	virtual void OnDisable();

	virtual uint32_t NumberOfQuads() const { return 1; }
	virtual void SetupQuad(glm::vec3* positions, glm::vec2* uvs, glm::vec4* colors, uint16_t* indices, uint16_t current_vertex_index, astd::vector<const Texture*>& textures);

protected:
	void UpdateBounds();

	uint32_t		m_sprite;
};

}

#endif // IMAGE_H
