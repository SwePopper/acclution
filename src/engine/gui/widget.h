/*
   Acclution
   Copyright (c) 2016 Christoffer Lindqvist

   This software is provided 'as-is', without any express or implied
   warranty. In no event will the authors be held liable for any
   damages arising from the use of this software.

   Permission is granted to anyone to use this software for any
   purpose, including commercial applications, and to alter it and
   redistribute it freely, subject to the following restrictions:

   1. The origin of this software must not be misrepresented; you
      must not claim that you wrote the original software. If you use
      this software in a product, an acknowledgment in the product
      documentation would be appreciated but is not required.

   2. Altered source versions must be plainly marked as such, and
      must not be misrepresented as being the original software.

   3. This notice may not be removed or altered from any source
      distribution.

   Christoffer Lindqvist
   swepopper@gmail.com
*/

#ifndef WIDGET_H
#define WIDGET_H
#pragma once

namespace Acclution
{

class GUI;
class Root;
class Texture;

class Widget
{
public:
	enum class HAlignment : uint8_t { kLeft, kCenter, kRight };
	enum class VAlignment : uint8_t { kTop, kCenter, kBottom };

	using ChildContainer = astd::vector<Widget*>;

	Widget(GUI* gui, Widget* parent, const char** attributes);
	virtual ~Widget();

	// Some Hierarchy functions
	Root* GetRoot();
	Widget* GetParent() { return m_parent; }
	const Widget* GetParent() const { return m_parent; }
	void SetParent(Widget* parent);
	void ClearChildren();

	virtual Rectangle GetRect() const { return m_rect; }
	virtual void SetRect(const Rectangle& rect) { m_rect = rect; }

	virtual Rectangle GetContentRect() const { return m_content_rect; }

	virtual void UpdateLayout();

	virtual void Draw() {}

	virtual void OnEnable();
	virtual void OnDisable();

	virtual void OnAccept() {}
	virtual bool CanSelect() { return false; }
	virtual void OnSelect() {}
	virtual void OnDeselect() {}
	virtual void OnPressed() {}

	virtual uint32_t NumberOfQuads() const { return 0; }
	virtual void SetupQuad(glm::vec3* positions, glm::vec2* uvs, glm::vec4* colors, uint16_t* indices, uint16_t current_vertex_index, astd::vector<const Texture*>& textures);

protected:
	glm::vec2 GetAlignPosition(const glm::vec2& size) const;

	GUI*				m_gui;
	Rectangle			m_rect;
	Rectangle			m_content_rect;
	uint32_t			m_user_id;

	HAlignment			m_horizontal_alignment;
	VAlignment			m_vertical_alignment;

	Widget*				m_parent;
	ChildContainer		m_children;
};

}

#endif // GUIWIDGET_H
