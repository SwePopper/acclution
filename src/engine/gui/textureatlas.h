#ifndef TEXTUREATLAS_H
#define TEXTUREATLAS_H
#pragma once

namespace Acclution
{

class Texture;
class ContentManager;

class TextureAtlas
{
public:
	struct Sprite
	{
		uint32_t		m_name_id;
		Rectangle		m_rect;
	};
	using SpriteContainer = astd::vector<Sprite>;

	TextureAtlas() : m_texture(nullptr) {}

	bool Load(const char* path, ContentManager* content_manager);

	const Texture* GetTexture() const { return m_texture; }
	void SetTexture(Texture* texture);

	bool GetSprite(const uint32_t& name_id, Sprite& info) const;
	void AddSprite(const Sprite& info) { m_sprites.push_back(info); }

private:
	static void StartElementHandler(void* user_data, const char* name, const char** atts);

	Texture*					m_texture;
	SpriteContainer				m_sprites;
};

}

#endif // TEXTUREATLAS_H
