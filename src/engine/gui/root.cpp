#include "engine/platform.h"
#include "root.h"
#include "engine/utilities.h"

namespace Acclution
{

enum class RootAttributeNameID : uint32_t
{
	kName = Utilities::Hash32("name")
};

Root::Root(GUI* gui, Widget* parent, const char** attributes)
	: Widget(gui, parent, attributes),
	  m_name(NULL),
	  m_name_hash(UINT32_MAX)
{
	for(uint32_t i = 0; attributes[i] != NULL; i += 2)
	{
		switch((RootAttributeNameID)Utilities::Hash32(attributes[i]))
		{
		case RootAttributeNameID::kName: {
				m_name = new char[strlen(attributes[i+1])+1];
				strcpy(m_name, attributes[i+1]);
			} continue;
		default: continue;
		}
	}

	if(m_name != NULL && strlen(m_name) > 0)
	{
		m_name_hash = Utilities::Hash32(m_name);
	}
}

Root::~Root()
{
	delete[] m_name;
}

}
