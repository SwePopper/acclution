#ifndef ACTION_H
#define ACTION_H
#pragma once

#include <RuntimeObjectSystem/IObject.h>

namespace Acclution
{

class Action : public IObject
{
public:
	virtual ~Action() {}

	virtual void OnAccept()=0;
};

}

#endif // ACTION_H
