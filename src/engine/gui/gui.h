/*
   Acclution
   Copyright (c) 2016 Christoffer Lindqvist

   This software is provided 'as-is', without any express or implied
   warranty. In no event will the authors be held liable for any
   damages arising from the use of this software.

   Permission is granted to anyone to use this software for any
   purpose, including commercial applications, and to alter it and
   redistribute it freely, subject to the following restrictions:

   1. The origin of this software must not be misrepresented; you
      must not claim that you wrote the original software. If you use
      this software in a product, an acknowledgment in the product
      documentation would be appreciated but is not required.

   2. Altered source versions must be plainly marked as such, and
      must not be misrepresented as being the original software.

   3. This notice may not be removed or altered from any source
      distribution.

   Christoffer Lindqvist
   swepopper@gmail.com
*/

#ifndef GUI_H
#define GUI_H
#pragma once

namespace Acclution
{

class PlatformEvent;
class ContentManager;
class CommandBuffer;
class Texture;
class TextureAtlas;
class Widget;
class Root;
class InputActionData;

class GUI
{
public:
	using CreateWidgetFunc = Widget* (*)(GUI* gui, Widget* parent, const char** atts);
	using WidgetRegister = astd::map<uint32_t, CreateWidgetFunc>;
	using AtlasContainer = astd::vector<TextureAtlas*>;
	using WidgetContainer = astd::vector<Widget*>;

	GUI();
	~GUI();

	bool Initialize();
	void Release();

	void Update();

	void Render(CommandBuffer* command);

	ContentManager* GetContentManager() const { return m_content_manager; }
	const AtlasContainer& GetTextureAtlases() const { return m_texture_atlases; }

	void AddSelectable(Widget* selectable);
	void RemoveSelectable(Widget* selectable);
	void AddDrawable(Widget* drawable);
	void RemoveDrawable(Widget* drawable);

	void ReDraw() { m_redraw.store(true); }

	const glm::vec2& PixelToScreen() const { return m_pixel_to_screen; }
	const glm::vec2& ScreenToPixel() const { return m_screen_size; }
	const float& GetScreenAspect() const { return m_screen_aspect; }

	void RegisterWidgetUserID(Widget* widget, const uint32_t& user_id);
	void UnRegisterWidgetUserID(const uint32_t& user_id);
	Widget* GetWidgetFromUserID(const uint32_t& user_id);

private:
	using UniqueWidgetRegister = astd::map<uint32_t, Widget*>;

	static void StartElementHandler(void* user_data, const char* name, const char** atts);
	static void EndElementHandler(void* user_data, const char* name);
	static void CharacterData(void* user_data, const char* str, int length);

	static void OnMouseMove(const InputActionData* input_action_data);
	static void OnAccept(const InputActionData* input_action_data);
	static void OnBack(const InputActionData* input_action_data);
	static void OnMoveUp(const InputActionData* input_action_data);
	static void OnMoveLeft(const InputActionData* input_action_data);
	static void OnMoveDown(const InputActionData* input_action_data);
	static void OnMoveRight(const InputActionData* input_action_data);

	Widget* GetNextSelection(float (*Comparer)(Widget*,Widget*));
	void SelectAny();

	static void OnSystemEvent(const PlatformEvent* event, void* user_param);

	using RootContainer = astd::vector<Root*>;

	ContentManager*			m_content_manager;
	WidgetRegister			m_widget_register;
	RootContainer			m_roots;
	UniqueWidgetRegister	m_unique_widget_register;
	uint32_t				m_current_root;
	std::atomic_bool		m_redraw;

	WidgetContainer			m_selectables;
	WidgetContainer			m_drawables;
	Widget*					m_current_selected;
	Widget*					m_pressed_widget;

	glm::vec2				m_screen_size;
	glm::vec2				m_pixel_to_screen;
	float					m_screen_aspect;

	// Drawing Data
	uint32_t				m_shader;
	uint32_t				m_vertex_positions;
	uint32_t				m_vertex_uvs;
	uint32_t				m_vertex_colors;
	uint32_t				m_vertex_indices;
	uint32_t				m_uniform_buffer;

	glm::mat4				m_gui_projection;

	AtlasContainer			m_texture_atlases;
};

}

#endif
