/*
   Acclution
   Copyright (c) 2016 Christoffer Lindqvist

   This software is provided 'as-is', without any express or implied
   warranty. In no event will the authors be held liable for any
   damages arising from the use of this software.

   Permission is granted to anyone to use this software for any
   purpose, including commercial applications, and to alter it and
   redistribute it freely, subject to the following restrictions:

   1. The origin of this software must not be misrepresented; you
      must not claim that you wrote the original software. If you use
      this software in a product, an acknowledgment in the product
      documentation would be appreciated but is not required.

   2. Altered source versions must be plainly marked as such, and
      must not be misrepresented as being the original software.

   3. This notice may not be removed or altered from any source
      distribution.

   Christoffer Lindqvist
   swepopper@gmail.com
*/

#include "engine/platform.h"
#include "widget.h"
#include "engine/utilities.h"
#include "root.h"
#include "gui.h"

namespace Acclution
{

enum class WidgetAttributeNameID : uint32_t
{
	kUserID = Utilities::Hash32("user_id"),
	kHAlign = Utilities::Hash32("align_h"),
	kVAlign = Utilities::Hash32("align_v")
};

Widget::Widget(GUI* gui, Widget* parent, const char** attributes)
	: m_gui(gui),
	  m_rect(0.0f, 0.0f, 1.0f, 1.0f),
	  m_content_rect(0.0f, 0.0f, 1.0f, 1.0f),
	  m_user_id(UINT32_MAX),
	  m_horizontal_alignment(HAlignment::kLeft),
	  m_vertical_alignment(VAlignment::kTop),
	  m_parent(NULL)
{
	SetParent(parent);
	for(uint32_t i = 0; attributes[i] != NULL; i += 2)
	{
		switch((WidgetAttributeNameID)Utilities::Hash32(attributes[i]))
		{
		case WidgetAttributeNameID::kUserID:
			m_user_id = Utilities::Hash32(attributes[i+1]);
			ALogInfo("Widget user id: %u = %s", m_user_id, attributes[i+1]);
			m_gui->RegisterWidgetUserID(this, m_user_id);
			break;
		case WidgetAttributeNameID::kHAlign: m_horizontal_alignment = (HAlignment)atoi(attributes[i+1]); break;
		case WidgetAttributeNameID::kVAlign: m_vertical_alignment = (VAlignment)atoi(attributes[i+1]); break;
		default: continue;
		}
	}
}

Widget::~Widget()
{
	ClearChildren();
	m_gui->RemoveSelectable(this);
	m_gui->RemoveDrawable(this);

	if(m_user_id != UINT32_MAX)
	{
		m_gui->UnRegisterWidgetUserID(m_user_id);
		m_user_id = UINT32_MAX;
	}
}

Root* Widget::GetRoot()
{
	Root* root = nullptr;
	Widget* parent = m_parent;
	while(parent != nullptr)
	{
		root = dynamic_cast<Root*>(parent);
		if(root != nullptr)
			break;
		parent = parent->GetParent();
	}

	return root;
}

void Widget::ClearChildren()
{
	for(ChildContainer::iterator it = m_children.begin(); it != m_children.end(); ++it)
	{
		// Will call ClearChildren in destructor automaticaly
		delete *it;
	}
	m_children.clear();
}

void Widget::SetParent(Widget* parent)
{
	if(m_parent != nullptr && !m_parent->m_children.empty())
	{
		astd::EraseRemove(m_parent->m_children, this);
	}
	m_parent = parent;
	if(m_parent != nullptr)
	{
		m_parent->m_children.push_back(this);
	}
}

void Widget::UpdateLayout()
{
	for(ChildContainer::iterator it = m_children.begin(); it != m_children.end(); ++it)
	{
		(*it)->SetRect(m_rect);
		(*it)->UpdateLayout();
	}
}

void Widget::OnEnable()
{
	for(ChildContainer::iterator it = m_children.begin(); it != m_children.end(); ++it)
	{
		(*it)->OnEnable();
	}
}

void Widget::OnDisable()
{
	for(ChildContainer::iterator it = m_children.begin(); it != m_children.end(); ++it)
	{
		(*it)->OnDisable();
	}
}

glm::vec2 Widget::GetAlignPosition(const glm::vec2& size) const
{
	glm::vec2 position = m_rect.Position();

	if(m_horizontal_alignment == HAlignment::kCenter) {
		position.x += (m_rect.Size().x * 0.5f) - (size.x * 0.5f);
	} else if(m_horizontal_alignment == HAlignment::kRight) {
		position.x += m_rect.Size().x - size.x;
	}

	if(m_vertical_alignment == VAlignment::kCenter) {
		position.y += (m_rect.Size().y * 0.5f) - (size.y * 0.5f);
	} else if(m_vertical_alignment == VAlignment::kBottom) {
		position.y += m_rect.Size().y - size.y;
	}

	return position;
}


void Widget::SetupQuad(glm::vec3* positions, glm::vec2* uvs, glm::vec4* colors, uint16_t* indices, uint16_t current_vertex_index, astd::vector<const Texture*>& /*textures*/)
{
	glm::vec2& position = m_rect.Position();
	glm::vec2& size = m_rect.Size();

	positions[0] = glm::vec3(position.x + size.x, position.y, 0.0f);
	positions[1] = glm::vec3(position.x, position.y, 0.0f);
	positions[2] = glm::vec3(position.x, position.y + size.y, 0.0f);
	positions[3] = glm::vec3(position.x + size.x, position.y + size.y, 0.0f);

	uvs[0] = glm::vec2(1.0f, 0.0f);
	uvs[1] = glm::vec2(0.0f, 0.0f);
	uvs[2] = glm::vec2(0.0f, 1.0f);
	uvs[3] = glm::vec2(1.0f, 1.0f);

	colors[0] = glm::vec4(1.0f, 1.0f, 1.0f, 1.0f);
	colors[1] = glm::vec4(1.0f, 1.0f, 1.0f, 1.0f);
	colors[2] = glm::vec4(1.0f, 1.0f, 1.0f, 1.0f);
	colors[3] = glm::vec4(1.0f, 1.0f, 1.0f, 1.0f);

	indices[0] = current_vertex_index;
	indices[1] = current_vertex_index + 1;
	indices[2] = current_vertex_index + 2;
	indices[3] = current_vertex_index;
	indices[4] = current_vertex_index + 2;
	indices[5] = current_vertex_index + 3;
}

}
