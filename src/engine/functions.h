#ifndef FUNCTIONS_H
#define FUNCTIONS_H

namespace Acclution
{

namespace astd
{

void GetCommandLine(astd::string* command_line, int& count);

template<typename T>
inline constexpr const T& min(const T& a, const T& b) { return a < b ? a : b; }

template<typename T>
inline constexpr const T& max(const T& a, const T& b) { return b < a ? a : b; }

template <typename T>
inline constexpr const T & multi_min(const T & a) { return a; }
template <typename T, typename ... R >
inline constexpr T multi_min(const T & a, const T & b, const R &... args)
{
	return multi_min(a < b ? a : b, args...);
}

template <typename T>
inline constexpr const T & multi_max(const T & a) { return a; }
template <typename T, typename ... R >
inline constexpr T multi_max(const T & a, const T & b, const R &... args)
{
	return multi_max(a > b ? a : b, args...);
}

template<typename T>
inline constexpr const T& clamp(const T& value, const T& _min, const T& _max) { return max(min(value, _max), _min); }

template<typename T>
inline constexpr T sign(T value) { return value < static_cast<T>(0) ? static_cast<T>(-1) : static_cast<T>(1); }

inline bool check_bit(const uint32_t& value, const uint32_t& position) { return !!(value & (1 << position)); }
inline void set_bit(uint32_t& value, const uint32_t& position) { value |= (1 << position); }
inline void clear_bit(uint32_t& value, const uint32_t& position) { value &= ~(1 << position); }
inline void toggle_bit(uint32_t& value, const uint32_t& position) { value ^= (1 << position); }

template<typename T>
inline bool is_ready(const std::future<T>& f) { return f.wait_for(std::chrono::seconds(0)) == std::future_status::ready; }

// https://graphics.stanford.edu/~seander/bithacks.html#RoundUpPowerOf2
inline uint32_t NextPowerTwo(uint32_t value)
{
	value += (value == 0);
	--value;
	value |= value >> 1;
	value |= value >> 2;
	value |= value >> 4;
	value |= value >> 8;
	value |= value >> 16;
	return ++value;
}

// https://graphics.stanford.edu/~seander/bithacks.html#IntegerLogDeBruijn
inline uint32_t LogBase2(uint32_t value)
{
	static const uint32_t MultiplyDeBruijnBitPosition[32] =
	{
		0, 9, 1, 10, 13, 21, 2, 29, 11, 14, 16, 18, 22, 25, 3, 30,
		8, 12, 20, 28, 15, 17, 24, 7, 19, 27, 23, 6, 26, 5, 4, 31
	};

	value |= value >> 1; // first round down to one less than a power of 2
	value |= value >> 2;
	value |= value >> 4;
	value |= value >> 8;
	value |= value >> 16;

	return MultiplyDeBruijnBitPosition[(uint32_t)(value * 0x07C4ACDDU) >> 27];
}

// Debian is a bit slow to update so I made this
// TODO popper: remove when Debian have updated gcc
inline void* align(const size_t align, const size_t size, void*& ptr, size_t& space)
{
	// Unlikely..
	if(align <= 1)
		return ptr;

	const uintptr_t ptr_value = (uintptr_t)ptr;
	const uintptr_t aligned_ptr = ((size_t)((ptr_value + align - 1) / align)) * align;
	const uintptr_t diff = aligned_ptr - ptr_value;
	if(size + diff > space)
	{
		return nullptr;
	}

	space -= diff;
	return ptr = (void*)aligned_ptr;
}

template<typename T, typename Container>
inline void EraseRemove(Container& list, T item)
{
	list.erase(std::remove(list.begin(), list.end(), item), list.end());
}

template<typename T>
inline void EraseRemove(astd::set<T>& list, T item)
{
	for(typename astd::set<T>::iterator it = list.begin(); it != list.end(); )
	{
		if(*it == item)
			list.erase(it++);
		else
			++it;
	}
}

template<typename Container>
inline void RemoveDuplicates(Container& list)
{
	list.erase(std::unique(list.begin(), list.end()), list.end());
}

template<typename ForwardIterator, typename T>
inline void replace(ForwardIterator first, ForwardIterator last, const T& old_value, const T& new_value)
{
#if defined(USE_EASTL)
	eastl::
#else
	std::
#endif
		replace(first, last, old_value, new_value);
}

}

}

#endif // FUNCTIONS_H
