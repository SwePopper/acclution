@echo off

:: Get some vs vars
if exist "%VS140COMNTOOLS%VsDevCmd.bat" call "%VS140COMNTOOLS%VsDevCmd.bat"

:: Update the submodules
echo "Updating git submodules.."
git submodule update --init --recursive

mkdir build
cd build
conan install .. --build=missing
cd ..
