/*
   Acclution
   Copyright (c) 2016 Christoffer Lindqvist

   This software is provided 'as-is', without any express or implied
   warranty. In no event will the authors be held liable for any
   damages arising from the use of this software.

   Permission is granted to anyone to use this software for any
   purpose, including commercial applications, and to alter it and
   redistribute it freely, subject to the following restrictions:

   1. The origin of this software must not be misrepresented; you
      must not claim that you wrote the original software. If you use
      this software in a product, an acknowledgment in the product
      documentation would be appreciated but is not required.

   2. Altered source versions must be plainly marked as such, and
      must not be misrepresented as being the original software.

   3. This notice may not be removed or altered from any source
      distribution.

   Christoffer Lindqvist
   swepopper@gmail.com
*/

#include <platform.h>

#define CATCH_CONFIG_RUNNER
#include <catch.hpp>

#define BUILD_TYPE_DEBUG
#include <debugserver.h>
#include <iothreads.h>
#include <readtask.h>
#include <string>

struct ReadUserData
{
	std::string exec;
	std::atomic_bool run;
	uint32_t hash;
};
ReadUserData* data = NULL;

void ReadData(Acclution::ReadTask::ReadContext& context)
{
	if(!context.IsValid())
	{
		LogError("Failed to read file!");
		return;
	}

	ReadUserData* data = (ReadUserData*)context.UserData();
	data->hash = 0;

	// testing to read the data in an odd size to see if eferything is ok
	struct Test
	{
		char a, b, c;
	};

	Test value;
	while(!context.IsDone() && context.IsValid())
	{
		if(!context.Deserialize(value))
			break;
		data->hash += value.a;
		data->hash += value.b;
		data->hash += value.c;
	}

	data->run = false;
}

TEST_CASE("ReadTask ", "Read File Tests" ) {
	Acclution::IOThreads threads;
	REQUIRE(threads.Initialize());

	Acclution::IOThreads::IOTask* task = Acclution::ReadTask::CreateReadTask(data->exec.c_str(), true, true, ReadData, data);
	REQUIRE(task != NULL);
	REQUIRE(threads.AddIOTask(task));

	FILE* file = fopen(data->exec.c_str(), "rb");
	fseek(file, 0, SEEK_END);
	uint32_t size = ftell(file);
	fseek(file, 0, SEEK_SET);

	char* file_data = (char*)malloc(size);
	size = fread(file_data, 1, size, file);
	fclose(file);

	uint32_t secon_hash = 0;
	for(uint32_t i = 0; i < size; ++i)
	{
		secon_hash += file_data[i];
	}

	free(file_data);

	while(data->run)
		;

	REQUIRE(data->hash == secon_hash);
	threads.Release();
}

int main(int argc, char* const argv[])
{
	data = new ReadUserData;
	data->run = true;
	data->exec = argv[0];
	int ret = Catch::Session().run( argc, argv );
	delete data;
	return ret;
}
