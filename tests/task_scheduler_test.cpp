/*
   Acclution
   Copyright (c) 2016 Christoffer Lindqvist

   This software is provided 'as-is', without any express or implied
   warranty. In no event will the authors be held liable for any
   damages arising from the use of this software.

   Permission is granted to anyone to use this software for any
   purpose, including commercial applications, and to alter it and
   redistribute it freely, subject to the following restrictions:

   1. The origin of this software must not be misrepresented; you
      must not claim that you wrote the original software. If you use
      this software in a product, an acknowledgment in the product
      documentation would be appreciated but is not required.

   2. Altered source versions must be plainly marked as such, and
      must not be misrepresented as being the original software.

   3. This notice may not be removed or altered from any source
      distribution.

   Christoffer Lindqvist
   swepopper@gmail.com
*/

#include <platform.h>

#define CATCH_CONFIG_MAIN
#include <catch.hpp>
#include <taskscheduler.h>
#include <task.h>

bool Initialize(Acclution::TaskScheduler& scheduler)
{
	if(!scheduler.Initialize())
	{
		scheduler.Release();
		return false;
	}
	return true;
}

void Release(Acclution::TaskScheduler& scheduler)
{
	scheduler.Release();
}

void FiberFunc(std::atomic_int* count)
{
	count->fetch_sub(1);
}

class Test
{
public:
	void Function(std::atomic_int* count, int add)
	{
		count->fetch_sub(add);
	}
};

bool Run(Acclution::TaskScheduler& scheduler)
{
	//
	// Run 150 fibers where each fiber subtracts 1 from count. Fail if we time out or if count is not <= 0
	//

	std::atomic_int count(150);

	Test test;
	scheduler.AddTask(Acclution::Task::make_taskmf(&Test::Function, &test, &count, 1));

	for(int i = 0; i < 149; ++i)
	{
		scheduler.AddTask(Acclution::Task::make_task(&FiberFunc, &count));
	}

	std::chrono::system_clock::time_point start = std::chrono::high_resolution_clock::now();
	while(count > 0 && (std::chrono::high_resolution_clock::now() - start).count() < 100000000.0f)
	{
		std::this_thread::yield();
	}

	return count <= 0;
}

TEST_CASE("Fiber Scheduler ", "Initialize, Release, Run some fibers" ) {
	Acclution::TaskScheduler scheduler;

	REQUIRE(Initialize(scheduler) == true);
	REQUIRE(Run(scheduler) == true);

	Release(scheduler);
}
