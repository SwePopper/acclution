/*
   Acclution
   Copyright (c) 2016 Christoffer Lindqvist

   This software is provided 'as-is', without any express or implied
   warranty. In no event will the authors be held liable for any
   damages arising from the use of this software.

   Permission is granted to anyone to use this software for any
   purpose, including commercial applications, and to alter it and
   redistribute it freely, subject to the following restrictions:

   1. The origin of this software must not be misrepresented; you
      must not claim that you wrote the original software. If you use
      this software in a product, an acknowledgment in the product
      documentation would be appreciated but is not required.

   2. Altered source versions must be plainly marked as such, and
      must not be misrepresented as being the original software.

   3. This notice may not be removed or altered from any source
      distribution.

   Christoffer Lindqvist
   swepopper@gmail.com
*/

#include <platform.h>

#define CATCH_CONFIG_MAIN
#include <catch.hpp>
#include <serializer.h>

class Awesome
{
public:
	int64_t lol;
	uint16_t abc;
	uint16_t def;
	int32_t ghi;
};

class Test
{
public:
	uint8_t foo;
};

class Custom : public Test
{
public:
	int64_t GetVelocity() const { return m_velocity; }
	void SetVelocity(const int64_t& velocity) { m_velocity = velocity; }

	uint32_t abc;
	Awesome def;
	int32_t ghi;

	bool Equal(const Custom& b) { return abc == b.abc && ghi == b.ghi && m_velocity == b.m_velocity; }

private:
	int64_t m_velocity;
};

#include "Custom.cpp"

template<typename T>
bool TestType(Acclution::Serializer& serializer, const T value)
{
	T ret;
	return serializer.Serialize(value) && serializer.Deserialize(ret) && value == ret;
}

template<typename T>
bool TestType(Acclution::Serializer& serializer, const T* value, size_t count)
{
	size_t size = sizeof(T) * count;
	T* ret = (T*)alloca(size);
	return serializer.Serialize(value, count) && serializer.Deserialize(ret, count) && memcmp(value, ret, size) == 0;
}

TEST_CASE("Serializer ", "Serialize and Deserialize some structs" ) {
	Acclution::Serializer serializer;

	SECTION("Testing 8bit") {
		REQUIRE(TestType<int8_t>(serializer, (int8_t)INT8_MIN));
		REQUIRE(TestType<int8_t>(serializer, (int8_t)INT8_MAX));
		REQUIRE(TestType<int8_t>(serializer, (int8_t)32));
		REQUIRE(TestType<uint8_t>(serializer, (uint8_t)UINT8_MAX));
	}

	SECTION("Testing 16bit") {
		REQUIRE(TestType<int16_t>(serializer, (int16_t)INT16_MIN));
		REQUIRE(TestType<int16_t>(serializer, (int16_t)INT16_MAX));
		REQUIRE(TestType<int16_t>(serializer, (int16_t)1472));
		REQUIRE(TestType<uint16_t>(serializer, (uint16_t)UINT16_MAX));
	}

	SECTION("Testing 32bit") {
		REQUIRE(TestType<int32_t>(serializer, (int32_t)INT32_MIN));
		REQUIRE(TestType<int32_t>(serializer, (int32_t)INT32_MAX));
		REQUIRE(TestType<int32_t>(serializer, (int32_t)264321));
		REQUIRE(TestType<uint32_t>(serializer, (uint32_t)UINT32_MAX));
	}

	SECTION("Testing 64bit") {
		REQUIRE(TestType<int64_t>(serializer, (int64_t)INT64_MIN));
		REQUIRE(TestType<int64_t>(serializer, (int64_t)INT64_MAX));
		REQUIRE(TestType<int64_t>(serializer, (int64_t)(2147483747)));
		REQUIRE(TestType<uint64_t>(serializer, (uint64_t)UINT64_MAX));
	}

	SECTION("Testing float") {
		REQUIRE(TestType<float>(serializer, FLT_MIN));
		REQUIRE(TestType<float>(serializer, FLT_MAX));
	}

	SECTION("Testing double") {
		REQUIRE(TestType<double>(serializer, DBL_MIN));
		REQUIRE(TestType<double>(serializer, DBL_MAX));
	}

	SECTION("Testing c strings") {
		REQUIRE(TestType<char>(serializer, "HELLO", 6));
		REQUIRE(TestType<wchar_t>(serializer, L"HELLO", 6));
	}

	SECTION("Testing Custom class") {
		Custom test;
		test.abc = 5;
		test.ghi = 15;
		test.SetVelocity(655423);
		REQUIRE(SerializeCustom::Serialize(serializer, test));

		Custom test2;
		REQUIRE(SerializeCustom::Deserialize(serializer, test2));

		REQUIRE(test.Equal(test2));
	}
}
