#ifndef PROCESSOR_H
#define PROCESSOR_H

#include <string>
#include <vector>
#include <map>
#include <future>
#include <memory>

class BaseProcessor;

class Processor
{
public:
	static void LogMessage(const char* file, int line, const char* format, ...);
	static bool MakeDirectoryTree(const std::string& path);
	static bool RemoveDirectoryTree(const std::string& path);

	Processor();
	~Processor();

	void CleanOutput() { m_clean_output = true; }
	void SetProjectFilePath(const char* project_file_path);

	bool Start();

	const std::string& GetProjectDirectory() const { return m_project_directory; }
	const std::string& GetOutputDirectory() const { return m_output_directory; }
	const std::string& GetIntermediateDirectory() const { return m_intermediate_directory; }
	const std::string& GetPlatform() const { return m_platform; }
	const bool& Compress() const { return m_compress; }

private:
	bool ProcessLine(const std::string& line);
	bool CheckProcesses();
	static bool MyCopyFile(std::string src_file, std::string dst_file);

	using ProcessorCreator = BaseProcessor* (*)(Processor* processor);
	using AsyncProcesses = std::vector<std::future<bool> >;
	using ProcessorRegister = std::map<uint32_t, ProcessorCreator>;

	std::string		m_project_file_path;
	std::string		m_project_directory;
	uint32_t		m_max_async_processes;
	bool			m_clean_output;

	ProcessorRegister	m_processor_register;

	// Global Properties
	std::string		m_output_directory;
	std::string		m_intermediate_directory;
	std::string		m_platform;
	bool			m_compress;

	AsyncProcesses	m_async_processes;

	BaseProcessor*	m_current_processor;
	std::vector<BaseProcessor*> m_processors;
};

#define LOG_INFO(...) Processor::LogMessage(__FILE__, __LINE__, __VA_ARGS__)
#define STRINGIFY(arg) #arg

// http://isthe.com/chongo/tech/comp/fnv/#FNV-param
static constexpr uint32_t kHash32Prime = 16777619u;
static constexpr uint32_t kHash32Offset = 2166136261u;
constexpr uint32_t Hash32(const char* s, uint32_t last_hash = kHash32Offset)
{
	return *s ? Hash32(s+1, static_cast<uint32_t>((*s ^ last_hash) * static_cast<uint64_t>(kHash32Prime))) : last_hash;
}

#endif
