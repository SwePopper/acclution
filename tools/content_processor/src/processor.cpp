#include "processor.h"

#include <stdarg.h>
#include <iostream>
#include <fstream>
#include <algorithm>
#include <string.h>

#if __linux__
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <dirent.h>
#elif WIN32 || _WIN32

#ifndef WIN32_LEAN_AND_MEAN
#define WIN32_LEAN_AND_MEAN
#endif

#include <windows.h>
#include <direct.h>
#endif

#include "processors/texture_processor.h"
#include "processors/font_processor.h"
#include "processors/spirv_shader_processor.h"
#include "processors/model_processor.h"

namespace
{
	template<typename T>
	BaseProcessor* CreateProcessor(Processor* processor)
	{
		return new T(processor);
	}

	constexpr size_t va_buff_size = 4096;
	std::mutex log_lock;
}

void Processor::LogMessage(const char* file, int line, const char* format, ...)
{
	char buff[va_buff_size];
	va_list args;
	va_start(args, format);
	vsnprintf(buff, va_buff_size, format, args);
	va_end(args);

	std::lock_guard<std::mutex> guard(log_lock);
	fprintf(stderr, "%s:%i - %s\n", file, line, buff);
	printf("%s:%i - %s\n", file, line, buff);
}

bool Processor::MakeDirectoryTree(const std::string& path)
{
	std::string dir = path;

	// if file. remove file name
	size_t dot_pos = dir.rfind('.');
	size_t slash_pos = dir.rfind('/');
	if(dot_pos != std::string::npos && slash_pos != std::string::npos)
	{
		if(slash_pos < dot_pos)
		{
			dir = dir.substr(0, slash_pos);
			slash_pos = dir.rfind('/');
		}
	}

	struct stat statbuf;

	// Check parent dir
	if(slash_pos != std::string::npos)
	{
		std::string parent_dir = dir.substr(0, slash_pos);
		if(stat(parent_dir.c_str(), &statbuf) == -1)
		{
			if(!MakeDirectoryTree(parent_dir))
				return false;
		}
	}

	if(stat(dir.c_str(), &statbuf) == -1)
	{
#if __linux__
		if(mkdir(dir.c_str(), 0700) != 0)
			return false;
#elif WIN32 || _WIN32
		if(_mkdir(dir.c_str()) != 0)
			return false;
#else
#error Implement
#endif
	}


	return true;
}

bool Processor::RemoveDirectoryTree(const std::string& path)
{
#if __linux__
	DIR* directory = opendir(path.c_str());
	if(directory != nullptr)
	{
		struct dirent* ptr = nullptr;
		while((ptr = readdir(directory)) != nullptr)
		{
			if(strncmp(ptr->d_name, ".", 2) == 0 || strncmp(ptr->d_name, "..", 2) == 0 )
				continue;

			std::string child_path = path + ptr->d_name;

			struct stat statbuf;
			if(stat(child_path.c_str(), &statbuf) == 0)
			{
				if(S_ISDIR(statbuf.st_mode))
				{
					if(!RemoveDirectoryTree(child_path + "/"))
						return false;
				}
				else
				{
					if(remove(child_path.c_str()) != 0)
						return false;
				}
			}
		}

		closedir(directory);
		rmdir(path.c_str());
	}
#elif WIN32 || _WIN32

	WIN32_FIND_DATA ffd;
	HANDLE find = FindFirstFile(path.c_str(), &ffd);
	if(INVALID_HANDLE_VALUE == find)
	{
		LOG_INFO("Failed to find first file!\n");
		return false;
	}

	do {
		if(ffd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)
		{
			RemoveDirectory(ffd.cFileName);
		}
		else
		{
			if(remove(ffd.cFileName) != 0)
				return false;
		}
	} while(FindNextFile(find, &ffd) != 0);

#else
#error Implement
#endif

	return true;
}

void Processor::SetProjectFilePath(const char* project_file_path)
{
	m_project_file_path = project_file_path;
	std::replace(m_project_file_path.begin(), m_project_file_path.end(), '\\', '/');

	size_t pos = m_project_file_path.rfind('/');
	if(pos == std::string::npos)
	{
		m_project_directory = "./";
	}
	else
	{
		m_project_directory = m_project_file_path.substr(0, pos+1);
	}
}

Processor::Processor()
	: m_project_file_path(""),
	  m_clean_output(false)
{}

Processor::~Processor()
{
	for(uint32_t i = 0; i < m_async_processes.size(); ++i)
	{
		m_async_processes[i].get();
	}
	m_async_processes.clear();

	for(std::vector<BaseProcessor*>::iterator it = m_processors.begin(); it != m_processors.end(); ++it)
	{
		delete *it;
	}
	m_processors.clear();
}

bool Processor::Start()
{
	if(m_project_file_path.empty())
	{
		LOG_INFO("Empty project file path!");
		return false;
	}

	m_processor_register.clear();
	m_processor_register[Hash32(STRINGIFY(TextureProcessor))] = CreateProcessor<TextureProcessor>;
	m_processor_register[Hash32(STRINGIFY(FontProcessor))] = CreateProcessor<FontProcessor>;
	m_processor_register[Hash32(STRINGIFY(SpirVShaderProcessor))] = CreateProcessor<SpirVShaderProcessor>;
	m_processor_register[Hash32(STRINGIFY(ModelProcessor))] = CreateProcessor<ModelProcessor>;

	m_output_directory = m_intermediate_directory = m_platform = "";
	m_compress = false;
	m_async_processes.clear();
	for(std::vector<BaseProcessor*>::iterator it = m_processors.begin(); it != m_processors.end(); ++it)
	{
		delete *it;
	}
	m_processors.clear();
	m_current_processor = nullptr;

	m_max_async_processes = std::thread::hardware_concurrency();
	if(m_max_async_processes <= 0)
		m_max_async_processes = 2;

	{
		std::ifstream project_file(m_project_file_path.c_str(), std::ios_base::in);
		if(!project_file.is_open())
		{
			LOG_INFO("Failed to open project file: %s", m_project_file_path.c_str());
			return false;
		}

		std::string line = "";
		while(std::getline(project_file, line))
		{
			if(!ProcessLine(line))
				return false;
		}
	}

	bool result = true;
	for(uint32_t i = 0; i < m_async_processes.size(); ++i)
	{
		result = result && m_async_processes[i].get();
	}
	m_async_processes.clear();

	return result;
}

bool Processor::ProcessLine(const std::string& line)
{
	if(line.empty() || line[0] == '#')
		return true;

	size_t split = line.find_first_of(':');
	if(split == std::string::npos)
	{
		LOG_INFO("Invalid line: %s\n", line.c_str());
		return false;
	}

	std::string key = line.substr(0, split);
	std::string value = line.substr(split+1);

	// Hashes are calculated compiletime except for what is in the key variable
	switch(Hash32(key.c_str()))
	{
	// Global Properties
	case Hash32("/outputDir"): {
			m_output_directory = m_project_directory + value + "/";
			if(m_clean_output)
				RemoveDirectoryTree(m_output_directory.c_str());
		} break;
	case Hash32("/intermediateDir"): {
			m_intermediate_directory = m_project_directory + value + "/";
			if(m_clean_output)
				RemoveDirectoryTree(m_intermediate_directory.c_str());
		} break;
	case Hash32("/platform"): m_platform = value; break;
	case Hash32("/compress"): m_compress = value == "True"; break;

	// References

	case Hash32("/processor"): {
			ProcessorRegister::iterator it = m_processor_register.find(Hash32(value.c_str()));
			if(it == m_processor_register.end())
			{
				LOG_INFO("Could not find content processor %s", value.c_str());
				return false;
			}
			m_current_processor = it->second(this);
			m_processors.push_back(m_current_processor);
		} break;

	case Hash32("/processorParam"): {
			if(m_current_processor == nullptr)
			{
				LOG_INFO("No current content processor for %s", line.c_str());
				return false;
			}
			m_current_processor->AddParameter(value);
		} break;

	case Hash32("/build"): {
			if(m_current_processor == nullptr)
			{
				LOG_INFO("No current content processor for %s", line.c_str());
				return false;
			}

			if(!CheckProcesses())
				return false;
			m_async_processes.push_back(m_current_processor->Build(value));
		} break;

	// Content
	case Hash32("/copy"): {
			if(!CheckProcesses())
				return false;
			m_async_processes.push_back(std::async(std::launch::async, MyCopyFile,
												   m_project_directory + value,
												   m_output_directory + value));
		} break;
	}

	return true;
}

bool Processor::CheckProcesses()
{
	if(m_async_processes.size() >= m_max_async_processes)
	{
		bool result = m_async_processes.front().get();
		m_async_processes.erase(m_async_processes.begin());
		if(!result)
			return false;
	}

	return true;
}

bool Processor::MyCopyFile(std::string src_file, std::string dst_file)
{
	MakeDirectoryTree(dst_file);

	std::ifstream src(src_file.c_str(), std::ios::binary);
	std::ofstream dst(dst_file.c_str(), std::ios::binary);

	if(!src.is_open())
	{
		LOG_INFO("Failed to open source file for copy process: %s", src_file.c_str());
		return false;
	}

	if(!dst.is_open())
	{
		LOG_INFO("Failed to open destination file for copy process: %s", dst_file.c_str());
		return false;
	}

	dst << src.rdbuf();
	return true;
}
