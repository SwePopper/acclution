#ifndef CHILDPROCESS_H
#define CHILDPROCESS_H

#include <string>
#include <vector>

class ChildProcess
{
public:
	static ChildProcess* Start(std::string program_path, std::vector<std::string> args);
	virtual ~ChildProcess() {}

	virtual bool WaitForCompletion()=0;
	virtual const std::string& GetOutputFilename()const=0;

protected:
	ChildProcess() {}

	virtual bool StartUp(std::string program_path, std::vector<std::string> args)=0;
};

#endif // CHILDPROCESS_H
