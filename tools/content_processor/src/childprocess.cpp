#include "childprocess.h"

#include <cstdio> // For tempnam
#include <string.h>

#include "processor.h" // For LOG_INFO

// http://www.cplusplus.com/forum/lounge/17684/

#if __linux__

#include <unistd.h>
#include <sys/wait.h>

class ChildProcessUnix : public ChildProcess
{
public:
	ChildProcessUnix() : m_process_id(-1), m_temp_name("") { m_arguments.clear(); }
	virtual ~ChildProcessUnix()
	{
		if(!m_temp_name.empty())
		{
			remove(m_temp_name.c_str());
		}

		for(uint32_t i = 0; i < m_arguments.size(); ++i)
			delete[] m_arguments[i];
		m_arguments.clear();
	}

	virtual bool StartUp(std::string program_path, std::vector<std::string> args)
	{
		for(uint32_t i = 0; i < m_arguments.size(); ++i)
			delete[] m_arguments[i];
		m_arguments.clear();

		std::string program_name = program_path;
		size_t pos = program_path.rfind('/');
		if(pos != std::string::npos)
			program_name = program_path.substr(pos+1);

		AddArgument(program_name);

		for(uint32_t i = 0; i < args.size(); ++i)
		{
			AddArgument(args[i]);
		}
		m_arguments.push_back(nullptr);

		m_temp_name = "/tmp/tmpfileXXXXXX";
		int output = mkstemp(&m_temp_name[0]);

		m_process_id = fork();
		switch(m_process_id)
		{
		case -1: LOG_INFO("Failed to fork process: %s", program_path.c_str()); break;
		case 0: {
				dup2(output, 1); // redirect stdout
				execvp(program_path.c_str(), m_arguments.data());
				LOG_INFO("Failed to execl process: %s %i", program_path.c_str(), errno);
				exit(1);
			}break;
		default: break;
		}

		return true;
	}

	virtual bool WaitForCompletion()
	{
		if(m_process_id == -1)
			return false;

		int status = 0;

		do
		{
			waitpid(m_process_id, &status, 0);
		} while(!WIFEXITED(status));

		return true;
	}

	virtual const std::string& GetOutputFilename() const { return m_temp_name; }

private:
	void AddArgument(const std::string& argument)
	{
		char* arg = new char[argument.size()+1];
		strncpy(arg, argument.c_str(), argument.size());
		arg[argument.size()] = '\0';

		m_arguments.push_back(arg);
	}

	pid_t				m_process_id;
	std::string			m_temp_name;
	std::vector<char*>	m_arguments;
};

using Process = ChildProcessUnix;

#elif WIN32 || _WIN32

#ifndef WIN32_LEAN_AND_MEAN
#define WIN32_LEAN_AND_MEAN
#endif

#include <Windows.h>

class ChildProcessWindows : public ChildProcess
{
public:
	ChildProcessWindows()
		: m_process(nullptr), m_thread(nullptr), m_temp_name(""), m_argument(nullptr), m_temp_file(nullptr)
	{}
	virtual ~ChildProcessWindows()
	{
		if(m_process != nullptr) CloseHandle(m_process);
		if(m_thread != nullptr) CloseHandle(m_thread);
		if(m_temp_file != nullptr) CloseHandle(m_temp_file);
		delete[] m_argument;

		if(!m_temp_name.empty())
		{
			DeleteFile(m_temp_name.c_str());
		}
	}

	virtual bool StartUp(std::string program_path, std::vector<std::string> args)
	{
		std::string arguments = program_path + " ";
		for(uint32_t i = 0; i < args.size(); ++i)
		{
			arguments += args[i] + " ";
		}

		m_argument = new char[arguments.size()];
		strncpy(m_argument, arguments.c_str(), arguments.size());
		m_argument[arguments.size()-1] = '\0';

		char temp_path[MAX_PATH] = { 0 };
		DWORD ret = GetTempPath(MAX_PATH, temp_path);
		if(ret > MAX_PATH || ret == 0)
		{
			LOG_INFO("Failed to get temp path\n");
			return false;
		}

		char temp_name[MAX_PATH] = { 0 };
		ret = GetTempFileName(temp_path, TEXT("tmpfile"), 0, temp_name);
		if(ret == 0)
		{
			LOG_INFO("Failed to get temp file name\n");
			return false;
		}

		m_temp_name = temp_name;

		SECURITY_ATTRIBUTES sa;
		sa.nLength = sizeof(sa);
		sa.lpSecurityDescriptor = NULL;
		sa.bInheritHandle = TRUE;

		m_temp_file = CreateFile(m_temp_name.c_str(),
									  FILE_APPEND_DATA,
									  FILE_SHARE_WRITE | FILE_SHARE_READ,
									  &sa,
									  CREATE_ALWAYS,
									  FILE_ATTRIBUTE_NORMAL,
									  NULL);
		if(m_temp_file == INVALID_HANDLE_VALUE)
		{
			LOG_INFO("Failed to create temp file\n");
			return false;
		}

		STARTUPINFO startup_info;
		memset(&startup_info, 0, sizeof(startup_info));
		startup_info.cb = sizeof(startup_info);
		startup_info.hStdOutput = m_temp_file;
		startup_info.hStdError = nullptr;
		startup_info.hStdInput = nullptr;
		startup_info.dwFlags |= STARTF_USESTDHANDLES;

		PROCESS_INFORMATION process_info;
		memset(&process_info, 0, sizeof(process_info));

		if(!CreateProcess(nullptr, m_argument,
						  nullptr, nullptr, true, CREATE_NO_WINDOW, nullptr, nullptr,
						  &startup_info, &process_info))
		{
			LOG_INFO("Failed to start process: %s %u\n", program_path.c_str(), GetLastError());
			return false;
		}

		m_process = process_info.hProcess;
		m_thread = process_info.hThread;
		return true;
	}

	virtual bool WaitForCompletion()
	{
		if(m_process == nullptr ||
		   m_thread == nullptr ||
		   WaitForSingleObject(m_process, INFINITE) != 0 ||
		   WaitForSingleObject(m_thread, INFINITE) != 0)
			return false;

		CloseHandle(m_process);
		CloseHandle(m_thread);
		CloseHandle(m_temp_file);
		m_process = m_thread = m_temp_file = nullptr;

		return true;
	}

	virtual const std::string& GetOutputFilename() const { return m_temp_name; }

private:
	HANDLE				m_process;
	HANDLE				m_thread;
	std::string			m_temp_name;

	char*				m_argument;

	HANDLE				m_temp_file;
};

using Process = ChildProcessWindows;

#else
#error Implement
#endif

ChildProcess* ChildProcess::Start(std::string program_path, std::vector<std::string> args)
{
	Process* process = new Process();
	if(!process->StartUp(program_path, args))
	{
		delete process;
		process = nullptr;
	}
	return process;
}
