#include "serializer.h"

#include <stdio.h>
#include <vector>
#include <algorithm>

#include "processor.h"

namespace
{
	std::vector<const char*> kPlatforms {
		"DesktopGL"		// DesktopGL
	};

	enum class SerializerFlags : uint8_t
	{
		kNone			= 0x0,
		kCompressed		= 0x1
	};

	template<typename T>
	const T& MyMin(const T& a, const T& b) { return a < b ? a : b; }
}

Serializer::~Serializer()
{
	if(m_file != nullptr)
	{
		if(m_file_size_position != std::string::npos)
		{
			uint64_t file_size = ftell(m_file);
			fseek(m_file, (long)m_file_size_position, SEEK_SET);
			Serialize(file_size - m_file_size_position);
		}

		fclose(m_file);
		m_file = nullptr;
	}
}

bool Serializer::Initialize(std::string file_path, Processor* processor)
{
	file_path += ".adf";

	m_file_size_position = std::string::npos;

	Processor::MakeDirectoryTree(file_path);
	m_file = fopen(file_path.c_str(), "wb");
	if(m_file == nullptr)
	{
		LOG_INFO("Failed to open file: %s\n", file_path.c_str());
		return false;
	}

	// write the identifier ADF (Acclution Data File)
	if(!Serialize('A') || !Serialize('D') || !Serialize('F'))
	{
		LOG_INFO("Failed to serialize identifier\n");
		return false;
	}

	// Write the platform
	std::vector<const char*>::iterator it =
			std::find(kPlatforms.begin(), kPlatforms.end(), processor->GetPlatform());
	if(it == kPlatforms.end())
	{
		LOG_INFO("Failed to get platform index\n");
		return false;
	}

	uint8_t platform = (uint8_t)std::distance(kPlatforms.begin(), it);
	if(!Serialize(platform))
	{
		LOG_INFO("Failed to serialize platform\n");
		return false;
	}

	// Write serializer version
	uint8_t version = 1;
	if(!Serialize(version))
	{
		LOG_INFO("Failed to serialize version\n");
		return false;
	}

	uint8_t flags = processor->Compress() ? (uint8_t)SerializerFlags::kCompressed :
											(uint8_t)SerializerFlags::kNone;
	if(!Serialize(flags))
	{
		LOG_INFO("Failed to serialize flags\n");
		return false;
	}

	m_file_size_position = ftell(m_file);
	uint64_t file_size = 0;
	if(!Serialize(file_size))
	{
		LOG_INFO("Failed to reserve file size\n");
		return false;
	}

	if(processor->Compress())
	{
		uint64_t compressed_size = 0;
		if(!Serialize(compressed_size))
			return false;

		// TODO: support this..
		LOG_INFO("Compressed format unsuported =(\n");
		return false;
	}

	return m_file != nullptr;
}

bool Serializer::Serialize(const void* buffer, const uint64_t& size)
{
	if(m_file == nullptr)
		return false;
	fflush(m_file);

	uintptr_t current = (uintptr_t)buffer;
	uintptr_t end = current + size;

	if(size > 10000)
	{
		LOG_INFO("pop");
	}

	while(current < end)
	{
		uint64_t count = MyMin((uint64_t)BUFSIZ, (uint64_t)(end - current));
		uint64_t written = fwrite((const void*)current, 1, count, m_file);
		if(ferror(m_file))
			return false;
		fflush(m_file);
		current += written;
	}

	return true;
}

bool Serializer::SerializeFile(std::string path)
{
	if(m_file == nullptr)
		return false;

	FILE* src = fopen(path.c_str(), "rb");
	if(src == nullptr)
	{
		LOG_INFO("Failed to open file: %s", path.c_str());
		return false;
	}

	fseek(src, 0, SEEK_END);
	uint64_t size = ftell(src);
	fseek(src, 0, SEEK_SET);

	if(!Serialize(size))
	{
		LOG_INFO("Failed to serialize file size for file: %s", path.c_str());
		fclose(src);
		return false;
	}

	char buffer[BUFSIZ];
	while(!feof(src))
	{
		uint64_t read = fread(buffer, 1, BUFSIZ, src);
		uint64_t written = fwrite(buffer, 1, read, m_file);
		if(written != read)
		{
			LOG_INFO("Failed to write buffer");
			fclose(src);
			return false;
		}
	}

	fclose(src);
	fflush(m_file);
	return true;
}
