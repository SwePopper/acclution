#include <stdio.h>

#include "processor.h"

void PrintUsage()
{
	printf("Acclution Content Processor, version 1.0.0\n");
	printf("Usage: content_processor [<options>] project_file.apf\n\n");
	printf("Options:\n");
	printf("\t-h, --help: Display this text\n");
	printf("\t-c, --clean: Clean output directories\n");
}

int main(int argc, char** argv)
{
	Processor processor;

	if(argc <= 1)
	{
		PrintUsage();
		return EXIT_SUCCESS;
	}
	else
	{
		for(int i = 1; i < argc; ++i)
		{
			std::string argument = argv[i];

			if("-h" == argument || "--help" == argument)
			{
				PrintUsage();
				return EXIT_SUCCESS;
			}
			else if("-c" == argument || "--clean" == argument)
			{
				processor.CleanOutput();
			}
			else
			{
				processor.SetProjectFilePath(argv[i]);
			}
		}
	}

	if(!processor.Start())
	{
		PrintUsage();
		return EXIT_FAILURE;
	}
	return EXIT_SUCCESS;
}
