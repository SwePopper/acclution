#ifndef SERIALIZE_H
#define SERIALIZE_H

#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <memory>
#include <string>
#include <vector>

class Processor;

class Serializer
{
public:
	Serializer() : m_file(nullptr) {}
	~Serializer();

	bool Initialize(std::string file_path, Processor* processor);

	template<typename T>
	bool Serialize(const T& value)
	{
		static_assert(std::is_trivial<T>::value, "Must be a trivial object");

		uint8_t* buffer = (uint8_t*)alloca(sizeof(T));

		uint8_t* ptr = (uint8_t*)&value;
		for(uint64_t i = 0; i < sizeof(T); ++i)
		{
			buffer[i] = ptr[i];
		}

		return Serialize(buffer, sizeof(T));
	}

	bool Serialize(const std::string& value)
	{
		// Count
		uint64_t size = value.size();
		if(!Serialize(size))
			return false;
		// Data
		return Serialize(value.c_str(), size);
	}

	bool Serialize(const char* value)
	{
		// Count
		uint64_t size = strlen(value);
		if(!Serialize(size))
			return false;
		// Data
		return Serialize(value, size);
	}

	template<typename T>
	bool Serialize(const std::vector<T>& value)
	{
		static_assert(std::is_trivial<T>::value, "Must be a trivial object");

		// Count
		uint64_t count = value.size();
		if(!Serialize(count))
			return false;

		for(uint64_t i = 0; i < count; ++i)
		{
			if(!Serialize(value[i]))
				return false;
		}
		return true;
	}

	bool Serialize(const void* buffer, const uint64_t& size);
	bool SerializeFile(std::string path);

private:
	FILE*		m_file;

	uint64_t	m_file_size_position;
};

#endif
