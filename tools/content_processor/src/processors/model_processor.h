#ifndef MODEL_PROCESSOR_H
#define MODEL_PROCESSOR_H

#include "base_processor.h"

class Serializer;
struct aiMesh;
struct aiMaterial;
struct aiNode;

class ModelProcessor : public BaseProcessor
{
public:
	ModelProcessor(Processor* processor);
	virtual ~ModelProcessor() {}

	virtual void AddParameter(const std::string& param);

protected:
	virtual bool Process();

	bool Serialize(Serializer* serializer, const aiMaterial* material);
	bool Serialize(Serializer* serializer, const aiMesh* mesh);
	bool Serialize(Serializer* serializer, const aiNode* node);

	std::string		m_default_effect;
	float			m_x_rotation;
	float			m_y_rotation;
	float			m_z_rotation;
	float			m_scale;
	uint32_t		m_flags;
};

#endif // MODEL_PROCESSOR_H
