#ifndef FONT_PROCESSOR_H
#define FONT_PROCESSOR_H

#include "base_processor.h"

#include <vector>

class FontProcessor : public BaseProcessor
{
public:
	FontProcessor(Processor* processor) : BaseProcessor(processor) {}
	virtual ~FontProcessor() {}

	virtual void AddParameter(const std::string& param) {}

protected:
	virtual bool Process();
};

#endif
