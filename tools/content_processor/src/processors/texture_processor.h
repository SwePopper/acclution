#ifndef TEXTURE_PROCESSOR_H
#define TEXTURE_PROCESSOR_H

#include "base_processor.h"

class TextureProcessor : public BaseProcessor
{
public:
	TextureProcessor(Processor* processor);
	virtual ~TextureProcessor() {}

	virtual void AddParameter(const std::string& param);

protected:
	virtual bool Process();

	uint8_t		m_color_key[4];
	bool		m_color_key_enabled;
	bool		m_generate_mipmaps;
	bool		m_pre_multiply_alpha;
	bool		m_resize_to_power_of_two;
	bool		m_make_square;
	uint32_t	m_texture_format;
};

#endif
