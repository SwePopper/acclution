#include "texture_processor.h"
#include "processor.h"
#include "childprocess.h"
#include "serializer.h"

#include <vector>
#include <algorithm>
#include <FreeImagePlus.h>

namespace
{
	std::vector<const char*> kSurfaceFormats = {
		"Color"
	};

	constexpr bool IsPOW(const uint32_t& value)
	{
		return value && !(value & (value - 1));
	}

	uint32_t NextPOW(uint32_t value)
	{
		if(value > 1)
		{
			float f = (float)value;
			unsigned int const t = 1U << ((*(unsigned int *)&f >> 23) - 0x7f);
			return t << (t < value);
		}

		return 1;
	}

	template<typename T>
	const T& MyMax(const T& a, const T& b) { return a < b ? b : a; }
}

TextureProcessor::TextureProcessor(Processor* processor)
	: BaseProcessor(processor),
	  m_color_key(),
	  m_color_key_enabled(false),
	  m_generate_mipmaps(true),
	  m_pre_multiply_alpha(false),
	  m_resize_to_power_of_two(true),
	  m_make_square(false),
	  m_texture_format(0)
{}

void TextureProcessor::AddParameter(const std::string &param)
{
	if(param.empty())
		return;

	size_t split = param.find_first_of('=');
	if(split == std::string::npos)
	{
		LOG_INFO("Invalid param: %s\n", param.c_str());
		return;
	}

	std::string key = param.substr(0, split);
	std::string value = param.substr(split+1);

	// Hashes are calculated compiletime except for what is in the key variable
	switch(Hash32(key.c_str()))
	{
	case Hash32("ColorKeyEnabled"): m_color_key_enabled = value == "True"; break;
	case Hash32("GenerateMipmaps"): m_generate_mipmaps = value == "True"; break;
	case Hash32("PremultiplyAlpha"): m_pre_multiply_alpha = value == "True"; break;
	case Hash32("ResizeToPowerOfTwo"): m_resize_to_power_of_two = value == "True"; break;
	case Hash32("MakeSquare"): m_make_square = value == "True"; break;

	case Hash32("ColorKeyColor"): {
			for(uint32_t i = 0; i < 4 && !value.empty(); ++i)
			{
				std::string next_color = value;

				size_t pos = next_color.find(',');
				if(pos != std::string::npos)
				{
					next_color = next_color.substr(0, pos);
					value = value.substr(pos+1);
				}
				else
				{
					value = "";
				}

				m_color_key[i] = (uint8_t)strtol(next_color.c_str(), nullptr, 10);
			}
		} break;

	case Hash32("TextureFormat"): {
			std::vector<const char*>::iterator it =
					std::find(kSurfaceFormats.begin(), kSurfaceFormats.end(), value);
			if(it == kSurfaceFormats.end())
			{
				LOG_INFO("Failed to get TextureFormat index\n");
				m_texture_format = 0;
				return;
			}
			m_texture_format = (uint32_t)std::distance(kSurfaceFormats.begin(), it);
		} break;

	default: LOG_INFO("Unsupported texture param: %s\n", key.c_str()); break;
	}
}

bool TextureProcessor::Process()
{
	Serializer serializer;
	if(!serializer.Initialize(m_processor->GetOutputDirectory() + m_file_path, m_processor))
		return false;

	if(!serializer.Serialize(STRINGIFY(TextureProcessor)))
		return false;

	std::string source_path = m_processor->GetProjectDirectory() + m_file_path;

	fipImage image;
	if(!image.load(source_path.c_str()))
	{
		LOG_INFO("Failed to load image file: %s\n", source_path.c_str());
		return false;
	}

	uint32_t width = image.getWidth();
	uint32_t height = image.getHeight();

	if(m_resize_to_power_of_two)
	{
		if(!IsPOW(width))
			width = NextPOW(width);
		if(!IsPOW(height))
			height = NextPOW(height);
	}

	if(m_make_square && width != height)
	{
		width = height = MyMax(width, height);
	}

	if(image.getWidth() != width || image.getHeight() != height)
	{
		// TODO: explore the different filters
		if(!image.rescale(width, height, FILTER_LANCZOS3))
		{
			LOG_INFO("Failed to resize image\n");
			return false;
		}
	}

	if(!image.convertTo32Bits())
	{
		LOG_INFO("Failed to convert image\n");
		return false;
	}

	if(!image.flipVertical())
	{
		LOG_INFO("Failed to flip image\n");
		return false;
	}

	if(!serializer.Serialize(width))
	{
		LOG_INFO("Failed to serialize texture width\n");
		return false;
	}

	if(!serializer.Serialize(height))
	{
		LOG_INFO("Failed to serialize texture height\n");
		return false;
	}

	if(!serializer.Serialize(m_generate_mipmaps))
	{
		LOG_INFO("Failed to serialize mipmap setting\n");
		return false;
	}

	uint32_t data_size = width * height * image.getBitsPerPixel();
	if(!serializer.Serialize(data_size))
	{
		LOG_INFO("Failed to serialize texture level size\n");
		return false;
	}

	for(uint32_t y = 0; y < height; ++y)
	{
		for(uint32_t x = 0; x < width; ++x)
		{
			RGBQUAD pixel;
			if(!image.getPixelColor(x, y, &pixel))
			{
				LOG_INFO("Failed to get pixel\n");
				return false;
			}

#if FREEIMAGE_COLORORDER != FREEIMAGE_COLORORDER_BGR
			// Make bgr if it is not
			std::swap(pixel.rgbBlue, pixel.rgbRed);
#endif

			if(!serializer.Serialize(&pixel, image.getBitsPerPixel() / 8))
			{
				LOG_INFO("Failed to serialize texture level data\n");
				return false;
			}
		}
	}

	/*
	bool		m_pre_multiply_alpha;
	bool		m_color_key_enabled
	*/

	return true;
}
