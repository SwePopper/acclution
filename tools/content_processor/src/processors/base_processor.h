#ifndef BASE_PROCESSOR_H
#define BASE_PROCESSOR_H

#include <string>
#include <future>

class Processor;

class BaseProcessor
{
public:
	BaseProcessor(Processor* processor) : m_processor(processor), alive(true) {}
	virtual ~BaseProcessor() { alive = false; }

	virtual void AddParameter(const std::string& param)=0;

	std::future<bool> Build(const std::string& file)
	{
		m_file_path = file;
		return std::async(std::launch::async, [&] { return Process(); });
	}

protected:
	virtual bool Process()=0;

	Processor*		m_processor;
	std::string		m_file_path;
	bool alive;
};

#endif
