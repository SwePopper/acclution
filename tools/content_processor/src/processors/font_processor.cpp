#include "font_processor.h"
#include "processor.h"
#include "childprocess.h"
#include "serializer.h"

bool FontProcessor::Process()
{
	Serializer serializer;
	if(!serializer.Initialize(m_processor->GetOutputDirectory() + m_file_path, m_processor))
		return false;

	if(!serializer.Serialize(STRINGIFY(FontProcessor)))
		return false;

	if(!serializer.SerializeFile(m_processor->GetProjectDirectory() + m_file_path))
		return false;

	return true;
}
