#include "spirv_shader_processor.h"
#include "processor.h"
#include "childprocess.h"
#include "serializer.h"

#include <fstream>
#include <sstream>
#include <stdlib.h>

enum class ShaderType : uint8_t {
	kUnknown,
	kVertex,
	kTessellationControl,
	kTesselationEvaluation,
	kGeometry,
	kFragment,
	kCompute
};

bool SpirVShaderProcessor::Process()
{
	if(!ProcessGlsl())
		return false;

	if(!ProcessVulkan())
		return false;

	Serializer serializer;
	if(!serializer.Initialize(m_processor->GetOutputDirectory() + m_file_path, m_processor))
		return false;

	if(!serializer.Serialize(STRINGIFY(SpirVShaderProcessor)))
		return false;

	std::string shader_name = m_file_path;
	size_t pos = shader_name.rfind('/');
	if(pos != std::string::npos)
		shader_name = shader_name.substr(pos+1);

	pos = shader_name.find('.');

	std::string shader_type_string = "";
	if(pos != std::string::npos)
	{
		shader_type_string = shader_name.substr(pos+1);
		shader_name = shader_name.substr(0, pos);
	}

	if(!serializer.Serialize(shader_name))
		return false;

	ShaderType shader_type = ShaderType::kUnknown;
	if(shader_type_string == "vert") shader_type = ShaderType::kVertex;
	else if(shader_type_string == "frag") shader_type = ShaderType::kFragment;
	else if(shader_type_string == "geom") shader_type = ShaderType::kGeometry;
	else if(shader_type_string == "tese") shader_type = ShaderType::kTesselationEvaluation;
	else if(shader_type_string == "tesc") shader_type = ShaderType::kTessellationControl;
	else if(shader_type_string == "comp") shader_type = ShaderType::kCompute;

	if(!serializer.Serialize((uint8_t)shader_type))
		return false;

	if(!serializer.Serialize(m_glsl_source))
		return false;

	if(!serializer.SerializeFile(m_processor->GetIntermediateDirectory() + m_file_path))
		return false;

	return true;
}

bool SpirVShaderProcessor::ProcessGlsl()
{
	std::vector<std::string> args {
		"-E",
		m_processor->GetProjectDirectory() + m_file_path.c_str()
	};

	std::string glslangValidator = "glslangValidator";

#if WIN32 || _WIN32
	char* path = getenv("VK_SDK_PATH");
	if(path != nullptr)
	{
		glslangValidator = path;
		glslangValidator += "\\Bin\\glslangValidator.exe";
	}
#endif

	ChildProcess* process = ChildProcess::Start(glslangValidator, args);
	if(process == nullptr)
	{
		LOG_INFO("Failed to start glslangValidator process!");
		return false;
	}

	process->WaitForCompletion();

	std::ifstream output_file(process->GetOutputFilename().c_str());
	if(!output_file.is_open())
	{
		LOG_INFO("Failed to open glslangValidator output");
		delete process;
		return false;
	}

	std::stringstream buffer;
	buffer << output_file.rdbuf();

	m_glsl_source = buffer.str();

	if(m_glsl_source.empty())
	{
		LOG_INFO("Failed to get output from glslangValidator");
		delete process;
		return false;
	}

	delete process;
	return true;
}

bool SpirVShaderProcessor::ProcessVulkan()
{
	Processor::MakeDirectoryTree(m_processor->GetIntermediateDirectory() + m_file_path);

	std::vector<std::string> args {
		"-o",
		m_processor->GetIntermediateDirectory() + m_file_path.c_str(),
		"-V",
		m_processor->GetProjectDirectory() + m_file_path.c_str()
	};

	ChildProcess* process = ChildProcess::Start("glslangValidator", args);
	if(process == nullptr)
	{
		LOG_INFO("Failed to start glslangValidator process!");
		return false;
	}

	process->WaitForCompletion();
	delete process;

	return true;
}
