#include "model_processor.h"
#include "processor.h"
#include "childprocess.h"
#include "serializer.h"

#ifndef AI_MAX_NUMBER_OF_COLOR_SETS
#define AI_MAX_NUMBER_OF_COLOR_SETS 0x4
#endif
#ifndef AI_MAX_NUMBER_OF_TEXTURECOORDS
#define AI_MAX_NUMBER_OF_TEXTURECOORDS 0x4
#endif

#include <assimp/Importer.hpp>
#include <assimp/scene.h>
#include <assimp/postprocess.h>
#include <assimp/config.h>

namespace
{
	enum class VertexType : uint32_t
	{
		kPosition =		0x1,
		kNormal =		0x2,
		kTangents =		0x4,
		kTexture1 =		0x10,
		kTexture2 =		0x20,
		kTexture3 =		0x40,
		kTexture4 =		0x80,
		kColor1 =		0x100,
		kColor2 =		0x200,
		kColor3 =		0x400,
		kColor4 =		0x800,
	};

	bool Serialize(Serializer* serializer, const aiVector2D& value)
	{
		for(uint32_t i = 0; i < 2; ++i)
		{
			if(!serializer->Serialize(value[i]))
				return false;
		}
		return true;
	}

	bool Serialize(Serializer* serializer, const aiVector3D& value)
	{
		for(uint32_t i = 0; i < 3; ++i)
		{
			if(!serializer->Serialize(value[i]))
				return false;
		}
		return true;
	}

	bool Serialize(Serializer* serializer, const aiColor4D& value)
	{
		for(uint32_t i = 0; i < 4; ++i)
		{
			if(!serializer->Serialize(value[i]))
				return false;
		}
		return true;
	}

	bool Serialize(Serializer* serializer, const aiColor3D& value)
	{
		for(uint32_t i = 0; i < 3; ++i)
		{
			if(!serializer->Serialize(value[i]))
				return false;
		}
		return true;
	}

	bool Serialize(Serializer* serializer, const aiMatrix4x4& value)
	{
		for(uint32_t i = 0; i < 16; ++i)
		{
			if(!serializer->Serialize(value[i]))
				return false;
		}
		return true;
	}

	template<typename T>
	bool SerializeIndices(Serializer* serializer, const aiMesh* mesh)
	{
		for(uint32_t i = 0; i < mesh->mNumFaces; ++i)
		{
			for(uint32_t j = 0; j < mesh->mFaces[i].mNumIndices; ++j)
			{
				if(!serializer->Serialize(static_cast<T>(mesh->mFaces[i].mIndices[j])))
					return false;
			}
		}
		return true;
	}
}

ModelProcessor::ModelProcessor(Processor* processor)
	: BaseProcessor(processor),
	  m_default_effect(""),
	  m_x_rotation(0.0f),
	  m_y_rotation(0.0f),
	  m_z_rotation(0.0f),
	  m_scale(1.0f),
	  m_flags(aiProcessPreset_TargetRealtime_MaxQuality)
{}

void ModelProcessor::AddParameter(const std::string &param)
{
	if(param.empty())
		return;

	size_t split = param.find_first_of('=');
	if(split == std::string::npos)
	{
		LOG_INFO("Invalid param: %s\n", param.c_str());
		return;
	}

	std::string key = param.substr(0, split);
	std::string value = param.substr(split+1);

	// Hashes are calculated compiletime except for what is in the key variable
	switch(Hash32(key.c_str()))
	{
	case Hash32("DefaultEffect"): m_default_effect = value; break;
	case Hash32("RotationX"): m_x_rotation = strtof(value.c_str(), nullptr); break;
	case Hash32("RotationY"): m_y_rotation = strtof(value.c_str(), nullptr); break;
	case Hash32("RotationZ"): m_z_rotation = strtof(value.c_str(), nullptr); break;
	case Hash32("Scale"): m_scale = strtof(value.c_str(), nullptr); break;
	default: LOG_INFO("Unsupported model param: %s\n", key.c_str()); break;
	}
}

bool ModelProcessor::Process()
{
	Assimp::Importer importer;
	importer.SetPropertyInteger(AI_CONFIG_PP_SLM_VERTEX_LIMIT,   AI_SLM_DEFAULT_MAX_VERTICES); // TODO: set to reasonable values
	importer.SetPropertyInteger(AI_CONFIG_PP_SLM_TRIANGLE_LIMIT, AI_SLM_DEFAULT_MAX_TRIANGLES);
	importer.SetPropertyInteger(AI_CONFIG_PP_LBW_MAX_WEIGHTS, 4);

	std::string path = m_processor->GetProjectDirectory() + m_file_path;
	const aiScene* scene = importer.ReadFile(path.c_str(), m_flags);
	if(scene == nullptr)
	{
		LOG_INFO("Failed to load model: %s", path.c_str());
		return false;
	}

	Serializer serializer;
	if(!serializer.Initialize(m_processor->GetOutputDirectory() + m_file_path, m_processor))
		return false;

	if(!serializer.Serialize(STRINGIFY(ModelProcessor)))
		return false;

	if(!serializer.Serialize(static_cast<uint32_t>(scene->mNumMeshes)))
	{
		LOG_INFO("Failed to serialize num meshes!");
		return false;
	}

	if(!serializer.Serialize(static_cast<uint32_t>(scene->mNumMaterials)))
	{
		LOG_INFO("Failed to serialize num materials!");
		return false;
	}

	for(uint32_t i = 0; i < scene->mNumMeshes; ++i)
	{
		if(!Serialize(&serializer, scene->mMeshes[i]))
		{
			LOG_INFO("Failed to serialize mesh: %u", i);
			return false;
		}
	}

	for(uint32_t i = 0; i < scene->mNumMaterials; ++i)
	{
		if(!Serialize(&serializer, scene->mMaterials[i]))
		{
			LOG_INFO("Failed to serialize mesh: %u", i);
			return false;
		}
	}

	return Serialize(&serializer, scene->mRootNode);
}

bool ModelProcessor::Serialize(Serializer* serializer, const aiMaterial* material)
{
	aiString svalue;
	material->Get(AI_MATKEY_NAME, svalue);
	if(!serializer->Serialize(svalue.C_Str()))
	{
		LOG_INFO("Failed to serialize material name");
		return false;
	}

	int ivalue = 0;
	material->Get(AI_MATKEY_TWOSIDED, ivalue);
	if(!serializer->Serialize(ivalue != 0))
	{
		LOG_INFO("Failed to serialize material two sized");
		return false;
	}

	material->Get(AI_MATKEY_ENABLE_WIREFRAME, ivalue);
	if(!serializer->Serialize(ivalue != 0))
	{
		LOG_INFO("Failed to serialize material wire frame");
		return false;
	}

	material->Get(AI_MATKEY_SHADING_MODEL, ivalue);
	if(!serializer->Serialize(static_cast<uint8_t>(ivalue)))
	{
		LOG_INFO("Failed to serialize material shading mode");
		return false;
	}

	material->Get(AI_MATKEY_BLEND_FUNC, ivalue);
	if(!serializer->Serialize(static_cast<uint8_t>(ivalue)))
	{
		LOG_INFO("Failed to serialize material blend func");
		return false;
	}

	float fvalue;
	material->Get(AI_MATKEY_OPACITY, fvalue);
	if(!serializer->Serialize(fvalue))
	{
		LOG_INFO("Failed to serialize material opacity");
		return false;
	}

	material->Get(AI_MATKEY_BUMPSCALING, fvalue);
	if(!serializer->Serialize(fvalue))
	{
		LOG_INFO("Failed to serialize material bump scaling");
		return false;
	}

	material->Get(AI_MATKEY_SHININESS, fvalue);
	if(!serializer->Serialize(fvalue))
	{
		LOG_INFO("Failed to serialize material shininess");
		return false;
	}

	material->Get(AI_MATKEY_SHININESS_STRENGTH, fvalue);
	if(!serializer->Serialize(fvalue))
	{
		LOG_INFO("Failed to serialize material shininess strength");
		return false;
	}

	material->Get(AI_MATKEY_REFLECTIVITY, fvalue);
	if(!serializer->Serialize(fvalue))
	{
		LOG_INFO("Failed to serialize material reflectivity");
		return false;
	}

	material->Get(AI_MATKEY_REFRACTI, fvalue);
	if(!serializer->Serialize(fvalue))
	{
		LOG_INFO("Failed to serialize material refracti");
		return false;
	}

	aiColor3D cvalue;
	material->Get(AI_MATKEY_COLOR_DIFFUSE, cvalue);
	if(!::Serialize(serializer, cvalue))
	{
		LOG_INFO("Failed to serialize material diffuse");
		return false;
	}

	material->Get(AI_MATKEY_COLOR_AMBIENT, cvalue);
	if(!::Serialize(serializer, cvalue))
	{
		LOG_INFO("Failed to serialize material ambient");
		return false;
	}

	material->Get(AI_MATKEY_COLOR_SPECULAR, cvalue);
	if(!::Serialize(serializer, cvalue))
	{
		LOG_INFO("Failed to serialize material specular");
		return false;
	}

	material->Get(AI_MATKEY_COLOR_EMISSIVE, cvalue);
	if(!::Serialize(serializer, cvalue))
	{
		LOG_INFO("Failed to serialize material emissive");
		return false;
	}

	material->Get(AI_MATKEY_COLOR_TRANSPARENT, cvalue);
	if(!::Serialize(serializer, cvalue))
	{
		LOG_INFO("Failed to serialize material transparent");
		return false;
	}

	material->Get(AI_MATKEY_COLOR_REFLECTIVE, cvalue);
	if(!::Serialize(serializer, cvalue))
	{
		LOG_INFO("Failed to serialize material reflective");
		return false;
	}

	for(uint32_t i = aiTextureType_DIFFUSE; i < aiTextureType_UNKNOWN; ++i)
	{
		uint32_t texture_count = material->GetTextureCount(static_cast<aiTextureType>(i));
		if(!serializer->Serialize(texture_count))
		{
			LOG_INFO("Failed to serialize material texture count");
			return false;
		}

		for(uint32_t j = 0; j < texture_count; ++j)
		{
			aiTextureMapping mapping = aiTextureMapping_UV;
			uint32_t uvindex = 0;
			aiTextureOp op = aiTextureOp_Multiply;

			aiTextureMapMode mapmode[] = {
				aiTextureMapMode_Wrap,
				aiTextureMapMode_Wrap
			};

			if(AI_SUCCESS != material->GetTexture(static_cast<aiTextureType>(i), j, &svalue, &mapping, &uvindex, &fvalue, &op, mapmode))
			{
				LOG_INFO("Failed to get texture info");
				return false;
			}

			if(!serializer->Serialize(svalue.C_Str()))
			{
				LOG_INFO("Failed to serialize material texture name");
				return false;
			}

			if(!serializer->Serialize(static_cast<uint32_t>(mapping)))
			{
				LOG_INFO("Failed to serialize material texture mapping");
				return false;
			}

			if(!serializer->Serialize(uvindex))
			{
				LOG_INFO("Failed to serialize material texture uvindex");
				return false;
			}

			if(!serializer->Serialize(fvalue))
			{
				LOG_INFO("Failed to serialize material texture blend");
				return false;
			}

			if(!serializer->Serialize(static_cast<uint32_t>(op)))
			{
				LOG_INFO("Failed to serialize material texture op");
				return false;
			}

			if(!serializer->Serialize(static_cast<uint32_t>(mapmode[0])))
			{
				LOG_INFO("Failed to serialize material texture mapmode");
				return false;
			}

			if(!serializer->Serialize(static_cast<uint32_t>(mapmode[1])))
			{
				LOG_INFO("Failed to serialize material texture mapmode");
				return false;
			}
		}
	}

	return true;
}

bool ModelProcessor::Serialize(Serializer* serializer, const aiMesh* mesh)
{
	uint32_t vertex_type = 0;
	vertex_type |= mesh->HasPositions() ? static_cast<uint32_t>(VertexType::kPosition) : 0;
	vertex_type |= mesh->HasNormals() ? static_cast<uint32_t>(VertexType::kNormal) : 0;
	vertex_type |= mesh->HasTangentsAndBitangents() ? static_cast<uint32_t>(VertexType::kTangents) : 0;

	for(uint32_t i = 0; i < AI_MAX_NUMBER_OF_TEXTURECOORDS; ++i)
		vertex_type |= mesh->HasTextureCoords(i) ? (static_cast<uint32_t>(VertexType::kTexture1)) << i : 0;

	for(uint32_t i = 0; i < AI_MAX_NUMBER_OF_COLOR_SETS; ++i)
		vertex_type |= mesh->HasVertexColors(i) ? (static_cast<uint32_t>(VertexType::kColor1)) << i : 0;

	if(!serializer->Serialize(mesh->mName.C_Str()))
	{
		LOG_INFO("Failed to serialize mesh name");
		return false;
	}

	if(!serializer->Serialize(mesh->mMaterialIndex))
	{
		LOG_INFO("Failed to serialize mesh material index");
		return false;
	}

	if(!serializer->Serialize(mesh->mPrimitiveTypes))
	{
		LOG_INFO("Failed to serialize mesh material index");
		return false;
	}

	if(!serializer->Serialize(vertex_type))
	{
		LOG_INFO("Failed to serialize vertex type");
		return false;
	}

	if(!serializer->Serialize(mesh->mNumVertices))
	{
		LOG_INFO("Failed to serialize vertex count");
		return false;
	}

	for(uint32_t i = 0; i < mesh->mNumVertices; ++i)
	{
		if(mesh->HasPositions() && !::Serialize(serializer, mesh->mVertices[i]))
		{
			LOG_INFO("Failed to serialize vertex position");
			return false;
		}

		if(mesh->HasNormals() && !::Serialize(serializer, mesh->mNormals[i]))
		{
			LOG_INFO("Failed to serialize vertex normals");
			return false;
		}

		if(mesh->HasTangentsAndBitangents())
		{
			if(!::Serialize(serializer, mesh->mTangents[i]) || !::Serialize(serializer, mesh->mBitangents[i]))
			{
				LOG_INFO("Failed to serialize vertex tangents");
				return false;
			}
		}

		for(uint32_t j = 0; j < AI_MAX_NUMBER_OF_TEXTURECOORDS; ++j)
		{
			if(mesh->HasTextureCoords(j))
			{
				aiVector2D coord(mesh->mTextureCoords[j][i].x, mesh->mTextureCoords[j][i].y);
				if(!::Serialize(serializer, coord))
				{
					LOG_INFO("Failed to serialize vertex texture coords");
					return false;
				}
			}
		}

		for(uint32_t j = 0; j < AI_MAX_NUMBER_OF_COLOR_SETS; ++j)
		{
			if(mesh->HasVertexColors(j))
			{
				aiColor4D color(mesh->mColors[j][i].r, mesh->mColors[j][i].g, mesh->mColors[j][i].b, mesh->mColors[j][i].a);
				if(!::Serialize(serializer, color))
				{
					LOG_INFO("Failed to serialize vertex texture coords");
					return false;
				}
			}
		}
	}

	if(mesh->HasFaces())
	{
		uint32_t index_count = mesh->mFaces[0].mNumIndices * mesh->mNumFaces;
		if(!serializer->Serialize(index_count))
		{
			LOG_INFO("Failed to serialize index count");
			return false;
		}

		if(mesh->mNumVertices <= UCHAR_MAX)
		{
			if(!SerializeIndices<uint8_t>(serializer, mesh))
			{
				LOG_INFO("Failed to serialize index data");
				return false;
			}
		}
		else if(mesh->mNumVertices <= USHRT_MAX)
		{
			if(!SerializeIndices<uint16_t>(serializer, mesh))
			{
				LOG_INFO("Failed to serialize index data");
				return false;
			}
		}
		else
		{
			if(!SerializeIndices<uint32_t>(serializer, mesh))
			{
				LOG_INFO("Failed to serialize index data");
				return false;
			}
		}
	}
	else
	{
		if(!serializer->Serialize(static_cast<uint32_t>(0)))
		{
			LOG_INFO("Failed to serialize index count");
			return false;
		}
	}

	return true;
}

bool ModelProcessor::Serialize(Serializer* serializer, const aiNode* node)
{
	if(!serializer->Serialize(node->mName.C_Str()))
	{
		LOG_INFO("Failed to serialize node name");
		return false;
	}

	if(!::Serialize(serializer, node->mTransformation))
	{
		LOG_INFO("Failed to serialize node transform");
		return false;
	}

	if(!serializer->Serialize(node->mNumMeshes))
	{
		LOG_INFO("Failed to serialize node num mesh");
		return false;
	}

	if(node->mNumMeshes > 0)
	{
		for(uint32_t i = 0; i < node->mNumMeshes; ++i)
		{
			if(!serializer->Serialize(node->mMeshes[i]))
			{
				LOG_INFO("Failed to serialize node meshs");
				return false;
			}
		}
	}

	if(!serializer->Serialize(node->mNumChildren))
	{
		LOG_INFO("Failed to serialize node num mesh");
		return false;
	}

	for(uint32_t i = 0; i < node->mNumChildren; ++i)
	{
		if(!Serialize(serializer, node->mChildren[i]))
		{
			LOG_INFO("Failed to serialize node child");
			return false;
		}
	}

	return true;
}
