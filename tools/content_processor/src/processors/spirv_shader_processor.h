#ifndef SPIRV_SHADER_PROCESSOR_H
#define SPIRV_SHADER_PROCESSOR_H

#include "base_processor.h"

class SpirVShaderProcessor : public BaseProcessor
{
public:
	SpirVShaderProcessor(Processor* processor) : BaseProcessor(processor) {}
	virtual ~SpirVShaderProcessor() {}

	virtual void AddParameter(const std::string& param) {}

protected:
	virtual bool Process();

private:
	bool ProcessGlsl();
	bool ProcessVulkan();

	std::string		m_glsl_source;
};

#endif
