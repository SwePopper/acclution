#!/usr/bin/python

# updates the copyright information for all .cs files
# usage: call recursive_traversal, with the following parameters
# parent directory, old copyright text content, new copyright text content

import os

excludedir = ["../../.git", "../../Assets", "../../build", "../../DebugServer", "../../external"]
filetypes = [".cpp", ".h", ".hpp"]

def ReadFile(path):
    if not os.path.exists(path):
        return None
    f = open(path, "r+")
    text = f.read()
    f.close()
    return text

def WriteFile(path, text):
    f = open(path, "w")
    f.write(text)
    f.close()

def update_source(filename, oldcopyright, copyright):
    utfstr = chr(0xef)+chr(0xbb)+chr(0xbf)
    fdata = ReadFile(filename)
    isUTF = False
    if (fdata.startswith(utfstr)):
        isUTF = True
        fdata = fdata[3:]
    if (oldcopyright != None):
        if (fdata.startswith(oldcopyright)):
            fdata = fdata[len(oldcopyright):]
    if not (fdata.startswith(copyright)):
        print("updating " + filename)
        fdata = copyright + fdata
        if (isUTF):
            WriteFile(filename, utfstr+fdata)
        else:
            WriteFile(filename, fdata)

def recursive_traversal(dir,  oldcopyright, copyright):
    global excludedir
    fns = os.listdir(dir)
    print("listing " + dir)
    for fn in fns:
        fullfn = os.path.join(dir,fn)
        if (fullfn in excludedir):
            continue
        if (os.path.isdir(fullfn)):
            recursive_traversal(fullfn, oldcopyright, copyright)
        else:
            for filetype in filetypes:
                if (fullfn.endswith(filetype)):
                    update_source(fullfn, oldcopyright, copyright)
                    break

def main():
    oldcright = ReadFile("oldcr.txt")
    cright = ReadFile("copyright_text.txt")
    recursive_traversal("../../", oldcright, cright)

if __name__ == "__main__":
    main()