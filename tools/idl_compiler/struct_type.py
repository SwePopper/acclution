import collections

class StructType:
	def __init__(self, name):
		self.versions = {}
		self.current_members = {}
		self.current_version = 0
		self.name = name

	def AddMember(self, type, name):
		name = name.strip(';')
		self.current_members[name] = type
		self.current_members = collections.OrderedDict(sorted(self.current_members.items()))

	def SetVersion(self, version):
		self.current_version = version
		self.current_members = {}

	def EndVersion(self):
		self.versions[self.current_version] = self.current_members
		self.current_members = {}
		self.current_version = 0

	def Compile(self, file, includes, namespace, names):
		version_nr = 0
		for key in self.versions:
			if version_nr < key:
				version_nr = key

		self.__CompileSerializeFunction(file, includes, namespace, names, version_nr)
		self.__CompileDeserializeFunction(file, includes, namespace, names, version_nr)

	def __CompileSerializeFunction(self, file, includes, namespace, names, highest_version):
		file.write("\n")
		file.write("\tbool Serialize(Acclution::Serializer& serializer, const " + self.name + "& value)\n\t{\n")
		file.write("\t\t// Serialize the most current version nr\n")
		file.write("\t\tif(!serializer.Serialize<uint32_t>(" + str(highest_version) + "))\n")
		file.write("\t\t\treturn false;\n")

		for member in self.versions[highest_version]:
			var_name = member
			if(member.endswith("()")):
				# This is a function add Get before name
				var_name = "Get" + var_name

			# if member is another custom class or an inherited class
			if self.versions[highest_version][member] == var_name:
				file.write("\t\tif(!Serialize(serializer, *(const " + self.versions[highest_version][member] + "*)&value))\n")
			elif self.versions[highest_version][member] in names:
				file.write("\t\tif(!Serialize(serializer, value." + var_name + "))\n")
			else:
				file.write("\t\tif(!serializer.Serialize(value." + var_name + "))\n")

			file.write("\t\t\treturn false;\n")

		file.write("\t\treturn true;\n")
		file.write("\t}\n")

	def __CompileDeserializeFunction(self, file, includes, namespace, names, highest_version):
		for key in self.versions:
			self.__CompileDeserializeVersion(file, includes, namespace, names, key, highest_version)

		file.write("\n")
		file.write("\ttypedef bool (*DeserializeFunc" + self.name + ")(Acclution::Serializer& serializer, " + self.name + "& value);\n")
		file.write("\tDeserializeFunc" + self.name + " deserialize" + self.name + "[] = {\n")
		for key in self.versions:
			file.write("\t\t&" + self.name + "Ver" + str(key) + "::Deserialize,\n")
		file.write("\t\tNULL\n\t};\n")
		file.write("\n")
		file.write("\tbool Deserialize(Acclution::Serializer& serializer, " + self.name + "& value)\n\t{\n")
		file.write("\t\t// Deserialize the version nr\n")
		file.write("\t\tuint32_t version_nr = 0;\n")
		file.write("\t\tif(!serializer.Deserialize(version_nr))\n")
		file.write("\t\t\treturn false;\n")
		file.write("\t\treturn deserialize" + self.name + "[version_nr-1](serializer, value);\n")
		file.write("\t}\n")

	def __CompileDeserializeVersion(self, file, includes, namespace, names, version, highest_version):
		file.write("\n\tnamespace " + self.name + "Ver" + str(version) + "\n\t{\n")
		file.write("\t\tbool Deserialize(Acclution::Serializer& serializer, " + self.name + "& value) {\n")
		for member in self.versions[version]:
			exist = True
			next_ver = version+1
			while next_ver <= highest_version:
				if next_ver in self.versions:
					if not member in self.versions[next_ver] or self.versions[version][member] != self.versions[next_ver][member]:
						exist = False
						break
				next_ver += 1

			if exist:
				if self.versions[version][member] == member:
					file.write("\t\t\tif(!" + namespace + "::Deserialize(serializer, *(" + self.versions[highest_version][member] + "*)&value))\n")
					file.write("\t\t\t\treturn false;\n")
				else:
					var_name = member
					if(member.endswith("()")):
						var_name = var_name[:-2]
					file.write("\t\t\t" + self.versions[version][member] + " " + var_name + ";\n");
					if self.versions[version][member] in names:
						file.write("\t\t\tif(!" + namespace + "::Deserialize(serializer, " + var_name + "))\n")
					else:
						file.write("\t\t\tif(!serializer.Deserialize(" + var_name + "))\n")
					file.write("\t\t\t\treturn false;\n")

					if(member.endswith("()")):
						file.write("\t\t\tvalue.Set" + member[:-1] + var_name + ");\n")
					else:
						file.write("\t\t\tvalue." + member + " = " + var_name + ";\n")
			else:
				file.write("\t\t\tserializer.SkipDeserialize<" + self.versions[version][member] + ">(1);\n")
		file.write("\t\t\treturn true;\n\t\t}\n\t}\n")
