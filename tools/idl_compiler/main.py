#!/usr/bin/python

import sys
import idl_compiler

def main(argv=sys.argv):
	# process arguments
	argv = argv[1:]

	input_files = []
	output_folder = ""

	parsing_input_files = 0
	for arg in argv:
		if(arg == "-i"):
			parsing_input_files = 1
		elif(arg == "-o"):
			parsing_input_files = 2
		else:
			if(parsing_input_files == 1):
				input_files.append(arg)
			elif(parsing_input_files == 2):
				output_folder = arg
			else:
				break

	if(len(input_files) == 0) or (output_folder == ""):
		print("Args is idl files for making marshalled c/c++ structs\n")
		print("Usage:\n\tmain.py -i file1.idl file2.idl -o output_folder")
		sys.exit(0)

	idl_compiler.Compile(input_files, output_folder)
	print("Done!")

if __name__ == "__main__":
	main()