#!/usr/bin/python

import os
import sys
from struct_type import StructType

def CompileFile(file_path, output_file):
	if not os.path.exists(file_path):
		print("File not found: " + file_path)
		return

	if file_path[file_path.rindex('.'):] != ".idl":
		print("Wrong file type: " + file_path)
		return

	tokens = []
	f = open(file_path, 'r')
	for line in f:
		line_tokens = line.split()
		for token in line_tokens:
			if token[0:1] == "//":
				break
			tokens.append(token)
	f.close()

	index = 0
	names = []
	structs = []
	includes = []
	current_struct = StructType("")
	while index < len(tokens):
		if(tokens[index] == "struct"):
			current_struct = StructType(tokens[index+1])
			names.append(tokens[index+1])
			index += 3
		elif(tokens[index] == "};"):
			structs.append(current_struct)
			current_struct = StructType("")
			index += 1
		elif(tokens[index] == "version"):
			current_struct.SetVersion(int(tokens[index+1]))
			index += 3
		elif(tokens[index] == "}"):
			current_struct.EndVersion()
			index += 1
		elif(tokens[index] == "#include"):
			include_file = tokens[index+1].strip('"')
			include_file = include_file[:include_file.rindex('.')]
			includes.append(include_file)
			index += 2
		else:
			current_struct.AddMember(tokens[index], tokens[index+1])
			index += 2

	name = output_file[output_file.rindex('/')+1:output_file.rindex('.')]
	namespace = "Serialize" + name;

	f = open(output_file, "w")
	f.write("#ifndef SERIALIZE_" + name.upper() + "\n")
	f.write("#define SERIALIZE_" + name.upper() + "\n\n")
	f.write("#include \"serializer.h\"\n")
	for include in includes:
		f.write("#include " + include.lower() + ".cpp \n")

	f.write("\n// Auto Generated file any change will be overwritten!!\n")
	f.write("\nnamespace " + namespace + "\n{\n")
	for struct in structs:
		struct.Compile(f, includes, namespace, names)
	f.write("\n}\n\n")
	f.write("#endif //SERIALIZE_" + name.upper() + "\n")
	f.close()

def Compile(input_file_paths, output_folder):
	if not os.path.exists(os.path.dirname(output_folder)):
		try:
			os.makedirs(os.path.dirname(output_folder))
		except:
			print("Failed to create output folder!")
			sys.exit(0)

	for file_path in input_file_paths:
		output_file = output_folder
		try:
			slashindex = 0
			try:
				slashindex = file_path.rindex('/')
			except ValueError:
				slashindex = 0

			extindex = 0
			try:
				extindex = file_path.rindex('.')
			except ValueError:
				extindex = len(file_path)

			output_file += file_path[slashindex:extindex] + ".cpp"
		except:
			print("Failed to compile file: " + file_path)
			continue
		CompileFile(file_path, output_file)