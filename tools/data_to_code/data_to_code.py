#!/usr/bin/python

import os
import sys

TEXTCHARS = ''.join(map(chr, [7,8,9,10,12,13,27] + range(0x20, 0x100)))
ALLBYTES = ''.join(map(chr, range(256)))

def IsBinarie(bytes):
	""" Determine if a string is classified as binary rather than text.
	Parameters
	----------
	bytes : str
	Returns
	-------
	is_binary : bool
	"""
	nontext = bytes.translate(ALLBYTES, TEXTCHARS)
	return bool(nontext)

def CreateSource(source_path, data_path, header_path, name):
	data_file = open(data_path, "r")
	data = data_file.read()
	is_binarie = IsBinarie(data)
	data_file.close()
	if is_binarie:
		data_file = open(data_path, "rb")
		data = data_file.read()
		data_file.close()

	f = open(source_path, "w")

	f.write("#include \"engine/platform.h\"\n")
	f.write("#include \"" + header_path + "\"\n\n")

	f.write("namespace " + name + "\n")
	f.write("{\n\n")

	f.write("const uint32_t DataSize = " + str(len(data)) + ";\n")
	f.write("const uint8_t Data[] =")

	if is_binarie:
		f.write(" {");
		for i in range(0, len(data)):
			if i % 10 == 0:
				f.write("\n\t");
			f.write("0x{0:02x}".format(ord(data[i])))
			if i != len(data)-1:
				f.write(", ")
		f.write("\n};\n\n")
	else:
		f.write("\n\t\"");
		for i in range(0, len(data)):
			if data[i] == '\n':
				f.write("\"\n\t\"");
			elif data[i] == '\"':
				f.write("\\\"")
			else:
				f.write(data[i])
		f.write("\";\n\n");
	f.write("}\n")

	f.close()

def CreateHeader(header_path, name):
	f = open(header_path, "w")
	f.write("#ifndef " + name.upper() + "_DATA_H\n")
	f.write("#define " + name.upper() + "_DATA_H\n")
	f.write("#pragma once\n\n")

	f.write("namespace " + name + "\n")
	f.write("{\n\n")

	f.write("extern const uint8_t Data[];\n")
	f.write("extern const uint32_t DataSize;\n\n")

	f.write("}\n\n")
	f.write("#endif //" + name.upper() + "_DATA_H\n")
	f.close()

def CreateFiles(input_file, output_name):
	if not os.path.exists(input_file):
		print("File not found: " + input_file)
		return

	header_file_path = output_name.lower() + ".h"
	source_file_path = output_name.lower() + ".cpp"

	CreateHeader(header_file_path, output_name)
	CreateSource(source_file_path, input_file, header_file_path, output_name)

def main(argv=sys.argv):
	argv = argv[1:]

	input_file = ""
	output_name = ""

	for i in range(0, len(argv)):
		if argv[i] == "-i":
			i = i+1
			input_file = argv[i]
		elif argv[i] == "-o":
			i = i+1
			output_name = argv[i]

	if input_file == "" or output_name == "":
		print("Args is the data file and a name for the data variable in c/c++\n")
		print("Usage:\n\tdata_to_code.py -i icon.png -o Icon")
		sys.exit(0)

	CreateFiles(input_file, output_name)
	print("Done!")

if __name__ == "__main__":
	main()
