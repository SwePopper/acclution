# This module tries to find libmagic library and include files
#
# SDL2_IMAGE_INCLUDE_DIRS, path where to find libwebsockets.h
# SDL2_IMAGE_LIBRARIES, the library to link against
# SDL2_IMAGE_FOUND, If false, do not try to use libWebSockets
#
# This currently works probably only for Linux

FIND_LIBRARY(SDL2_IMAGE_LIBRARIES SDL2_image
	PATHS
	/usr/local/include
	/usr/local
	/usr/include
	/usr
	"$ENV{WIN_LIB_PATHS}/lib64"
)

FIND_PATH(SDL2_IMAGE_INCLUDE_DIRS "SDL2/SDL_image.h"
	PATHS
	/usr/local/include
	/usr/local
	/usr/include
	/usr
	"$ENV{WIN_LIB_PATHS}/include"
)

SET ( SDL2_IMAGE_FOUND "NO" )
IF ( SDL2_IMAGE_INCLUDE_DIRS )
	IF ( SDL2_IMAGE_LIBRARIES )
		SET ( SDL2_IMAGE_FOUND "YES" )
	ENDIF ( SDL2_IMAGE_LIBRARIES )
ENDIF ( SDL2_IMAGE_INCLUDE_DIRS )

MARK_AS_ADVANCED(
	SDL2_IMAGE_LIBRARIES
	SDL2_IMAGE_INCLUDE_DIRS
)
