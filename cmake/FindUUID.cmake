# This module tries to find libmagic library and include files
#
# UUID_INCLUDE_DIR, path where to find libwebsockets.h
# UUID_LIBRARIES, the library to link against
# UUID_FOUND, If false, do not try to use libWebSockets
#
# This currently works probably only for Linux

FIND_LIBRARY(UUID_LIBRARIES uuid)

FIND_PATH(UUID_INCLUDE_DIR "uuid/uuid.h")

SET ( UUID_FOUND "NO" )
IF ( UUID_INCLUDE_DIR )
	IF ( UUID_LIBRARIES )
		SET ( UUID_FOUND "YES" )
	ENDIF ( UUID_LIBRARIES )
ENDIF ( UUID_INCLUDE_DIR )

MARK_AS_ADVANCED(
	UUID_LIBRARIES
	UUID_INCLUDE_DIR
)
