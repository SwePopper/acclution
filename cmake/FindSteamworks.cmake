# This module tries to find steamworks library and include files
#
# STEAM_INCLUDE_DIR, path where to find libwebsockets.h
# STEAM_LIBRARIES, the library to link against
# STEAM_FOUND, If false, do not try to use libWebSockets
#
# This currently works probably only for Linux

# check environment variable
set(_steam_ENV_ROOT_DIR "$ENV{STEAMWORKS_SDK}")

if(NOT STEAMWORKS_SDK AND _steam_ENV_ROOT_DIR)
	set(STEAMWORKS_SDK "${_steam_ENV_ROOT_DIR}")
endif(NOT STEAMWORKS_SDK AND _steam_ENV_ROOT_DIR)

if(NOT STEAMWORKS_SDK)
	set(STEAMWORKS_SDK "/home/popper/bin/steamworks")
endif()

set(LIB_PATH "/redistributable_bin/linux32")
if(CMAKE_SIZEOF_VOID_P EQUAL 8)
	set(LIB_PATH "/redistributable_bin/linux64")
endif(CMAKE_SIZEOF_VOID_P EQUAL 8)

FIND_LIBRARY(STEAM_LIBRARIES steam_api
	PATHS
	/usr/local/lib
	/usr/local
	/usr/lib
	/usr
	"${STEAMWORKS_SDK}${LIB_PATH}")

find_path(STEAM_INCLUDE_DIR NAMES "steam/steam_api.h"
	PATHS
	/usr/local/include
	/usr/local
	/usr/include
	/usr
	"${STEAMWORKS_SDK}/public"
)

set(STEAM_FOUND "NO")
if(STEAM_INCLUDE_DIR)
	if(STEAM_LIBRARIES)
		set(STEAM_FOUND "YES")
	endif(STEAM_LIBRARIES)
endif(STEAM_INCLUDE_DIR)

MARK_AS_ADVANCED(
	STEAM_LIBRARIES
	STEAM_INCLUDE_DIR
)
