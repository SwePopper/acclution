set(SDL2_BUILDING_LIBRARY 1)

# Find the required packages
find_package(GLM REQUIRED)
find_package(OpenGL REQUIRED)
find_package(GLEW REQUIRED)
find_package(SDL2 REQUIRED)
find_package(EXPAT REQUIRED)
find_package(Asio REQUIRED)
find_package(Freetype REQUIRED)

find_package(Steamworks)
if(STEAM_FOUND)
	add_definitions(-DSTEAMWORKS)
else()
	set(STEAM_INCLUDE_DIR "")
	set(STEAM_LIBRARIES "")
endif(STEAM_FOUND)

# Find Doxygen
if(USE_DOXYGEN)
	find_package(Doxygen)
endif(USE_DOXYGEN)

# Find the physics library to use. (Try PhysX first then Bullet)
find_package(PhysX)
if(PHYSX_FOUND)
	add_definitions(-DUSE_PHYSX)
	set(PHYSICS_INCLUDE ${PHYSX_INCLUDE_DIR})
	set(PHYSICS_LIBRARIES ${PHYSX_LIBRARIES})
	message(STATUS "Using PhysX for physics library")
else()
	find_package(Bullet)
	if(BULLET_FOUND)
		add_definitions(-DUSE_BULLET)
		set(PHYSICS_INCLUDE ${BULLET_INCLUDE_DIRS})
		set(PHYSICS_LIBRARIES ${BULLET_LIBRARIES})
		message(STATUS "Using Bullet for physics library")
	else()
		add_definitions(-DNO_PHYSICS)
		message(STATUS "Could not find any physics libraries.")
	endif(BULLET_FOUND)
endif(PHYSX_FOUND)

# Find RCC++
if(EXISTS ${CMAKE_CURRENT_SOURCE_DIR}/../external/RuntimeCompiledCPlusPlus/Aurora)
	option(BUILD_EXAMPLES "Build example applications" OFF)
	option(GLFW_SYSTEM    "Use the operating system glfw library" OFF)
	add_subdirectory(${CMAKE_CURRENT_SOURCE_DIR}/../external/RuntimeCompiledCPlusPlus/Aurora ${CMAKE_CURRENT_BINARY_DIR}/RuntimeCompiledCPlusPlus)
	set(SCRIPT_INCLUDE
		${CMAKE_CURRENT_SOURCE_DIR}/../external/RuntimeCompiledCPlusPlus/Aurora
	)
	set(SCRIPT_LIBRARIES RuntimeCompiler RuntimeObjectSystem)

	set(RCCPP_COMPILE_FLAGS )
	if(CMAKE_COMPILER_IS_GNUCC)
		set(RCCPP_COMPILE_FLAGS "-Wno-switch")
		# Seems like mingw on windows ignores -fPIC (and that gives me an error)
		if(NOT WIN32)
			#set(RCCPP_COMPILE_FLAGS ${RCCPP_COMPILE_FLAGS} " -fPIC")
		endif(NOT WIN32)
	endif(CMAKE_COMPILER_IS_GNUCC)

	# Add the compile flags
	set_property(TARGET RuntimeCompiler APPEND_STRING PROPERTY COMPILE_FLAGS ${RCCPP_COMPILE_FLAGS})
	set_property(TARGET RuntimeObjectSystem APPEND_STRING PROPERTY COMPILE_FLAGS ${RCCPP_COMPILE_FLAGS})
endif(EXISTS ${CMAKE_CURRENT_SOURCE_DIR}/../external/RuntimeCompiledCPlusPlus/Aurora)

# Find EASTL
if(USE_EASTL)
	add_subdirectory(${CMAKE_CURRENT_SOURCE_DIR}/../external/EASTL ${CMAKE_CURRENT_BINARY_DIR}/EASTL)
	set(EASTL_LIBRARIES
		EABase
		EASTL
	)

	set(EASTL_COMPILE_FLAGS )
	if(CMAKE_COMPILER_IS_GNUCC)
		# Seems like mingw on windows ignores -fPIC (and that gives me an error)
		if(NOT WIN32)
			set(EASTL_COMPILE_FLAGS ${EASTL_COMPILE_FLAGS} " -fPIC")
		endif(NOT WIN32)
	endif(CMAKE_COMPILER_IS_GNUCC)

	# Add the compile flags
	set_property(TARGET EASTL APPEND_STRING PROPERTY COMPILE_FLAGS ${EASTL_COMPILE_FLAGS})
endif(USE_EASTL)

if(WIN32)
	set(PLATFORM_DLLS dbghelp rpcrt4)
else()
	find_package(UUID REQUIRED)
endif(WIN32)

find_package(VULKAN)
if(VULKAN_FOUND)
	find_package(X11)
	if(X11_FOUND)
		add_definitions(-DVK_USE_PLATFORM_XLIB_KHR)
		add_definitions(-DUSE_VULKAN)
	else(WIN32)
		add_definitions(-DVK_USE_PLATFORM_WIN32_KHR)
		add_definitions(-DUSE_VULKAN)
	endif(X11_FOUND)
else()
	set(VULKAN_INCLUDE_DIR "")
	set(VULKAN_LIBRARY "")
endif(VULKAN_FOUND)
