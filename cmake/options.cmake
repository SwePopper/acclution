if(NOT DEFINED CMAKE_BUILD_TYPE)
	set(CMAKE_BUILD_TYPE Release CACHE STRING "Build type")
endif()

set(CMAKE_BUILD_TYPE Debug)
if(CMAKE_BUILD_TYPE MATCHES Debug)
	add_definitions(-D_DEBUG -DDEBUG -DGLM_FORCE_PURE)
else()
	add_definitions(-DNDEBUG)
endif(CMAKE_BUILD_TYPE MATCHES Debug)

option(USE_EASTL "Use EASTL instead of standard stl" ON)
option(USE_DOXYGEN "Use Doxygen" OFF)
option(DISABLE_LOGGING "Disable Logging" OFF)
option(BUILD_EDITOR "Build the acclution editor" ON)

if(MSVC)
	set(USE_STATIC_LIBRARY ON)
	option(USE_COTIRE "Use cotire to generate precompiled headers" ON)
else()
	option(USE_STATIC_LIBRARY "Create the acclution library staticaly" OFF)
	option(USE_COTIRE "Use cotire to generate precompiled headers" OFF)
endif()

option(BUILD_TOOLS "Build Tools" ON)
option(BUILD_TESTS "Build Tests" OFF)

if(USE_EASTL)
	add_definitions(-DEASTL_USER_DEFINED_ALLOCATOR)
	
	# TODO popper: Seems like eastl fails on mingw in variadic templates.. fix later
	if(WIN32 AND CMAKE_COMPILER_IS_GNUCC)
		add_definitions(-DEA_COMPILER_NO_VARIADIC_TEMPLATES)
	endif(WIN32 AND CMAKE_COMPILER_IS_GNUCC)
endif()
