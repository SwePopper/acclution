# - Try to find Vulkan SDK
# Once done this will define
#
#  VULKAN_FOUND - system has Vulkan SDK
#  VULKAN_INCLUDE_DIR - the Vulkan include directory
#  VULKAN_LIBRARY - Link these to use Vulkan

FIND_PATH(VULKAN_INCLUDE_DIR vulkan/vulkan.h
	/usr/local/include
	/usr/include
	/sw/include
	/opt/local/include
	/opt/csw/include
	/opt/include
	$ENV{VK_SDK_PATH}/Include
)

set(WINDOWS_LIB_PATH $ENV{VK_SDK_PATH}/Bin32)
if(CMAKE_SIZEOF_VOID_P EQUAL 8)
	set(WINDOWS_LIB_PATH $ENV{VK_SDK_PATH}/Bin)
endif(CMAKE_SIZEOF_VOID_P EQUAL 8)

FIND_LIBRARY(VULKAN_LIBRARY
	NAMES vulkan vulkan-1
	PATHS
	/usr/local/lib
	/usr/lib
	/sw/lib
	/opt/local/lib
	/opt/csw/lib
	/opt/lib
	${WINDOWS_LIB_PATH}
)

SET(VULKAN_FOUND "NO")
IF(VULKAN_INCLUDE_DIR AND VULKAN_LIBRARY)
	SET(VULKAN_FOUND "YES")
ENDIF(VULKAN_INCLUDE_DIR AND VULKAN_LIBRARY)

mark_as_advanced(VULKAN_LIBRARY VULKAN_INCLUDE_DIR)
