# This module tries to find steamworks library and include files
#
# FREEIMAGE_INCLUDE_DIR, path where to find libwebsockets.h
# FREEIMAGE_LIBRARIES, the library to link against
# FREEIMAGE_FOUND, If false, do not try to use libWebSockets
#

# check environment variable
FIND_LIBRARY(FREEIMAGE_LIBRARIES freeimage
	PATHS
	/usr/local/lib
	/usr/local
	/usr/lib
	/usr
)

FIND_LIBRARY(FREEIMAGE_PLUS_LIBRARIES freeimageplus
	PATHS
	/usr/local/lib
	/usr/local
	/usr/lib
	/usr
)

set(FREEIMAGE_LIBRARIES ${FREEIMAGE_LIBRARIES} ${FREEIMAGE_PLUS_LIBRARIES})

find_path(FREEIMAGE_INCLUDE_DIR NAMES FreeImage.h
	PATHS
	/usr/local/include
	/usr/local
	/usr/include
	/usr
)

set(FREEIMAGE_FOUND "NO")
if(FREEIMAGE_INCLUDE_DIR)
	if(FREEIMAGE_LIBRARIES)
		set(FREEIMAGE_FOUND "YES")
	endif(FREEIMAGE_LIBRARIES)
endif(FREEIMAGE_INCLUDE_DIR)

MARK_AS_ADVANCED(
	FREEIMAGE_LIBRARIES
	FREEIMAGE_INCLUDE_DIR
)
