# This module tries to find asio library and include files
#
# ASIO_INCLUDE_DIR, path where to find libwebsockets.h
# ASIO_FOUND, If false, do not try to use libWebSockets
#
# This currently works probably only for Linux

FIND_PATH(ASIO_INCLUDE_DIR "asio.hpp")

SET(ASIO_FOUND "NO")
IF(ASIO_INCLUDE_DIR)
	SET(ASIO_FOUND "YES")
ENDIF(ASIO_INCLUDE_DIR)

MARK_AS_ADVANCED(
	ASIO_INCLUDE_DIR
)
