set(PACKAGE "acclution")

# CPack defines
set(CPACK_PACKAGE_NAME "${PACKAGE}")
set(CPACK_PACKAGE_VERSION_MAJOR "0")
set(CPACK_PACKAGE_VERSION_MINOR "1")
set(CPACK_PACKAGE_VERSION_PATCH "3")
set(CPACK_PACKAGE_VERSION "${CPACK_PACKAGE_VERSION_MAJOR}.${CPACK_PACKAGE_VERSION_MINOR}.${CPACK_PACKAGE_VERSION_PATCH}")
set(CPACK_PACKAGE_VENDOR "swepopper@gmail.com")
set(CPACK_PACKAGE_DESCRIPTION_SUMMARY "${PACKAGE} ${PACKAGE_VERSION}")
set(CPACK_PACKAGE_DESCRIPTION_FILE "${CMAKE_SOURCE_DIR}/README.md")
set(CPACK_RESOURCE_FILE_LICENSE "${CMAKE_SOURCE_DIR}/LICENSE")

set(CPACK_GENERATOR "TGZ")

# Acclution defines
set(VERSION "${CPACK_PACKAGE_VERSION}")

set(ACCLUTION_VERSION ${CPACK_PACKAGE_VERSION})
set(ACCLUTION_VERSION_MAJOR ${CPACK_PACKAGE_VERSION_MAJOR})
set(ACCLUTION_VERSION_MINOR ${CPACK_PACKAGE_VERSION_MINOR})
set(ACCLUTION_VERSION_PATCH ${CPACK_PACKAGE_VERSION_PATCH})

add_definitions(-DPROJECT_NAME="${PACKAGE}")
add_definitions(-DACCLUTION_VERSION="${ACCLUTION_VERSION}")
add_definitions(-DACCLUTION_VERSION_MAJOR=${ACCLUTION_VERSION_MAJOR})
add_definitions(-DACCLUTION_VERSION_MINOR=${ACCLUTION_VERSION_MINOR})
add_definitions(-DACCLUTION_VERSION_PATCH=${ACCLUTION_VERSION_PATCH})

if(DISABLE_LOGGING)
	add_definitions(-DNO_LOGGING)
endif(DISABLE_LOGGING)

add_definitions(-DGLEW_STATIC)
add_definitions(-D__STDC_FORMAT_MACROS)

set(ACCLUTION_COMPILE_FLAGS )
set(ACCLUTION_LINKER_FLAGS )
set(ACCLUTION_EXEC_FLAG )

# Add more warnings
if(MSVC)
	# using Visual Studio C++
	set(ACCLUTION_EXEC_FLAG WIN32)
	set(ACCLUTION_COMPILE_FLAGS "/EHsc /GF /MP /W4 /WX")

	set(ACCLUTION_LINKER_FLAGS "/VERSION:${ACCLUTION_VERSION_MAJOR}.${ACCLUTION_VERSION_MINOR} /DYNAMICBASE /NXCOMPAT /LARGEADDRESSAWARE /IGNORE:4221")

	if(CMAKE_BUILD_TYPE MATCHES Debug)
		set(ACCLUTION_COMPILE_FLAGS "${ACCLUTION_COMPILE_FLAGS} /MDd /Od /RTC1 /Zi")
	else()
		set(ACCLUTION_COMPILE_FLAGS "${ACCLUTION_COMPILE_FLAGS} /MD /O2 /Oy /GL /Gy")
		set(ACCLUTION_LINKER_FLAGS "${ACCLUTION_LINKER_FLAGS} /LTCG /OPT:REF /OPT:ICF /INCREMENTAL:NO")
	endif(CMAKE_BUILD_TYPE MATCHES Debug)

	if(CMAKE_CXX_FLAGS MATCHES "/W[0-4]")
		string(REGEX REPLACE "/W[0-4]" "" CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS}")
	endif()
else()
	set(ACCLUTION_COMPILE_FLAGS "-g -Wall -Weffc++ -pedantic -pedantic-errors -Wextra -Waggregate-return -Wcast-align -Wwrite-strings")
	set(ACCLUTION_COMPILE_FLAGS "${ACCLUTION_COMPILE_FLAGS} -Wcast-qual -Wchar-subscripts -Wcomment -Wconversion -Wdisabled-optimization -Werror")
	set(ACCLUTION_COMPILE_FLAGS "${ACCLUTION_COMPILE_FLAGS} -Wfloat-equal -Wformat -Wformat=2 -Wformat-security -Wformat-y2k")
	set(ACCLUTION_COMPILE_FLAGS "${ACCLUTION_COMPILE_FLAGS} -Wimplicit -Wimport  -Winit-self -Winline -Winvalid-pch -Wfatal-errors")
	set(ACCLUTION_COMPILE_FLAGS "${ACCLUTION_COMPILE_FLAGS} -Wlong-long -Wmissing-braces -Wmissing-field-initializers -Wmissing-format-attribute")
	set(ACCLUTION_COMPILE_FLAGS "${ACCLUTION_COMPILE_FLAGS} -Wmissing-include-dirs -Wmissing-noreturn -Wpacked -Wparentheses -Wpointer-arith")
	set(ACCLUTION_COMPILE_FLAGS "${ACCLUTION_COMPILE_FLAGS} -Wredundant-decls -Wreturn-type -Wsequence-point -Wshadow -Wsign-compare -Wstack-protector")
	set(ACCLUTION_COMPILE_FLAGS "${ACCLUTION_COMPILE_FLAGS} -Wstrict-aliasing -Wstrict-aliasing=2 -Wswitch -Wswitch-default -Wswitch-enum -Wtrigraphs")
	set(ACCLUTION_COMPILE_FLAGS "${ACCLUTION_COMPILE_FLAGS} -Wuninitialized -Wunknown-pragmas -Wunreachable-code -Wunused -Wunused-function -Wunused-label")
	set(ACCLUTION_COMPILE_FLAGS "${ACCLUTION_COMPILE_FLAGS} -Wunused-parameter -Wunused-value -Wunused-variable -Wvariadic-macros -Wvolatile-register-var")

	if(CMAKE_BUILD_TYPE MATCHES Debug)
		set(ACCLUTION_COMPILE_FLAGS "${ACCLUTION_COMPILE_FLAGS} -fno-omit-frame-pointer -O0")
	else()
		set(ACCLUTION_COMPILE_FLAGS "${ACCLUTION_COMPILE_FLAGS} -O3")
	endif(CMAKE_BUILD_TYPE MATCHES Debug)
endif()
