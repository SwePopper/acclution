# - Try to find SQLite3
# Once done this will define
#
#  SQLITE3_FOUND - system has Vulkan SDK
#  SQLITE3_INCLUDE_DIR - the Vulkan include directory
#  SQLITE3_LIBRARY - Link these to use Vulkan

FIND_PATH(SQLITE3_INCLUDE_DIR sqlite3.h
	/usr/local/include
	/usr/include
	/sw/include
	/opt/local/include
	/opt/csw/include
	/opt/include
	"$ENV{WIN_LIB_PATHS}/include"
)

FIND_LIBRARY(SQLITE3_LIBRARY
	NAMES sqlite3
	PATHS
	/usr/local/lib
	/usr/lib
	/sw/lib
	/opt/local/lib
	/opt/csw/lib
	/opt/lib
	"$ENV{WIN_LIB_PATHS}/lib64"
	"$ENV{WIN_LIB_PATHS}"
)

SET(SQLITE3_FOUND "NO")
IF(SQLITE3_INCLUDE_DIR AND SQLITE3_LIBRARY)
	SET(SQLITE3_FOUND "YES")
ENDIF(SQLITE3_INCLUDE_DIR AND SQLITE3_LIBRARY)

mark_as_advanced(SQLITE3_INCLUDE_DIR SQLITE3_LIBRARY)
